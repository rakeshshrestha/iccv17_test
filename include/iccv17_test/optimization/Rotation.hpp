#ifndef ICCV_ROTATION
#define ICCV_ROTATION

#include "ceres/rotation.h"
#include "ceres/ceres.h"

struct Rotation {
    
    Rotation(const double _x1,const double _y1,const double _z1,
             const double _x2,const double _y2,const double _z2):
    x1(_x1),y1(_y1),z1(_z1),x2(_x2),y2(_y2),z2(_z2){
    }
    
    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const {
        
        T rotated1[3];
        T p1[3]={(T)x1,(T)y1,(T)z1};
        T p2[3]={(T)x2,(T)y2,(T)z2};
        ceres::AngleAxisRotatePoint(camera,p1,rotated1);
        residuals[0]=rotated1[0]-p2[0];
        residuals[1]=rotated1[1]-p2[1];
        residuals[2]=rotated1[2]-p2[2];
        return true;
    }
    
    static ceres::CostFunction* Create(const double x1,
                                       const double y1,
                                       const double z1,
                                       const double x2,
                                       const double y2,
                                       const double z2) {
        
        return (new ceres::AutoDiffCostFunction<Rotation,3,3>(new Rotation(x1,y1,z1,x2,y2,z2)));
        
    };
    
    double x1;
    double y1;
    double z1;
    double x2;
    double y2;
    double z2;
};

#endif