#ifndef ICCV_ROTATION
#define ICCV_ROTATION

#include "ceres/rotation.h"
#include "ceres/ceres.h"

#include <Eigen/Eigen>
/*
 * @param: (x1, y1), (x2, y2) -> corresponding points in two images (in image coord)
 */
struct EpipolarOptimization {
    
    EpipolarOptimization(
                            const double _x1,const double _y1,
                            const double _x2,const double _y2
                        ):
                        x1(_x1),y1(_y1),x2(_x2),y2(_y2) {
    }
    
    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const {
        
        T rotated1[3];

        Eigen::Matrix<T, 3, 1> p, q;
        // the role of p and q are reversed (p = p2, q = p1), cuz our frame of reference is camera 0
        q << (T)x1, (T)y1, (T)1;
        p << (T)x2, (T)y2, (T)1;
        
        T R_arr[9];
        ceres::AngleAxisToRotationMatrix<T>(camera, R_arr);
        
        Eigen::Matrix<T, 3, 3> R;
        Eigen::Matrix<T, 3, 1> t;
        Eigen::Matrix<T, 3, 3> tx;

        R <<    R_arr[0], R_arr[3], R_arr[6],
                R_arr[1], R_arr[4], R_arr[7],
                R_arr[2], R_arr[5], R_arr[8];

        t << camera[3], camera[4], camera[5];

        tx <<   (T)0,      (T)-t(2),      (T)t(1),
                (T)t(2),       (T)0,      (T)-t(0),
                (T)-t(1),      (T)t(0),       (T)0;

        Eigen::Matrix<T, 3, 3> E = tx * R;
        Eigen::Matrix<T, 3, 1> Ep = E * p;
        Eigen::Matrix<T, 3, 1> Etq = E.transpose() * q;

        // when you have 2D image coordinates (SAMPSON ERROR)
        T num   = q.transpose() * Ep;;
        T denom = sqrt( pow(Ep(0), 2) + pow(Ep(1), 2) + pow(Etq(0), 2) + pow(Etq(1), 2) );

        // when you have 3d bearing vectors
        // T num   = q.transpose() * E * p;
        // T denom = sqrt((Ep.transpose()*Ep + Etq.transpose() * Etq)(0)); // sqrt(Ep(0)*Ep(0) + Ep(1)*Ep(1) + Etq(0)*Etq(0) + Etq(1)*Etq(1));

        residuals[0] = num/denom;

        return true;
    }
    
    static ceres::CostFunction* Create(const double x1,
                                       const double y1,
                                       const double x2,
                                       const double y2) {
        
        return (new ceres::AutoDiffCostFunction<EpipolarOptimization,1,6>(new EpipolarOptimization(x1,y1,x2,y2)));
        
    };
    
    double x1;
    double y1;
    double x2;
    double y2;
};

#endif