#ifndef ICCV17_RELATIVE_POSE_ESTIMATION
#define ICCV17_RELATIVE_POSE_ESTIMATION

#include <Eigen/Eigen>
#include <opengv/relative_pose/methods.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/relative_pose/CentralRelativePoseSacProblem.hpp>
#include <opengv/relative_pose/NoncentralRelativeMultiAdapter.hpp>
#include <opengv/sac/MultiRansac.hpp>
#include <opengv/sac_problems/relative_pose/MultiNoncentralRelativePoseSacProblem.hpp>
#include <opengv/sac_problems/relative_pose/EigensolverSacProblem.hpp>
#include <sstream>
#include <fstream>

#include "ceres/rotation.h"
#include "ceres/ceres.h"

#include "opencv2/core/core.hpp"

using namespace cv;
using namespace opengv;

namespace iccv17
{
namespace relative_pose_estimation
{


Eigen::Vector3d estimateRelativeTranslation(const std::vector<Eigen::Vector3d> &pts1,
                                            const std::vector<Eigen::Vector3d> &pts2);

void preSolve(const std::vector<Eigen::Vector3d> &pts1,
              const std::vector<Eigen::Vector3d> &pts2,
              Eigen::Matrix3d& rotation,
              Eigen::Vector3d& translation);

void preSolve(const bearingVectors_t &pts1,
              const bearingVectors_t &pts2,
              Eigen::Matrix3d& rotation,
              Eigen::Vector3d& translation,
              double &timeOut);

void preSolveWithOptimization(  
    const bearingVectors_t &pts1,
    const bearingVectors_t &pts2,
    Eigen::Matrix3d &preSolveRotation,
    Eigen::Vector3d &preSolveTranslation,
    Eigen::Matrix3d &optimizedRotation,
    Eigen::Vector3d &optimizedTranslation,
    Eigen::Matrix3d &trueRotation,
    Eigen::Vector3d &trueTranslation,
    double &preSolveTimeOut,
    double &optimizationTimeOut );

void estimateRotationTranslation(const double lossThreshold,
                                 const bearingVectors_t &pts1,
                                 const bearingVectors_t &pts2,
                                 Eigen::Matrix3d& rotation,
                                 Eigen::Vector3d& translation,
                                 double &timeOut);


} // namespace relative_pose_estimation
} // namespace iccv17
#endif