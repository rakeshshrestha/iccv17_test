#ifndef RELATIVE_POSE_RANSAC
#define RELATIVE_POSE_RANSAC

using namespace iccv17::relative_pose_estimation;
#include <iccv17_test/optimization/RotationTranslation.hpp>
#include <iccv17_test/optimization/Rotation.hpp>

// some utility functions
inline double computeError(const Eigen::Vector3d& p1,
                            const Eigen::Vector3d& p2,
                            const Eigen::Matrix3d& rotation,
                            const Eigen::Vector3d& translation){
    
    
    Eigen::Vector3d rotated=rotation*p1;
    Eigen::Vector3d axis;
    ceres::CrossProduct(rotated.data(),translation.data(),axis.data());
    
    double D=-(axis(0)*rotated(0)+axis(1)*rotated(1)+axis(2)*rotated(2));
    double norm=axis(0)*axis(0)+axis(1)*axis(1)+axis(2)*axis(2);
    double t=(axis(0)*p2(0)+axis(1)*p2(1)+axis(2)*p2(2)+D)/norm;
        
    rotated(0)=p2(0)+axis(0)*t;
    rotated(1)=p2(1)+axis(1)*t;
    rotated(2)=p2(2)+axis(2)*t;

    
    rotated.normalize();
    rotated=rotated-p2;
    
    //std::cout<<"evaluation error"<<rotated.norm()<<std::endl;
    return rotated.norm();
}

static bool haveColPlannarPoints(const std::vector<Eigen::Vector3d>& pt3d,const int count){
    
    // check that the i-th selected point does not on
    // a plane contains some previously selected points
    
    for(int i = 0; i < count-1; i++ ){
        
        Eigen::Vector3d pt1=pt3d[i];
        Eigen::Vector3d pt2=pt3d[count-1];
        Eigen::Vector3d norm=pt1.cross(pt2);
        norm.normalize();
        
        for(int j = 0; j < i; j++ ){
            double angle=abs(norm.dot(pt3d[j]));
            if (angle<=DBL_EPSILON) {
                return true;
            }
        }
    }
    return false;
}

static bool checkSubset(const std::vector<Eigen::Vector3d>& spts1,
                        const std::vector<Eigen::Vector3d>& spts2,
                        const int count){
    return !haveColPlannarPoints(spts1,count)&&!haveColPlannarPoints(spts2,count);
}

static int modelPoints=5;
std::vector<int> testinliers;
bool getSubset(cv::RNG& rng,
               const std::vector<Eigen::Vector3d>& pts1,
               const std::vector<Eigen::Vector3d>& pts2,
               std::vector<Eigen::Vector3d>& spts1,
               std::vector<Eigen::Vector3d>& spts2){
    
    //modelPoints=pts1.size();
    //testinliers.clear();
    spts1.resize(modelPoints);
    spts2.resize(modelPoints);
    std::vector<int> _idx(modelPoints);
    int* idx = &_idx[0];
    int maxAttempts=1000;
    int count=pts1.size();
    int i = 0, j, k, iters = 0;
    
    
    for(; iters < maxAttempts; iters++){
        
        for(i = 0; i < modelPoints && iters < maxAttempts; ){
            
            int idx_i = 0;
            for(;;){
                idx_i = idx[i] = rng.uniform(0, count);
                // std::cout << "Random number1: " << idx_i << std::endl;
                
                for( j = 0; j < i; j++ )
                    if( idx_i == idx[j] )
                        break;
                if( j == i )
                    break;
            }
            
            spts1[i]=(pts1[idx_i]);
            spts2[i]=(pts2[idx_i]);
            
            if(!checkSubset(spts1,spts2,i+1)){
                i = rng.uniform(0, i+1);
                iters++;
                // std::cout << "Random number2: " << i << std::endl;
                
                continue;
            }
            i++;
        }
        
        if(i == modelPoints && !checkSubset(spts1,spts2,i))
            continue;
        break;
    }
    
    for (int i=0;i<modelPoints;i++) {
        testinliers.push_back(idx[i]);
        std::cout<<"select"<<idx[i]<<std::endl;
    }
    
    return i == modelPoints && iters < maxAttempts;
}

int RANSACUpdateNumIters( double p, double ep, int modelPoints, int maxIters ){
    
    if( modelPoints <= 0 )
        std::cerr << "the number of model points should be positive" << std::endl; // CV_Error(cv::Error::StsOutOfRange, "the number of model points should be positive" );
    
    p  = MAX(p,0.);
    p  = MIN(p,1.);
    ep = MAX(ep,0.);
    ep = MIN(ep,1.);
    
    // avoid inf's & nan's
    double num = MAX(1. - p, DBL_MIN);
    double denom = 1. - std::pow(1. - ep, modelPoints);
    if( denom < DBL_MIN )
        return 0;
    
    num = std::log(num);
    denom = std::log(denom);
    
    return denom >= 0 || -num >= maxIters*(-denom) ? maxIters : cvRound(num/denom);
}

void findInliers(const std::vector<Eigen::Vector3d>& pts1,
                const std::vector<Eigen::Vector3d>& pts2,
                const Eigen::Matrix3d& rotation,
                const Eigen::Vector3d& translation,
                std::vector<int>& inliers,
                double thresh){
    inliers.clear();
    for (int i=0;i<pts1.size();i++) {
        if (computeError(pts1[i],pts2[i],rotation,translation)<=thresh) {
            inliers.push_back(i);
        }
    }
}

void estimateRotationTranslation(const std::vector<Eigen::Vector3d> &pts1,
                                 const std::vector<Eigen::Vector3d> &pts2,
                                 Eigen::Matrix3d& rotation,
                                 Eigen::Vector3d& translation,
                                 ceres::LossFunction* loss_function=NULL,
                                 bool initialized=false) {
    
    
    assert(pts1.size()==pts2.size());
    if (!initialized) {
        preSolve(pts1,pts2,rotation,translation);
    }
    double motion[6]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    motion[3]=translation(0);
    motion[4]=translation(1);
    motion[5]=translation(2);
    
    
    ceres::Problem problem;
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=RotationTranslation::Create(pts1[i](0),
                                                                       pts1[i](1),
                                                                       pts1[i](2),
                                                                       pts2[i](0),
                                                                       pts2[i](1),
                                                                       pts2[i](2));
        problem.AddResidualBlock(cost_function,loss_function,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;
    ceres::Solve(options,&problem, &summary);
    
    
    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    translation(0)=motion[3];
    translation(1)=motion[4];
    translation(2)=motion[5];
    
}

static int run7Point(const cv::Mat& _m1,const cv::Mat& _m2,cv::Mat& _fmatrix )
{
    double a[7*9], w[7], u[9*9], v[9*9], c[4], r[3];
    double* f1, *f2;
    double t0, t1, t2;
    cv::Mat A( 7, 9, CV_64F, a );
    cv::Mat U( 7, 9, CV_64F, u );
    cv::Mat Vt( 9, 9, CV_64F, v );
    cv::Mat W( 7, 1, CV_64F, w );
    cv::Mat coeffs( 1, 4, CV_64F, c );
    cv::Mat roots( 1, 3, CV_64F, r );
    
    const cv::Point3d* m1 = _m1.ptr<cv::Point3d>();
    const cv::Point3d* m2 = _m2.ptr<cv::Point3d>();
    
    double* fmatrix = _fmatrix.ptr<double>();
    int i, k, n;
    
    // form a linear system: i-th row of A(=a) represents
    // the equation: (m2[i], 1)'*F*(m1[i], 1) = 0
    for( i = 0; i < 7; i++ ){
        
        double x0 = m1[i].x, y0 = m1[i].y,z0=m1[i].z;
        double x1 = m2[i].x, y1 = m2[i].y,z1=m2[i].z;
        
        a[i*9+0] = x1*x0;
        a[i*9+1] = x1*y0;
        a[i*9+2] = x1*z0;
        a[i*9+3] = y1*x0;
        a[i*9+4] = y1*y0;
        a[i*9+5] = y1*z0;
        a[i*9+6] = x0*z1;
        a[i*9+7] = y0*z1;
        a[i*9+8] = z0*z1;
    }
    
    // A*(f11 f12 ... f33)' = 0 is singular (7 equations for 9 variables), so
    // the solution is linear subspace of dimensionality 2.
    // => use the last two singular vectors as a basis of the space
    // (according to SVD properties)
    cv::SVDecomp( A, W, U, Vt,cv::SVD::MODIFY_A + cv::SVD::FULL_UV );
    f1 = v + 7*9;
    f2 = v + 8*9;
    
    // f1, f2 is a basis => lambda*f1 + mu*f2 is an arbitrary f. matrix.
    // as it is determined up to a scale, normalize lambda & mu (lambda + mu = 1),
    // so f ~ lambda*f1 + (1 - lambda)*f2.
    // use the additional constraint det(f) = det(lambda*f1 + (1-lambda)*f2) to find lambda.
    // it will be a cubic equation.
    // find c - polynomial coefficients.
    for( i = 0; i < 9; i++ )
        f1[i] -= f2[i];
    
    t0 = f2[4]*f2[8] - f2[5]*f2[7];
    t1 = f2[3]*f2[8] - f2[5]*f2[6];
    t2 = f2[3]*f2[7] - f2[4]*f2[6];
    
    c[3] = f2[0]*t0 - f2[1]*t1 + f2[2]*t2;
    
    c[2] = f1[0]*t0 - f1[1]*t1 + f1[2]*t2 -
    f1[3]*(f2[1]*f2[8] - f2[2]*f2[7]) +
    f1[4]*(f2[0]*f2[8] - f2[2]*f2[6]) -
    f1[5]*(f2[0]*f2[7] - f2[1]*f2[6]) +
    f1[6]*(f2[1]*f2[5] - f2[2]*f2[4]) -
    f1[7]*(f2[0]*f2[5] - f2[2]*f2[3]) +
    f1[8]*(f2[0]*f2[4] - f2[1]*f2[3]);
    
    t0 = f1[4]*f1[8] - f1[5]*f1[7];
    t1 = f1[3]*f1[8] - f1[5]*f1[6];
    t2 = f1[3]*f1[7] - f1[4]*f1[6];
    
    c[1] = f2[0]*t0 - f2[1]*t1 + f2[2]*t2 -
    f2[3]*(f1[1]*f1[8] - f1[2]*f1[7]) +
    f2[4]*(f1[0]*f1[8] - f1[2]*f1[6]) -
    f2[5]*(f1[0]*f1[7] - f1[1]*f1[6]) +
    f2[6]*(f1[1]*f1[5] - f1[2]*f1[4]) -
    f2[7]*(f1[0]*f1[5] - f1[2]*f1[3]) +
    f2[8]*(f1[0]*f1[4] - f1[1]*f1[3]);
    
    c[0] = f1[0]*t0 - f1[1]*t1 + f1[2]*t2;
    
    // solve the cubic equation; there can be 1 to 3 roots ...
    n = solveCubic( coeffs, roots );
    
    if( n < 1 || n > 3 )
        return n;
    
    for( k = 0; k < n; k++, fmatrix += 9 )
    {
        // for each root form the fundamental matrix
        double lambda = r[k], mu = 1.;
        double s = f1[8]*r[k] + f2[8];
        
        // normalize each matrix, so that F(3,3) (~fmatrix[8]) == 1
        if( fabs(s) > DBL_EPSILON )
        {
            mu = 1./s;
            lambda *= mu;
            fmatrix[8] = 1.;
        }
        else
            fmatrix[8] = 0.;
        
        for( i = 0; i < 8; i++ )
            fmatrix[i] = f1[i]*lambda + f2[i]*mu;
    }
    return n;
}

bool estimateRotationTranslationRANSAC2(
                                        const std::vector<Eigen::Vector3d> &pts1,
                                       const std::vector<Eigen::Vector3d> &pts2,
                                       std::vector<int>& bestInliers,
                                       Eigen::Matrix3d& rotation,
                                       Eigen::Vector3d& translation,
                                       int   maxIterations=200,
                                       const double lossThreshold=0.005,
                                       const double inlierThreshold=0.01) {
    modelPoints=7;
    
    // cv::RNG rng((uint64)-1);
    int count=pts1.size();
    int niters = maxIterations;
    
    
    bestInliers.clear();
    Eigen::Matrix3d bestRotation;
    Eigen::Vector3d bestTranslation;
    
    /*for (int i=0;i<pts1.size();i++) {
     std::cout<<pts1[i].transpose()<<std::endl;
     }*/
    
    for(int iter = 0; iter < niters; iter++){
        
        std::vector<Eigen::Vector3d> spts1,spts2;
        bool found = getSubset(rng,pts1,pts2,spts1,spts2);
        if(!found ){
            if( iter == 0 ) {
                std::cerr << "Exiting, no subset found" << std::endl;
                return false;
            }
            break;
        }
        
        /*for (int i=0;i<spts1.size();i++) {
         std::cout<<"subset "<<spts1[i].transpose()<<std::endl;
         }
         getchar();*/
        
        
        //Eigen::Matrix3d rotation;
        //Eigen::Vector3d translation;
        //estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        //std::cout<<"rotation 1"<<rotation.transpose()<<std::endl;
        //getchar();
        /*spts1.clear();
         spts2.clear();
         
         for (int i=0;i<testinliers.size();i++) {
         spts1.push_back(pts1[testinliers[i]]);
         spts2.push_back(pts2[testinliers[i]]);
         }
         estimateRotationTranslation(spts1,spts2,rotation,translation);
         
         std::cout<<"rotation 2"<<rotation<<std::endl;
         
         
         std::cout<<rotation<<std::endl;*/
        cv::Mat _m1(modelPoints,3,CV_64FC1),_m2(modelPoints,3,CV_64FC1),_fmatrix(9,3,CV_64FC1);
        cv::Point3d* m1 = _m1.ptr<cv::Point3d>();
        cv::Point3d* m2 = _m2.ptr<cv::Point3d>();
        
        for(int s=0;s<modelPoints;s++){
            
            m1[s].x=spts1[s](0);
            m1[s].y=spts1[s](1);
            m1[s].z=spts1[s](2);
            
            m2[s].x=spts2[s](0);
            m2[s].y=spts2[s](1);
            m2[s].z=spts2[s](2);
        }

        int N=run7Point(_m1,_m2,_fmatrix);
        if(N < 1 || N > 3){
            continue;
        }
        
        
        std::vector<Eigen::Matrix3d> F(N);
        for(int n=0;n<N;n++){
            for(int j1=0;j1<3;j1++){
                for(int j2=0;j2<3;j2++){
                    F[n](j1,j2)=_fmatrix.at<double>(3*n+j1,j2);
                }
            }
        }
        
        for(int n=0;n<N;n++){
            
            std::vector<int> inliers(0);
            for (int j= 0; j< pts1.size(); j++){
                Eigen::Vector3d planeVector=F[n]*pts1[j];
                double distance=pts2[j].transpose()*planeVector;
                distance=std::abs(distance)/planeVector.norm();
                if (distance<inlierThreshold){
                    inliers.push_back(j);
                }
            }
            
            //int goodCount=inliers.size();
            //std::cout<<"good count"<<goodCount<<std::endl;
            if( inliers.size() > MAX(bestInliers.size(), modelPoints-1) ){
                bestInliers=inliers;
                bestRotation=rotation;
                bestTranslation=translation;
                niters = RANSACUpdateNumIters(0.98, (double)(count-inliers.size())/count, modelPoints, niters );
            }
        }
        //std::vector<int> inliers;
        //findInliers(pts1,pts2,rotation,translation,inliers,inlierThreshold);
    }
    
    //std::cout<<"rotation 1"<<bestRotation.transpose()<<std::endl;
    //getchar();
    
    //getchar();
    std::cout<<"final iter"<<niters<<std::endl;
    std::vector<Eigen::Vector3d> inliers1(bestInliers.size());
    std::vector<Eigen::Vector3d> inliers2(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    
    
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation);
    findInliers(pts1,pts2,bestRotation,bestTranslation,bestInliers,inlierThreshold);
    inliers1.resize(bestInliers.size());
    inliers2.resize(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    std::cout<<"good count"<<bestInliers.size()<<std::endl;
    ceres::LossFunction* loss=new ceres::HuberLoss(lossThreshold);
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation,loss,true);
    
    rotation=bestRotation;
    translation=bestTranslation;
    return true;
}


#endif