#ifndef ICCV17_MISC_HELPERS
#define ICCV17_MISC_HELPERS

#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#include <cstdarg>
#include <cctype>

#include <Eigen/Eigen>

namespace iccv17
{
namespace misc_helpers
{

double outputRotationTranslation( 
    std::fstream  &outputStream,
    Eigen::Matrix3d rotation,
    Eigen::Vector3d translation
);

double readRotationTranslation(
    std::ifstream &inputStream,
    Eigen::Matrix3d &rotation,
    Eigen::Vector3d &translation
);

double calculateRotationError(  Eigen::Matrix3d trueRotation,
                                Eigen::Matrix3d measuredRotation    );

double calculateTranslationError( Eigen::Vector3d trueTranslation,
                                Eigen::Vector3d measuredTranslation);

void calculateError(Eigen::Matrix3d trueRotation,
                    Eigen::Vector3d trueTranslation,
                    Eigen::Matrix3d measuredRotation,
                    Eigen::Vector3d measuredTranslation,
                    double &errorAngle,
                    double &errorTranslation);

double getRandomBetween(double minimum, double maximum);

bool doublePairCompare(const std::pair<double, double>& firstElem, const std::pair<double, double>& secondElem);

//template <class T>
double vectAverage(std::vector<double> &vect);
double vectStandardDeviation(std::vector<double> &vect);

void vectRemoveNaN(std::vector<double> &vect);
void vectRemoveNaN(std::vector<std::pair<double, double>> &vect);

} // misc_helpers
} // iccv17

#endif