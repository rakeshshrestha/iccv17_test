#ifndef ICCV17_OPENGV_HELPERS
#define ICCV17_OPENGV_HELPERS

#include <iccv17_test/utils/experiment_helpers.hpp>
#include <iccv17_test/utils/time_measurement.hpp>
#include <iccv17_test/utils/random_generators.hpp>

#include <iccv17_test/config/config.hpp>

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <iomanip>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#include <cstdarg>
#include <cctype>

namespace iccv17
{
namespace opengv_helpers
{
void nonLinearOpimization(  const bearingVectors_t &pts1,
                            const bearingVectors_t &pts2,
                            sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t &initialModel,
                            sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t &optimizedModel,
                            double &timeOut );

void randomWithOptimization(const bearingVectors_t &pts1,
                            const bearingVectors_t &pts2,
                            Eigen::Matrix3d& rotation,
                            Eigen::Vector3d& translation,
                            double &timeOut);

void solveEigenSacProblem(  bearingVectors_t &bearingVectors1, 
                            bearingVectors_t &bearingVectors2,
                            Eigen::Matrix3d &rotationOut,
                            Eigen::Vector3d &translationOut,
                            Eigen::Matrix3d &rotationOptimizedOut,
                            Eigen::Vector3d &translationOptimzedOut,
                            double &ransacTimeOut,
                            double &optimizationTimeOut );


void solveCentralSacProblemWithoutOptimization(
                            bearingVectors_t &bearingVectors1, 
                            bearingVectors_t &bearingVectors2,
                            Eigen::Matrix3d &rotationOut,
                            Eigen::Vector3d &translationOut,
                            std::vector<int> &inliers,
                            double &ransacTimeOut,
                            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::Algorithm algorithm,
                            int ransacIterations = NO_OF_RANSAC_ITERATIONS
                        );

void solveCentralSacProblem(bearingVectors_t &bearingVectors1, 
                            bearingVectors_t &bearingVectors2,
                            Eigen::Matrix3d &rotationOut,
                            Eigen::Vector3d &translationOut,
                            Eigen::Matrix3d &rotationOptimizedOut,
                            Eigen::Vector3d &translationOptimzedOut,
                            double &ransacTimeOut,
                            double &optimizationTimeOut,
                            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::Algorithm algorithm);

void solveStew( bearingVectors_t &bearingVectors1, 
                bearingVectors_t &bearingVectors2,
                Eigen::Matrix3d &rotationOut,
                Eigen::Vector3d &translationOut,
                Eigen::Matrix3d &rotationOptimizedOut,
                Eigen::Vector3d &translationOptimzedOut,
                double &ransacTimeOut,
                double &optimizationTimeOut );                        

void solveNister(   bearingVectors_t &bearingVectors1, 
                    bearingVectors_t &bearingVectors2,
                    Eigen::Matrix3d &rotationOut,
                    Eigen::Vector3d &translationOut,
                    Eigen::Matrix3d &rotationOptimizedOut,
                    Eigen::Vector3d &translationOptimzedOut,
                    double &ransacTimeOut,
                    double &optimizationTimeOut );                        

void solveSevenPt(  bearingVectors_t &bearingVectors1, 
                    bearingVectors_t &bearingVectors2,
                    Eigen::Matrix3d &rotationOut,
                    Eigen::Vector3d &translationOut,
                    Eigen::Matrix3d &rotationOptimizedOut,
                    Eigen::Vector3d &translationOptimzedOut,
                    double &ransacTimeOut,
                    double &optimizationTimeOut );                        

void solveEightPt(  bearingVectors_t &bearingVectors1, 
                    bearingVectors_t &bearingVectors2,
                    Eigen::Matrix3d &rotationOut,
                    Eigen::Vector3d &translationOut,
                    Eigen::Matrix3d &rotationOptimizedOut,
                    Eigen::Vector3d &translationOptimzedOut,
                    double &ransacTimeOut,
                    double &optimizationTimeOut );                        
} // namespace opengv_helpers
} // namespace iccv17

#endif