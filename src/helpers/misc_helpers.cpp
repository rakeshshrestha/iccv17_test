#include <iccv17_test/helpers/misc_helpers.hpp>
#include "ceres/rotation.h"
#include "ceres/ceres.h"

double iccv17::misc_helpers::outputRotationTranslation(   
                                  std::fstream &outputStream,
                                  Eigen::Matrix3d rotation,
                                  Eigen::Vector3d translation
                                )
{
  outputStream  << rotation(0, 0) << " "
                << rotation(0, 1) << " "
                << rotation(0, 2) << " "
                << translation(0) << std::endl
                << rotation(1, 0) << " "
                << rotation(1, 1) << " "
                << rotation(1, 2) << " "
                << translation(1) << std::endl
                << rotation(2, 0) << " "
                << rotation(2, 1) << " "
                << rotation(2, 2) << " "
                << translation(2) << std::endl;
}

double iccv17::misc_helpers::readRotationTranslation(
                                  std::ifstream &inputStream,
                                  Eigen::Matrix3d &rotation,
                                  Eigen::Vector3d &translation
                              )
{
  inputStream >> rotation(0, 0) >> rotation(0, 1) >> rotation(0, 2) >> translation(0) 
              >> rotation(1, 0) >> rotation(1, 1) >> rotation(1, 2) >> translation(1) 
              >> rotation(2, 0) >> rotation(2, 1) >> rotation(2, 2) >> translation(2);
}                            

double iccv17::misc_helpers::calculateRotationError(  Eigen::Matrix3d trueRotation,
                                Eigen::Matrix3d measuredRotation    )
{
    // rotation error
    Eigen::Vector3d angleDiff;
    Eigen::Matrix3d rotationDiff=measuredRotation.transpose() * trueRotation;
    ceres::RotationMatrixToAngleAxis(rotationDiff.data(), angleDiff.data());
    return angleDiff.norm();
}

double iccv17::misc_helpers::calculateTranslationError( Eigen::Vector3d trueTranslation,
                                Eigen::Vector3d measuredTranslation)
{
    // translation error
    Eigen::Vector3d originalTranslation = measuredTranslation;

    measuredTranslation.normalize();
    trueTranslation.normalize();

    double error = std::min(std::acos(measuredTranslation.dot(trueTranslation)),
                            std::acos(measuredTranslation.dot(-trueTranslation)));
    if (std::isnan(error)) {
        // std::cout << "[Error] " << trueTranslation << originalTranslation << "Error is NaN" << std::endl;
        error = 3.1416; // high error
    }

    return error;
}

void iccv17::misc_helpers::calculateError(Eigen::Matrix3d trueRotation,
                    Eigen::Vector3d trueTranslation,
                    Eigen::Matrix3d measuredRotation,
                    Eigen::Vector3d measuredTranslation,
                    double &errorAngle,
                    double &errorTranslation)
{
    // rotation error
    errorAngle = calculateRotationError(trueRotation, measuredRotation);
        
    // translation error
    errorTranslation = calculateTranslationError(trueTranslation, measuredTranslation);
}

double iccv17::misc_helpers::getRandomBetween(double minimum, double maximum)
{
    // random number between 0 and 1
    double random0to1 = ((double) rand())/ ((double) RAND_MAX);
    // random number between minimum and maximum
    double random_custom = minimum + (maximum - minimum) * random0to1;

    return random_custom;
}

bool iccv17::misc_helpers::doublePairCompare(const std::pair<double, double>& firstElem, const std::pair<double, double>& secondElem) 
{
  return firstElem.first < secondElem.first;
}

//template <class T>
double iccv17::misc_helpers::vectAverage(std::vector<double> &vect)
{
  return std::accumulate(vect.begin(), vect.end(), 0.0) / vect.size();
}

double iccv17::misc_helpers::vectStandardDeviation(std::vector<double> &vect)
{
  double sum = std::accumulate(vect.begin(), vect.end(), 0.0);
  double mean = sum / vect.size();

  std::vector<double> diff(vect.size());
  std::transform(vect.begin(), vect.end(), diff.begin(),
                 std::bind2nd(std::minus<double>(), mean));
  double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
  double stdev = std::sqrt(sq_sum / vect.size());

  return stdev;
}

void iccv17::misc_helpers::vectRemoveNaN(std::vector<double> &vect)
{

  std::vector<double> vectTemp;
  vectTemp.reserve(vect.size());

  for (int i = 0; i < vect.size(); i++) {
    if (!std::isnan(vect[i])) {
      vectTemp.push_back(vect[i]);
    }
  }
  vect.clear();
  vect = vectTemp;

}

void iccv17::misc_helpers::vectRemoveNaN(std::vector<std::pair<double, double>> &vect)
{

  std::vector<std::pair<double, double>> vectTemp;
  vectTemp.reserve(vect.size());

  for (int i = 0; i < vect.size(); i++) {
    if (!std::isnan(vect[i].first) && !std::isnan(vect[i].second)) {
      vectTemp.push_back(vect[i]);
    }
  }
  
  vect.clear();
  vect = vectTemp;

}