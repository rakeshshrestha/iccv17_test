#include <iccv17_test/helpers/opengv_helpers.hpp>

void iccv17::opengv_helpers::nonLinearOpimization(  const bearingVectors_t &pts1,
                            const bearingVectors_t &pts2,
                            sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t &initialModel,
                            sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t &optimizedModel,
                            double &timeOut )
{
    struct timeval tic;
    struct timeval toc;
    
    assert(pts1.size()==pts2.size());

    // every point is inlier for now
    std::vector<int> inliers(pts1.size());
    for (int i = 0; i < pts1.size(); i++) {
        inliers.push_back(i);
    }

    relative_pose::CentralRelativeAdapter adapter(pts1, pts2);
            
    std::shared_ptr<sac_problems::relative_pose::CentralRelativePoseSacProblem>
    relposeproblem_ptr(new sac_problems::relative_pose::CentralRelativePoseSacProblem(
        adapter, 
        sac_problems::relative_pose::CentralRelativePoseSacProblem::STEWENIUS // the algorithm doesn't matter, we aren't using the given method, just the non-linear optimization
    ));
    
    gettimeofday( &tic, 0 );
    relposeproblem_ptr->optimizeModelCoefficients(inliers,
                                                  initialModel,
                                                  optimizedModel);
    gettimeofday( &toc, 0 );
    timeOut = TIMETODOUBLE(timeval_minus(toc,tic));

}

void iccv17::opengv_helpers::randomWithOptimization(const bearingVectors_t &pts1,
                            const bearingVectors_t &pts2,
                            Eigen::Matrix3d& rotation,
                            Eigen::Vector3d& translation,
                            double &timeOut)
{
    struct timeval tic;
    struct timeval toc;
    
    assert(pts1.size()==pts2.size());

    // start with random translation and identity rotation
    
    sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t initialModel;
    initialModel.block<3,3>(0, 0) = Eigen::Matrix3d::Identity();
    initialModel.col(3) = opengv::generateRandomTranslation(MAX_TRANSLATION);

    sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t optimizedModel;
    
    nonLinearOpimization(   pts1,
                            pts2,
                            initialModel,
                            optimizedModel,
                            timeOut );
    
    rotation = optimizedModel.block<3,3>(0, 0);
    translation = optimizedModel.col(3);
    
    return;
}
void iccv17::opengv_helpers::solveEigenSacProblem(  bearingVectors_t &bearingVectors1, 
                                    bearingVectors_t &bearingVectors2,
                                    Eigen::Matrix3d &rotationOut,
                                    Eigen::Vector3d &translationOut,
                                    Eigen::Matrix3d &rotationOptimizedOut,
                                    Eigen::Vector3d &translationOptimzedOut,
                                    double &ransacTimeOut,
                                    double &optimizationTimeOut ) 
{
    //Run the experiment
    struct timeval tic;
    struct timeval toc;

    relative_pose::CentralRelativeAdapter adapter(  bearingVectors1,
                                                    bearingVectors2);
            
    sac::Ransac<
    sac_problems::relative_pose::EigensolverSacProblem> ransac;
    std::shared_ptr<
    sac_problems::relative_pose::EigensolverSacProblem> eigenproblem_ptr(new sac_problems::relative_pose::EigensolverSacProblem(adapter,10));
    
    ransac.sac_model_ = eigenproblem_ptr;
    ransac.threshold_ = 5.0;
    ransac.max_iterations_ = NO_OF_RANSAC_ITERATIONS;
    
   
    gettimeofday( &tic, 0 );
    ransac.computeModel();
    gettimeofday( &toc, 0 );
    ransacTimeOut = TIMETODOUBLE(timeval_minus(toc,tic));
    
    //do final polishing of the model over all inliers
    sac_problems::relative_pose::EigensolverSacProblem::model_t optimizedModel;
    
    gettimeofday( &tic, 0 );
    eigenproblem_ptr->optimizeModelCoefficients(ransac.inliers_,
                                                ransac.model_coefficients_,
                                                optimizedModel);
    gettimeofday( &toc, 0 );
    optimizationTimeOut = ransacTimeOut + TIMETODOUBLE(timeval_minus(toc, tic));

    rotationOut = ransac.model_coefficients_.rotation;
    translationOut = ransac.model_coefficients_.translation;

    rotationOptimizedOut = optimizedModel.rotation;
    translationOptimzedOut = optimizedModel.translation;

    return;
}

void iccv17::opengv_helpers::solveCentralSacProblemWithoutOptimization(
                            bearingVectors_t &bearingVectors1, 
                            bearingVectors_t &bearingVectors2,
                            Eigen::Matrix3d &rotationOut,
                            Eigen::Vector3d &translationOut,
                            std::vector<int> &inliers,
                            double &ransacTimeOut,
                            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::Algorithm algorithm,
                            int ransacIterations
                        )
{
    //Run the experiment
    struct timeval tic;
    struct timeval toc;

    relative_pose::CentralRelativeAdapter adapter(  bearingVectors1,
                                                    bearingVectors2);
            
    sac::Ransac<sac_problems::relative_pose::CentralRelativePoseSacProblem> ransac;
    std::shared_ptr<sac_problems::relative_pose::CentralRelativePoseSacProblem>
    relposeproblem_ptr(new sac_problems::relative_pose::CentralRelativePoseSacProblem(adapter, algorithm));
    ransac.sac_model_ = relposeproblem_ptr;
    ransac.threshold_ = 3.0*(1.0 - cos(atan(sqrt(2.0)*0.5/800.0))); // sqrt(2.0) / 850; // 
    ransac.max_iterations_ = ransacIterations;

    gettimeofday( &tic, 0 );
    ransac.computeModel();
    rotationOut = ransac.model_coefficients_.block<3,3>(0,0);
    translationOut =ransac.model_coefficients_.col(3);
    gettimeofday( &toc, 0 );
    ransacTimeOut = TIMETODOUBLE(timeval_minus(toc, tic));

    inliers = std::vector<int>(ransac.inliers_.begin(), ransac.inliers_.end());
}

void iccv17::opengv_helpers::solveCentralSacProblem(
                            bearingVectors_t &bearingVectors1, 
                            bearingVectors_t &bearingVectors2,
                            Eigen::Matrix3d &rotationOut,
                            Eigen::Vector3d &translationOut,
                            Eigen::Matrix3d &rotationOptimizedOut,
                            Eigen::Vector3d &translationOptimzedOut,
                            double &ransacTimeOut,
                            double &optimizationTimeOut,
                            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::Algorithm algorithm
                        ) 
{
    //Run the experiment
    struct timeval tic;
    struct timeval toc;

    relative_pose::CentralRelativeAdapter adapter(  bearingVectors1,
                                                    bearingVectors2);
            
    sac::Ransac<sac_problems::relative_pose::CentralRelativePoseSacProblem> ransac;
    std::shared_ptr<sac_problems::relative_pose::CentralRelativePoseSacProblem>
    relposeproblem_ptr(new sac_problems::relative_pose::CentralRelativePoseSacProblem(adapter, algorithm));
    ransac.sac_model_ = relposeproblem_ptr;
    ransac.threshold_ = 3.0*(1.0 - cos(atan(sqrt(2.0)*0.5/800.0)));
    ransac.max_iterations_ = NO_OF_RANSAC_ITERATIONS;

    gettimeofday( &tic, 0 );
    ransac.computeModel();
    rotationOut = ransac.model_coefficients_.block<3,3>(0,0);
    translationOut =ransac.model_coefficients_.col(3);
    gettimeofday( &toc, 0 );
    ransacTimeOut = TIMETODOUBLE(timeval_minus(toc, tic));
    
    
    sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t optimizedModel;
    gettimeofday( &tic, 0 );
    relposeproblem_ptr->optimizeModelCoefficients(ransac.inliers_,
                                                  ransac.model_coefficients_,
                                                  optimizedModel);
    gettimeofday( &toc, 0 );
    optimizationTimeOut = ransacTimeOut + TIMETODOUBLE(timeval_minus(toc,tic));

    rotationOptimizedOut = optimizedModel.block<3,3>(0,0);
    translationOptimzedOut = optimizedModel.col(3);

    // debug
    // std::cout << "My optmization result: " << translationOptimzedOut << std::endl;

    return;
}

void iccv17::opengv_helpers::solveStew( bearingVectors_t &bearingVectors1, 
                bearingVectors_t &bearingVectors2,
                Eigen::Matrix3d &rotationOut,
                Eigen::Vector3d &translationOut,
                Eigen::Matrix3d &rotationOptimizedOut,
                Eigen::Vector3d &translationOptimzedOut,
                double &ransacTimeOut,
                double &optimizationTimeOut )
{
  solveCentralSacProblem( bearingVectors1,
                          bearingVectors2,
                          rotationOut,
                          translationOut,
                          rotationOptimizedOut,
                          translationOut,
                          ransacTimeOut,
                          optimizationTimeOut,
                          sac_problems::relative_pose::CentralRelativePoseSacProblem::STEWENIUS );
} 

void iccv17::opengv_helpers::solveNister( bearingVectors_t &bearingVectors1, 
                bearingVectors_t &bearingVectors2,
                Eigen::Matrix3d &rotationOut,
                Eigen::Vector3d &translationOut,
                Eigen::Matrix3d &rotationOptimizedOut,
                Eigen::Vector3d &translationOptimzedOut,
                double &ransacTimeOut,
                double &optimizationTimeOut )
{
  solveCentralSacProblem( bearingVectors1,
                          bearingVectors2,
                          rotationOut,
                          translationOut,
                          rotationOptimizedOut,
                          translationOut,
                          ransacTimeOut,
                          optimizationTimeOut,
                          sac_problems::relative_pose::CentralRelativePoseSacProblem::NISTER );
} 

void iccv17::opengv_helpers::solveSevenPt( bearingVectors_t &bearingVectors1, 
                bearingVectors_t &bearingVectors2,
                Eigen::Matrix3d &rotationOut,
                Eigen::Vector3d &translationOut,
                Eigen::Matrix3d &rotationOptimizedOut,
                Eigen::Vector3d &translationOptimzedOut,
                double &ransacTimeOut,
                double &optimizationTimeOut )
{
  solveCentralSacProblem( bearingVectors1,
                          bearingVectors2,
                          rotationOut,
                          translationOut,
                          rotationOptimizedOut,
                          translationOut,
                          ransacTimeOut,
                          optimizationTimeOut,
                          sac_problems::relative_pose::CentralRelativePoseSacProblem::SEVENPT );
} 

void iccv17::opengv_helpers::solveEightPt( bearingVectors_t &bearingVectors1, 
                bearingVectors_t &bearingVectors2,
                Eigen::Matrix3d &rotationOut,
                Eigen::Vector3d &translationOut,
                Eigen::Matrix3d &rotationOptimizedOut,
                Eigen::Vector3d &translationOptimzedOut,
                double &ransacTimeOut,
                double &optimizationTimeOut )
{
  solveCentralSacProblem( bearingVectors1,
                          bearingVectors2,
                          rotationOut,
                          translationOut,
                          rotationOptimizedOut,
                          translationOut,
                          ransacTimeOut,
                          optimizationTimeOut,
                          sac_problems::relative_pose::CentralRelativePoseSacProblem::EIGHTPT );
} 