#include <iccv17_test/relative_motion/relative_pose_estimation.hpp>
#include <iccv17_test/optimization/RotationTranslation.hpp>
#include <iccv17_test/optimization/Rotation.hpp>

#include <iccv17_test/utils/time_measurement.hpp>
#include <iccv17_test/utils/random_generators.hpp>

#include <iccv17_test/helpers/opengv_helpers.hpp>
#include <iccv17_test/helpers/misc_helpers.hpp>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/core/mat.hpp"
#include "opencv/cv.hpp"

Eigen::Vector3d 
iccv17::relative_pose_estimation::estimateRelativeTranslation(  const std::vector<Eigen::Vector3d> &pts1,
                                                                const std::vector<Eigen::Vector3d> &pts2) {
    
    int num_point=pts1.size();
    std::vector<Eigen::Vector3d> norms(num_point);
    Eigen::Matrix3Xd allNorms=Eigen::Matrix3Xd(3,num_point);
    Eigen::Vector3d preResult,curResult;
    for(int i=0;i<num_point;i++){
        norms[i]=pts1[i].cross(pts2[i]);
        norms[i].normalize();
        allNorms.col(i)=norms[i];
    }
    Eigen::Matrix3d NtN=allNorms*allNorms.transpose();
    Eigen::JacobiSVD<Eigen::Matrix3d> svd(NtN,Eigen::ComputeFullV);
    Eigen::Matrix3d V=svd.matrixV();
    curResult=V.col(2);
    return curResult;
}

void iccv17::relative_pose_estimation::preSolve(const std::vector<Eigen::Vector3d> &pts1,
              const std::vector<Eigen::Vector3d> &pts2,
              Eigen::Matrix3d& rotation,
              Eigen::Vector3d& translation){
    
    
    double motion[3]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    ceres::Problem problem;
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=Rotation::Create(pts1[i](0),
                                                            pts1[i](1),
                                                            pts1[i](2),
                                                            pts2[i](0),
                                                            pts2[i](1),
                                                            pts2[i](2));
        problem.AddResidualBlock(cost_function,NULL,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;
    ceres::Solve(options,&problem, &summary);
    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    
    std::vector<Eigen::Vector3d> _pts1(pts1.size()),_pts2(pts2.size());
    
    for(int p=0;p<pts1.size();p++){
        _pts1[p]=rotation*pts1[p];
        _pts2[p]=pts2[p];
    }
    translation=estimateRelativeTranslation(_pts1,_pts2);
}

void iccv17::relative_pose_estimation::preSolve(const bearingVectors_t &pts1,
              const bearingVectors_t &pts2,
              Eigen::Matrix3d& rotation,
              Eigen::Vector3d& translation,
              double &timeOut ) {

    struct timeval tic;
    struct timeval toc;
    
    assert(pts1.size()==pts2.size());
    
    double motion[3]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    ceres::Problem problem;
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=Rotation::Create(pts1[i](0),
                                                            pts1[i](1),
                                                            pts1[i](2),
                                                            pts2[i](0),
                                                            pts2[i](1),
                                                            pts2[i](2));
        problem.AddResidualBlock(cost_function,NULL,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;

    gettimeofday( &tic, 0 );
    ceres::Solve(options,&problem, &summary);
    gettimeofday( &tic, 0 );
    timeOut = TIMETODOUBLE(timeval_minus(toc, tic));

    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    
    std::vector<Eigen::Vector3d> _pts1(pts1.size()),_pts2(pts2.size());
    
    for(int p=0;p<pts1.size();p++){
        _pts1[p]=rotation*pts1[p];
        _pts2[p]=pts2[p];
    }

    gettimeofday( &tic, 0 );
    translation=estimateRelativeTranslation(_pts1,_pts2);
    gettimeofday( &tic, 0 );
    
    timeOut += TIMETODOUBLE(timeval_minus(toc, tic));

}

void iccv17::relative_pose_estimation::preSolveWithOptimization(  
    const bearingVectors_t &pts1,
    const bearingVectors_t &pts2,
    Eigen::Matrix3d &preSolveRotation,
    Eigen::Vector3d &preSolveTranslation,
    Eigen::Matrix3d &optimizedRotation,
    Eigen::Vector3d &optimizedTranslation,
    Eigen::Matrix3d &trueRotation,
    Eigen::Vector3d &trueTranslation,
    double &preSolveTimeOut,
    double &optimizationTimeOut ) 
{
    
    struct timeval tic;
    struct timeval toc;
    
    assert(pts1.size()==pts2.size());

    gettimeofday( &tic, 0 );
    preSolve(pts1,pts2,preSolveRotation,preSolveTranslation, preSolveTimeOut);
    gettimeofday( &toc, 0);
    
    // the model coefficients (transformation from frame1 to frame2, but R and t from presolve is from frame2 to frame1)
    sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t initialModel;
    initialModel.block<3,3>(0, 0) = preSolveRotation.transpose();
    initialModel.col(3) = initialModel.block<3,3>(0, 0) * preSolveTranslation;

    if (
        iccv17::misc_helpers::calculateTranslationError(trueTranslation, -preSolveTranslation) < 
        iccv17::misc_helpers::calculateTranslationError(trueTranslation, preSolveTranslation) 
    ) {
        initialModel.col(3) = -initialModel.col(3);
    }

    sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t optimizedModel;
    
    iccv17::opengv_helpers::nonLinearOpimization(   
        pts1,
        pts2,
        initialModel,
        optimizedModel,
        optimizationTimeOut 
    );
    optimizationTimeOut += preSolveTimeOut;
    
    return;
}  
void iccv17::relative_pose_estimation::estimateRotationTranslation(const double lossThreshold,
                                 const bearingVectors_t &pts1,
                                 const bearingVectors_t &pts2,
                                 Eigen::Matrix3d& rotation,
                                 Eigen::Vector3d& translation,
                                 double &timeOut) {
    
    struct timeval tic;
    struct timeval toc;
    timeOut = 0;

    assert(pts1.size()==pts2.size());

    gettimeofday( &tic, 0 );
    preSolve(pts1,pts2,rotation,translation,timeOut);
    gettimeofday( &toc, 0);

    timeOut += TIMETODOUBLE(timeval_minus(toc, tic));

    double motion[6]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    motion[3]=translation(0);
    motion[4]=translation(1);
    motion[5]=translation(2);

    
    ceres::Problem problem;
    ceres::LossFunction* loss_function = new ceres::HuberLoss(lossThreshold);
    
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=RotationTranslation::Create(pts1[i](0),
                                                                       pts1[i](1),
                                                                       pts1[i](2),
                                                                       pts2[i](0),
                                                                       pts2[i](1),
                                                                       pts2[i](2));
        problem.AddResidualBlock(cost_function,NULL,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;

    gettimeofday( &tic, 0 );
    ceres::Solve(options,&problem, &summary);
    gettimeofday( &toc, 0 );
    
    timeOut += TIMETODOUBLE(timeval_minus(toc, tic));
    

    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    translation(0)=motion[3];
    translation(1)=motion[4];
    translation(2)=motion[5];
}