function [T_bundler, inliers] = solveMatlab5pt(v1, v2, T_gt, ransac_round)
    addpath('/home/rakesh/workspace/iccv17_test/Matlab');
    % addpath('/home/rakesh/workspace/rakesh/5point/ICCV17_Test/Matlab')

    %Set some parameter first
    if nargin < 4
        ransac_round =   1000;
    end

    thr = 0.005;
    nMatches = size(v1, 2);
    K1 = eye(3);
    K2 = eye(3);

    % P1 = R(P2) + t
    R_gt = T_gt(1:3, 1:3);
    t_gt = T_gt(1:3, 4);

    % P2 = R(P1-t)
    [E, ~, t, inliers]  = compute_E_ransac_with_inliers([v1(1, :);v1(2, :); ones(1,size(v1, 2))], [v2(1, :);v2(2, :); ones(1,size(v2, 2))], ransac_round, K1, K2, thr);
    % get the best rotation
    [R1, R2] = getTwoRotationsFromE(E);
    
    R_cat = cat(3, R1, R2);
    [~, best_rotation_index] = min([rotationError( R_gt, R1'), rotationError( R_gt, R2')]);
    R_bundler = R_cat(:, :, best_rotation_index);
    
    % get the best translation
    % t = choose_best_t(R, t, inv(K1)*v1, inv(K2)*v2);
    bundler_translation_errors = [0, 0];
    t_cat = cat(2, R_bundler'*t, -R_bundler'*t);
    for t_idx = 1:size(t_cat, 2)
        bundler_translation_errors(t_idx) = translationError(t_gt, t_cat(:, t_idx));
    end
    [~, best_translation_index] = min(bundler_translation_errors);
    t_bundler = t_cat(:, best_translation_index);
    T_bundler = getTransformationMat(R_bundler', t_bundler);
end

function [translation_error] = translationError(t1, t2)
    t1 = t1 ./ norm(t1);
    t2 = t2 ./ norm(t2);

    translation_error = min(acos(dot(t1, t2)), ...
                            acos(dot(-t1, t2))) * 180/pi;
end

function [rotation_error] = rotationError(R1, R2)
    rotation_diff = R1' * R2;
    rotation_error = rotationMatrixToVector(rotation_diff);
    rotation_error = norm(rotation_error) * 180/pi;
end    
