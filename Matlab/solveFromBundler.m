function [R_errors_median, R_errors_max, t_errors_median, t_errors_max, Rt_estimate_all, inliers] = solveFromBundler(number_of_iterations, number_of_points, noise_range, path_prefix)
    % addpath('/localhome/rakeshs/workspace/opengv/matlab/helpers/')
    % addpath('/localhome/rakeshs/workspace/opengv/matlab')

    R_errors_median = [];
    R_errors_max = [];
    Rt_estimate_all = [];

    t_errors_median = [];
    t_errors_max = [];
    
    % Set intrisinc matrix
    K1 = eye(3);
    K2 = eye(3);

    
    bearing_concat = dlmread([path_prefix '/' num2str(number_of_points) '_bearing.in']);
    bearing1_concat = bearing_concat(:, 1:3);
    bearing2_concat = bearing_concat(:, 4:end);
    
    ground_truth_concat = dlmread([path_prefix '/' num2str(number_of_points) '_ground_truth1.in']);
    
    % keep track of where your data is
    bearing_idx_counter = 1;
    ground_truth_idx_counter = 1;
    
    inliers = cell(1, length(noise_range) * number_of_iterations);
    inliers_idx = 1;

    for noise_idx = 1:length(noise_range)
        noise = noise_range(noise_idx);

        temp_R_errors = [];
        temp_t_errors = [];

        disp(['noise: ' num2str(noise)]);

        for iteration = 1:number_of_iterations
            if (mod(iteration, 50) == 0)
                disp(['Iteration ' num2str(iteration) ' / ' num2str(number_of_iterations)]);
            end
            bearing_idx = bearing_idx_counter:(bearing_idx_counter+number_of_points-1); %[(iteration-1)*number_of_points + 1 : iteration * number_of_points];
            bearing_idx_counter = bearing_idx_counter + number_of_points;
                        
            ground_truth_idx = ground_truth_idx_counter:(ground_truth_idx_counter+3-1); %[(iteration-1)*3 + 1: iteration*3];
            ground_truth_idx_counter = ground_truth_idx_counter + 3;

            T_gt = ground_truth_concat(ground_truth_idx, :);
            R_gt = T_gt(1:3, 1:3);
            t_gt = T_gt(1:3, 4);
            % make it 4x4
            T_gt = getTransformationMat(R_gt, t_gt);

            v1 = bearing1_concat(bearing_idx, :)';
            v2 = bearing2_concat(bearing_idx, :)';
            
            % make homogeneous
            v1 = v1 ./ v1(3, :);
            v2 = v2 ./ v2(3, :);

            [T_bundler, inliers_iter] = solveMatlab5pt(v1, v2, T_gt);
            
            inliers{inliers_idx} = inliers_iter;
            inliers_idx = inliers_idx + 1;

            R_bundler = T_bundler(1:3, 1:3);
            t_bundler = T_bundler(1:3, 4);

            Rt_estimate_all = [Rt_estimate_all; T_bundler(1:3, 1:4)];
            
            [R_error, t_error] = transformationError(T_gt, T_bundler);
            temp_R_errors = [temp_R_errors R_error];
            temp_t_errors = [temp_t_errors t_error];
        end
        R_errors_median = [R_errors_median median(temp_R_errors)];
        R_errors_max = [R_errors_max max(temp_R_errors)];

        t_errors_median = [t_errors_median median(temp_t_errors)];
        t_errors_max = [t_errors_max max(temp_t_errors)];
    end
end

function [rotation_error, translation_error] = transformationError(T1, T2)
    rotation_error = rotationError(T1(1:3, 1:3), T2(1:3, 1:3));
    translation_error = translationError(T1(1:3, 4), T2(1:3, 4));
end

function [translation_error] = translationError(t1, t2)
    t1 = t1 ./ norm(t1);
    t2 = t2 ./ norm(t2);

    translation_error = min( acos(dot(t1, t2)), ...
        acos(dot(-t1, t2)) ) * 180/pi;
end

function [rotation_error] = rotationError(R1, R2)
    rotation_diff = R1' * R2;
    rotation_error = rotationMatrixToVector(rotation_diff);
    rotation_error = norm(rotation_error) * 180/pi;
end    
