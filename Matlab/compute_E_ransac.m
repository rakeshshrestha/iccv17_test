% Compute the Essential matrix robustly using 5-point algorithm

function [E_best, R, t] = compute_E_ransac(l_pts, r_pts, ransac_t, K_l, K_r, thr)

% Function [E_best, R, t] = compute_E_ransac(l_pts, r_pts, ransac_t, K_l, K_r, thr)
% Zhaopeng Cui 2015.2
%
% Inputs:
% l_pts, r_pts -- 3*N matrix, which contains the feature coordinates in the
% left / right image;
% ransac_t -- The parameter controls the times of performing RANSAC;
% K_l, K_r -- 3*3 matrix, the intrinsic matrix of left / right image;
% thr -- The threshold for judging the inliers;
%
% Outputs:
% E_best -- The computed essential matrix;
% R -- The relative rotation matrix between two images decomposed from E;
% t -- The relative translation direction between two images decomposed from E. 
% E_best = [t]x R, here [t]x is the Skew-symmetric matrix of vector t.

    E_best = [];
    num_pts = size(l_pts,2);
    thr2 = thr^2;

   % Compute the inverse of K_r and K_l
    K_l_inv = inv(K_l);
    K_r_inv = inv(K_r);
    
    % Compute the normalized points
    l_pts_norm = K_l_inv * l_pts;
    r_pts_norm = K_r_inv * r_pts;
    
    max_num_inlers = -1;
    % Loop for RANSAC
    for i = 1: ransac_t
        indices = randperm(num_pts,5);
        l_pts_inner = l_pts_norm(:, indices);
        r_pts_inner = r_pts_norm(:, indices);
        
        Evec   = calibrated_fivepoint(l_pts_inner, r_pts_inner);
        
        for j=1:size(Evec,2)
            E = reshape(Evec(:,j),3,3)';     
            F = K_r_inv' * E * K_l_inv;
            
            % Compute the inliers
            num_inliers = 0;
            for k = 1:num_pts
                 err = compute_sampon_error(l_pts(:,k), r_pts(:,k), F);
                 if err< thr2
                     num_inliers = num_inliers+1;
                 end
            end
           
            % Update E
            if max_num_inlers < num_inliers
                E_best = E;
                max_num_inlers = num_inliers;
            end
        end % end of  j=1:size(Evec,2)
        
    end
    % fprintf('Number of inliers: %d \n',max_num_inlers);
    [R, t] = Get_Rt_from_E(E_best,l_pts_norm,r_pts_norm);

end

function err = compute_sampon_error(l_pt, r_pt, F)
    F_l = F*l_pt;
    F_r = F'*r_pt;
    err = (r_pt'*F_l)^2*(1/(F_l(1)^2+F_l(2)^2) + 1/(F_r(1)^2+F_r(2)^2) );
    %err = (r_pt'*F_l)^2/((F_l(1)^2+F_l(2)^2) + (F_r(1)^2+F_r(2)^2) );
end