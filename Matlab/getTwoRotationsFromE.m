function [R1, R2] = getTwoRotationsFromE(E)
    [u, s, v]=svd(E);

    W=[0 -1 0
       1 0 0
       0 0 1];

    R1=u*W*v';
    if det(R1)<0
        R1=-R1;
    end;

    R2=u*W'*v';

    if det(R2)<0
        R2=-R2;
    end;
end