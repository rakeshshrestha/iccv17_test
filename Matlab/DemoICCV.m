%%% Demo for computing the essential matrix using 5 point algorithm %%%

addpath('/localhome/rakeshs/workspace/opengv/matlab/helpers/')
addpath('/localhome/rakeshs/workspace/opengv/matlab')

%% Configure the benchmark

% central case -> only one camera
cam_number = 1;
% Getting 10 points, and testing all algorithms with the respective number of points
pt_number = 10;
% noise test, so no outliers
outlier_fraction = 0.0;
% repeat 5000 tests per noise level
iterations = 200;
% opengv and Zhaopeng's
num_algorithms = 3;
names = { 'OpenGv Five Point'; 'Bundler Five Point'; 'Bundler Optimized' };
algorithm_group1 = [1, 2]; % low error algorithms
algorithm_group2 = [2, 3]; % optimized vs ransac

%Set some parameter first
ransac_round = 50;
thr = 0.005;
nMatches = pt_number;

% Set intrisinc matrix
K1 = eye(3);
K2 = eye(3);

% The maximum noise to analyze
max_noise = 10;
% The step in between different noise levels
noise_step = 1;

%% Run the benchmark

%prepare the overall result arrays
number_noise_levels = max_noise / noise_step + 1;

mean_rotation_errors = zeros(num_algorithms, number_noise_levels);
median_rotation_errors = zeros(num_algorithms, number_noise_levels);
max_rotation_errors = zeros(num_algorithms, number_noise_levels);

mean_translation_errors = zeros(num_algorithms, number_noise_levels);
median_translation_errors = zeros(num_algorithms, number_noise_levels);
max_translation_errors = zeros(num_algorithms, number_noise_levels);


noise_levels = zeros(1,number_noise_levels);

%Run the experiment
for n=1:number_noise_levels

    noise = (n - 1) * noise_step;
    noise_levels(1,n) = noise;
    display(['Analyzing noise level: ' num2str(noise)])
    
    rotation_errors = zeros(2,iterations);
    translation_errors = zeros(2, iterations);

    counter = 0;
    
    validIterations = 0;

    for i=1:iterations
        [v1_unhomogeneous,v2_unhomogeneous,t_gt,R_gt,threeDpoints] = create2D2DExperimentWith3DPoints(pt_number,cam_number,noise,outlier_fraction);
        T_gt = getTransformationMat(R_gt, t_gt);
        
        % ------------------------------ OpenGv ---------------------------------- %
        % P1 = R*P2 + t
        [Out, inliers] = opengv('fivept_nister_ransac',v1_unhomogeneous,v2_unhomogeneous);
        if ~isempty(Out)
            R_opengv = Out(:,1:3);
            t_opengv = Out(:, 4);
            T_opengv = getTransformationMat(R_opengv, t_opengv);
            
            rotation_errors(1,validIterations+1) = evaluateRotationError( R_gt, R_opengv);
            [~, translation_errors(1, validIterations+1)] = evaluateTransformationError(T_gt, T_opengv);
            
            
        else
            rotation_errors(1,validIterations+1) = inf;
            translation_errors(1,validIterations+1) = inf;
        end
        
        % ------------------------------ Bundler ---------------------------------- %
        % make homogeneous
        v1 = v1_unhomogeneous ./ v1_unhomogeneous(3, :);
        v2 = v2_unhomogeneous ./ v2_unhomogeneous(3, :);
        
        % P2 = R(P1-t)
        [E, ~, t, inliers]  = compute_E_ransac_with_inliers([v1(1, :);v1(2, :); ones(1,nMatches)], [v2(1, :);v2(2, :); ones(1,nMatches)], ransac_round, K1, K2, thr);
        % get the best rotation
        [R1, R2] = getTwoRotationsFromE(E);
        
        R_cat = cat(3, R1, R2);
        [rotation_errors(2, validIterations + 1), best_rotation_index] = min([evaluateRotationError( R_gt, R1'), evaluateRotationError( R_gt, R2')]);
        R_bundler = R_cat(:, :, best_rotation_index);
        
        % get the best translation
        % t = choose_best_t(R, t, inv(K1)*v1, inv(K2)*v2);
        bundler_translation_errors = [0, 0];
        t_cat = cat(2, R_bundler'*t, -R_bundler'*t);
        for t_idx = 1:2
            T = getTransformationMat(R_bundler', t_cat(:, t_idx));
            [~, bundler_translation_errors(t_idx)] = evaluateTransformationError(T_gt, T);
        end
        [translation_errors(2, validIterations + 1), best_translation_index] = ...
            min(bundler_translation_errors);
        t_bundler = t_cat(:, best_translation_index);
        
        T_bundler = getTransformationMat(R_bundler', t_bundler);

        % ------------------------------ Non-linear Optimization ---------------------------------- %
        Out_optimized = opengv('rel_nonlin_central',1:pt_number, v1_unhomogeneous, v2_unhomogeneous, T_bundler(1:3, :));
        R_optimized = Out_optimized(1:3, 1:3);
        t_optimized = Out_optimized(1:3, 4);
        T_optimized = getTransformationMat(R_optimized, t_optimized);
        
        rotation_errors(3,validIterations+1) = evaluateRotationError( R_gt, R_optimized);
        [~, translation_errors(3, validIterations+1)] = evaluateTransformationError(T_gt, T_optimized);

        % debug
        % disp(sprintf('Noise: %d', noise));
        % error_gt = norm( v2_unhomogeneous - reprojectPoints(T_gt, threeDpoints) )
        % error_opengv = norm( v2_unhomogeneous - reprojectPoints(T_opengv, threeDpoints) )
        % error_bundler = norm( v2_unhomogeneous - reprojectPoints(T_bundler, threeDpoints) )
        % error_optimized = norm( v2_unhomogeneous - reprojectPoints(T_optimized, threeDpoints) )

        
        %disp([R;t']);
        validIterations = validIterations + 1;
        counter = counter + 1;
        if counter == 100
            counter = 0;
            display(['Iteration ' num2str(i) ' of ' num2str(iterations) '(noise level ' num2str(noise) ')']);
        end
    end

    for a=1:num_algorithms
        mean_rotation_errors(a,n) = mean(rotation_errors(a,1:validIterations)) * 180.0 / pi;
        median_rotation_errors(a,n) = median(rotation_errors(a,1:validIterations)) * 180.0 / pi;
        max_rotation_errors(a,n) = max(rotation_errors(a,1:validIterations)) * 180.0 / pi;

        mean_translation_errors(a,n) = mean(translation_errors(a,1:validIterations));
        median_translation_errors(a,n) = median(translation_errors(a,1:validIterations));
        max_translation_errors(a,n) = max(translation_errors(a,1:validIterations));
    end
    
end

figure(1);
suptitle('Max rotation');

subplot(1,2,1);
plot(noise_levels,max_rotation_errors(algorithm_group1, :),'LineWidth',2)
legend(names(algorithm_group1),'Location','NorthWest')
xlabel('noise level [pix]')
ylabel('max rot. error [degree]')
grid on

subplot(1,2,2);
plot(noise_levels,max_rotation_errors(algorithm_group2, :),'LineWidth',2)
legend(names(algorithm_group2),'Location','NorthWest')
xlabel('noise level [pix]')
ylabel('max rot. error [degrees]')
grid on

figure(2);
suptitle('Max translation');

subplot(1,2,1);
plot(noise_levels,max_translation_errors(algorithm_group1, :),'LineWidth',2)
legend(names(algorithm_group1),'Location','NorthWest')
xlabel('noise level [pix]')
ylabel('max trans. error [units]')
grid on

subplot(1,2,2);
plot(noise_levels,max_translation_errors(algorithm_group2, :),'LineWidth',2)
legend(names(algorithm_group2),'Location','NorthWest')
xlabel('noise level [pix]')
ylabel('max trans. error [uints]')
grid on

figure(3);
suptitle('Median rotation')
subplot(1,2,1);
plot(noise_levels,median_rotation_errors(algorithm_group1, :),'LineWidth',2)
legend(names(algorithm_group1),'Location','NorthWest')
xlabel('noise level [pix]')
ylabel('median rot. error [degrees]')
grid on

subplot(1,2,2);
plot(noise_levels,median_rotation_errors(algorithm_group2, :),'LineWidth',2)
legend(names(algorithm_group1),'Location','NorthWest')
xlabel('noise level [pix]')
ylabel('median rot. error [degeees]')
grid on

figure(4);
suptitle('Median translation')
subplot(1,2,1);
plot(noise_levels,median_translation_errors(algorithm_group1, :),'LineWidth',2)
legend(names(algorithm_group1),'Location','NorthWest')
xlabel('noise level [pix]')
ylabel('median trans. error [units]')
grid on

subplot(1,2,2);
plot(noise_levels,median_translation_errors(algorithm_group2, :),'LineWidth',2)
legend(names(algorithm_group2),'Location','NorthWest')
xlabel('noise level [pix]')
ylabel('median trans. error [units]')
grid on

function [reprojections] = reprojectPoints(T, points)
    % convert from world coordinates to camera coordinate ( p_world = T * p_camera)
    if (size(points, 1) == 3)
        % make homogeneous
        points = [points; ones(1, size(points, 2))];
    end

    camera_points = inv(T) * points;
    camera_points = camera_points(1:3, :);

    reprojections = normalizePoints(camera_points);
end

function [normalized_points] =  normalizePoints(unnormalized_points)
    normalized_points = zeros(size(unnormalized_points, 1), size(unnormalized_points, 2));
    for i = 1:size(unnormalized_points, 2)
        normalized_points(:, i) = unnormalized_points(:, i) / norm(unnormalized_points(:, i));
    end
end

function [T] = getTransformationMat(R, t)
    T = eye(4);
    T(1:3, 1:3) = R;
    T(1:3, 4) = t;
end

function [T]= choose_best_t(R,t, posit1, posit2)

    T=cat(3, t, -1*t);


    M(:,:,1)=depth(R, t, posit1, posit2);
    M(:,:,2)=depth(R, -1*t, posit1, posit2);
    

    positive=[0 0];

    [row,col, height]=size(M);

    for i=1:2
        for j=1:col
            if M(1, j, i)>0
                positive(i)=positive(i)+1;
            end;
             if M(2, j, i)>0
                positive(i)=positive(i)+1;
            end;
        end;
        
    end;

    [how, who]=max(positive);
    T=T(:,:,who);
end


        