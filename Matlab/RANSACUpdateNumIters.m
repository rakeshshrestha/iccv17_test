function [iterations] = RANSACUpdateNumIters( confidence, outlierFraction, modelPoints, numPoints, maxIters ) 
    
    if (modelPoints <= 0)
        disp('the number of model points should be positive');
        return;
    end

    confidence  = max(confidence,0.);
    confidence  = min(confidence,1.);
    outlierFraction = max(outlierFraction,0.);
    outlierFraction = min(outlierFraction,1.);
    
    numInliers = numPoints * (1. - outlierFraction);

    % avoid inf's & nan's
    num = max(1. - confidence, realmin('double') ); % the alarm rate
    
    % consider that the points chosen are not replaced for ransac (more accurate)
    denom = 1.;
    % probability that all are inliers
    for i = 0:(modelPoints-1)
        denom = denom * (numInliers - i) / (numPoints - i);
    end

    % probability that all are outliers
    denom = 1 - denom;

    if ( denom < realmin('double') )
       iterations = 0;
       return;
    end
    
    num = log(num);
    denom = log(denom);
    
    if (denom >= 0 || -num >= maxIters*(-denom))
       iterations =  maxIters;
    else
        iterations = round(num/denom);
    end

end