function [R T ]= choose_Rt(r,rr,t, posit1, posit2)

R=cat(3, r, r, rr, rr);
T=cat(3, t, -1*t, t, -1*t);


M(:,:,1)=depth(r, t, posit1, posit2);
M(:,:,2)=depth(r, -1*t, posit1, posit2);
M(:,:,3)=depth(rr, t, posit1, posit2);
M(:,:,4)=depth(rr, -1*t, posit1, posit2);

positive=[0 0 0 0];

[row,col, height]=size(M);

for i=1:4
    
    for j=1:col
        if M(1, j, i)>0
            positive(i)=positive(i)+1;
        end;
         if M(2, j, i)>0
            positive(i)=positive(i)+1;
        end;
    end;
    
end;

[how, who]=max(positive);
R=R(:,:,who);
T=T(:,:,who);

