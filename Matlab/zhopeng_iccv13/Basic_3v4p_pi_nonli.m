%------------------------------------------------------------------------
% The 3v4p alogrithm using nonlinear interative refinement.
% Usage: P= Basic_3v4p_pi_nonli(x1, x2, x3,K1,K2,K3,Search_flag)
% 
% Input: 
%       x1, x2, x3 -- 3*N matrix, the original homogenous coordinates of the
%                                       matching points in three view;
%       K1,K2,K3 -- 3*3 matrix, the true intrinsic matrix of the cameras;
%       Search_flag -- The basic search flag of 3v4p: 1->40 0->1000;
%
% Output:
%       P -- 3*4*3 Matrix where P1 = [eye(3),zeros(3,1)];
%
% By Zhaopeng Cui, 2012.10
%------------------------------------------------------------------------

function P= Basic_3v4p_pi_nonli(x1, x2, x3,K1,K2,K3,Search_flag)

global Global_x1  Global_x2 Global_x3 Global_w1 Global_w2_1 Global_H21 Global_f3 Global_B1_v  Global_B2_v R2_best t2_best R3_best t3_best err_best

Global_x1 = x1;
Global_x2 = x2;
Global_x3 = x3;

Global_f3 = (K3(1,1)+K3(2,2))/2;

% Find homography that transform the DIAC from the second view to the
% coorinate of the first view
Global_w1 = inv(K1*K1');
w2 = inv(K2*K2');
Global_H21 = estimateH_DLT([x2(1:2,:)',x1(1:2,:)'],1);
Global_w2_1 = inv(Global_H21)'* w2 *inv(Global_H21); % w2_1 represent the conic in the first image of IAC w2.

R2_best = [];
t2_best = [];
R3_best = [];
t3_best = [];


err_store = [];
err_best =  2000000;

%Using the four points to generate the basis.
[Global_B1_v,Global_B2_v] = Get_BasisVector_from_4p(x1);

if isempty(Global_B1_v) ||isempty(Global_B2_v)
    P1=[];
    P2=[];
    P3=[];
else
    if ~Search_flag
        theta_step = pi/1000;
        for theta_B = 0:theta_step:pi
            err_v_temp = myfun_3v4p(theta_B); 
            err_store = cat(1,err_store,sum(err_v_temp.^2));
        end

    else

        theta_step = pi/40;
        for theta_B = 0:theta_step:pi
            err_v_temp = myfun_3v4p(theta_B); 
            err_store = cat(1,err_store,sum(err_v_temp.^2));
        end  
    end


    % data(find(diff(sign(diff(data)))==-2)+1)
    local_min = find(diff(sign(diff(err_store)))==2)+1; 

    if isempty(local_min)
        P1=[];
        P2=[];
        P3=[];
    else      
        %local_theta = (local_min-1)*theta_step;

        theta_best = zeros(size(local_min,1),1);
        options = optimset('Display','off', 'MaxIter', 1000);

        local_best_after_non = zeros(size(local_min,1),1);
        for i = 1:size(local_min,1)
            %[theta_best(i),local_best_after_non(i)] =  lsqnonlin_cui(@myfun_3v4p,theta_step*(local_min(i)-1),[],[],options);
            [theta_temp,local_best_temp] =  lsqnonlin_cui(@myfun_3v4p,theta_step*(local_min(i)-1),[],[],options);
            %If find the nonlin is wrong, use the original 
            if isempty([theta_temp,local_best_temp])
                theta_best(i) = theta_step*(local_min(i)-1);
                local_best_after_non(i) = sum(myfun_3v4p(theta_best(i)).^2);
            else
                theta_best(i) = theta_temp;
                local_best_after_non(i) = local_best_temp;
                
            end
        end

        [err_best_temp, index] = min(local_best_after_non);

	%Get the best R2_best, t2_best, R3_best,t3_best
        err_v_temp = myfun_3v4p(theta_best(index));

        if err_best<2000000
            P1 = K1*[eye(3),zeros(3,1)];
            P2 = K2*[R2_best,t2_best];
            P3 = K3*[R3_best,t3_best];
        else
            P1=[];
            P2=[];
            P3=[];
        end
    end
end



P(:,:,1) = P1;
P(:,:,2) = P2;
P(:,:,3) = P3;

clear Global_x1  Global_x2 Global_x3 Global_w1 Global_w2_1 Global_H21 Global_f3 Global_B1_v  Global_B2_v R2_best t2_best R3_best t3_best err_best
end



