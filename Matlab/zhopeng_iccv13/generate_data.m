% Test the influence of the accuracy of theta.
clear all ;close all; clc;
format long;
digits(4) ;

BASEPATH = '/local_home/rakeshs/5point/narrow_fov/';

for no_matches = [1000]
    first_time = true;
    for Sigma = 0:0.2:1
        for no_iterations = [1:1000] % 1:500
            %% Generate the synthetic data

            %number of matches desired
            % no_matches = 500; 
            foc = 1.00;
            trials = 100; %the number of requested trials

            Baseline = 0.1;
            Depth = 0.5;
            Image_dimensions = [352, 288]; % [800, 600]; % 
            Noise_std = 1;
            FoV = 45; % 70; % 
            num_cameras = 3;
            rotation_multplier = 8; %The maximum degree of roataion 
            foc = cot(FoV/2*pi/180)*Image_dimensions(1);

            %%generate 3D data
            X = rand(3,no_matches);
            for i = 1:no_matches 
                X(3,i) =  1 + Depth * X(3,i); 
                % Make sure that the generated points are within the image of thefirst camera 
                X(1,i) = (X(1,i) -1/2)*Image_dimensions(1)/foc;
                X(2,i) = (X(2,i) -1/2)*Image_dimensions(2)/foc;
            end
            tm = ones(no_matches,1); 
            X_true = [X', tm ] ; 

            %generate translation, where C2 is in the middle of C1 and C3  
            C3 = rand(3,1);
            C3 = C3/norm(C3)*Baseline;

            %C2 = 1/2*C3; % C2 is located in the middle of points C1 and C3
            C2 = (rand(3,1)-0.5)*Baseline + 1/2*C3; % C2 is located in a sphere centered at the middel point of C1 and C2;

            C3_true = C3;
            C2_true = C2;

            %generate rotation  

            theta2 = 1/360 * 2 * pi * (2*rand -1)* rotation_multplier;
            n2 = 1/360 * 2 * pi * (2*rand -1) * rotation_multplier;
            p2 = 1/360 * 2 * pi * (2*rand -1) * rotation_multplier;

            R2(1,1) = (1 - cos(p2)) * cos(n2)* ( cos(n2) * cos(theta2) + sin(theta2) * sin(n2) ) + cos(p2)* cos(theta2);
            R2(1,2) = (1 - cos(p2))* cos(n2) * ( sin(n2) *cos(theta2) - sin(theta2) *cos(n2) ) - cos(p2) *sin(theta2);
            R2(1,3) = sin(n2) *sin(p2);
            R2(2,1) = (1 - cos(p2)) *sin(n2)  *( cos(n2) *cos(theta2) + sin(theta2)* sin(n2) ) + cos(p2)* sin(theta2);
            R2(2,2) = (1 - cos(p2)) *sin(n2) * ( sin(n2) *cos(theta2) - sin(theta2) *cos(n2) ) + cos(p2)* cos(theta2);
            R2(2,3) = -cos(n2) * sin(p2);
            R2(3,1) = -sin(p2) * ( sin(n2) * cos(theta2) - sin(theta2) * cos(n2));
            R2(3,2) = sin(p2) * ( cos(n2) * cos(theta2) + sin(theta2) * sin(n2));
            R2(3,3) = cos(p2);
            R2_true = R2;

            t2 = -R2*C2;
            t2_true = t2;



            theta3 = 1/360 * 2 * pi * (2*rand -1)* rotation_multplier;
            n3 = 1/360 * 2 * pi * (2*rand -1) * rotation_multplier;
            p3 = 1/360 * 2 * pi * (2*rand -1) * rotation_multplier;

            R3(1,1) = (1 - cos(p3)) * cos(n3)* ( cos(n3) * cos(theta3) + sin(theta3) * sin(n3) ) + cos(p3)* cos(theta3);
            R3(1,2) = (1 - cos(p3))* cos(n3) * ( sin(n3) *cos(theta3) - sin(theta3) *cos(n3) ) - cos(p3) *sin(theta3);
            R3(1,3) = sin(n3) *sin(p3);
            R3(2,1) = (1 - cos(p3)) *sin(n3)  *( cos(n3) *cos(theta3) + sin(theta3)* sin(n3) ) + cos(p3)* sin(theta3);
            R3(2,2) = (1 - cos(p3)) *sin(n3) * ( sin(n3) *cos(theta3) - sin(theta3) *cos(n3) ) + cos(p3)* cos(theta3);
            R3(2,3) = -cos(n3) * sin(p3);
            R3(3,1) = -sin(p3) * ( sin(n3) * cos(theta3) - sin(theta3) * cos(n3));
            R3(3,2) = sin(p3) * ( cos(n3) * cos(theta3) + sin(theta3) * sin(n3));
            R3(3,3) = cos(p3);
            R3_true = R3;

            t3 = -R3*C3;
            t3_true = t3;

            R1_true = eye(3);
            t1_true = zeros(3,1);
            C1_true = zeros(3,1);

            c2_err_thr = 1;
            c3_err_thr = 1;


            %generate Projection Matriices
            f1 = foc;
            f2 = foc;  
            f3 = foc;

            P1_true = zeros(3,4);
            K1_true = diag([f1,f1,1]);
            P1_true= K1_true*[R1_true,t1_true]  ;

            P2_true = zeros(3,4);
            K2_true = diag([f2,f2,1]);
            P2_true= K2_true*[R2_true,t2_true] ;

            E2_true = skew_sym(t2_true)*R2_true;
            E2_true = E2_true/norm(E2_true);


            P3_true = zeros(3,4);
            K3_true = diag([f3,f3,1]);
            P3_true= K3_true*[R3_true,t3_true] ;

            E3_true = skew_sym(t3_true)*R3_true;
            E3_true = E3_true/norm(E3_true);

            perfect_matches = zeros(6,no_matches);

            %Begin projection onto three images
            for i = 1:no_matches 
                   img1 =   P1_true*X_true(i,:)' ;
                   img2 =   P2_true*X_true(i,:)' ;
                   img3 =   P3_true*X_true(i,:)' ;
                   perfect_matches(1,i) = img1(1)/img1(3);
                   perfect_matches(2,i) = img1(2)/img1(3);
                   perfect_matches(3,i) = img2(1)/img2(3);
                   perfect_matches(4,i) = img2(2)/img2(3);   
                   perfect_matches(5,i) = img3(1)/img3(3);
                   perfect_matches(6,i) = img3(2)/img3(3);
            end

            % Add Gaussian noises
            Mu =0.0;
            % Sigma = 1;
            noise = normrnd(Mu,Sigma,size(perfect_matches));
            matches = perfect_matches + noise;

            x1 = matches(1:2,:);
            x2 = matches(3:4,:);
            x3 = matches(5:6,:);  

            x1 = [x1;ones(1,size(x1,2))];
            x2 = [x2;ones(1,size(x2,2))];
            x3 = [x3;ones(1,size(x3,2))];


            x_range = Image_dimensions(1)/2;
            y_range = Image_dimensions(2)/2; 

            fisible = ones(no_matches,3); %Whether the 3D point is visible in the image  
            match_pair = zeros(no_matches,3,3,2); %The matches between images, record the index of the point in differnt images
            match_num = zeros(3,3); % The number of matches of each pair of images
            matches3 = [];

            for i=1 : no_matches
                
                if abs(x2(1,i))> x_range || abs(x2(2,i))> y_range 
                    fisible(i,2) = 0;
                else
                    match_num(1,2) =  match_num(1,2)+1;
                    match_pair(match_num(1,2),1,2,1) =  i;
                    match_pair(match_num(1,2),1,2,2) =  match_num(1,2);
                end
                
                if abs(x3(1,i))> x_range || abs(x3(2,i))> y_range 
                    fisible(i,3) = 0;  
                else
                    match_num(1,3) = match_num(1,3) +1;
                    match_pair(match_num(1,3),1,3,1) =  i;
                    match_pair(match_num(1,3),1,3,2) =  match_num(1,3);
                end
                
                if fisible(i,2) && fisible(i,3)
                    match_num(2,3) = match_num(2,3) +1;
                    match_pair(match_num(2,3),2,3,1) =  match_num(1,2);
                    match_pair(match_num(2,3),2,3,2) =  match_num(1,3);
                    matches3 = cat(2,matches3,matches(:,i));
                else
                    % no match
                    % fprintf('no match = %d \n', i);
                    matches3 = cat(2, matches3, ones(6, 1) * -1000);
                end
                
            end


            filename = [BASEPATH, '/', num2str(no_matches)];
            mkdir(filename);

            fid_points=fopen([filename,'/points2D.txt'],'w+');
            for p1 = 1: no_matches
                fprintf(fid_points,'%d ', sum(fisible(p1,:)));
                fprintf(fid_points,'1 %d ', p1-1);
                if fisible(p1,2)
                    fprintf(fid_points,'2 %d ',match_pair(p1,1,2,2)-1);
                end
                if fisible(p1,3)
                    fprintf(fid_points,'3 %d ',match_pair(p1,1,3,2)-1);
                end
                fprintf(fid_points,'\n');
            end
            fclose(fid_points);

            % fprintf('\nWriting the file of match.txt...\n');
            fid_match=fopen([filename,'/matches.init.txt'],'w+');
            for c1=1:num_cameras
                for c2=c1+1:num_cameras
                    fprintf(fid_match,'%d %d\n%d\n',c1-1,c2-1,match_num(c1,c2));
                    for i = 1:match_num(c1,c2)
                        fprintf(fid_match,'%d %d\n',match_pair(i,c1,c2,1)-1,match_pair(i,c1,c2,2)-1);
                    end
                        
                end
            end
            fclose(fid_match);

            % fprintf('\nWriting the files of key...\n');
            for c1=1:num_cameras
                temp_c1 = c1-1;
                if temp_c1<10
                    fid_key=fopen([filename,'/0000',num2str(temp_c1),'.key'],'w+');
                else
                    if temp_c1<100
                        fid_key=fopen([filename,'/000',num2str(temp_c1),'.key'],'w+');
                    else
                        if temp_c1<1000
                            fid_key=fopen([filename,'/00',num2str(temp_c1),'.key'],'w+');
                        else
                            if temp_c1<10000
                                fid_key=fopen([filename,'/0',num2str(temp_c1),'.key'],'w+');
                            else
                                fid_key=fopen([filename,'/',num2str(temp_c1),'.key'],'w+');
                            end
                        end
                    end
                end

                temp_keys_num = sum(fisible(:,c1));
                fprintf(fid_key,'%d 128\n',temp_keys_num);
                for k1 =1 : no_matches
                    if fisible(k1,c1)
                        fprintf(fid_key,'%f %f 0 0\n',matches(2*c1,k1),matches(2*c1-1,k1)); % y, x
                        for zeros_lines= 1:6
                            fprintf(fid_key,'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n'); 
                        end
                        fprintf(fid_key,'0 0 0 0 0 0 0 0\n');
                    end
                end
                fclose(fid_key);
            end

            % fprintf('\nWriting the file of Pmatrix.txt...\n');

            if (first_time)
                file_permission = 'w';
            else
                file_permission = 'a';
            end

            fid_Pmatrix=fopen([filename,'_GroundTruthMotion.txt'], file_permission);

            fprintf(fid_Pmatrix,'%17.16e %17.16e %17.16e %17.16e\n', [R1_true,t1_true]',[R2_true,t2_true]',[R3_true,t3_true]');
            fclose(fid_Pmatrix);

            % fprintf('\nWriting the file of Points3D.txt...\n');
            fid_Points3D=fopen([filename,'/Points3D.txt'],'w+');
            for p1 = 1: no_matches
                fprintf(fid_Points3D,'%17.16e %17.16e %17.16e\n',X_true(:,1:3)');
            end
            fclose(fid_Points3D);

            if (first_time)
                dlmwrite([filename,'_FeatureTrack.txt'], matches3');
            else
                dlmwrite([filename,'_FeatureTrack.txt'], matches3', '-append');
            end

            first_time = false;
            fprintf('\nFinished iter: %d, Sigma: %f, no_matches: %d.\n', no_iterations, Sigma, no_matches);

            x1 = matches3(1:2,:);
            x2 = matches3(3:4,:);
            x3 = matches3(5:6,:);  

            x1 = [x1;ones(1,size(x1,2))];
            x2 = [x2;ones(1,size(x2,2))];
            x3 = [x3;ones(1,size(x3,2))];


            % threshold = 1*sqrt(2)/foc
            % Search_flag = 1;  % choose the basic search times: 1->40 0->1000;
        end
    end
end





