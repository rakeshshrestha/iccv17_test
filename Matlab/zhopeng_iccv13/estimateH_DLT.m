%------------------------------------------------------------------------
% The function of the DLT alogorithm for 2D homographies with or without normalization. 
% Usage: H = estimateH_DLT(matches,nor_flag)
% Inputs:
%        matches: n*4 matrix, the coordinates of match points in Image1 and Image2
%        nor_flag: "1 " with normalization; "0" without normalization
% Outputs:
%        H: 3*3 matrix, the estimated Homography matrix, which satisfies s[x2,y2,1]'=H*[x1,y1,1]'
% By Zhaopeng Cui, Xidian University, 2012.7.
%------------------------------------------------------------------------

function H = estimateH_DLT(matches,nor_flag)

if (size(matches, 2)~=4) | (size(matches,1)<4)
   disp('Error: parametres incorrect')
else
    % Normalization
    if nor_flag
        [matches, T1, T2] = norm_matches(matches);
    end
    
    matches_num = size(matches,1);
  
    x1 = matches(:,1);
    y1 = matches(:,2);
    x2 = matches(:,3);
    y2 = matches(:,4);
    
    A=zeros(2*matches_num,9);
    
    for i = 1 : matches_num
        A(2*i-1,4) = -x1(i);
        A(2*i-1,5) = -y1(i);
        A(2*i-1,6) = -1;
        A(2*i-1,7) = x1(i)*y2(i);
        A(2*i-1,8) = y1(i)*y2(i);
        A(2*i-1,9) = y2(i);
        A(2*i,1) = x1(i);
        A(2*i,2) = y1(i);
        A(2*i,3) = 1;
        A(2*i,7) = -x1(i)*x2(i);
        A(2*i,8) = -y1(i)*x2(i);
        A(2*i,9) = -x2(i);
    end

    [U, S, V] = svd(A);
    h = V(:,end);   
    if h(9) ~= 0
        h = h/h(9);
    end
    H = reshape(h, 3, 3);    
    H = H';

    % Denormalization
    if nor_flag
        H = inv(T2)*H*T1;
    end
    
    if H(3,3)~=0
    H = H./H(3,3);
    end
    
end
   
end

