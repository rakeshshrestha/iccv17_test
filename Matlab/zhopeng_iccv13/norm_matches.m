%------------------------------------------------------------------------
% Normalize the points within [-1, 1] 
% Usage: [matches_output, T1, T2] = norm_matches(matches)
% Inputs:
%       matches: n*4 matrix, the coordinates of match points in Image1 and Image2
% Outputs:
%       matches_output: n*4 matrix, the nomalized coordinates of match points in Image1 and Image2.
%       T1: the transformation matrix which satifies x1'=T1*x1;
%       T2: the transformation matrix which satifies x2'=T2*x2;
% By Zhaopeng Cui, Xidian University, 2012.7.
%------------------------------------------------------------------------

function [matches_output, T1, T2] = norm_matches(matches)

if (size(matches, 2)~=4)
   disp('Error: parametres incorrect')
else
    maxx1=max(matches(:,1));
    maxy1=max(matches(:,2));
    maxx2=max(matches(:,3));
    maxy2=max(matches(:,4));
    minx1=min(matches(:,1));
    miny1=min(matches(:,2));
    minx2=min(matches(:,3));
    miny2=min(matches(:,4));

    matches_output(:,1)=(2/(maxx1-minx1)).*(matches(:,1)-minx1)-1;
    matches_output(:,2)=(2/(maxy1-miny1)).*(matches(:,2)-miny1)-1;
    matches_output(:,3)=(2/(maxx2-minx2)).*(matches(:,3)-minx2)-1;
    matches_output(:,4)=(2/(maxy2-miny2)).*(matches(:,4)-miny2)-1;

    T1=[2/(maxx1-minx1) 0 -(2/(maxx1-minx1)*minx1+1); 0 2/(maxy1-miny1) -(2/(maxy1-miny1)*miny1+1); 0 0 1];
    T2=[2/(maxx2-minx2) 0 -(2/(maxx2-minx2)*minx2+1); 0 2/(maxy2-miny2) -(2/(maxy2-miny2)*miny2+1); 0 0 1];
end

end