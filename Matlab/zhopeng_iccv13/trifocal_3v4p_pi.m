%------------------------------------------------------------------------
% Compute the three view relative pose using 3v4p method. 
% Usage: [P_normal,err,max_num] = trifocal_3v4p_pi(x1_input, x2_input,
% x3_input,K1_input, K2_input, K3_input, trials, threshold,Search_flag)
% 
% Input: 
%       x1_input, x2_input, x3_input -- 3*N matrix, the original homogenous coordinates of the
%                                       matching points in three view;
%       K1_input, K2_input, K3_input -- 3*3 matrix, the true intrinsic matrix of the cameras;
%       trials -- the number of requested trials  eg. trials = 800;
%       threshold -- the threshold for the liners eg. threshold = 0.001;
%       Search_flag -- The basic search flag of 3v4p: 1->40 0->1000;
%
% Output:
%       P_normal -- 3*4*3 Matrix where P1 = [eye(3),zeros(3,1)];
%       err -- The reprojection err of all the points
%       max_num -- The number of inliers
%
% By Zhaopeng Cui, 2012.10
%------------------------------------------------------------------------

function [P_normal,err,max_num] = trifocal_3v4p_pi(x1_input, x2_input, x3_input,K1_input, K2_input, K3_input, trials, threshold,Search_flag)

[row, col]=size(x1_input);

x1 = inv(K1_input)*x1_input;
x2 = inv(K2_input)*x2_input;
x3 = inv(K3_input)*x3_input;
K1 = eye(3);
K2 = eye(3);
K3 = eye(3);

P = zeros(3,4,3,trials);

choice = zeros(trials,4);

parfor i=1:trials
%    i
    a = randperm(col);
    choice(i,:) = a(1:4);
    P_temp = Basic_3v4p_pi_nonli(x1(:,choice(i,:)), x2(:,choice(i,:)), x3(:,choice(i,:)),K1,K2,K3,Search_flag);
    if ~isempty(P_temp)
       % P = cat(4,P,P_temp);
       P(:,:,:,i) = P_temp;
    else
       P(:,:,:,i) = zeros(3,4,3);
    end 
end



%inlier=[];
%store=[];

nums=zeros(size(P,4),1);
max_index = 0;
max_num = 0;

for i=1:size(P,4)  
    %tmp=[];
    runner=0;
 
    if P(1,1,1,i) ~=0
        for j=1:col
            if threshold>tri6_err(P(:,:,:,i),x1(:,j), x2(:,j),x3(:,j));
                runner=runner+1;
               % tmp(1,runner)=j;
            end;
        end;
    end

%    store(i, 1:runner) = tmp;
    nums(i) = runner;

%     if runner> max_num
% %       inlier=tmp;
%         max_index = i;
%         max_num = runner;
%     end;
end

max_num = max(nums);

max_index = find(nums==max_num);

if max_num>0
    err_sum = zeros(size(max_index,1),1);
    for i = 1:size(max_index,1)
        P_temp=P(:,:,:,max_index(i));

        err =zeros(col,1);
        for j=1:size(x1,2)
            err(j) = tri6_err(P_temp,x1(:,j), x2(:,j),x3(:,j));
        end
        
        err_sum(i) = sum(err);
    end
    
    [min_err,min_index] = min(err_sum);
    
    P_normal = P(:,:,:,max_index(min_index));
    
    
else
    %Of course, this kind of situation could not happen....
    P_normal = [];
    err =[];
end

end

