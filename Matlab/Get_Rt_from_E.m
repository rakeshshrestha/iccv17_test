function [R2, t2] = Get_Rt_from_E(E12,x1,x2)  

[u, s, v]=svd(E12);

c2_0=v(:, 3);
W=[0 -1 0
   1 0 0
   0 0 1];

R2_10=u*W*v';
if det(R2_10)<0
    R2_10=-R2_10;
end;

R2_20=u*W'*v';

if det(R2_20)<0
    R2_20=-R2_20;
end;

[R2, c2]=choose_Rt(R2_10, R2_20, c2_0, x1(:,1:3), x2(:,1:3));

t2 = -R2*c2;
t2 = t2/norm(t2);

end