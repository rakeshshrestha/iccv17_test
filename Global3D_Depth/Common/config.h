#pragma once
//#define COLOR
//#define TRIPLETPT
//#define ADDMISSINGTRIPLET

#define Pi 3.14159265358979323846
#define MATCHING_SCORE_RATIO 0.7

//#define MINIMUM_RAW_CORRESPONDENCES 30
//#define MININUM_INLIER_CORRESPONDENCES 30

#define MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs 10
#define MININUM_INLIER_CORRESPONDENCES_PairwiseEGs 10

#define EG_RANSAC_ROUND 1024//8192
//#define MIN_TRIANGULATE_ANGLE 1
#define MIN_TRIANGULATE_ANGLE_CT 2
#define E_THRESHOLD 2.0//2.25
//#define OUTLIER_THRESHOLD 16.0

#define  motion_t 0.5

//2015.3.30
#define nColorChannel 1

//#define PBA_device 1 //default for PBA_CUDA_DEVICE_DEFAULT;1 for PBA_CPU_FLOAT; 2 for PBA_CPU_DOUBLE

//#define BundleMethod 1 //1 for PBA 0 for SBA

//#define VisualizeEnergy
#define nSelectPt 4
static float SBA_T = 16.0;
const double p_k = 0.05;
const int margin = 40;
const double view_change1 = 0.7; //0.7
const double view_change2 = 0.5; //0.5

const double min_match1 = 0.6;  //0.65
const double min_match2 = 0.5;  //0.55

const double view_angle = -0.5;

#define EigOffset 0.0000000001

