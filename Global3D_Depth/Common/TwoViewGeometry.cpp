#include "TwoViewGeometry.h"
#include "triangulate.h"
#include "Utilities.h"
#include "config.h"

Vector3r Triangulate(Vector2r p, Vector2r q, 
					 CameraParam c1, CameraParam c2,
					 REAL &proj_error, bool &in_front, REAL &angle)
{
	/*Compute the angle between the rays */
	Vector3r proj1, proj2;
	proj1.v[0] = p.v[0]; proj1.v[1] = p.v[1]; proj1.v[2] = REAL(1.0);
	proj2.v[0] = q.v[0]; proj2.v[1] = q.v[1]; proj2.v[2] = REAL(1.0);

	Vector3r ray1, ray2;
	MatrixMatrixProduct(1, 3, 3, proj1.v, c1.R, ray1.v);
	MatrixMatrixProduct(1, 3, 3, proj2.v, c2.R, ray2.v);

	angle = VectorAngle(ray1.v, ray2.v, 3);
	angle = REAL(angle/Pi * 180);

	/*Triangulate the point */
	v2_t p_norm = v2_new(p.v[0], p.v[1]);
	v2_t q_norm = v2_new(q.v[0], q.v[1]);

	double R1[9], R2[9], t1[3], t2[3];
	for (int i = 0; i < 9; i++)
	{
		R1[i] = double(c1.R[i]);
		R2[i] = double(c2.R[i]);
	}

	REAL t[3];
	MatrixMatrixProduct(1, 3, 3, c1.cen, c1.R, t);
	MatrixScalarProduct(3, 1, t, REAL(-1), t);
	for (int i = 0; i < 3; i++) t1[i] = double(t[i]);

	MatrixMatrixProduct(1, 3, 3, c2.cen, c2.R, t);
	MatrixScalarProduct(3, 1, t, REAL(-1), t);
	for (int i = 0; i < 3; i++) t2[i] = double(t[i]);

	double proj_errord;
	v3_t pt = triangulate(p_norm, q_norm, R1, t1, R2, t2, &proj_errord);
	proj_error = REAL(proj_errord);

	Vector3r vpt;
	for (int i = 0; i < 3; i++)
		vpt.v[i] = REAL(pt.p[i]);

	/*Check cheirality */
	bool cc1 = CheckCheirality(c1, vpt);
	bool cc2 = CheckCheirality(c2, vpt);

	in_front = (cc1 && cc2);
	//////////////////////////////////////////////////////////*/

	return vpt ;
}

double sampsonError(double *p, double *q, double *F)
{
	double Ft[9];
	MatrixTranspose(F, Ft, 3);

	double Fp[3], Ftq[3];

	MatrixMatrixProduct(3, 3, 1, F, p, Fp);
	MatrixMatrixProduct(3, 3, 1, Ft, q, Ftq);

	double tmp = q[0]*Fp[0] + q[1]*Fp[1] + Fp[2];
	double num = tmp * tmp;
	double denom1 = Fp[0]*Fp[0] + Fp[1]*Fp[1];
	double denom2 = Ftq[0]*Ftq[0] + Ftq[1]*Ftq[1];
	return num/(denom1 + denom2);
}