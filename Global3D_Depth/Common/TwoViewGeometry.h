#pragma once

#include "MyMatrix.h"
#include "DataTypes.h"

Vector3r Triangulate(Vector2r p, Vector2r q, 
					 CameraParam c1, CameraParam c2,
							    REAL &proj_error, bool &in_front, REAL &angle);

//bool CheckCheirality(CameraParam c, Vector3r pt);

double sampsonError(double *p, double *q, double *F);

