#pragma once
#include "DataTypes.h"
#include "MyMatrix.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "keys.h"

#include "..\lib\sba-1.5\sba.h"

using namespace std;

struct sba_camera_t
{
	double R[9];
	double cen[3];
	double k[5]; //f, k1, k2, u, v
};

struct sba_point_t
{
	double p[3];
	//int color[3];

	sba_point_t(){};
	sba_point_t(double x, double y, double z)
	{
		p[0] = x; p[1] = y; p[2] = z;
		//color[0] = color[1] = color[2];
	}
	inline sba_point_t & operator= (const sba_point_t & r) { p[0] = r.p[0]; p[1] = r.p[1]; p[2] = r.p[2]; return *this; }
};

struct sba_param
{
	int num_cameras;
	int num_points;
	int num_params_per_camera;
	int estimate_distortion;
	int estimate_focal;
	double f_scale;
	double k_scale;

	sba_camera_t *init_params;
	sba_point_t *points;
};

static sba_param global_params;
static double *global_last_ws = NULL;
static double *global_last_Rs = NULL;
static double *global_points = NULL;
static double *global_projs = NULL;
static int global_round = 0;

void rot_update(double *R, double *w, double *Rnew);
void sba_project_point3(int cam_id, int pt_id, double *cam_param, double *point_param, 
						double *proj_ij, void *adata);
/*double BundleAdjustment(CameraParam *cameras, int num_cameras,
					  PointParam* points, PtTrack** projections, int num_pts, vector<map<int, KeyPt>*> &m_keypts,
					  vector<string> key_list,
					  vector<int> &outlier,  double max_proj_error = 16.0, bool estimate_focal = false); */
void camera_refine_residual(const int *m, const int *n, double *x, double *fvec, int *iflag);
void TwoViewBundleAdjustment(double *R, double *t, 
							 double *intrinsic, sba_point_t *points, 
							 double *projections, vector<int> const &pt_idx,
							  vector<int> &ba_inlier, double max_proj_error = 16.0);

void RunSBA(int num_pts, int num_cameras, int ncons,
			char *vmask,
			double *projections,
			int estimate_focal,
			int undistort,
			sba_camera_t *init_camera_params,
			sba_point_t *init_pts, 
			int use_constraints, 
			double eps2 = 1.0e-12,
			double *Vout = NULL, 
			double *Sout = NULL,
			double *Uout=NULL, double *Wout=NULL
			/* size num_cameras ** 2 * cnp * cnp */);

inline void sba_project_point3
(int cam_id, int pt_id, 
 double *cam_param, double *point_param, 
 double *proj_ij, void *adata)
{
	sba_param *globs = (sba_param *)(adata);

	double K[9] = 
	{1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 1.0 };

	double *w, *dt, *k;

	/* Compute translation, rotation update */
	dt = cam_param + 0;
	w = cam_param + 3;

	if (globs->estimate_focal)
	{
		/* Compute intrinsics */
		k = cam_param + 7;
		K[0] = K[4] = cam_param[6]/globs->f_scale; 
	}
	else
	{
		k = cam_param + 6;
		K[0] = K[4] = double(globs->init_params[cam_id].k[0]);
	}

	if (w[0] != global_last_ws[3 * cam_id + 0] ||
		w[1] != global_last_ws[3 * cam_id + 1] ||
		w[2] != global_last_ws[3 * cam_id + 2]) 
	{
		rot_update(globs->init_params[cam_id].R, w, global_last_Rs + 9 * cam_id);
		global_last_ws[3 * cam_id + 0] = w[0];
		global_last_ws[3 * cam_id + 1] = w[1];
		global_last_ws[3 * cam_id + 2] = w[2];
	}

	double b_cam[3];

	/* Project! */
	double b2[3];
	b2[0] = point_param[0] - dt[0];
	b2[1] = point_param[1] - dt[1];
	b2[2] = point_param[2] - dt[2];
	MatrixMatrixProduct(3, 3, 1, &global_last_Rs[9 * cam_id], b2, b_cam);

	proj_ij[0] = b_cam[0] * K[0] / b_cam[2];
	proj_ij[1] = b_cam[1] * K[0] / b_cam[2];

	/* Apply radial distortion */
	if (globs->estimate_distortion) 
	{
		double k1 = k[0]/globs->k_scale;
		double k2 = k[1]/globs->k_scale;

		double rsq = (proj_ij[0] * proj_ij[0] + proj_ij[1] * proj_ij[1]) / (K[0] * K[0]);
		double factor = 1.0 + k1 * rsq + k2 * rsq * rsq;

		proj_ij[0] *= factor;
		proj_ij[1] *= factor;
	}
}


/* Compute an updated rotation matrix given the initial rotation (R)
* and the correction (w) */
inline void rot_update(double *R, double *w, double *Rnew) 
{
	double theta, sinth, costh, n[3];
	double nx[9], nxsq[9];
	double term2[9], term3[9];
	double dR[9];

	double ident[9] = 
	{ 1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 1.0 };

	theta = sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]);

	if (theta == 0.0) {
		memcpy(Rnew, R, sizeof(double) * 9);
		return;
	}

	n[0] = w[0] / theta;
	n[1] = w[1] / theta;
	n[2] = w[2] / theta;

	nx[0] = 0.0;   nx[1] = -n[2];  nx[2] = n[1];
	nx[3] = n[2];  nx[4] = 0.0;    nx[5] = -n[0];
	nx[6] = -n[1]; nx[7] = n[0];   nx[8] = 0.0;

	MatrixMatrixProduct(3, 3, 3, nx, nx, nxsq);	

	sinth = sin(theta);
	costh = cos(theta);

	MatrixScalarProduct(3, 3, nx, sinth, term2);
	MatrixScalarProduct(3, 3, nxsq, 1.0-costh, term3);

	for (int i = 0; i < 9; i++)
		dR[i] = ident[i] + term2[i] + term3[i];
	MatrixMatrixProduct(3, 3, 3, dR, R, Rnew);
}

inline void camera_refine_residual(const int *m, const int *n, double *x, double *fvec, int *iflag) 
{
	int i;
	double error = 0.0, error2 = 0.0;

	for (i = 0; i < global_params.num_points; i++) 
	{
		double pt[3] = {global_points[i * 3 + 0], global_points[i * 3 +1], global_points[i * 3 +2]};
		double proj[2], dx, dy;

		sba_project_point3(0, i, x, pt, proj, (void*)(&global_params));
		//project_point(0, i, x, pt, proj, (void*)(&global_params));

		dx = global_projs[i * 2 + 0] - proj[0];
		dy = global_projs[i * 2 + 1] - proj[1];

		fvec[2 * i + 0] = dx;
		fvec[2 * i + 1] = dy;

		if (*iflag == 0)
		{
			error += dx * dx + dy * dy;
			error2 += sqrt(dx * dx + dy * dy);
		}
	}

	if (global_params.estimate_focal)
	{
		double focal_diff = global_params.init_params->k[0] - x[6];
		fvec[2 * global_params.num_points] = global_params.num_points * 0.0001 * focal_diff;

		if (global_params.estimate_distortion)
		{
			fvec[2 * global_params.num_points + 1] = -0.05 * global_params.num_points * x[7];
			fvec[2 * global_params.num_points + 2] = -0.05 * global_params.num_points * x[8];
		}
	}
	else
	{
		if (global_params.estimate_distortion)
		{
			fvec[2 * global_params.num_points + 0] = -0.05 * global_params.num_points * x[6];
			fvec[2 * global_params.num_points + 1] = -0.05 * global_params.num_points * x[7];
		}
	}

	if (*iflag == 0) 
	{
		if (global_params.estimate_distortion)
		{
			printf("  Round[%d]: RMS error = %0.8f [%0.8f], f = %0.3f; %0.3e %0.3e\n", 
				global_round, sqrt(error / global_params.num_points), 
				error2 / global_params.num_points, x[6], x[7], x[8]);
		} 
		else
		{
			if (global_params.estimate_focal)
			{
				printf("  Round[%d]: RMS error = %0.8f [%0.8f], f = %0.3f\n", 
					global_round, sqrt(error / global_params.num_points),
					error2 / global_params.num_points, x[6]);
			} 
			else
			{
				printf("  Round[%d]: RMS error = %0.8f [%0.8f]\n", 
					global_round, sqrt(error / global_params.num_points),
					error2 / global_params.num_points);        
			}
		}
		global_round++;
	}  
	fflush(stdout);
}


inline void TwoViewBundleAdjustment
(double *R, double *t, double *intrinsics,
 sba_point_t*points, double *projections, vector<int> const &pt_idx, 
  vector<int> &ba_inlier, double max_proj_error)
{
	//intrinsic: f1, f2
	//refine pair-wise geometry
	int num_pts = int(pt_idx.size());
	int cnp = 6;
	int undistort = 0;
	int use_constraints = 0;
	double eps2 = 1.0e-8;
	char *vmask = new char [2 * num_pts];
	memset(vmask, 1, sizeof(char) * 2 * num_pts);

	sba_camera_t cameras[2];
	double Rt[9], cen[3];
	MatrixTranspose(R, Rt, 3);
	MatrixMatrixProduct(3, 3, 1, Rt, t, cen);
	MatrixScalarProduct(3, 1, cen, -1, cen);

	for (int i = 0; i < 3; i++)
	{
		cameras[0].cen[i] = 0;
		cameras[1].cen[i] = cen[i]; 
	}

	for (int i = 0; i < 9; i++)
	{
		cameras[0].R[i] = 0;
		cameras[1].R[i] = R[i];
	}
	cameras[0].R[0] = cameras[0].R[4] = cameras[0].R[8] = 1;

	cameras[0].k[0] = intrinsics[0]; cameras[0].k[1] = cameras[0].k[2] = 0;
	cameras[1].k[0] = intrinsics[1]; cameras[1].k[1] = cameras[1].k[2] = 0;

	RunSBA(num_pts, 2, 1,
		vmask, projections, 0, undistort, cameras, points, 
		use_constraints, eps2);

	for (int i = 0; i < 3; i++)
		cen[i] =cameras[1].cen[i];

	for (int i = 0; i < 9; i++)
		R[i] = cameras[1].R[i];

	MatrixMatrixProduct(3, 3, 1, R, cen, t);
	MatrixScalarProduct(3, 1, t, -1, t);
	delete []vmask;

	/*/////////get outlier///////////////////////////
	ba_inlier.clear();
	double error = 0.0;
	double b[3], b2[3];
	double pr[2];
	double dx, dy, dist;
	for (int j = 0; j < num_pts; j++) 
	{
		error = 0;
		for (int i = 0; i < 2; i++) 
		{
			b[0] = points[j].p[0] - cameras[i].cen[0];
			b[1] = points[j].p[1] - cameras[i].cen[1];
			b[2] = points[j].p[2] - cameras[i].cen[2];

			MatrixMatrixProduct(3, 3, 1, cameras[i].R, b, b2);

			pr[0] = b2[0]/b2[2] *  cameras[i].k[0];
			pr[1] = b2[1]/b2[2] * cameras[i].k[0];

			dx = pr[0] - projections[(j * 2 + i) * 2];
			dy = pr[1] - projections[(j * 2 + i) * 2 + 1];

			dist = dx * dx + dy * dy;
			error = max(dist, error);
		}

		if (error < max_proj_error)
			ba_inlier.push_back(pt_idx[j]);
	} 
	/////////////////////////////////////////////////////*/
}

/*inline double BundleAdjustment
(CameraParam *cameras, int num_cameras,
PointParam* points, PtTrack** pt_tracks, int num_pts, vector<map<int, KeyPt>*> &m_keypts,
vector<string> key_list,
 vector<int> &outlier, double max_proj_error, bool estimate_focal)
{
	sba_camera_t *m_sba_cameras = new sba_camera_t[num_cameras];
	sba_point_t *m_sba_points = new sba_point_t[num_pts];

	//initialize cameras and points
	for (int i = 0; i < num_cameras; i++)
	{
		for (int j= 0; j < 9; j++) m_sba_cameras[i].R[j] = double(cameras[i].R[j]);
		for (int j = 0 ; j < 3; j++) m_sba_cameras[i].cen[j] = double(cameras[i].cen[j]);
		for (int j = 0; j < 5; j++) m_sba_cameras[i].k[j] = double(cameras[i].k[j]);
	}

	for (int i = 0; i < num_pts; i++)
	{
		for (int j = 0;  j < 3; j++)
			m_sba_points[i].p[j] = double(points[i].p[j]);
	}
	/////////////////////////

	int num_projections = 0;
	for (int i = 0; i < num_pts; i++)
		num_projections += pt_tracks[i]->size();

	char *vmask = new char [num_cameras * num_pts];
	double *projections = new double [2 * num_projections];
	memset(vmask, 0, sizeof(char)*num_cameras * num_pts);

	KeyPt keypt;
	int proj_id = 0;

#ifndef LOADALLKEY
	int max_load_keys = 100;
	vector<vector<Keypoint>> m_keys;
	vector<int> key_id;
	m_keys.resize(num_cameras);
#endif

	for (int i = 0; i < num_pts; i++)
	{
		//int j = 0;
		for (int j = 0; j < num_cameras; j++)
		{
			map<int, int>::iterator miter = pt_tracks[i]->find(j);
			if (miter == pt_tracks[i]->end()) continue;

#ifdef LOADALLKEY
			map<int, KeyPt> &m_keymap = *(m_keypts[miter->first]);
			map<int, KeyPt>::iterator keyiter = m_keymap.find(miter->second);

			vmask[i * num_cameras + j] = 1;
			projections[2 * proj_id] = double((keyiter->second).x * cameras[j].k[0]);
			projections[2 * proj_id + 1] = double((keyiter->second).y * cameras[j].k[0]);
#else
			if (m_keys[j].empty())
			{
				m_keys[j] = ReadKeyFile(key_list[j].c_str());
				key_id.push_back(j);
			}

			if (key_id.size() > max_load_keys)
			{
				m_keys[key_id.front()].clear();
				key_id.erase(key_id.begin());
			}

			vmask[i * num_cameras + j] = 1;
			projections[2 * proj_id] = (m_keys[j])[miter->second].m_x - cameras[j].k[3];
			projections[2 * proj_id + 1] = (m_keys[j])[miter->second].m_y - cameras[j].k[4];
#endif
			proj_id++;
		}
	}

	for (int i = int(key_id.size()) - 1; i >= 0; --i)
	{
		m_keys[key_id[i]].clear();
		key_id.pop_back();
	}
	m_keys.clear();

	double eps2 = 1.0e-8;
	RunSBA(num_pts, num_cameras, 0, vmask, projections, estimate_focal, 0, m_sba_cameras,
		m_sba_points, 0, eps2);
	/////////get outlier///////////////////////////
	outlier.clear();
	double b[3], b2[3];
	double pr[2];
	double dx, dy, dist;
	double avg_error = 0;
	int total_proj = 0;
	proj_id = 0;
	for (int j = 0; j < num_pts; j++) 
	{
		vector<int> remove_proj;
		//for (map<int, int>::iterator miter = pt_tracks[j]->begin();
		//	miter != pt_tracks[j]->end(); miter++) 
		//{
		for (int i = 0; i < num_cameras; i++)
		{
			if (vmask[j * num_cameras + i])
			{
				b[0] = m_sba_points[j].p[0] - m_sba_cameras[i].cen[0];
				b[1] = m_sba_points[j].p[1] - m_sba_cameras[i].cen[1];
				b[2] = m_sba_points[j].p[2] - m_sba_cameras[i].cen[2];

				MatrixMatrixProduct(3, 3, 1, m_sba_cameras[i].R, b, b2);

				pr[0] = b2[0]/b2[2];
				pr[1] = b2[1]/b2[2];

				dx = pr[0]*m_sba_cameras[i].k[0] - projections[2 * proj_id];
				dy = pr[1]*m_sba_cameras[i].k[0] - projections[2 * proj_id+1];

				dist = sqrt(dx * dx + dy * dy);

				if (dist > max_proj_error)
					remove_proj.push_back(i);
				else
				{
					avg_error += dist;
					total_proj++;
				}
				proj_id++;
			}
		}

		for (size_t i = 0; i < remove_proj.size(); i++)
		{
			map<int, int>::iterator miter = pt_tracks[j]->find(remove_proj[i]);
			pt_tracks[j]->erase(miter);
		}

		if (pt_tracks[j]->size() < 2)
			outlier.push_back(j);
	}
	avg_error /= total_proj;

	printf("Average reprojection error is %f\n", avg_error);
	/////////////////////////////////////////////////////

	//fill output camera and point paremeters
	for (int i = 0; i < num_cameras; i++)
	{
		for (int j= 0; j < 9; j++) cameras[i].R[j] = REAL(m_sba_cameras[i].R[j]);
		for (int j = 0 ; j < 3; j++) cameras[i].cen[j] = REAL(m_sba_cameras[i].cen[j]);
		for (int j = 0; j < 5; j++) cameras[i].k[j] = REAL(m_sba_cameras[i].k[j]);
	}

	for (int i = 0; i < num_pts; i++)
	{
		for (int j = 0;  j < 3; j++)
			points[i].p[j] = REAL(m_sba_points[i].p[j]);
	}
	delete []projections;
	delete []vmask;
	delete []m_sba_cameras;
	delete []m_sba_points;
	return avg_error;
} */

inline void RunSBA(int num_pts, int num_cameras, int ncons,
			char *vmask,
			double *projections,
			int estimate_focal,
			int undistort,
			sba_camera_t *init_camera_params,
			sba_point_t *init_pts, 
			int use_constraints, 
			double eps2,
			double *Vout, 
			double *Sout,
			double *Uout, double *Wout
			/* size num_cameras ** 2 * cnp * cnp */)
{
	int cnp;
	double *params;
	double opts[6]; 
	double info[10];

	int i, j, base;
	int num_camera_params, num_pt_params, num_params;

	if (estimate_focal)
		cnp = 7;
	else
		cnp = 6;

	if (undistort) cnp += 2;

	num_camera_params = cnp * num_cameras;
	num_pt_params = 3 * num_pts;
	num_params = num_camera_params + num_pt_params;

	params = new double[num_params];

	/* Fill parameters */
	for (j = 0; j < num_cameras; j++)
	{
		int c = 0;

		/* Translation is zero */
		params[cnp * j + 0] = init_camera_params[j].cen[0]; // 0.0;
		params[cnp * j + 1] = init_camera_params[j].cen[1]; // 0.0;
		params[cnp * j + 2] = init_camera_params[j].cen[2]; // 0.0;

		/* Rotation is zero */
		params[cnp * j + 3] = 0.0;
		params[cnp * j + 4] = 0.0;
		params[cnp * j + 5] = 0.0;

		/* Focal length is initial estimate */
		params[cnp * j + 6] = init_camera_params[j].k[0];
		
		if (estimate_focal)
			c = 7;
		else
			c = 6;

		if (undistort) 
		{
			params[cnp * j + c] = init_camera_params[j].k[1];
			params[cnp * j + c+1] = init_camera_params[j].k[2];
		}
	}

	base = num_camera_params;
	for (i = 0; i < num_pts; i++) 
	{
		params[base + 3 * i + 0] = init_pts[i].p[0];
		params[base + 3 * i + 1] = init_pts[i].p[1];
		params[base + 3 * i + 2] = init_pts[i].p[2];
	}

	opts[0] = 1.0e-3;
	opts[1] = 1.0e-10; // 1.0e-15;
	opts[2] = eps2; // 0.0;  // 1.0e-10; // 1.0e-15;
	opts[3] = 1.0e-12;
	opts[4] = 0.0;
	opts[5] = 4.0e-2; // change this back to opts[4] for sba v1.2.1

	/* Create the constraints */
	bool use_point_constraints = false;
	camera_constraints_t *constraints = NULL;
	point_constraints_t *point_constraints = NULL;

	// 	if (use_constraints) 
	// 	{
	// 		constraints = new camera_constraints_t[m_nimgs];
	// 
	// 		for (i = 0; i < m_nimgs; i++) 
	// 		{
	// 			constraints[i].constrained = new char[cnp];
	// 			constraints[i].constraints = new double [cnp];
	// 			constraints[i].weights = new double [cnp]; 
	// 
	// // 			memcpy(constraints[i].constrained, 
	// // 				init_camera_params[i].constrained, cnp * sizeof(char));
	// // 			memcpy(constraints[i].constraints, 
	// // 				init_camera_params[i].constraints, cnp * sizeof(double));
	// // 			memcpy(constraints[i].weights,
	// // 				init_camera_params[i].weights, cnp * sizeof(double));
	// 		}
	// 	}

	/* Fill global param struct */
	global_params.num_cameras = num_cameras;
	global_params.num_points = num_pts;
	global_params.num_params_per_camera = cnp;
	global_params.estimate_distortion = undistort;
	global_params.init_params = init_camera_params;
	global_params.estimate_focal = estimate_focal;

	global_last_ws = new double[3 * num_cameras];
	global_last_Rs = new double[9 * num_cameras];
	global_params.points = &init_pts[0];

	for (i = 0; i < num_cameras; i++) 
	{
		global_last_ws[3 * i + 0] = 0.0;
		global_last_ws[3 * i + 1] = 0.0;
		global_last_ws[3 * i + 2] = 0.0;

		memcpy(global_last_Rs + 9 * i, init_camera_params[i].R, 9 * sizeof(double));
	}

	/* Run sparse bundle adjustment */
#define MAX_ITERS 150 // 256
#define VERBOSITY 3

	sba_motstr_levmar(num_pts, num_cameras, ncons, 
		vmask, params, cnp, 3, projections, NULL, 2, 
		sba_project_point3, NULL, 
		(void *) (&global_params),
		MAX_ITERS, VERBOSITY, opts, info,
		use_constraints, constraints,
		use_point_constraints,
		point_constraints, Vout, Sout, Uout, Wout);

	printf("[run_sfm] Number of iterations: %d\n", (int) info[5]);
	printf("info[6] = %0.3f\n", info[6]);

	/* Copy out the params */
	for (j = 0; j < num_cameras; j++) 
	{
		double *dt = params + cnp * j + 0;
		double *w = params + cnp * j + 3;
		double Rnew[9];
		int c;

		/* Translation */
		init_camera_params[j].cen[0] = dt[0];
		init_camera_params[j].cen[1] = dt[1];
		init_camera_params[j].cen[2] = dt[2];

		/* Rotation */
		rot_update(init_camera_params[j].R, w, Rnew);
		memcpy(init_camera_params[j].R, Rnew, 9 * sizeof(double));

		/* Focal length */
		if (estimate_focal)
		{
			init_camera_params[j].k[0] = params[cnp * j + 6];
			c = 7;
		}
		else
			c = 6;

		if (undistort)
		{
			init_camera_params[j].k[1] = params[cnp * j + c];
			init_camera_params[j].k[2] = params[cnp * j + c+1];
		}
	}

	base = num_camera_params;
	for (i = 0; i < num_pts; i++) 
	{
		init_pts[i].p[0] = params[base + 3 * i + 0];
		init_pts[i].p[1] = params[base + 3 * i + 1];
		init_pts[i].p[2] = params[base + 3 * i + 2];
	}

//#define DEBUG_SFM
#ifdef DEBUG_SFM
	for (i = 0; i < num_cameras; i++) 
	{
		int num_projs = 0;
		double error = 0.0;
		double error_max = 0.0;
		int idx_max = 0;
		double px_max = 0.0, py_max = 0.0;

		//CameraMatrix cam_mat;
		double b[3], b2[3];
		for (j = 0; j < num_pts; j++) 
		{
			double pr[2];
			double dx, dy, dist;

			if (!vmask[j * num_cameras + i])
				continue;

			b[0] = init_pts[j].p[0] - init_camera_params[i].cen[0];
			b[1] = init_pts[j].p[1] - init_camera_params[i].cen[1];
			b[2] = init_pts[j].p[2] - init_camera_params[i].cen[2];

			MatrixMatrixProduct(3, 3, 1, init_camera_params[i].R, b, b2);

			pr[0] = b2[0]/b2[2] *  init_camera_params[i].k[0];
			pr[1] = b2[1]/b2[2] * init_camera_params[i].k[0];

			dx = pr[0] - projections[(j * num_cameras + i) * 2];
			dy = pr[1] - projections[(j * num_cameras + i) * 2 + 1];
			dist = dx * dx + dy * dy;
			error += dist;

			if (dist > error_max)
			{
				idx_max = j;
				error_max = dist;
				px_max = projections[(j * num_cameras + i) * 2];
				py_max = projections[(j * num_cameras + i) * 2 + 1];
			}

			num_projs++;
		}

		printf("Camera %d:  error = %0.3f (%0.3f)\n", i,
			error, sqrt(error / num_projs));
		printf("           error_max = %0.3f (%d)\n", sqrt(error_max), idx_max);
		printf("           proj = %0.3f, %0.3f\n", px_max, py_max);
	}
#endif /* DEBUG_SFM */

	// 	if (use_constraints) 
	// 	{
	// 		for (i = 0; i < m_nimgs; i++) 
	// 		{
	// 			delete []constraints[i].constraints;
	// 			delete []constraints[i].constrained;
	// 			delete []constraints[i].weights;
	// 		}
	// 		delete []constraints;
	// 	}

	delete []params;
	delete []global_last_ws;
	delete []global_last_Rs;
}