#pragma once
#include <vector>
#include "alglib/ap.h"
#include "alglib/linalg.h"
//#include "alglib/svd.h"
#include "DataTypes.h"
#include "MyMatrix.h"
#include <algorithm>
#include "jpeglib.h"
#include <assert.h>
#include "matrix.h"

inline void LoadJPEG(const char *filename, Image_t &Img)
{
	Img.Destroy();
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);

	FILE *f = NULL;
	fopen_s(&f, filename, "rb");
	if (f  == NULL) 
	{
		printf("[LoadJPEG] Error: can't open file %s for reading\n", filename);
		return;
	}

	jpeg_stdio_src(&cinfo, f);
	jpeg_read_header(&cinfo, TRUE);
	jpeg_start_decompress(&cinfo);

	int w = cinfo.output_width;
	int h = cinfo.output_height;
	int n = cinfo.output_components;
	Img.w = w; Img.h = h;

	assert(n == 1 || n == 3);

	Img.pixels = new BYTE[w * h * 3];
	JSAMPROW row = new JSAMPLE[n * w];

	for (int y = 0; y < h; y++)
	{
		jpeg_read_scanlines(&cinfo, &row, 1);

		for (int x = 0; x < w; x++)
		{
			if (n == 3)
			{
				Img.pixels[3 * (x + y * w) + 0] = row[3 * x + 0];//r
				Img.pixels[3 * (x + y * w) + 1] = row[3 * x + 1];//g
				Img.pixels[3 * (x + y * w) + 2] = row[3 * x + 2];//b
			} else if (n == 1) 
			{
				Img.pixels[3 * (x + y * w) + 0] = row[x];//r
				Img.pixels[3 * (x + y * w) + 1] = row[x];//g
				Img.pixels[3 * (x + y * w) + 2] = row[x];//b
			}
		}
	}

	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);

	delete [] row;
	fclose(f);
}

inline void WriteJPEG(Image_t const  &img, const char *filename)
{
	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);


	FILE *outfile = NULL;
	fopen_s(&outfile, filename, "wb");
	if (outfile  == NULL) 
	{
		printf("[WriteJPEG] can't open file %s for writing\n", filename);
		return;
	}

	jpeg_stdio_dest(&cinfo, outfile);

	cinfo.image_width = img.w;     /* image width and height, in pixels */
	cinfo.image_height = img.h;
	cinfo.input_components = 3;     /* # of color components per pixel */
	cinfo.in_color_space = JCS_RGB; /* colorspace of input image */
	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, 98, TRUE);

	jpeg_start_compress(&cinfo, TRUE);

	JSAMPROW row = new JSAMPLE[3 * img.w];
	for (int y = 0; y < img.h; y++) 
	{
		// JSAMPROW row_pointer[1];        /* pointer to a single row */
		int row_stride;                 /* physical row width in buffer */
		row_stride = img.w * 3;        /* JSAMPLEs per row in image_buffer */

		for (int x = 0; x < img.w; x++)
		{
			row[3 * x + 0] = img.pixels[(x + y * img.w) * 3 + 0];
			row[3 * x + 1] = img.pixels[(x + y * img.w) * 3 + 1];
			row[3 * x + 2] = img.pixels[(x + y * img.w) * 3 + 2];
		}

		jpeg_write_scanlines(&cinfo, &row, 1);
	}

	jpeg_finish_compress(&cinfo);
	jpeg_destroy_compress(&cinfo);

	delete [] row;

	fclose(outfile);
}

inline REAL medianValue(vector<REAL> const& vs, REAL r, int opt = 0)
{
	if (vs.empty())  return 0;

 	REAL avg = 0.0;
 	REAL var = 0;
 	if (vs.size() <= 2) 
	{
 		for (size_t i = 0; i < vs.size(); ++i)
 			avg += vs[i];
 		avg /= vs.size();

		if (opt == 1)
		{
			for (size_t i = 0; i < vs.size(); ++i)
				var += (vs[i] - avg) * (vs[i] - avg);
			var = sqrt(var/vs.size());

			if (var/avg > 0.2) 
				return -1.0;
			else
				return avg;
		}
		else
			return avg;
 	}

	//sort list
	vector<REAL>temp; temp.assign(vs.begin(), vs.end());
	sort(temp.begin(), temp.end());

	int const radius = min(max(1, int(r * vs.size())), int(vs.size())/2 - 1);
	for (int k = int(temp.size())/2-radius; k <= int(temp.size())/2+radius; ++k) 
		avg += temp[k];
	avg /= (2*radius+1);

	if (opt == 1)
	{
		for (int k = int(temp.size())/2-radius; k <= int(temp.size())/2+radius; ++k) 
			var += (temp[k] - avg) * (temp[k] - avg);
		var = sqrt(var/(2 * radius + 1));

		if (var/avg > 0.1)
			return -1.0;
		else
			return avg;
	}
	else
		return avg;
}

inline Vector3r medianNormal(vector<Vector3r> const &normals, REAL r)
{
	Vector3r normal;
	if (normals.empty()) return normal;

	vector<Vector3r> tmp_normals; tmp_normals = normals;
	for (size_t i = 0; i < tmp_normals.size(); ++i)
	{
		for (size_t j = i + 1; j < tmp_normals.size(); ++j)
		{
			if (tmp_normals[i].v[2] > tmp_normals[j].v[2])
			{
				memcpy(normal.v, tmp_normals[i].v, sizeof(REAL) * 3);
				memcpy(tmp_normals[i].v, tmp_normals[j].v, sizeof(REAL) * 3);
				memcpy(tmp_normals[j].v, normal.v, sizeof(REAL) * 3);
			}
		}
	}

	alglib::real_2d_array A, u, vt;
	alglib::real_1d_array w;
	A.setlength(3, 3);
	vt.setlength(3, 3);
	w.setlength(3);

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			A(i, j) = 0;

	int const radius = min(max(1, int(r * tmp_normals.size())), int(tmp_normals.size())/2 - 1);
	for (int i = int(tmp_normals.size())/2-radius; i <= int(tmp_normals.size())/2+radius; ++i)
	{
		REAL const *vi = tmp_normals[i].v;
		for (int j = int(tmp_normals.size())/2-radius; j <= int(tmp_normals.size())/2+radius; ++j)
		{
			REAL const *vj = tmp_normals[j].v;

			for (int u = 0; u < 3; u++)
				for (int v = 0; v < 3; v++)
					A(u, v) += vi[u] * vj[v];
		}
	}

	alglib::rmatrixsvd(A, 3, 3, 0, 2, 2, w, u, vt);

	normal.v[0] = REAL(vt(0, 0));
	normal.v[1] = REAL(vt(0, 1));
	normal.v[2] = REAL(vt(0, 2));

	if (normal.v[2] > 0)
		MatrixScalarProduct(3, 1, normal.v, REAL(-1), normal.v);
	return normal;
}

inline void pseudoInverse(alglib::real_2d_array &src, int nrow, int ncol, alglib::real_2d_array &dst)
{
	alglib::real_2d_array u, vt;
	alglib::real_1d_array w;
	u.setlength(nrow, nrow);
	vt.setlength(ncol, ncol);
	w.setlength(ncol);

	for (int i = 0; i < ncol; ++i) w(i) = 0;

	alglib::rmatrixsvd(src, nrow, ncol, 2, 2, 2, w, u, vt);

	for (int i = 0; i < ncol; i++)
	{
		if (abs(w(i)) < 0.00001) w(i) = 0;
		else w(i) = 1/w(i);
	}

	for (int i = 0; i < ncol; i++)
	{
		for (int j = 0; j < ncol; j++)
			vt(i, j) *= w(i);
	}

	for (int r = 0; r < ncol; r++)
	{
		for (int c = 0; c < nrow; c++)
		{
			dst(r, c) = 0;
			for (int k = 0; k < min(ncol, nrow); k++)
				dst(r, c) += vt(k, r) * u(c, k);
		}
	}
}

inline void copyRotationMatrix
(REAL *src, int const rowStart, int const colStart, int const rowLen, int const colLen, 
 alglib::real_2d_array &dst, int const nDstRow, int const nDstCol, int const dstRow, int const dstCol)
{	
	int const rowEnd = min(rowStart + rowLen, 3);
	int const colEnd = min(colStart + colLen, 3);

	int c0, c1, r0, r1;

	for (c0 = colStart, c1 = dstCol; c0 < colEnd && c1 < nDstCol; ++c0, ++c1)
		for (r0 = rowStart, r1 = dstRow; r0 < rowEnd && r1 < nDstRow; ++r0, ++r1)
			dst(r1, c1) = src[r0 * 3 + c0];
}

inline void calPairwiseProjectiveDepth(REAL *left, REAL *right, REAL *transfer, REAL *vResult, int MethodID)
{
	switch (MethodID)
	{
		case 1:
			{
				//speed up the computation
				REAL AtA[2*2], A[3*2], At[3*2], b[3*1];
				for (int i = 0; i < 3; i++)
				{
					A[i * 2 + 0] = -left[i];
					A[i * 2 + 1] = right[i];
					b[i] = transfer[i];
				}
				MatrixTranspose(3, 2, A, At);
				MatrixMatrixProduct(2, 3, 2, At, A, AtA);
				MatrixMatrixProduct(2, 3, 1, At, b, b);

				vResult[1] = (AtA[0]*b[1] - b[0]*AtA[2])/(AtA[0]*AtA[3] - AtA[1]*AtA[2]);
				vResult[0] = (b[0] - AtA[1]*vResult[1])/AtA[0];
				vResult[2] = 0; 

				break;
			}
		case 2:
			{
				REAL LR[3];

				VectorCross(left, right, LR);
				VectorNormalize(LR, 3);

				REAL a, b,c;
				a =(LR[0]*right[1]*transfer[2] - LR[0]*right[2]*transfer[1] - LR[1]*right[0]*transfer[2] + LR[1]*right[2]*transfer[0] + LR[2]*right[0]*transfer[1] - LR[2]*right[1]*transfer[0])/(LR[0]*left[1]*right[2] - LR[0]*left[2]*right[1] - LR[1]*left[0]*right[2] + LR[1]*left[2]*right[0] + LR[2]*left[0]*right[1] - LR[2]*left[1]*right[0]);
				b =(LR[0]*left[1]*transfer[2] - LR[0]*left[2]*transfer[1] - LR[1]*left[0]*transfer[2] + LR[1]*left[2]*transfer[0] + LR[2]*left[0]*transfer[1] - LR[2]*left[1]*transfer[0])/(LR[0]*left[1]*right[2] - LR[0]*left[2]*right[1] - LR[1]*left[0]*right[2] + LR[1]*left[2]*right[0] + LR[2]*left[0]*right[1] - LR[2]*left[1]*right[0]);
				c =-(left[0]*right[1]*transfer[2] - left[0]*right[2]*transfer[1] - left[1]*right[0]*transfer[2] + left[1]*right[2]*transfer[0] + left[2]*right[0]*transfer[1] - left[2]*right[1]*transfer[0])/(LR[0]*left[1]*right[2] - LR[0]*left[2]*right[1] - LR[1]*left[0]*right[2] + LR[1]*left[2]*right[0] + LR[2]*left[0]*right[1] - LR[2]*left[1]*right[0]);

				vResult[0] = a;
				vResult[1] = b;
				vResult[2] = 0;
				break;
			}
		default:
			{
				int m = 3, n = 3;
				alglib::real_2d_array a, vt, u;
				alglib::real_1d_array w;
				a.setlength(m, n);
				vt.setlength(n,n);
				w.setlength(n);

				a(0,0) = -left[0]; a(0,1) = right[0]; a(0,2) = -transfer[0];
				a(1,0) = -left[1]; a(1,1) = right[1]; a(1,2) = -transfer[1];
				a(2,0) = -left[2]; a(2,1) = right[2]; a(2,2) = -transfer[2];

				alglib::rmatrixsvd(a, m, n, 0, 2, 2, w, u, vt);

				vResult[0] =  REAL(vt(2, 0)/vt(2, 2));
				vResult[1] =  REAL(vt(2, 1)/vt(2, 2));
				vResult[2] = REAL(w(2)); 
			}
	}	
}

// inline void Undistort(REAL *pt, REAL *udpt, REAL f, REAL k1, REAL k2)
// {
// 	REAL rsq = (pt[0] * pt[0] + pt[1] * pt[1]) / (f * f);
// 	REAL factor = 1.0f + k1 * rsq + k2 * rsq * rsq;
// 
// 	udpt[0] = pt[0] * factor;
// 	udpt[1] = pt[1] * factor;
// }
// 
inline void ApplyRadialDistortion(REAL *pt, REAL *dpt, REAL f, REAL k1, REAL k2)
{
 	REAL rsq = (pt[0] * pt[0] + pt[1] * pt[1]) / (f * f);
 	REAL factor = 1.0f + k1 * rsq + k2 * rsq * rsq;
 
 	dpt[0] = pt[0] * factor;
 	dpt[1] = pt[1] * factor;
}

inline void InvertDistortion
(int n_in, int n_out, double r0, double r1,  double *k_in, double *k_out)
{
	const int NUM_SAMPLES = 20;

	int num_eqns = NUM_SAMPLES;
	int num_vars = n_out;

	double *A = new double[num_eqns * num_vars];
	double *b = new double[num_eqns];

	for (int i = 0; i < NUM_SAMPLES; i++) {
		double t = r0 + i * (r1 - r0) / (NUM_SAMPLES - 1);

		double p = 1.0;
		double a = 0.0;
		for (int j = 0; j < n_in; j++) {
			a += p * k_in[j];
			p = p * t;
		}

		double ap = 1.0;
		for (int j = 0; j < n_out; j++) {
			A[i * num_vars + j] = ap;
			ap = ap * a;
		}

		b[i] = t;
	}

	dgelsy_driver(A, b, k_out, num_eqns, num_vars, 1);

	delete [] A;
	delete [] b;
}

inline void UndistortNormalizedPoint(REAL *p, CalibrationData const &calib) 
{
	REAL r = sqrt(p[0] * p[0] + p[1] * p[1]);
	if (r == 0.0) return;

	double t = 1.0;
	double a = 0.0;

	for (int i = 0; i < 6; i++)
	{
		a += t * calib.k_inv[i];
		t = t * r;
	}
	REAL factor = REAL(a) / r;

	p[0] *= factor;
	p[1] *= factor;
}

inline bool CheckCheirality(CameraParam &c, Vector3r &pt)
{
	REAL depth = c.R[6] * (pt.v[0] - c.cen[0]) + c.R[7] * (pt.v[1] - c.cen[1]) + c.R[8] * (pt.v[2] - c.cen[2]);
	return depth > 0?true:false;
}