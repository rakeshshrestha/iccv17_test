#pragma once
#include "DataTypes.h"

static HANDLE hCountMutex;
static HANDLE hStartNewThread;
static HANDLE hKdTreeMutex;
static HANDLE hJPEGMutex;
static HANDLE hMemoryMutex;
static  int  ThreadNr = 0;                    // Number of threads started 
static  int nMaxThreadView = 4;   //max number of threads
static  int nMaxThreadPt = 20;
static  int  ThreadId = 0;

static void KillThread()
{
	WaitForSingleObject(hCountMutex, INFINITE);
	ThreadNr--;
	ReleaseMutex(hCountMutex);
}