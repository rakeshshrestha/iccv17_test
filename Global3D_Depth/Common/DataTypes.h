#pragma once
#include <map>
#include <hash_map>
#include <Windows.h>
#include <stdio.h>

using namespace std;
using namespace stdext;

//typedef map<int, int> PtTrack;
typedef float REAL;
//typedef pair<int, int> IntPair;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef pair<ushort, ushort> ushortPair;
typedef pair<int, int> intPair;

#define Pi 3.14159265358979323846
#define KEY_LIMIT 65536
#define MINTRIPOINTS 10
#define MAXNUMFEATURE 200
#define MAXKEYFILE  7000
#define SEARCHWINDOW 50

#define VCHANGE1 0.7071//45
#define VCHANGE2 0.5//60
#define MAXVANGLE -0.5

#define MINSCORE1 0.6  //0.65
#define MINSCORE2 0.5  //0.55

#define motion_t 0.5
#define motion_t2 0.4

#define MAXMATCHES 10000
#define MAXVIEWKEY 65536

#define nTriPts 1
#define minTriplet  6
#define maxTriplet 10
//#define VISUALIZE

static REAL p_Alpha =  0.05f;
static REAL b_Ratio = 0.3f;

struct Vector2r
{
	REAL v[2];
	Vector2r() { v[0] = v[1] = 0; }
	Vector2r(REAL x, REAL y) {v[0] = x; v[1] = y;}
};

struct Vector3r
{
	REAL v[3];
	Vector3r() { v[0] = v[1] = v[2] = 0;}
	Vector3r(REAL x, REAL y, REAL z) {v[0] = x; v[1] = y; v[2] = z; }
};

struct Vector3ii
{
	int v[3];
	Vector3ii() { v[0] = v[1] = v[2] = 0;}
	Vector3ii(int x, int y, int z) {v[0] = x; v[1] = y; v[2] = z; }
};

struct Vector3b
{
	BYTE v[3];
	Vector3b() { v[0] = v[1] = v[2] = 0;}
	Vector3b(BYTE x, BYTE y, BYTE z) {v[0] = x; v[1] = y; v[2] = z; }
};

struct ScenePt
{
	Vector3r pos;
	Vector3b color;
	int trackid;
	REAL error;
	bool IsGoodPt;
	bool adjusted;

	ScenePt(){trackid = -1; IsGoodPt = false; adjusted = false; };
	ScenePt(Vector3r const &p, int tid)
	{
		memcpy(pos.v, p.v, sizeof(REAL) * 3);
		trackid = tid;
		IsGoodPt = true;
		adjusted = false;
	}

	void UnsetPoint()
	{
		IsGoodPt = false;
		adjusted = false;
	}

	void SetPoint()
	{
		IsGoodPt = true;
	}
};

struct TripletKey
{
	TripletKey(){vid[0] = vid[1] = vid[2] = 0;}

	TripletKey(ushort v0, ushort v1, ushort v2) {
		if (v0 > v1 || v1 > v2 || v0 > v2)
		{
			printf("GraphData::TripletKey(): v0 < v1 < v2 failed! \n");
			fflush(stdout);
		}
			//cout << "GraphData::TripletKey(): v0 < v1 < v2 failed! " <<endl;

		vid[0] = v0;
		vid[1] = v1;
		vid[2] = v2;
	}

	bool operator<(TripletKey const& b) const
	{
		if (vid[0] < b.vid[0]) return true;
		if (vid[0] > b.vid[0]) return false;
		if (vid[1] < b.vid[1]) return true;
		if (vid[1] > b.vid[1]) return false;
		return vid[2] < b.vid[2];
	}
	ushort vid[3];
};

//////////////////////////////////////////////////////////////////
// Data structure for single view
struct Image_t
{
	/* Width, height */
	int w, h;

	/* Pixel data */
	BYTE *pixels;

	Image_t() 
	{
		pixels = NULL;
		w = h = 0;
	};

	void Destroy() 
	{
		if (pixels != NULL) delete []pixels; 
		pixels = NULL;
		w = 0; h = 0; 
	}
};

struct CalibrationData
{
	REAL ini_f;
	REAL f;           //focal length
	REAL u, v;      //principle point
	REAL k1, k2; //distortion parameter
	double k_inv[6];
	int img_w, img_h;

	CalibrationData()
	{
		f = 1.0f;
		u = v = k1 = k2 = 0;
		img_w = img_h = 0;

		for (int i = 0; i < 6; i++)
			k_inv[i] = 0;
		k_inv[1] = 1.0;
	};

	CalibrationData& operator= (const CalibrationData& src) 
	{
		f = src.f;
		u = src.u;
		v = src.v;
		k1 = src.k1;
		k2 = src.k2;
		memcpy(k_inv, src.k_inv, sizeof(double) * 6);
		img_h = src.img_h;
		img_w = src.img_w;
		return *this;
	};
};

struct CameraParam
{
	REAL R[9];
	REAL cen[3];

	CameraParam()
	{
		memset(R, 0, sizeof(REAL) * 9);
		memset(cen, 0, sizeof(REAL) * 3);
	}
};

struct PtPatch 
{
	ushort kid;

	vector<uint>* RefEgId;
	vector<REAL>* depths;  //with baseline length of reference EG = 1
	vector<Vector3r>* normals; 
	Vector3r normal;
	Vector3r loc;

	REAL Ep; //appearance consistency
	REAL tmpEp;

#ifdef VISUALIZE
	int visualize;
#endif

	PtPatch() {
		kid= -1; Ep = 0;
 		RefEgId = NULL;
 		depths = NULL;
		normals = NULL;
	};

	void Initialize()
	{
		RefEgId = new vector<uint>;
		depths = new vector<REAL>;
		normals = new vector<Vector3r>;
	};

	void Destroy()
	{
		if (RefEgId != NULL)
			delete RefEgId;
		RefEgId = NULL;
		if (depths != NULL)
			delete depths;
		depths = NULL;
		if (normals != NULL)
			delete normals;
		normals = NULL;
	}

	PtPatch& operator= (const PtPatch& src) 
	{
		kid = src.kid;
		Ep = src.Ep;
		depths->assign(src.depths->begin(), src.depths->end());
		RefEgId->assign(src.RefEgId->begin(), src.RefEgId->end());
		return *this;
	};
};

struct colorKey
{
	int tid; //track id the key is associated with
	BYTE r; 
	BYTE g;
	BYTE b;

	colorKey() {tid = -1; r = g = b = 0; };
	colorKey(int id, BYTE r0, BYTE g0, BYTE b0)
	{
		tid = id;
		r = r0;
		g = g0;
		b = b0;
	}
};

struct ViewData
{
	CameraParam cam;
	CalibrationData calib;
	vector<PtPatch> *m_pts;
	map<ushort, colorKey> mKey; //<kid, colorKey>
	int nVisibleKeys;
	bool IsGoodView;
	bool m_ignore;
	bool IsTreeNode;
	//bool adjusted;
	REAL fov;

	ViewData() {m_pts = NULL; IsGoodView = false;m_ignore = false; nVisibleKeys = 0; IsTreeNode = false;};

	void Initialize()
	{
		m_pts = new vector<PtPatch>;
	}

	void Destroy()
	{
		if (m_pts != NULL)
		{
			for (size_t i = 0; i < m_pts->size(); ++i)
				m_pts->at(i).Destroy();
			delete m_pts;
		}
		m_pts = NULL;
		nVisibleKeys = 0;
		mKey.clear();
	}

	void SetKey(ushort kid, int tid)
	{
		map<ushort, colorKey>::iterator miter = mKey.find(kid);
		if (miter == mKey.end()) 
		{
			colorKey ckey(tid, 0, 0, 0);
			mKey.insert(make_pair(kid, ckey));

			if (tid >= 0) nVisibleKeys++;
		}
		else
		{
			if ((miter->second).tid < 0 && tid >= 0) nVisibleKeys++;
			else if ((miter->second).tid >= 0 && tid < 0) nVisibleKeys--;
			(miter->second).tid = tid; 
		}
	}

	void ClearKey()
	{
		//for (map<ushort, colorKey>::iterator miter = mKey.begin(); miter != mKey.end(); miter++)
		//	(miter->second).tid = -1;
		mKey.clear();
		nVisibleKeys = 0;
	}

	void GetKeyColor(ushort kid, unsigned char &r0, unsigned char &g0, unsigned char &b0)
	{
		map<ushort, colorKey>::iterator miter = mKey.find(kid);
		if (miter == mKey.end())
		{
			r0 = g0 = b0 = 0;
		}
		else
		{
			r0 = (miter->second).r;
			g0 = (miter->second).g;
			b0 = (miter->second).b;
		}
	}
};

struct TrackData
{
	map<ushort, pair<ushort, char>> track; //vid, {kid, added}
	int ptid; //track id of added tracks
	int nSeenPts;

	TrackData() {ptid = -1; nSeenPts = 0;};
	TrackData(vector<pair<ushort, ushort>> &keys)
	{
		ptid = -1;
		nSeenPts = 0;
		for (size_t i = 0; i < keys.size(); ++i)
		{
			track.insert(make_pair(keys[i].first, make_pair(keys[i].second, 0)));
		}
	};

	void SetPoint(ushort vid)
	{
		map<ushort, pair<ushort, char>>::iterator piter = track.find(vid);
		if(piter == track.end()) return;

		if ((piter->second).second <= 0)
		{
			(piter->second).second = 1;
			nSeenPts++;
		}
	}

	void UnsetPoint(ushort vid)
	{
		map<ushort, pair<ushort, char>>::iterator piter = track.find(vid);
		if(piter == track.end()) return;

		if ((piter->second).second > 0)
		{
			(piter->second).second = 0;
			nSeenPts--;
		}
	}

	void SetOutlier(ushort vid)
	{
		map<ushort, pair<ushort, char>>::iterator piter = track.find(vid);
		if(piter == track.end()) return;

		if ((piter->second).second > 0)
		{
			(piter->second).second = -1;
			nSeenPts--;
		}
	}

	void UnsetAll()
	{
		for (map<ushort, pair<ushort, char>>::iterator piter = track.begin(); 
			piter != track.end(); piter++)
		{
			if ((piter->second).second > 0)
			{
				(piter->second).second = 0;
			}
		}
		nSeenPts = 0;
	}

	void Clear()
	{
		UnsetAll();
		ptid = -1;
	}
};

///////////////////////////////////////////////////////

////////////////////////////////////////////////////////
//Data structure for view pairs
struct PointCorrespondence
{
	bool goodPt; //Is the point reconstruction accurate
	//bool IsSelect; //selected for computing appearance consistency ??? no needed
	
	//2015.02.17
	bool goodPts[2]; // Set after computing depths

	ushort keyid[2];
	REAL depth[2];
	REAL Images[4];
	REAL angle;

	//2015.3.30
	Vector3r normal_l;
	Vector3r normal_r;

	//PointCorrespondence():goodPt(false){};
	PointCorrespondence() {goodPt = goodPts[0] = goodPts[1] = false;}
};

struct PairwiseEG
{
	/*view index*/
	ushort vid[2]; 

	/*	Epipolar geometry*/
	REAL R[9];
	REAL t[3];  //P_left = [I 0], P_right=[R t]
	REAL e[3];  //epipole in pure translation
	REAL baseLength; 
	REAL ibaseLength; //imaginary baselength
	REAL old_e_error;
	REAL DepthRatio; //2015.2.24 The depth of img1 / the depth img2
	REAL baseLengths[2]; //baseLengths[0] centered at v0, baseLengths[1] centered at v1 // It is the same with mViewDepth[i].CamerasDepths->at(j)
	REAL Rs[2][9];  // The two relative rotation computed by local ba
	REAL ts[2][3];  // The two relative translation computed by local ba
	
	
	int nTriplet;

	/*	flags*/
	bool IsGoodPair;  //pair form valid triplets--Set in GenerateCameraTriplets
	bool IsTreeEdge;  //pair is an edge on the spanning tree -- Set in InitializeGraph->MarkTreeEdge
	 bool IsTreeTriplet; //pair form triplets that can be traced out along the spanning tree.-- Set in InitializeGraph->MarkTreeTriplet
	bool IsInlier;    //pair is consistent with the global rotation determined by the spanning tree --Set in InitializeGraph->CheckRotationConsistency
	bool IsActive;      //pair is active for submodel reconstruction -- Set in ConstructDualGraph 
	
	//0315
	int IsCheckBA[2]; // The number of inliers
	//2015.4.1
	bool MissCheck[2];

	/*	Motion consistency*/
	REAL outlier_p;
	ushort LoopLen;

	/*	Appearance consistency*/
	REAL Ed; 
	HANDLE hMutex;

	/*	Structure*/
	//int ImgPts[4]; //representative point pairs
	int nOMatches;
	int nMatches;
	int nGoodMatches;

	hash_map<ushort, ushort> *keys0; 
	hash_map<ushort, ushort> *keys1;

protected:
	vector<PointCorrespondence> *corrs;

public:
	PairwiseEG() {		
		vid[0] = vid[1] = -1;
		baseLength = 1.0f;
		DepthRatio = -1.0f;

		IsGoodPair = false;
		IsTreeEdge = false;
		IsInlier = false;  
		IsActive = false;

		//2015.4.1
		MissCheck[0] = MissCheck[1] = false;
		IsCheckBA[0] = IsCheckBA[1] = 0;
		 
		outlier_p = 0.0f;
		Ed = 0.0f; 
		nTriplet = 0;

		//memset(ImgPts, -1, sizeof(int) * 4);
		nMatches = nGoodMatches = 0;
		keys0 = keys1 = NULL;
		corrs = NULL;
	};

	void Initialize() {
		Destroy();
		keys0 = new hash_map<ushort, ushort>;
		keys1 = new hash_map<ushort, ushort>;
		corrs = new vector<PointCorrespondence>;
	};

	void Initialize(int n) {
		Destroy();
		keys0 = new hash_map<ushort, ushort>;
		keys1 = new hash_map<ushort, ushort>;
		corrs = new vector<PointCorrespondence>;
		corrs->reserve(n);
		//nMatches = n;
	};

	void Destroy() {
		if (keys0 != NULL) 
		{
			keys0->clear();
			delete keys0;
		}
		if (keys1 != NULL) 
		{
			keys1->clear();
			delete keys1;
		}
		if (corrs != NULL) 
		{
			corrs->clear();
			delete corrs;
		}
		keys0 = keys1 = NULL;
		corrs  =NULL;
	}

	vector<PointCorrespondence> const &GetPointCorr() {return *corrs;}

	vector<PointCorrespondence>  &GetPointCorrEdit() {return *corrs;}

	void AddPointCorr(PointCorrespondence &corr) {
		if (corrs != NULL) {
			corrs->push_back(corr);
			nMatches = int(corrs->size());
		}
	}

	void RemovePointCorr(int i) {
		if (corrs != NULL && int(corrs->size()) > i) {
			corrs->erase(corrs->begin() + i);
			nMatches--;
		}
	}

	void SetDepth(int i, REAL depth0, REAL depth1) {
		if (corrs != NULL) {
			PointCorrespondence & corr = corrs->at(i);
			corr.depth[0] = depth0;
			corr.depth[1] = depth1;
		}
	}

	void SetDepthImageAngle(int i, REAL depth0, REAL depth1, REAL ImageX0,REAL ImageY0,REAL ImageX1,REAL ImageY1, REAL Angle) {
		if (corrs != NULL) {
			PointCorrespondence & corr = corrs->at(i);
			corr.depth[0] = depth0;
			corr.depth[1] = depth1;
			corr.Images[0] = ImageX0;
			corr.Images[1] = ImageY0;
			corr.Images[2] = ImageX1;
			corr.Images[3] = ImageY1;
			corr.angle = Angle;
		}
	}

	void SetGoodPt(int i) {
		if (corrs != NULL) {
			PointCorrespondence &corr = corrs->at(i);
			if (!corr.goodPt) {
				corr.goodPt = true;
				nGoodMatches++;
			}
		}
	}

	void SetGoodPt() {
		if (corrs != NULL) {
			for (size_t i = 0 ; i < corrs->size(); ++i) {
				PointCorrespondence &corr = corrs->at(i);
				corr.goodPt = true;
			}
			nGoodMatches = nMatches;
		}
	}

	void UnsetGoodPt(int i) {
		if (corrs != NULL) {
			PointCorrespondence &corr = corrs->at(i);
			if (corr.goodPt) {
				corr.goodPt =false;
				nGoodMatches--;
			}
		}
	}

	void UnsetGoodPt() {
		if (corrs != NULL) {
			for (size_t i = 0 ; i < corrs->size(); ++i) {
				PointCorrespondence &corr = corrs->at(i);
				corr.goodPt = false;
			}
			nGoodMatches = 0;
		}
	}

	/*void SelectPt(int i) {
		if (corrs != NULL) {
			PointCorrespondence &corr = corrs->at(i);
			corr.IsSelect = true;
		}
	}

	void SelectPt()
	{
		if (corrs != NULL) {
			for (size_t i = 0 ; i < corrs->size(); ++i) {
				PointCorrespondence &corr = corrs->at(i);
				corr.IsSelect = true;
			}
		}
	}

	void UnSelectPt(int i) {
		if (corrs != NULL) {
			PointCorrespondence &corr = corrs->at(i);
			corr.IsSelect = false;
		}
	}

	void UnSelectPt() {
		if (corrs != NULL) {
			for (size_t i = 0 ; i < corrs->size(); ++i) {
				PointCorrespondence &corr = corrs->at(i);
				corr.IsSelect = false;
			}
		}
	} */
};
///////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//Data structure for view triplets
struct TripletReconstruction
{
	ushort vid[3];
	uint pid[3];
	REAL R[27];
	REAL t[9];
	REAL cen[9];
	REAL baseLengths[3]; //b01, b20, b21
	REAL error; //triplet error
	REAL bdRatio[3]; //baselength-depth ratio = baselength/depth
	//REAL rscale[3]; //b01/b12, b12/b02, b02/b01
	REAL s012, s201, s120; //sine angle
	REAL s_012, s_201, s_120; //baselength ratio
	REAL w; //weight
	bool UsePoint;

	Vector3r Pt[12]; // four pair of corresponding points
	TripletReconstruction()
	{
		error = 0; 
		w = 0;
	}
};

///////////////////////////////////////////////////////////////
//Data structure for view depth information
struct ViewDepth
{
	REAL Scale;
	vector<REAL> *PointDepth;
	vector<REAL> *PointUncertainty;
	vector<REAL> *CamerasDepths;

	ViewDepth() {
		Scale = 1.0;
		PointDepth = NULL;
		PointUncertainty = NULL;
		CamerasDepths = NULL;
	};

	void Initialize()
	{
		Destroy();
		PointDepth = new vector<REAL>;
		PointUncertainty = new vector<REAL>;
		CamerasDepths = new vector<REAL>;
	};

	void Initialize(int m, int n)
	{
		Destroy();
		PointDepth = new vector<REAL>;
		PointUncertainty = new vector<REAL>;
		CamerasDepths = new vector<REAL>;
		PointDepth->assign(m,-1.0f);
		PointUncertainty->assign(m,-1.0f);
		CamerasDepths->assign(n,-1.0f);
	};

	void Destroy()
	{
		if (PointDepth != NULL)
			delete PointDepth;
		PointDepth = NULL;
		if (PointUncertainty != NULL)
			delete PointUncertainty;
		PointUncertainty = NULL;
		if (CamerasDepths != NULL)
			delete CamerasDepths;
		CamerasDepths = NULL;
	}
};