#pragma  once
#include <hash_map>
#include <hash_set>
#include <vector>
#include <algorithm>
#include "DataTypes.h"
#include "keys.h"
#include<stdio.h>
#include "config.h"

using namespace std;
using namespace stdext;

typedef hash_map<unsigned short, vector<KeypointMatch>> MatchAdjTable;
typedef hash_set<int> HashSetInt;
typedef pair<unsigned short, unsigned short> MatchIndex;
extern int MINIMUM_RAW_CORRESPONDENCES;
class AdjListElem
{
public:
	bool operator< (const AdjListElem &other) const
	{
		return m_index < other.m_index;
	}

	unsigned short m_index;
	vector<KeypointMatch> m_match_list;
};

typedef vector<AdjListElem> MatchAdjList;

class MatchTable
{
public:
	MatchTable() {}
	MatchTable(int num_images) 
	{
		m_match_lists.resize(num_images);
	}

	void SetMatch(MatchIndex idx) 
	{ 
		if (Contains(idx)) return;  

		AdjListElem e;
		e.m_index = idx.second;
		MatchAdjList &l = m_match_lists[idx.first];
		MatchAdjList::iterator p = lower_bound(l.begin(), l.end(), e);
		l.insert(p, e);
	}

	void AddMatch(MatchIndex idx, KeypointMatch m)
	{
		assert(Contains(idx));
		GetMatchList(idx).push_back(m);
	}

	void ClearMatch(MatchIndex idx) 
	{ // but don't erase!
		if (Contains(idx)) 
		{
			GetMatchList(idx).clear();
		}
	}

	void RemoveMatch(MatchIndex idx) 
	{
		if (Contains(idx)) 
		{
			std::vector<KeypointMatch> &match_list = GetMatchList(idx);
			match_list.clear();

			// Remove the neighbor
			AdjListElem e;
			e.m_index = idx.second;
			MatchAdjList &l = m_match_lists[idx.first];
			std::pair<MatchAdjList::iterator, MatchAdjList::iterator> p = 
				equal_range(l.begin(), l.end(), e);

			assert(p.first != p.second); 
			l.erase(p.first, p.second);        
		}
	}

	uint GetNumMatches(MatchIndex idx)
	{
		if (!Contains(idx))
			return 0;
		return uint(GetMatchList(idx).size());
	}

	std::vector<KeypointMatch> &GetMatchList(MatchIndex idx) 
	{
		AdjListElem e;
		e.m_index = idx.second;
		MatchAdjList &l = m_match_lists[idx.first];
		std::pair<MatchAdjList::iterator, MatchAdjList::iterator> p = 
			equal_range(l.begin(), l.end(), e);

		assert(p.first != p.second); 

		return (p.first)->m_match_list;
	}

	bool Contains(MatchIndex idx) const 
	{
		AdjListElem e;
		e.m_index = idx.second;
		const MatchAdjList &l = m_match_lists[idx.first];
		std::pair<MatchAdjList::const_iterator, 
			MatchAdjList::const_iterator> p = 
			equal_range(l.begin(), l.end(), e);

		return (p.first != p.second);
	}

	void RemoveAll() 
	{
		int num_lists = int(m_match_lists.size());
		for (int i = 0; i < num_lists; i++)
		{
			m_match_lists[i].clear();
		}
	}

	uint GetNumNeighbors(unsigned int i)
	{
		return uint(m_match_lists[i].size());
	}

	MatchAdjList &GetNeighbors(unsigned int i)
	{
		return m_match_lists[i];
	}

	MatchAdjList::iterator Begin(unsigned int i)
	{
		return m_match_lists[i].begin();
	}

	MatchAdjList::iterator End(unsigned int i)
	{
		return m_match_lists[i].end();
	}

private:
	std::vector<MatchAdjList> m_match_lists;
};

/* Return the match index of a pair of images */
inline MatchIndex GetMatchIndex(int i1, int i2) 
{
	return MatchIndex(unsigned short(i1), unsigned short(i2));
}

inline MatchIndex GetMatchIndexUnordered(int i1, int i2) 
{
	if (i1 < i2)
		return MatchIndex(unsigned short(i1), unsigned short(i2));
	else
		return MatchIndex(unsigned short(i2), unsigned short(i1));
}

inline void MakeMatchListSymmetric(unsigned int num_images, MatchTable &m_matches)
{
	for (unsigned int i = 0; i < num_images; i++) 
	{
		MatchAdjList::const_iterator iter;
		for (iter = m_matches.Begin(i); iter != m_matches.End(i); iter++)
		{
			unsigned short j = iter->m_index; 

			if (j <= i) continue;

			//assert(ImagesMatch(i, j));   

			MatchIndex idx = GetMatchIndex(i, j);
			MatchIndex idx_rev = GetMatchIndex(j, i);

			const std::vector<KeypointMatch> &list = iter->m_match_list;
			uint num_matches = uint(list.size());

			m_matches.SetMatch(idx_rev);
			m_matches.ClearMatch(idx_rev);

			for (unsigned int k = 0; k < num_matches; k++)
			{
				KeypointMatch m1, m2;

				m1 = list[k];

				m2.m_idx1 = m1.m_idx2;
				m2.m_idx2 = m1.m_idx1;
				m_matches.AddMatch(idx_rev, m2);
			}
		}
	}
}

inline void PruneDoubleMatches(char *filename, MatchTable &m_matches, int num_images)
{
	for (int i = 0; i < num_images; i++)
	{
		MatchAdjList::iterator iter;

		std::vector<unsigned short> remove;
		for (iter = m_matches.Begin(i); iter != m_matches.End(i); iter++)
		{
			HashSetInt seen;

			int num_pruned = 0;

			vector<KeypointMatch> &list = iter->m_match_list;

			/* Unmark keys */
			int num_matches = (int) list.size();

			for (int k = 0; k < num_matches; k++) 
			{
				unsigned short idx1 = list[k].m_idx1;

				if (seen.find(int(idx1)) != seen.end())
				{
					/* This is a repeat */
					list.erase(list.begin() + k);
					num_matches--;
					k--;

					num_pruned++;
				} else
				{
					/* Mark this key as matched */
					seen.insert(int(idx1));
				}
			}

			int j = int(iter->m_index); // first;

// 			printf("[PruneDoubleMatches] Pruned[%d,%d] = %d / %d\n",
// 				i, j, num_pruned, num_matches + num_pruned);

			if (num_matches < MINIMUM_RAW_CORRESPONDENCES) 
			{
				/* Get rid of... */
				remove.push_back(iter->m_index); // first);
			}
		}

		for (int j = 0; j < int(remove.size()); j++) 
		{
			int idx2 = remove[j];
			m_matches.RemoveMatch(GetMatchIndex(i, idx2));
			//printf("[PruneDoubleMatches] Removing[%d,%d]\n", i, idx2);
		}
	}

	/*if (filename != NULL)
	{
		FILE *out_file;
		fopen_s(&out_file, filename, "w");
		if (out_file != NULL)
		{
			for (int i = 0; i < num_images; i++)
			{
				for (MatchAdjList::iterator iter = m_matches.Begin(i); iter != m_matches.End(i); iter++)
				{
					if (i > int(iter->m_index)) continue;

					fprintf(out_file, "%d %d\n", i, int(iter->m_index));
					vector<KeypointMatch> &list = iter->m_match_list;

					int num_matches = int(list.size());
					for (int k = 0; k < num_matches; k++) 
					{
						fprintf(out_file, "%d %d\n", int(list[k].m_idx1),  int(list[k].m_idx2));
					}
				}
			}
			fclose(out_file);
		}

	}
	fflush(stdout); */
}

inline void LoadAllMatches(const char *filename, MatchTable &m_matches, vector<ushortPair> &blackList)
{
	//printf("[LoadAllMatches] Loading matches\n");
	fflush(stdout);
	m_matches.RemoveAll();

	FILE *in_file = NULL;
	fopen_s(&in_file, filename, "r");
	if (in_file == NULL)
	{
		printf("[LoadAllMatches] Error opening file %s for reading\n", filename);
		fflush(stdout);
		exit(1);
	}

	map<ushortPair, int> blackListMap;
	for (int i = 0; i < int(blackList.size()); i++)
	{
		blackListMap.insert(make_pair(blackList[i], i));
	}

	char buf[512];
	while (!feof(in_file))
	{
		int i1 = -1, i2 = -1;
		fscanf_s(in_file, "%d %d", &i1, &i2);

		if (feof(in_file)) break;

		map<ushortPair, int>::iterator iter = blackListMap.find(make_pair(ushort(i1), ushort(i2)));

		if (iter == blackListMap.end())
		{
			MatchIndex idx = GetMatchIndex(i1,  i2);
			m_matches.SetMatch(idx);

			int nMatches;
			fscanf_s(in_file, "%d", &nMatches);

			vector<KeypointMatch> matches;
			//int nMatches_rd = min(nMatches, MAXMATCHES);
			//float step = 1.0f * nMatches_rd/nMatches;
			//int mid = 0;
			for (int i = 0; i < nMatches; i++) 
			{
				int k1, k2;
				fscanf_s(in_file, "%d %d", &k1, &k2);

				//int j = int(floor(step * i));
				//if (j < mid) continue;

				KeypointMatch m;
				m.m_idx1 = unsigned short(k1);
				m.m_idx2 = unsigned short(k2);

				matches.push_back(m);
				//mid++;
			}
			m_matches.GetMatchList(idx) = matches;
		}
		else
		{
			int nMatches;
			fscanf_s(in_file, "%d", &nMatches);
			int k1, k2;
			for (int i = 0; i < nMatches; i++) 
			{
				fscanf_s(in_file, "%d %d", &k1, &k2);
			}
		}

	}
	fclose(in_file);
}
