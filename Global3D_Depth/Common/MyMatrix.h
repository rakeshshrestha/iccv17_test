#pragma once

#include "DataTypes.h"
#include <math.h>

#ifndef sign
template<class T>
inline int sign(const T& a)
{	return a > 0 ? 1 : (a < 0 ? -1 : 0);	}
#endif

/*	Utility*/
void RMatrix2ViewDir(float const*R, float *v);
void RViewDir2Matrix(float const*v, float *R);
void RMatrix2Angle(float const*R, float *angle);
void RAngle2Matrix(float const*angle, float *R);
void RMatrix2RodriguesPar(float const *R, float *RodriguesPar);
void RodriguesPar2RMatrix(float const *RodriguesPar, float *R);
void VectorCross(float const*v1, float const*v2, float *v);
void VectorNormalize(float *v, int n);
float VectorNorm(float const*v, int n);
void MatrixTranspose(float const*A, float *At, int n);
float VectorAngle(float const*v1, float const*v2, int n);
float VectorProduct(float const*v1, float const*v2, int n);
void MatrixMatrixProduct(int m, int n, int r, float const*A, float const*B, float *const C);
void MatrixScalarProduct(int m, int n, float const*A, float k, float *const B);
void MatrixTranspose(int m, int n, float const *A, float *B);
void NormalizeAngle(float &angle);
void VectorAddition(float const *v0, float const *v1, float *v2, int n);

void RMatrix2ViewDir(double const*R, double *v);
void RViewDir2Matrix(double const*v, double *R);
void RMatrix2Angle(double const*R, double *angle);
void RAngle2Matrix(double const*angle, double *R);
void RMatrix2RodriguesPar(double const *R, double *RodriguesPar);
void RodriguesPar2RMatrix(double const *RodriguesPar, double *R);
void VectorCross(double const*v1, double const*v2, double *v);
void VectorNormalize(double *v, int n);
double VectorNorm(double const*v, int n);
void MatrixTranspose(double const*A, double *At, int n);
double VectorAngle(double const*v1, double const*v2, int n);
double VectorProduct(double const*v1, double const*v2, int n);
void MatrixMatrixProduct(int m, int n, int r, double const*A, double const*B, double *const C);
void MatrixScalarProduct(int m, int n, double const*A, double k, double *const B);
void MatrixTranspose(int m, int n, double const *A, double *B);
void NormalizeAngle(double &angle);
void VectorAddition(double const *v0, double const *v1, double *v2, int n);

//////////////////////////////////
//FLOAT
//////////////////////////////////
inline void RMatrix2ViewDir(float const*R, float *v)
{
	v[0] = R[6];
	v[1] = R[7];
	v[2] = R[8];
}

inline void RMatrix2Angle(float const*R, float *angle)
{
	float twist /*Z*/, pan /*Y*/, tilt/* X*/; //right-handed ZXY intrinsic convention
	//R = RzRxRy
	//Z: c1, s1
	//X: c2, s2
	//Y: c3, s3
	//[c1*c3 + s1*s2*s3  c2*s1   c3*s1*s2-c1*s3 
	// c1*s2*s3-c3*s1     c1*c2   s1*s3+c1*c3*s2              
	//  c2*s3                     -s2         c2*c3]

	float c1, s1, c2, s2, c3, s3;
	s2 = -R[7]; 
	c2 = sqrt(1 - s2 * s2);

	if (c2 > 0.0001)
	{
		s1 = R[1]/c2;
		s3 = R[6]/c2;

		c3 = R[8]/c2;
		c1 = R[4]/c2;
	}
	else
	{
		//printf("Matrix to angle conversion is not unique, assume 0 twist!\n");
		s3 = 0.0; c3 = 1.0;
		s1 = R[3]; c1 = R[5]/s2;
	}

	//convert to angle: tilt is always between -pi/2 ~ pi/2
	s1 = min(1.0f, max(-1.0f, s1));
	s2 = min(1.0f, max(-1.0f, s2));
	s3 = min(1.0f, max(-1.0f, s3));

	//twist: c1, s1
	//tilt: c2, s2
	//pan: c3, s3
   //-pi/2 ~ pi/2
	twist = asin(s1);  
	if (c1 < 0) twist = float(Pi) - twist;

	tilt = asin(s2);

	pan = asin(s3); 
	if (c3 < 0) pan = float(Pi) - pan;

	NormalizeAngle(pan);

	angle[0] = tilt;
	angle[1] = pan;
	angle[2] = twist;
}

inline void RViewDir2Matrix(float const*v, float *R)
{
	R[6] = v[0];
	R[7] = v[1];
	R[8] = v[2];

	R[2] = 0;
	R[4] = sqrt(1-v[1] * v[1]);

	if (R[4] > 0.00001)
	{
		R[3] = -v[0] * v[1]/R[4];
		R[5] = -v[1] * v[2]/R[4];
	}
	else
	{
		R[5] = 1.0; 
		R[3] = R[4] = 0;
	}

	VectorCross(&R[3], &R[6], &R[0]);
	VectorNormalize(&R[0], 3);
	VectorNormalize(&R[3], 3);
}

inline void RAngle2Matrix(float const*angle, float *R)
{
	//R = RzRxRy
	//Z: c1, s1 twist
	//X: c2, s2 tilt
	//Y: c3, s3 pan
	//[c1*c3 + s1*s2*s3  c2*s1   c3*s1*s2-c1*s3 
	// c1*s2*s3-c3*s1     c1*c2   s1*s3+c1*c3*s2              
	//  c2*s3                     -s2         c2*c3]

	//2: c1, s1, 
	//0: c2, s2, 
	//1: c3, s3

	//tilt, pan, twist
	float c1, s1, c2, s2, c3, s3;
	c1 = cos(angle[2]); s1 = sin(angle[2]);  //twist
	c2 = cos(angle[0]); s2 = sin(angle[0]);  //tilt
	c3 = cos(angle[1]); s3 = sin(angle[1]);  //pan

	R[0] = c1*c3 + s1 * s2 * s3;
	R[1] = c2 * s1;
	R[2] = c3 * s1 * s2 - c1 * s3;
	R[3] = c1 * s2 * s3 - c3 * s1;
	R[4] = c1 * c2;
	R[5] = s1 * s3 + c1 * c3 * s2;
	R[6] = c2 * s3;
	R[7] = -s2;
	R[8] = c2 * c3;
}

inline void RMatrix2RodriguesPar(float const *R, float *RodriguesPar)
{
	float angle[3];
	RMatrix2Angle(R, angle);

	float c1, s1, c2, s2;
	c1 = cos(angle[0]/2);
	s1 = sin(angle[0]/2);
	c2 = cos(angle[1]/2);
	s2 = sin(angle[1]/2);
}

inline void RodriguesPar2RMatrix(float const *RodriguesPar, float *R)
{
	float a, b, c, d;
	a = RodriguesPar[0];
	b = RodriguesPar[1];
	c = RodriguesPar[2];
	d = RodriguesPar[3];

	R[0] = a*a + b*b - c*c - d*d;
	R[1] = 2 * (b*c - a*d);
	R[2] = 2 * (b*d + a*c);
	R[3] = 2 * (b*c + a*d);
	R[4] = a*a + c*c - b*b - d*d;
	R[5] = 2 * (c*d - a*b);
	R[6] = 2 * (b*d - a*c);
	R[7] = 2 * (c*d + a*b);
	R[8] = a*a + d*d - b*b - c*c;
}

inline void VectorCross(float const*v1, float const*v2, float *v)
{
	v[0] = v1[1] * v2[2] - v1[2] * v2[1];
	v[1] = v1[2] * v2[0] - v1[0] * v2[2];
	v[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

inline void VectorNormalize(float *const v, int n)
{
	float v_norm = 0;
	for (int i = 0 ; i < n; i++)
		v_norm += v[i] * v[i];
	v_norm = sqrt(v_norm);

	if (v_norm > 0)
	{
		for (int i = 0; i < n; i++)
			v[i] /= v_norm;
	}
}

inline float VectorNorm(float const*v, int n)
{
	float v_norm = 0;
	for (int i = 0 ; i < n; i++)
		v_norm += v[i] * v[i];
	v_norm = sqrt(v_norm);
	return v_norm;
}

inline void MatrixTranspose(float const*A, float *const At, int n)
{
	float *buf = new float[n * n];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			buf[i * n + j] = A[i + j * n];

	memcpy(At, buf, sizeof(float) * n * n);
	delete []buf;
}

inline float VectorProduct(float const*v1, float const*v2, int n)
{
	float prod = 0;
	for (int i = 0 ; i< n; i++)
		prod += v1[i] * v2[i];
	return prod;
}

inline float VectorAngle(float const*  v1, float const*  v2, int n)
{
	float prod = VectorProduct(v1, v2, n);
	float v1_n = VectorNorm(v1, n);
	float v2_n = VectorNorm(v2, n);

	if (v1_n > 0 && v2_n > 0)
	{
		prod /= v1_n * v2_n;
		prod = max(-1.0f, min(1.0f, prod));
	}
	else
		prod = 1;

	return acos(prod);
}

inline void MatrixMatrixProduct(int m, int n, int r, float const* A, float const* B, float *const C)
{
	float *buf = new float[m * r];
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < r; j++)
		{
			buf[i * r + j] = 0;
			for (int k = 0; k < n; k++)
				buf[i * r + j] += A[i * n + k] * B[k * r + j];
		}
	}

	for (int i = 0; i < m; i++)
		for (int j = 0; j < r; j++)
			C[i * r + j] = buf[i * r + j];

	delete []buf;
}

inline void MatrixScalarProduct(int m, int n, float const* A, float k, float *const C)
{
	for (int i = 0; i < m * n; i++)
		C[i] = A[i] * k;
}

inline void MatrixTranspose(int m, int n, float const *A, float *B)
{
	float *buf = new float[m * n];

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			buf[j * m + i] = A[i * n + j];
		}
	}

	memcpy(B, buf, sizeof(float) * m * n);
	delete []buf;
}

inline void NormalizeAngle(float &angle)
{
	while (angle < float(-Pi))
		angle += float(2 * Pi);
	while (angle > float(Pi))
		angle -= float(2 * Pi);
}

inline void VectorAddition(float const *v0, float const *v1, float *v2, int n)
{
	float *tmpv = new float[n];

	for (int i = 0; i < n; i++)
		tmpv[i] = v0[i] + v1[i];
	for (int i = 0; i < n; i++)
		v2[i] = tmpv[i];

	delete []tmpv;
}

//////////////////////////////////
//DOUBLE
//////////////////////////////////
inline void RMatrix2ViewDir(double const*R, double *v)
{
	v[0] = R[6];
	v[1] = R[7];
	v[2] = R[8];
}

inline void RMatrix2Angle(double const*R, double *angle)
{
	double twist /*Z*/, pan /*Y*/, tilt/* X*/; //right-handed ZXY intrinsic convention
	//R = RzRxRy
	//Z: c1, s1
	//X: c2, s2
	//Y: c3, s3
	//[c1*c3 + s1*s2*s3  c2*s1   c3*s1*s2-c1*s3 
	// c1*s2*s3-c3*s1     c1*c2   s1*s3+c1*c3*s2              
	//  c2*s3                     -s2         c2*c3]

	double c1, s1, c2, s2, c3, s3;
	s2 = -R[7]; 
	c2 = sqrt(1 - s2 * s2);

	if (c2 > 0.0001)
	{
		s1 = R[1]/c2;
		s3 = R[6]/c2;

		c3 = R[8]/c2;
		c1 = R[4]/c2;
	}
	else
	{
		//printf("Matrix to angle conversion is not unique, assume 0 twist!\n");
		s3 = 0.0; c3 = 1.0;
		s1 = R[3]; c1 = R[5]/s2;
	}

	//convert to angle: tilt is always between -pi/2 ~ pi/2
	s1 = min(1.0, max(-1.0, s1));
	s2 = min(1.0, max(-1.0, s2));
	s3 = min(1.0, max(-1.0, s3));

	//twist: c1, s1
	//tilt: c2, s2
	//pan: c3, s3
	//-pi/2 ~ pi/2
	twist = asin(s1);  
	if (c1 < 0) twist = Pi - twist;

	tilt = asin(s2);

	pan = asin(s3); 
	if (c3 < 0) pan = Pi - pan;

	NormalizeAngle(pan);

	angle[0] = tilt;
	angle[1] = pan;
	angle[2] = twist;
}

inline void RViewDir2Matrix(double const*v, double *R)
{
	R[6] = v[0];
	R[7] = v[1];
	R[8] = v[2];

	R[2] = 0;
	R[4] = sqrt(1-v[1] * v[1]);

	if (R[4] > 0.00001)
	{
		R[3] = -v[0] * v[1]/R[4];
		R[5] = -v[1] * v[2]/R[4];
	}
	else
	{
		R[5] = 1.0; 
		R[3] = R[4] = 0;
	}

	VectorCross(&R[3], &R[6], &R[0]);
	VectorNormalize(&R[0], 3);
	VectorNormalize(&R[3], 3);
}

inline void RAngle2Matrix(double const*angle, double *R)
{
	//R = RzRxRy
	//Z: c1, s1
	//X: c2, s2
	//Y: c3, s3
	//[c1*c3 + s1*s2*s3  c2*s1   c3*s1*s2-c1*s3 
	// c1*s2*s3-c3*s1     c1*c2   s1*s3+c1*c3*s2              
	//  c2*s3                     -s2         c2*c3]

	//2: c1, s1, 
	//0: c2, s2, 
	//1: c3, s3

	//tilt, pan, twist
	double c1, s1, c2, s2, c3, s3;
	c1 = cos(angle[2]); s1 = sin(angle[2]);  //twist
	c2 = cos(angle[0]); s2 = sin(angle[0]);  //tilt
	c3 = cos(angle[1]); s3 = sin(angle[1]);  //pan

	R[0] = c1*c3 + s1 * s2 * s3;
	R[1] = c2 * s1;
	R[2] = c3 * s1 * s2 - c1 * s3;
	R[3] = c1 * s2 * s3 - c3 * s1;
	R[4] = c1 * c2;
	R[5] = s1 * s3 + c1 * c3 * s2;
	R[6] = c2 * s3;
	R[7] = -s2;
	R[8] = c2 * c3;
}

inline void RMatrix2RodriguesPar(double const *R, double *RodriguesPar)
{
	double angle[3];
	RMatrix2Angle(R, angle);

	double c1, s1, c2, s2;
	c1 = cos(angle[0]/2);
	s1 = sin(angle[0]/2);
	c2 = cos(angle[1]/2);
	s2 = sin(angle[1]/2);
}

inline void RodriguesPar2RMatrix(double const *RodriguesPar, double *R)
{
	double a, b, c, d;
	a = RodriguesPar[0];
	b = RodriguesPar[1];
	c = RodriguesPar[2];
	d = RodriguesPar[3];

	R[0] = a*a + b*b - c*c - d*d;
	R[1] = 2 * (b*c - a*d);
	R[2] = 2 * (b*d + a*c);
	R[3] = 2 * (b*c + a*d);
	R[4] = a*a + c*c - b*b - d*d;
	R[5] = 2 * (c*d - a*b);
	R[6] = 2 * (b*d - a*c);
	R[7] = 2 * (c*d + a*b);
	R[8] = a*a + d*d - b*b - c*c;
}

inline void VectorCross(double const*v1, double const*v2, double *v)
{
	v[0] = v1[1] * v2[2] - v1[2] * v2[1];
	v[1] = v1[2] * v2[0] - v1[0] * v2[2];
	v[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

inline void VectorNormalize(double *const v, int n)
{
	double v_norm = 0;
	for (int i = 0 ; i < n; i++)
		v_norm += v[i] * v[i];
	v_norm = sqrt(v_norm);

	if (v_norm > 0)
	{
		for (int i = 0; i < n; i++)
			v[i] /= v_norm;
	}
}

inline double VectorNorm(double const*v, int n)
{
	double v_norm = 0;
	for (int i = 0 ; i < n; i++)
		v_norm += v[i] * v[i];
	v_norm = sqrt(v_norm);
	return v_norm;
}

inline void MatrixTranspose(double const*A, double *const At, int n)
{
	double *buf = new double[n * n];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			buf[i * n + j] = A[i + j * n];

	memcpy(At, buf, sizeof(double) * n * n);
	delete []buf;
}

inline double VectorProduct(double const*v1, double const*v2, int n)
{
	double prod = 0;
	for (int i = 0 ; i< n; i++)
		prod += v1[i] * v2[i];
	return prod;
}

inline double VectorAngle(double const*  v1, double const*  v2, int n)
{
	double prod = VectorProduct(v1, v2, n);
	double v1_n = VectorNorm(v1, n);
	double v2_n = VectorNorm(v2, n);
	prod /= v1_n * v2_n;
	prod = max(-1.0, min(1.0, prod));

	return acos(prod);
}

inline void NormalizeAngle(double &angle)
{
	while (angle < -Pi)
		angle += 2 * Pi;
	while (angle > Pi)
		angle -= 2 * Pi;
}

inline void MatrixMatrixProduct(int m, int n, int r, double const* A, double const* B, double *const C)
{
	double *buf = new double[m * r];
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < r; j++)
		{
			buf[i * r + j] = 0;
			for (int k = 0; k < n; k++)
				buf[i * r + j] += A[i * n + k] * B[k * r + j];
		}
	}

	for (int i = 0; i < m; i++)
		for (int j = 0; j < r; j++)
			C[i * r + j] = buf[i * r + j];

	delete []buf;
}

inline void MatrixScalarProduct(int m, int n, double const* A, double k, double *const C)
{
	for (int i = 0; i < m * n; i++)
		C[i] = A[i] * k;
}

inline void MatrixTranspose(int m, int n, double const *A, double *B)
{
	double *buf = new double[m * n];

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			buf[j * m + i] = A[i * n + j];
		}
	}

	memcpy(B, buf, sizeof(double) * m * n);
	delete []buf;
}

inline void VectorAddition(double const *v0, double const *v1, double *v2, int n)
{
	double *tmpv = new double[n];

	for (int i = 0; i < n; i++)
		tmpv[i] = v0[i] + v1[i];
	for (int i = 0; i < n; i++)
		v2[i] = tmpv[i];

	delete []tmpv;
}