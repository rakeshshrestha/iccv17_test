The "vocsearch" package contains source-code as well a pre-trained vocabulary tree
and is therefore ready to use. The source-code is contained in a few classes only and does not need
additional libraries.
The vocabulary tree is trained on SIFT features created with Andrea Vedaldis SIFT implementation with
is available at http://vision.ucla.edu/~vedaldi/code/sift/sift.html.
The Matlab script "extractsift.m" shows how the store the extracted SIFT features in the format used in the "vocsearch" package.
The vocsearch package includes an example program and example data.
The example program shows image search on part of the UKY dataset (http://www.vis.uky.edu/~stewe/ukbench/). The example
data consists of SIFT features extracted from the first 1000 images of the UKY dataset.
The included vocabulary tree was trained on a large number of images from the internet.


History:

v1.0: 07/22/2008