I1=imreadbw("imagename...") ;

I1=I1-min(I1(:)) ;
I1=I1/max(I1(:)) ;

[frames1,descr1,gss1,dogss1] = sift(I1,'verbosity',1) ;

% write position and descriptor
strout = "siftfilename..."; % file to which the Sift features of the image should be written
fout = fopen(strout,'w');
for i = 1:size(frames1,2)
    fwrite(fout, 255*descr1(:,i), 'uchar');
end
fclose(fout);
