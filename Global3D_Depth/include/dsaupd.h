#include <math.h>
#include "SuperLU/slu_ddefs.h"
#include <map>
#include <utility>
//#include "csparse/csparse.h"
#include "csparse/cs.h"
using namespace std;

extern "C" void dsaupd_(int *ido, char *bmat, int *n, char *which,
			int *nev, double *tol, double *resid, int *ncv,
			double *v, int *ldv, int *iparam, int *ipntr,
			double *workd, double *workl, int *lworkl,
			int *info);

extern "C" void dseupd_(int *rvec, char *All, int *select, double *d,
			double *z, int *ldz, double *sigma, 
			char *bmat, int *n, char *which, int *nev,
			double *tol, double *resid, int *ncv, double *v,
			int *ldv, int *iparam, int *ipntr, double *workd,
			double *workl, int *lworkl, int *ierr);

void dsaupd(int n, int nev, int nnz, map<pair<int, int>, double> &A_sparse, cs *A_cs, double *Evals, double *Evecs)
{

	/*//------------OUTPUT A----------------------------------------------------
	FILE *fp = NULL;
	fopen_s(&fp, "A.txt", "w");
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			map<pair<int, int>, double>::iterator miter;
			
			if (j < i)
				miter = A_sparse.find(make_pair(j, i));
			else
				miter = A_sparse.find(make_pair(i, j));

			if (miter != A_sparse.end())
			{
				fprintf_s(fp, "%f", float(miter->second));
			}
			else
				fprintf_s(fp, "0");

			if (j < n-1) fprintf_s(fp, " ");
			else
				fprintf_s(fp, "\n");
		}
	}
	fclose(fp);
	//------------------------------------------------------------------------*/

	int ido = 0;   
	double tol = 0.0; 

	double *resid;
	resid = new double[n];

	int ncv = 4 * nev; //EDIT
	if (ncv > n) ncv = n;

	double *v;
	int ldv = n;
	v = new double[ldv*ncv];

	int *iparam;
	iparam = new int[11]; 
	iparam[0] = 1;   // Specifies the shift strategy (1->exact)
	iparam[2] = 3*n; // Maximum number of iterations //EDIT

	iparam[3] = 1;
	iparam[6] = 1;   /* Sets the mode of dsaupd.
		      1 is exact shifting,
		      2 is user-supplied shifts,
		      3 is shift-invert mode,
		      4 is buckling mode,
		      5 is Cayley mode. */

	int *ipntr;
	ipntr = new int[11]; /* Indicates the locations in the work array workd
			  where the input and output vectors in the
			  callback routine are located. */
	ipntr[0] = 0;
	ipntr[1] = n;
	ipntr[2] = 2*n;

	double *workd;
	workd = new double[3*n];

	double *workl;

	int lworkl = ncv*(ncv+8); 
	workl = new double[lworkl];

	int info = 0; 
	int rvec = 1; 

	int *select;
	select = new int[ncv];
	double *d;
	d = new double[2*ncv]; //storage for eigenvalues

	double sigma = 0;
	int ierr;

#define  SHIFT_INVERT
#ifndef SHIFT_INVERT
	char bmat[2] = "I";
	char which[3] = "SM"; 
	while(1)
	{ 
		dsaupd_(&ido, bmat, &n, which, &nev, &tol, resid, 
			    &ncv, v, &ldv, iparam, ipntr, workd, workl,
			    &lworkl, &info);

		//fortran is one-based indexing
		if (ido == -1 || ido == 1) 
		{
			/*    
			%--------------------------------------------%   
			| Perform  y <-- OP*x = A * x   |   
			| to force the starting vector into the      |   
			| range of OP.                               |   
			|               x = workd(ipntr(1))          |   
			|               y = workd(ipntr(2))          |   
			%--------------------------------------------% */

			double *x = &workd[ipntr[0]-1];
			double *y = &workd[ipntr[1]-1];
			for (int i = 0; i < n; i++)
			{
				y[i] = 0;

				//y[i] = A(i, :)x
				for (int j = 0; j < n; j++)
				{
					map<pair<int, int>, double>::iterator miter;
					
					if (j < i)
						miter = A_sparse.find(make_pair(j, i));
					else
						miter = A_sparse.find(make_pair(i, j));

					if (miter != A_sparse.end())
						y[i] += miter->second * x[j];
				}
			}
	    }  
		else if (ido == 99)
		{
			break;
		}
	}
#else
	char bmat[2] = "G";
	char which[3] = "LM"; 

	iparam[6] = 3;   /* Sets the mode of dsaupd.
		      1 is exact shifting,
		      2 is user-supplied shifts,
		      3 is shift-invert mode,
		      4 is buckling mode,
		      5 is Cayley mode. */
	//------------------------------------------------------------------//                                    
	SuperMatrix A, AC, L, U, B;
	int *rowind = NULL, *colptr = NULL;
	double *values = NULL;

	if (!A_cs)
	{
		if ( !(rowind = intMalloc(nnz)) ) 
		{
			printf("[SuperLU]Malloc fails for rowind[].");
			fflush(stdout);
			exit(1);
				//ABORT("[SuperLU]Malloc fails for rowind[].");
		}
		if ( !(colptr = intMalloc(n+1)) )
		{
			printf("[SuperLU]Malloc fails for colptr[].");
			fflush(stdout);
			//ABORT("[SuperLU]Malloc fails for colptr[].");
		}
		if ( !(values = doubleMalloc(nnz)) ) 
		{
			printf("Malloc fails for values[].");
			fflush(stdout);
			//ABORT("Malloc fails for values[].");
		}

		//compute A-sigmaM = A
		int nzidx = 0;
		for (int j = 0; j < n; j++)
		{
			colptr[j] = nzidx;
			for (int i = 0; i < n; i++)
			{ 
				map<pair<int, int>, double>::iterator miter;

				if (j < i)
					miter = A_sparse.find(make_pair(j, i));
				else
					miter = A_sparse.find(make_pair(i, j));

				if (miter != A_sparse.end())
				{
					rowind[nzidx] = i;
					values[nzidx] = miter->second;
					nzidx++;
				}
			}
		}
		colptr[n] = nnz;
	}
	else
	{
		rowind = A_cs->i;
		colptr = A_cs->p;
		values = A_cs->x;
	}

	printf("[SuperLU]Create compressed column matrix.\n");
	fflush(stdout);
	dCreate_CompCol_Matrix(&A, n, n, nnz, values, rowind, colptr, SLU_NC, SLU_D, SLU_GE);

	//--------------------------------------------------------------------------------------
	superlu_options_t options; 	set_default_options(&options);	//options.ColPerm = MMD_AT_PLUS_A;
	SuperLUStat_t stat; StatInit(&stat);

	int *perm_r; /* row permutations from partial pivoting */
	int *perm_c; /* column permutation vector */
	int *etree;

	if ( !(perm_r = intMalloc(n)) ) ABORT("[SuperLU]Malloc fails for perm_r[].");
	if ( !(perm_c = intMalloc(n)) ) ABORT("[SuperLU]Malloc fails for perm_c[].");
	if ( !(etree = intMalloc(n)) ) ABORT("Malloc fails for etree[].");

	get_perm_c(1, &A, perm_c);
	sp_preorder(&options, &A, perm_c, etree, &AC);
	int panel_size = sp_ienv(1);
	int relax = sp_ienv(2);

	dgstrf(&options, &AC, relax, panel_size, etree, NULL, 0, perm_c, perm_r, 
		    &L, &U, &stat, &info);

	SUPERLU_FREE(etree);
	Destroy_CompCol_Permuted(&AC);

	printf("Solve eigen problem.\n");
	fflush(stdout);
	int nrhs = 1;
	while(1)
	{ 
		dsaupd_(&ido, bmat, &n, which, &nev, &tol, resid, 
			    &ncv, v, &ldv, iparam, ipntr, workd, workl,
			    &lworkl, &info);

		//fortran is one-based indexing
		if (ido == -1) 
		{
			//    
			//%--------------------------------------------%   
			//| Perform  y <-- OP*x = inv[A-sigma*M]*M*x   |   
			//| to force the starting vector into the      |   
			//| range of OP.                               |   
			//|               x = workd(ipntr(1))          |   
			//|               y = workd(ipntr(2))          |   
			//%--------------------------------------------% 

			dCreate_Dense_Matrix(&B, n, nrhs, &workd[ipntr[0]-1], n, SLU_DN, SLU_D, SLU_GE);
			dgstrs (NOTRANS, &L, &U, perm_c, perm_r, &B, &stat, &info);

			DNformat    *Xstore = (DNformat *)(B.Store);
			memcpy(&workd[ipntr[1]-1], (double *)(Xstore->nzval), sizeof(double)*n);

			Destroy_SuperMatrix_Store(&B);
	    }  
		else if (ido == 1) 
		{        
		//%-----------------------------------------%      
		//| Perform y <-- OP*x = inv[A-sigma*M]*M*x |      
		//| M*x has been saved in workd(ipntr(3)).  |      
		//|      M*x = workd(ipntr(3)               |   
		//|        y = workd(ipntr(2)).             |
		//%-----------------------------------------% 
		
		    dCreate_Dense_Matrix(&B, n, nrhs, &workd[ipntr[2]-1], n, SLU_DN, SLU_D, SLU_GE);
			dgstrs (NOTRANS, &L, &U, perm_c, perm_r, &B, &stat, &info);

			DNformat    *Xstore = (DNformat *)(B.Store);
			memcpy(&workd[ipntr[1]-1], (double *)(Xstore->nzval), sizeof(double)*n);

			Destroy_SuperMatrix_Store(&B);
		}
		else if (ido == 2) 
		{
			
			//%-----------------------------------------%
			//|  Perform  y <--- M*x                    |
			//|               x = workd(ipntr(1))       |
			//|               y = workd(ipntr(2))       |
			//%-----------------------------------------%*/
			
			memcpy(&workd[ipntr[1]-1], &workd[ipntr[0]-1], sizeof(double)*n);
		}
		else if (ido == 99)
		{
			break;
		}
	} 


	//delete []values;
	//delete []rowind;
	//delete []colptr;
	SUPERLU_FREE (perm_r);
	SUPERLU_FREE (perm_c);

	if (!A_cs)
	{
		Destroy_CompCol_Matrix(&A);
	}
	else
	{
		Destroy_SuperMatrix_Store(&A);
	}
	Destroy_SuperNode_Matrix(&L);
	Destroy_CompCol_Matrix(&U);
	StatFree(&stat);
#endif

	printf("Extract eigenvectors.\n");
	fflush(stdout);
	if (info == 0) //extract eigen values and compute eigenvectors
	{
		dseupd_(&rvec, "A", select, d, v, &ldv, &sigma, bmat,
			&n, which, &nev, &tol, resid, &ncv, v, &ldv,
			iparam, ipntr, workd, workl, &lworkl, &ierr);

		if (ierr!=0) 
		{
			printf("Error with dseupd, info = %d\n", ierr);
			printf("Check the documentation of dseupd.\n");
		} else if (info==1) 
		{
			printf("Maximum number of iterations reached.\n");
		} else if (info==3) 
		{
			printf("No shifts could be applied during implicit\n");
			printf("Arnoldi update, try increasing NCV.\n");
		}

	}
	else
	{
		switch(info)
		{
		case 1:
			{
				printf("Maximum number of iterations taken."
					"All possible eigenvalues has been found.\n");
				break;
			}
		case 2:
			{
				printf("No longer an informational error.\n");
				break;
			}
		case 3:
			{
				printf("No shifts could be applied during a cycle of"
					" the implicitly restarted Arnoldi iteration.\n");
				break;
			}
		case -1:
			{
				printf("N must be positive\n");
				break;
			}
		case -2:
			{
				printf("NEV must be positive\n");
				break;
			}
		case -3:
			{
				printf("NCV must be greater than NEV and less than or "
					   "equal to N.\n");
				break;
			}
		case -4:
			{
				printf("The maximum number of Arnoldi update iterations allowed"
					" must be greater than zero\n");
				break;
			}
		case -5:
			{
				printf("WHICH must be one of 'LM', 'SM', 'LA', 'SA' or 'BE'\n");
				break;
			}
		case -6:
			{
				printf("BMAT must be one of 'I' or 'G'\n");
				break;
			}
		case -7:
			{
				printf("Length of private work array WORKL is no sufficient\n");
				break;
			}
		case -8:
			{
				printf("Error return from trid. egigenvalue calculation.\n");
				break;
			}
		case -9:
			{
				printf("Starting vector is zero\n");
				break;
			}
		case -10:
			{
				printf("IPARAM(7) must be 1, 2, 3, 4, 5\n");
				break;
			}
		case -11:
			{
				printf("IPARAM(7) = 1 and BMAT = 'G' are incompatable.\n");
				break;
			}
		case -12:
			{
				printf("IPARAM(7) = 1 and BMAT = 'G' are incompatable\n");
				break;
			}
		case -13:
			{
				printf("NEV and WHICH = 'BE' are incompatable.\n");
				break;
			}
		default:
			{
				//printf("[ARPACK]Fatal error occurred. Fail to compute required eigenvectors\n");
				printf("Could not build an Arnoldi factorization.Not enough work space.\n"
					   "Current size of Arnoldi factorization is %d\n", iparam[4]);
			}
		}
	}
    
    for (int i = 0; i < nev; i++) 
		Evals[i] = d[i];

	memcpy(Evecs, v, sizeof(double)*nev*n);

    delete resid;
    delete v;
    delete iparam;
    delete ipntr;
    delete workd;
    delete workl;
    delete select;
    delete d;

	fflush(stdout);
// 	printf("Eigenvalues: %f %f %f\n", Evals[0], Evals[1], Evals[2]);
// 	printf("Eigenvectors: \n");
// 	for(int i = 0; i < n; i++)
// 	{
// 		printf("%f %f %f\n", Evecs[i], Evecs[i + n], Evecs[i + 2 * n]);
// 	}
}





