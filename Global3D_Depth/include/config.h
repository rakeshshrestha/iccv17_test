#ifndef PI
#define PI 3.1415926
#endif

#define MATCHING_SCORE_RATIO 0.6f

#define MINIMUM_RAW_CORRESPONDENCES 30
#define MINIMUM_INLIER_MATCHES_RATIO 0.3
#define MININUM_INLIER_CORRESPONDENCES 30

#define EG_INLIER_THRESHOLD   0.00195 //2.0f/1024
#define PANORAMA_INLIER_THRESHOLD 3.0
#define HOMOGRAPHY_INLIER_THRESHOLD 3.0
#define MINIMUM_HOMOGRAPHY_CORRESPONDENCES 30

#define MATCHING_MAX_KEYS 7168
#define MAXCORRESPONDENCE   1000

#define EG_RANSAC_ROUND 512
#define MIN_TRIANGULATE_ANGLE 3.0
#define MAXREPROERROR      4.0

const float  motion_t =  0.5;

#define MINCORRESPONDENCE 50

//#define VisualizeEnergy
#define nSelectPt 4
#define  select_motion  0.4

#define NO_DISTORTION

const double p_k = 0.05;
const int margin = 40;
const double view_change1 = 0.7; //0.7
const double view_change2 = 0.5; //0.5

const double min_match1 = 0.6;  //0.65
const double min_match2 = 0.5;  //0.55

const double view_angle = -0.5;
const int pixel_err_t = 20;

