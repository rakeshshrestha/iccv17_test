/* 
*  Copyright (c) 2011-2012  Nianjuan Jiang (g0700213@nus.edu.sg)
*    and the National University of Singapore
*/

// PairwiseEGs.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <time.h>
#include <string.h>
#include <stdio.h>

#include <map>
#include "Windows.h"
#include "atlimage.h"
#include "MatchPoint.h"
#include "config.h"

#include <iostream>
#include <vector>
#include <sstream>

#include "vector.h"
#include "5point.h"
#include "Epipolar.h"

using namespace std;
#define ADDIMAGE
//#define TRIANGULATE
#define RELEASE

FILE *EGsOutPutFile;

void printRt(double R[], double t[])
{
	printf("R: \n");
	for (int i = 0; i<3; i++)
	{
		for (int j=0; j<3; j++)
		{
			printf("%f ", R[3*i+j]);
		}
		printf("\n");
	}

	printf("t: \n");
	for (int i = 0; i<3; i++)
	{
		printf("%f ", t[i]);
	}
	printf("\n");
}

int main(int argc, char **argv)
{
	

	double E_THRESHOLD_NEW = 0.05;

	if (argc < 3) {
        std::cerr << "Usage: parser <num_points> <base_path>" << std::endl;
        return 1;
    }
    
    int nMatches = atoi(argv[1]);
    std::string base_path = argv[2];

    int num_iterations;
    int num_points_start, num_points_end, num_points_step;
    int noise_start, noise_end, noise_step;
    // camera indices
    int v1 = 0, v2 = 1;
    char buf[256];

    // ------------------------------------------------------------------------------- //
    std::stringstream bearing_vector_filename;
    bearing_vector_filename << base_path << "\\" << nMatches << "_bearing.in";
	std::cout << bearing_vector_filename.str().c_str() << std::endl;
    FILE * fptr_bearing_vector = fopen(bearing_vector_filename.str().c_str(), "r");
    if (!fptr_bearing_vector) {
        std::cerr << "ERROR: Unable to open bearing vector file" << std::endl;
		return 1;
    }

    std::stringstream ground_truth_filename;
    ground_truth_filename << base_path << "\\" << nMatches << "_ground_truth1.in";
    FILE * fptr_ground_truth = fopen(ground_truth_filename.str().c_str(), "r");
    if (!fptr_ground_truth) {
        std::cerr << "ERROR: Unable to open ground_truth_filename file" << std::endl;
		return 1;
    }

    std::stringstream meta_info_filename;
    meta_info_filename << base_path << "\\" <<  "meta.in";
    FILE * fptr_meta_info = fopen(meta_info_filename.str().c_str(), "r");
    if (!fptr_meta_info) {
        std::cerr << "ERROR: Unable to open meta info file" << std::endl;
		return 1;
    }

    // ------------------------------------------------------------------------------- //

	// Camera matrix (identity in our bearing vector case)
    double K1[9], K2[9];
    for (int i = 0; i < 9; i++) {
        K1[i] = K2[i] = 0;
    }
    K1[0] = K1[4] = K1[8] = 1;
    K2[0] = K2[4] = K2[8] = 1;
    
    // get meta info
    fgets(buf, 256, fptr_meta_info);
    sscanf(buf, "%d", &num_iterations);
    
    fgets(buf, 256, fptr_meta_info);
    sscanf(buf, "%d %d %d", &num_points_start, &num_points_end, &num_points_step);

    fgets(buf, 256, fptr_meta_info);
    sscanf(buf, "%d %d %d", &noise_start, &noise_end, &noise_step);

    // read the features
    double lx, ly, rx, ry;
    for (int noise = /*noise_start*/0; noise <= /*noise_end*/0; noise++) {
        for (int iteration_idx = 0; iteration_idx < /*num_iterations*/1; iteration_idx++) {
			double R_gt[9], t_gt[3];
			
			for (int i = 0; i < 3; i++) {
				fgets(buf, 256, fptr_ground_truth);
				sscanf(	buf, "%lf %lf %lf %lf", 
						&R_gt[3*i], &R_gt[3*i + 1], &R_gt[3*i + 2],
						&t_gt[i]
				);
			}
			
			v2_t *k1_pts = new v2_t[nMatches];
			v2_t *k2_pts = new v2_t[nMatches];
            
			for (int bearing_vector_idx = 0; bearing_vector_idx < nMatches; bearing_vector_idx++) {
                double bearing_vectors1[3]; 
                double bearing_vectors2[3];

                fgets(buf, 256, fptr_bearing_vector);
                sscanf( buf, "%lf %lf %lf %lf %lf %lf", 
                        &bearing_vectors1[0], &bearing_vectors1[1], &bearing_vectors1[2],
                        &bearing_vectors2[0], &bearing_vectors2[1], &bearing_vectors2[2]
                );

                // normalize the features
                lx = bearing_vectors1[0] / bearing_vectors1[2];
                ly = bearing_vectors1[1] / bearing_vectors1[2];

                rx = bearing_vectors2[0] / bearing_vectors2[2];
                ry = bearing_vectors2[1] / bearing_vectors2[2];

				// The coordinate used in bundler is different form normal ones
				k1_pts[bearing_vector_idx] = v2_new(lx-K1[2], -ly+K1[5]);
				k2_pts[bearing_vector_idx] = v2_new(rx-K2[2], -ry+K2[5]);

            }



			double R[9], t[3];
			double E[9];
			int num_inliers;
			// Nonlinear optimiziation results
			double R_r[9], t_r[3];
		
			computeRelativeMotion(k1_pts, k2_pts, nMatches, K1, K2, E_THRESHOLD_NEW, R, t, R_r, t_r, num_inliers, E);

			printf("\n[%d %d] have %d inliers among total %d feature matches.\n", v1,v2, num_inliers,nMatches);
			printf("Ransac output\n");
			printRt(R, t);

			printf("\nRefined R and t.\n");
			printRt(R_r, t_r);

			printf("\nGroundtruth output\n");
			printRt(R_gt, t_gt);

			printf("Ransac Rotation error\n");
			printf( "%lf\n", calculateRotationError(Eigen::Matrix3d(R_gt).transpose(), Eigen::Matrix3d(R)) );

			printf("Ransac Translation error\n");
			printf("%lf\n", calculateTranslationError(Eigen::Vector3d(t_gt), Eigen::Vector3d(t)));
			
			printf("Optimization Rotation error\n");
			printf("%lf\n", calculateRotationError(Eigen::Matrix3d(R_gt).transpose(), Eigen::Matrix3d(R)) );

			printf("Optimization Translation error\n");
			printf("%lf\n", calculateTranslationError(Eigen::Vector3d(t_gt), Eigen::Vector3d(t_r)));

        }
    }

    fclose(fptr_bearing_vector);
    fclose(fptr_ground_truth);
    fclose(fptr_meta_info);
	
	
	return true;
}

