#include "Epipolar.h"

#include <assert.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <limits.h>
#include <cstdarg>
#include <cctype>

#include "5point.h"

//#include "defines.h"
#include "fmatrix.h"
#include "matrix.h"
#include "triangulate.h"
#include "vector.h"
#include "levmar/levmar.h"
#include "config.h"

#include "eigen/Eigen/Geometry"

extern FILE * EGsOutPutFile;

double calculateRotationError(  Eigen::Matrix3d truerotation,
                                Eigen::Matrix3d measuredrotation    )
{
    // rotation error
    Eigen::Matrix3d rotationdiff = measuredrotation.transpose() * truerotation;
    Eigen::AngleAxisd anglediff;
	anglediff.fromRotationMatrix(rotationdiff);

	return anglediff.angle();
}

double calculateTranslationError( Eigen::Vector3d trueTranslation,
                                Eigen::Vector3d measuredTranslation)
{
    // translation error
    Eigen::Vector3d originalTranslation = measuredTranslation;

    measuredTranslation.normalize();
    trueTranslation.normalize();

    double error = min(std::acos(measuredTranslation.dot(trueTranslation)),
                            std::acos(measuredTranslation.dot(-trueTranslation)));
    if (_isnan(error)) {
        // std::cout << "[Error] " << trueTranslation << originalTranslation << "Error is NaN" << std::endl;
        error = 3.1416; // high error
    }

    return error;
}

//void calculateError(Eigen::Matrix3d trueRotation,
//                    Eigen::Vector3d trueTranslation,
//                    Eigen::Matrix3d measuredRotation,
//                    Eigen::Vector3d measuredTranslation,
//                    double &errorAngle,
//                    double &errorTranslation)
//{
//    // rotation error
//    errorAngle = calculateRotationError(trueRotation, measuredRotation);
//        
//    // translation error
//    errorTranslation = calculateTranslationError(trueTranslation, measuredTranslation);
//}

void computeRelativeMotion(	v2_t *k1_pts, v2_t *k2_pts, int nMatches, double K1[9], double K2[9], double E_THRESHOLD_NEW, 
							double R_ransac[], double t_ransac[], double R_optimized[], double t_optimized[], int &num_inliers, double E_best[]	)
{
	num_inliers = compute_pose_ransac_with_E(
		nMatches, k1_pts, k2_pts, 
		K1, K2, E_THRESHOLD_NEW, 50, R_ransac, t_ransac, E_best
	); // 50 is the ransac round and you may change it.

	
	//change coordinate system
	R_ransac[1] = -R_ransac[1]; R_ransac[2] = -R_ransac[2];
	R_ransac[3] = -R_ransac[3]; R_ransac[6] = -R_ransac[6];
	t_ransac[1] = -t_ransac[1]; t_ransac[2] = -t_ransac[2];

	// Do the nonlinear optimiziation 
	for (int i = 0; i < nMatches; i++) 
	{
		k1_pts[i].p[0] /= K1[0];
		k1_pts[i].p[1] = -k1_pts[i].p[1]/K1[0];

		k2_pts[i].p[0] /= K1[0];
		k2_pts[i].p[1] = -k2_pts[i].p[1]/K1[0];
	}

	RefineRtNonlinear(nMatches, k1_pts, k2_pts, K1[0], K2[0], R_ransac, t_ransac, R_optimized, t_optimized);
}

int getTwoRotationsFromE(double *E, 
						double *Ra, double *Rb)
{
    double tmp[9], tmp2[3], Qv[3];
    double U[9], S[3], VT[9];
    
    double D[9] =
	{  0.0, 1.0, 0.0,
	  -1.0, 0.0, 0.0,
	   0.0, 0.0, 1.0 };

    double DT[9] =
	{  0.0, -1.0, 0.0,
	   1.0,  0.0, 0.0,
	   0.0,  0.0, 1.0 };

    double I[9] =
	{  1.0, 0.0, 0.0,
	   0.0, 1.0, 0.0,
	   0.0, 0.0, 1.0 };

    double t0[3] = { 0.0, 0.0, 0.0 };

    /* Now find the SVD of E */
    dgesvd_driver(3, 3, E, U, S, VT);

    matrix_product33(U, D, tmp);
    matrix_product33(tmp, VT, Ra);
    matrix_product33(U, DT, tmp);

    matrix_product33(tmp, VT, Rb);

    if (matrix_determinant3(Ra) < 0.0) {
		matrix_scale(3, 3, Ra, -1.0, Ra);
    }

    if (matrix_determinant3(Rb) < 0.0) {
		matrix_scale(3, 3, Rb, -1.0, Rb);
    }
}

void computeEssentialFromRT(double *E, double *R, double *t)
{
	double tx[9];
	tx[0] = tx[4] = tx[8] = 0;
	tx[1] = -t[2]; tx[3] = -tx[1];
	tx[2] = t[1]; tx[6] = -tx[2];
	tx[5] = -t[0]; tx[7] = -tx[5];
	MatrixMatrixProduct(3, 3, 3, tx, R, E);
}

void rotationMatrixfromRodrigues(double *omega, double *R)
{
	double const theta = VectorNorm(omega, 3);
	memset(R, 0, sizeof(double) * 9);
	R[0] = R[4] = R[8] = 1;
	if (fabs(theta) > 1e-6)
	{
		double J[9], J2[9];
		J[0] = J[4] = J[8] = 0;
		J[1] = -omega[2]; J[3] = -J[1];
		J[2] = omega[1]; J[6] = -J[2];
		J[5] = -omega[0]; J[7] = -J[5];

		MatrixMatrixProduct(3, 3, 3, J, J, J2);

		double const c1 = sin(theta)/theta;
		double const c2 = (1-cos(theta))/(theta*theta);
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 3; ++j)
				R[i * 3 + j] += c1 * J[i * 3 + j] + c2 * J2[i * 3 + j];
	}
}

void UpdateRT(double *p, double *R, double *t)
{
	//update R and t
	double *iniR = m_global_data.ini_R;
	double *iniT = m_global_data.ini_t;

	double dR[9];
	rotationMatrixfromRodrigues(p, dR);
	MatrixMatrixProduct(3, 3, 3, iniR, dR, R);

	t[0] = iniT[0] + p[3];
	t[1] = iniT[1] + p[4];
	t[2] = iniT[2] + p[5];

	VectorNormalize(t, 3);
}

void evaluateRT(double *par, double *hx, int m, int n, void *adata)
{
	RT_global_data *_global_data = (RT_global_data*)(adata);
	double R[9], t[3], E[9], Et[9], Ep[3], Etq[3];

	UpdateRT(par, R, t);
	computeEssentialFromRT(E, R, t);
	MatrixTranspose(E, Et, 3);

	double p[3], q[3];
	p[2] = 1; q[2] = 1;
	//double error = 0;
	for (int i = 0; i < n; i++)
	{
		p[0] = double(_global_data->l_pts[i].p[0]);
		p[1] = double(_global_data->l_pts[i].p[1]);

		q[0] = double(_global_data->r_pts[i].p[0]);
		q[1] = double(_global_data->r_pts[i].p[1]);

		MatrixMatrixProduct(3, 3, 1, E, p, Ep);
		MatrixMatrixProduct(3, 3, 1, Et, q, Etq);

		double const num   = q[0]*Ep[0] + q[1]*Ep[1] + Ep[2];
		double const denom = sqrt(Ep[0]*Ep[0] + Ep[1]*Ep[1] + Etq[0]*Etq[0] + Etq[1]*Etq[1]);

		hx[i] = num/denom * _global_data->weights[i];
		//error += hx[i] * hx[i];
	} 
	//printf("||e||^2 : %f\n", error);
}

void fillWeights(int num_pts, v2_t *l_pts, v2_t *r_pts, 
				 double fl, double fr, double *R, double *t, double *w, double sampsonThreshold)
{
	double E[9], F[9], Ft[9], invKrt[9], invKl[9];
	computeEssentialFromRT(E, R, t);

	memset(invKl, 0, sizeof(double) * 9);
	memset(invKrt, 0, sizeof(double) * 9);
	invKl[0] = invKl[4] = 1/fl; invKl[8] = 1;
	invKrt[0] = invKrt[4] = 1/fr; invKrt[8] = 1;

	MatrixMatrixProduct(3, 3, 3, invKrt, E, F);
	MatrixMatrixProduct(3, 3, 3, F, invKl, F);
	MatrixTranspose(F, Ft, 3);

	double Fp[3], Ftq[3];
	double p[3], q[3];
	p[2] = q[2] = 1;
	for (int i = 0; i < num_pts; i++)
	{
		p[0] = l_pts[i].p[0] * fl;
		p[1] = l_pts[i].p[1] * fl;
		q[0] = r_pts[i].p[0] * fr;
		q[1] = r_pts[i].p[1] * fr;

		MatrixMatrixProduct(3, 3, 1, F, p, Fp);
		MatrixMatrixProduct(3, 3, 1, Ft, q, Ftq);

		double const num   = q[0]*Fp[0] + q[1]*Fp[1] + Fp[2];
		double const denom = sqrt(Fp[0]*Fp[0] + Fp[1]*Fp[1] + Ftq[0]*Ftq[0] + Ftq[1]*Ftq[1]);

		double const e = num/denom;

		///cui 20130312
		//deal with the outlier

		/*if (abs(e)<sampsonThreshold*2)
		{
			w[i] = (abs(e) < sampsonThreshold) ? 1.0 : sqrt(sampsonThreshold / abs(e));
		}
		else
		{
			w[i]=0;
		} */
		w[i] = (abs(e) < sampsonThreshold) ? 1.0 : sqrt(sampsonThreshold / abs(e));
		
	} 
}

void calJacobian(double *par, double *jacb, int m, int n, void *adata)
{
	RT_global_data *_global_data = (RT_global_data*)(adata);
	double R[9], t[3];
	double E[9], Et[9];

	UpdateRT(par, R, t);
	computeEssentialFromRT(E, R, t);
	MatrixTranspose(E, Et, 3);

	double innerDer[9*6]; // dE/dOmega and dE/dt
	memset(innerDer, 0, sizeof(double) * 9 * 6);

	// dE/dOmega_x
	innerDer[0 * 6 + 0] = 0; innerDer[1 * 6 + 0] = E[2]; innerDer[2 * 6 + 0] = -E[1];
	innerDer[3 * 6 + 0] = 0; innerDer[4 * 6 + 0] = E[5]; innerDer[5 * 6 + 0] = -E[4];
	innerDer[6 * 6 + 0] = 0; innerDer[7 * 6 + 0] = E[8]; innerDer[8 * 6 + 0] = -E[7];

	// dE/dOmega_y
	innerDer[0 * 6 + 1] = -E[2]; innerDer[1 * 6 + 1] = 0; innerDer[2 * 6 + 1] = E[0];
	innerDer[3 * 6 + 1] = -E[5]; innerDer[4 * 6 + 1] = 0; innerDer[5 * 6 + 1] = E[3];
	innerDer[6 * 6 + 1] = -E[8]; innerDer[7 * 6 + 1] = 0; innerDer[8 * 6 + 1] = E[6];

	// dE/dOmega_z
	innerDer[0 * 6 + 2] = E[1]; innerDer[1 * 6 + 2] = -E[0]; innerDer[2 * 6 + 2] = 0;
	innerDer[3 * 6 + 2] = E[4]; innerDer[4 * 6 + 2] = -E[3]; innerDer[5 * 6 + 2] = 0;
	innerDer[6 * 6 + 2] = E[7]; innerDer[7 * 6 + 2] = -E[6]; innerDer[8 * 6 + 2] = 0;

	// dE/dT_x
	innerDer[0 * 6 + 3] = 0;       innerDer[1 * 6 + 3] = 0;       innerDer[2 * 6 + 3] = 0;
	innerDer[3 * 6 + 3] = -R[6]; innerDer[4 * 6 + 3] = -R[7]; innerDer[5 * 6 + 3] = -R[8];
	innerDer[6 * 6 + 3] = R[3];  innerDer[7 * 6 + 3] = R[4];   innerDer[8 * 6 + 3] = R[5];

	// dE/dT_y
	innerDer[0 * 6 + 4] = R[6];   innerDer[1 * 6 + 4] = R[7];  innerDer[2 * 6 + 4] = R[8];
	innerDer[3 * 6 + 4] = 0;       innerDer[4 * 6 + 4] = 0;       innerDer[5 * 6 + 4] = 0;
	innerDer[6 * 6 + 4] = -R[0]; innerDer[7 * 6 + 4] = -R[1]; innerDer[8 * 6 + 4] = -R[2];

	// dE/dT_z
	innerDer[0 * 6 + 5] = -R[3]; innerDer[1 * 6 + 5] = -R[4]; innerDer[2 * 6 + 5] = -R[5];
	innerDer[3 * 6 + 5] = R[0];  innerDer[4 * 6 + 5] = R[1];   innerDer[5 * 6 + 5] = R[2];
	innerDer[6 * 6 + 5] = 0;      innerDer[7 * 6 + 5] = 0;        innerDer[8 * 6 + 5] = 0;

	double Ep[3], Etq[3];
	double outerDer[9]; //dSampson/dE
	double numDer[9]; //dNum/dE = d(qt*E*p)/dE
	double sqrDenomDer[9];
	double derivative[6];
	double p[3], q[3];
	p[2] = 1; q[2] = 1;
	for (int i = 0; i < n; ++i)
	{
		p[0] = double(_global_data->l_pts[i].p[0]);
		p[1] = double(_global_data->l_pts[i].p[1]);

		q[0] = double(_global_data->r_pts[i].p[0]);
		q[1] = double(_global_data->r_pts[i].p[1]);

		MatrixMatrixProduct(3, 3, 1, E, p, Ep);
		MatrixMatrixProduct(3, 3, 1, Et, q, Etq);

		double num = q[0] * Ep[0] + q[1]*Ep[1] + Ep[2];

		numDer[0] = q[0] * p[0]; numDer[1] = q[0] * p[1]; numDer[2] = q[0];
		numDer[3] = q[1] * p[0]; numDer[4] = q[1] * p[1]; numDer[5] = q[1];
		numDer[6] = p[0]         ;  numDer[7] = p[1];          numDer[8] = 1;

		sqrDenomDer[0] = 2*Ep[0]*p[0] + 2*Etq[0]*q[0];
		sqrDenomDer[1] = 2*Ep[0]*p[1] + 2*Etq[1]*q[0];
		sqrDenomDer[2] = 2*Ep[0];
		sqrDenomDer[3] = 2*Ep[1]*p[0] + 2*Etq[0]*q[1];
		sqrDenomDer[4] = 2*Ep[1]*p[1] + 2*Etq[1]*q[1];
		sqrDenomDer[5] = 2*Ep[1];
		sqrDenomDer[6] = 2*Etq[0];
		sqrDenomDer[7] = 2*Etq[1];
		sqrDenomDer[8] = 0.0;

		double const sqrDenom = Ep[0]*Ep[0] + Ep[1]*Ep[1] + Etq[0]*Etq[0] + Etq[1]*Etq[1];
		double const denom = sqrt(sqrDenom);
		double const invDenom = 1.0 / denom;
		double const invSqrDenom = 1.0 / sqrDenom;

		for (int j = 0; j < 9; ++j)
			outerDer[j] = (numDer[j]*denom - num*0.5*invDenom*sqrDenomDer[j]) * invSqrDenom;

		MatrixMatrixProduct(1, 9, 6, outerDer, innerDer, derivative);

		for (int j = 0; j < 6; ++j) 
			jacb[i * 6 + j] = derivative[j];
	} // end for (i)

// 	printf("Jacob for parameter: %f %f %f %f %f %f\n", par[0], par[1], par[2], par[3], par[4], par[5]);
// 	for (int i = 0; i < 9; i++)
// 	{
// 		for (int j =0 ; j < 6; j++)
// 			printf("%f ", jacb[i * 6 + j]);
// 		printf("\n");
// 	}

	fflush(stdout);
} 

void RefineRtNonlinear
(int num_pts, v2_t *l_pts, v2_t *r_pts, double fl, double fr, 
 double *R0, double *t0, double *R_out, double *t_out)
{
	memcpy(m_global_data.ini_R, R0, sizeof(double) * 9);
	memcpy(m_global_data.ini_t, t0, sizeof(double) * 3);

	m_global_data.r_pts = r_pts;
	m_global_data.l_pts = l_pts;
	m_global_data.fr = fr;
	m_global_data.fl = fl;
	m_global_data.weights = new double[num_pts];
	fillWeights(num_pts, l_pts, r_pts, fl, fr, R0, t0, m_global_data.weights, E_THRESHOLD/2);

	void (*func)(double *, double *, int , int , void*) = &evaluateRT;
	void (*jacf)(double *, double *, int, int, void*) = &calJacobian;

	double p[6]; //R and T
	memset(p, 0, sizeof(double) * 6);

	int itmax = 100;
	double info[LM_INFO_SZ];

//	dlevmar_dif(func, p, NULL, 6, num_pts, itmax, NULL, info, NULL, NULL, (void *)(&m_global_data));
 	dlevmar_der(func, jacf, p, NULL, 6, num_pts, itmax, NULL, 
  		info, NULL, NULL, (void *)(&m_global_data));
	delete []m_global_data.weights;

	int stop_cond = int(info[6]);
	/*switch(stop_cond)
	{
	case 1:
		{
			printf("LM: stopped by small gradient\n");
			break;
		}
	case 2:
		{
			printf("LM: stopped by small Dp. \n");
			break;
		}
	case 3:
		{
			printf("LM: stopped by itmax.\n");
			break;
		}
	case 4:
		{
			printf("LM: singular matrix. Restart from current p with increased \mu. \n");
			break;
		}
	case 5:
		{
			printf("LM: no further error reduction is possible. Restart with increased mu. \n");
			break;
		}
	case 6:
		{
			printf("LM: stopped by small ||e||_2");
			break;
		}
	case 7:
		{
			printf("LM: stopped by invalid (i.e. NaN or Inf) \"func\" values");
			break;
		}
	}*/
	UpdateRT(p, R_out, t_out);

	m_global_data.l_pts = NULL;
	m_global_data.r_pts = NULL;
}

/* Estimate an E-matrix from a given set of point matches */
std::vector<int> EstimateEMatrix(const std::vector<Keypoint> &k1, 
								 const std::vector<Keypoint> &k2, 
								 std::vector<KeypointMatch> matches, 
								 int num_trials, double threshold, 
								 double f1, double f2, 
								 double *E, double *F)
{
	int num_keys1 = (int)k1.size();
	int num_keys2 = (int)k2.size();

	std::vector<Keypoint> k1_norm, k2_norm;
	k1_norm.resize(num_keys1);
	k2_norm.resize(num_keys2);

	for (int i = 0; i < num_keys1; i++) {
		Keypoint k;
		k.m_x = float(k1[i].m_x / f1);
		k.m_y = float(k1[i].m_y / f1);
		k1_norm[i] = k;
	}

	for (int i = 0; i < num_keys2; i++) {
		Keypoint k;
		k.m_x = float(k2[i].m_x / f2);
		k.m_y = float(k2[i].m_y / f2);
		k2_norm[i] = k;
	}

	double scale = 0.5 * (f1 + f2);

	std::vector<int> inliers = 
		EstimateFMatrix(k1_norm, k2_norm, matches, num_trials, 
		threshold / (scale * scale), E, true);

	double K1_inv[9] = { 1.0 / f1, 0.0, 0.0, 
		0.0, 1.0 / f1, 0.0,
		0.0, 0.0, 1.0 };
	double K2_inv[9] = { 1.0 / f2, 0.0, 0.0, 
		0.0, 1.0 / f2, 0.0,
		0.0, 0.0, 1.0 };

	double tmp[9];
	matrix_product(3, 3, 3, 3, K1_inv, E, tmp);
	matrix_product(3, 3, 3, 3, K2_inv, tmp, F);

	return inliers;
}

/* Estimate relative pose from a given set of point matches */
int EstimatePose5Point(const std::vector<Keypoint> &k1, 
					   const std::vector<Keypoint> &k2, 
					   std::vector<KeypointMatch> matches, 
					   int num_trials, double threshold, 
					   double *cam_In1, double *cam_In2, 
					   double *R, double *t)
{
	int num_pts = (int) matches.size();

	v2_t *k1_pts = new v2_t[num_pts];
	v2_t *k2_pts = new v2_t[num_pts];

	for (int i = 0; i < num_pts; i++) 
	{
		unsigned short idx1 = matches[i].m_idx1;
		unsigned short idx2 = matches[i].m_idx2;

		k1_pts[i] = v2_new(k1[idx1].m_x - cam_In1[1], -k1[idx1].m_y + cam_In1[2]);
		k2_pts[i] = v2_new(k2[idx2].m_x - cam_In2[1], -k2[idx2].m_y + cam_In2[2]);
	}

	double K1[9], K2[9];
	K1[0] = K1[4] = cam_In1[0]; K1[8] = 1.0;
	K1[1] = K1[2] = K1[3] = K1[5] = K1[6] = K1[7] = 0;

	K2[0] = K2[4] = cam_In2[0]; K2[8] = 1.0;
	K2[1] = K2[2] = K2[3] = K2[5] = K2[6] = K2[7] = 0;

	double R0[9], t0[3];
	int num_inliers = compute_pose_ransac(num_pts, k1_pts, k2_pts, 
		K1, K2, threshold, num_trials, R0, t0);

	//change coordinate system
	R0[1] = -R0[1]; R0[2] = -R0[2];
	R0[3] = -R0[3]; R0[6] = -R0[6];
	t0[1] = -t0[1]; t0[2] = -t0[2];
	fprintf_s(EGsOutPutFile,"R0, t0: \n");
 	fprintf_s(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
 		       R0[0], R0[1], R0[2],R0[3],R0[4],R0[5],R0[6],R0[7],R0[8], t0[0], t0[1], t0[2]);
 
	for (int i = 0; i < num_pts; i++) 
	{
		k1_pts[i].p[0] /= cam_In1[0];
		k1_pts[i].p[1] = -k1_pts[i].p[1]/cam_In1[0];

		k2_pts[i].p[0] /= cam_In2[0];
		k2_pts[i].p[1] = -k2_pts[i].p[1]/cam_In2[0];
	}

#if 1
	RefineRtNonlinear(num_pts, k1_pts, k2_pts, cam_In1[0], cam_In2[0], R0, t0, R, t);
#else
	memcpy(R, R0, sizeof(double)*9);
	memcpy(t, t0, sizeof(double)*3);
#endif

 	printf("R, t: \n");
 	printf("%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
 		R[0], R[1], R[2],R[3],R[4],R[5],R[6],R[7],R[8], t[0], t[1], t[2]);
 	fflush(stdout);

	delete [] k1_pts;
	delete [] k2_pts;
	return num_inliers;
}

/* Estimate an F-matrix from a given set of point matches */
std::vector<int> EstimateFMatrix(const std::vector<Keypoint> &k1, 
								 const std::vector<Keypoint> &k2, 
								 std::vector<KeypointMatch> matches, 
								 int num_trials, double threshold, 
								 double *F, bool essential)
{
	int num_pts = (int) matches.size();

	/* num_pts should be greater than a threshold */
	if (num_pts < 20) {
		std::vector<int> inliers;
		return inliers;
	}

	v3_t *k1_pts = new v3_t[num_pts];
	v3_t *k2_pts = new v3_t[num_pts];

	v3_t *k1_pts_in = new v3_t[num_pts];
	v3_t *k2_pts_in = new v3_t[num_pts];

	for (int i = 0; i < num_pts; i++) {
		int idx1 = matches[i].m_idx1;
		int idx2 = matches[i].m_idx2;

		assert(idx1 < (int) k1.size());
		assert(idx2 < (int) k2.size());

		k1_pts[i] = v3_new(k1[idx1].m_x, k1[idx1].m_y, 1.0);
		k2_pts[i] = v3_new(k2[idx2].m_x, k2[idx2].m_y, 1.0);
	}

	estimate_fmatrix_ransac_matches(num_pts, k2_pts, k1_pts, 
		num_trials, threshold, 0.95,
		(essential ? 1 : 0), F);

	/* Find the inliers */
	std::vector<int> inliers;

	for (int i = 0; i < num_pts; i++) {
		double dist = fmatrix_compute_residual(F, k2_pts[i], k1_pts[i]);
		if (dist < threshold) {
			inliers.push_back(i);
		}
	}

	/* Re-estimate using inliers */
	int num_inliers = (int) inliers.size();

	for (int i = 0; i < num_inliers; i++) {
		k1_pts_in[i] = k1_pts[inliers[i]]; // v3_new(k1[idx1]->m_x, k1[idx1]->m_y, 1.0);
		k2_pts_in[i] = k2_pts[inliers[i]]; // v3_new(k2[idx2]->m_x, k2[idx2]->m_y, 1.0);
	}

	// printf("[1] num_inliers = %d\n", num_inliers);

#if 0
	double F0[9];
	double e1[3], e2[3];
	estimate_fmatrix_linear(num_inliers, k2_pts_in, k1_pts_in, F0, e1, e2);

	inliers.clear();
	for (int i = 0; i < num_pts; i++) {
		double dist = fmatrix_compute_residual(F0, k2_pts[i], k1_pts[i]);
		if (dist < threshold) {
			inliers.push_back(i);
		}
	}
	num_inliers = inliers.size();
	// printf("[2] num_inliers = %d\n", num_inliers);

	// matrix_print(3, 3, F0);
#else
	double F0[9];
	memcpy(F0, F, sizeof(double) * 9);
#endif

	if (!essential) {
		/* Refine using NLLS */
		for (int i = 0; i < num_inliers; i++) {
			k1_pts_in[i] = k1_pts[inliers[i]];
			k2_pts_in[i] = k2_pts[inliers[i]];
		}

		refine_fmatrix_nonlinear_matches(num_inliers, k2_pts_in, k1_pts_in, 
			F0, F);
	} else {
		memcpy(F, F0, sizeof(double) * 9);
	}

#if 0
	if (essential) {
		/* Compute the SVD of F */
		double U[9], S[3], VT[9];
		dgesvd_driver(3, 3, F, U, S, VT);
		double E0[9] = { 1.0, 0.0, 0.0,
			0.0, 1.0, 0.0,
			0.0, 0.0, 0.0 };

		double tmp[9];
		matrix_product(3, 3, 3, 3, U, E0, tmp);
		matrix_product(3, 3, 3, 3, tmp, VT, F);
	}
#endif

	inliers.clear();
	for (int i = 0; i < num_pts; i++) {
		double dist = fmatrix_compute_residual(F, k2_pts[i], k1_pts[i]);
		if (dist < threshold) {
			inliers.push_back(i);
		}
	}
	num_inliers = (int) inliers.size();

	delete [] k1_pts;
	delete [] k2_pts;
	delete [] k1_pts_in;
	delete [] k2_pts_in;

	return inliers;
}

std::vector<int> EstimateFMatrix(const std::vector<KeypointWithDesc> &k1, 
								 const std::vector<KeypointWithDesc> &k2, 
								 std::vector<KeypointMatch> matches, 
								 int num_trials, double threshold, 
								 double *F, bool essential)
{
	int num_keys1 = (int) k1.size();
	int num_keys2 = (int) k2.size();

	std::vector<Keypoint> k1_prime, k2_prime;

	k1_prime.resize(num_keys1);
	k2_prime.resize(num_keys2);

	for (int i = 0; i < num_keys1; i++) {
		Keypoint k(k1[i].m_x, k1[i].m_y);
		k1_prime[i] = k;
	}

	for (int i = 0; i < num_keys2; i++) {
		Keypoint k(k2[i].m_x, k2[i].m_y);
		k2_prime[i] = k;
	}

	return EstimateFMatrix(k1_prime, k2_prime, matches, num_trials, 
		threshold, F, essential);
}


bool LoadCalibratioinFile(const char* filename, vector<v3_t> & cam_intrinsics)
{
	FILE* fptr = fopen(filename, "r");
	if (fptr == NULL) 
	{
		return false;
	}

	double f, cx, cy;
	char buf[256];

	while (fgets(buf, 256, fptr))
	{
		sscanf(buf, "%lf %lf %lf", &f, &cx, &cy);
		cam_intrinsics.push_back(v3_new(f, cx, cy));
	}

	fclose(fptr);
	return true;

}