#pragma once

#include <vector>
#include "ff_voctree.h"
#include "ff_database.h"
#include "ff_sort.h"

#define NMATCH 80
#define MAXNIMG 20000
#define MAXNVW 1000

class VocTree
{
public:
	VocTree(void);
	~VocTree(void);

	void Clean();
	 int LoadVocTree(char *filename);
	 int FindImageCandidates(char *keyfile_in, int **match_id_p, int &nm);

private:
	ff_voctree voctree;
	ff_database db;
};
