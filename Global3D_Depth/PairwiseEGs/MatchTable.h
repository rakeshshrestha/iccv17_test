#pragma  once
#include <hash_map>
#include <hash_set>
#include <vector>
#include <algorithm>
#include "DataTypes.h"
#include "keys.h"
#include "config.h"

using namespace std;
using namespace stdext;

typedef hash_map<unsigned int, vector<KeypointMatch>> MatchAdjTable;
typedef hash_set<int> HashSetInt;
typedef pair<unsigned long, unsigned long> MatchIndex;
extern FILE *EGsOutPutFile;
class AdjListElem
{
public:
	bool operator< (const AdjListElem &other) const
	{
		return m_index < other.m_index;
	}

	unsigned int m_index;
	vector<KeypointMatch> m_match_list;
};

typedef vector<AdjListElem> MatchAdjList;

class MatchTable
{
public:
	MatchTable() {}
	MatchTable(int num_images) 
	{
		m_match_lists.resize(num_images);
	}

	void SetMatch(MatchIndex idx) 
	{ 
		if (Contains(idx)) return;  

		AdjListElem e;
		e.m_index = idx.second;
		MatchAdjList &l = m_match_lists[idx.first];
		MatchAdjList::iterator p = lower_bound(l.begin(), l.end(), e);
		l.insert(p, e);
	}

	void AddMatch(MatchIndex idx, KeypointMatch m)
	{
		assert(Contains(idx));
		GetMatchList(idx).push_back(m);
	}

	void ClearMatch(MatchIndex idx) 
	{ // but don't erase!
		if (Contains(idx)) 
		{
			GetMatchList(idx).clear();
		}
	}

	void RemoveMatch(MatchIndex idx) 
	{
		if (Contains(idx)) 
		{
			std::vector<KeypointMatch> &match_list = GetMatchList(idx);
			match_list.clear();

			// Remove the neighbor
			AdjListElem e;
			e.m_index = idx.second;
			MatchAdjList &l = m_match_lists[idx.first];
			std::pair<MatchAdjList::iterator, MatchAdjList::iterator> p = 
				equal_range(l.begin(), l.end(), e);

			assert(p.first != p.second); 
			l.erase(p.first, p.second);        
		}
	}

	unsigned int GetNumMatches(MatchIndex idx)
	{
		if (!Contains(idx))
			return 0;
		return GetMatchList(idx).size();
	}

	std::vector<KeypointMatch> &GetMatchList(MatchIndex idx) 
	{
		AdjListElem e;
		e.m_index = idx.second;
		MatchAdjList &l = m_match_lists[idx.first];
		std::pair<MatchAdjList::iterator, MatchAdjList::iterator> p = 
			equal_range(l.begin(), l.end(), e);

		assert(p.first != p.second); 

		return (p.first)->m_match_list;
	}

	bool Contains(MatchIndex idx) const 
	{
		AdjListElem e;
		e.m_index = idx.second;
		const MatchAdjList &l = m_match_lists[idx.first];
		std::pair<MatchAdjList::const_iterator, 
			MatchAdjList::const_iterator> p = 
			equal_range(l.begin(), l.end(), e);

		return (p.first != p.second);
	}

	void RemoveAll() 
	{
		int num_lists = m_match_lists.size();
		for (int i = 0; i < num_lists; i++)
		{
			m_match_lists[i].clear();
		}
	}

	unsigned int GetNumNeighbors(unsigned int i)
	{
		return m_match_lists[i].size();
	}

	MatchAdjList &GetNeighbors(unsigned int i)
	{
		return m_match_lists[i];
	}

	MatchAdjList::iterator Begin(unsigned int i)
	{
		return m_match_lists[i].begin();
	}

	MatchAdjList::iterator End(unsigned int i)
	{
		return m_match_lists[i].end();
	}

private:
	std::vector<MatchAdjList> m_match_lists;
};

/* Return the match index of a pair of images */
MatchIndex GetMatchIndex(int i1, int i2) 
{
	return MatchIndex((unsigned long) i1, (unsigned long) i2);
}

MatchIndex GetMatchIndexUnordered(int i1, int i2) 
{
	if (i1 < i2)
		return MatchIndex((unsigned long) i1, (unsigned long) i2);
	else
		return MatchIndex((unsigned long) i2, (unsigned long) i1);
}

inline void LoadAllMatches(char *filename, MatchTable &m_matches)
{
	//printf("[LoadAllMatches] Loading matches\n");
	fflush(stdout);
	m_matches.RemoveAll();

	//ifstream in_file; 
	FILE *in_file = NULL;
	//in_file.open(filename);
	fopen_s(&in_file, filename, "r");

	if (in_file == NULL)
	{
		printf("[LoadAllMatches] Error opening file %s for reading\n", filename);
		//in_file.close();
		fflush(stdout);
		exit(1);
	}

	char buf[256];
	while (fgets(buf, 256, in_file)!=NULL)
	{
		int i1 = -1, i2 = -1;
		int nMatches;
		sscanf(buf, "%d %d", &i1, &i2);

		MatchIndex idx = GetMatchIndex(i1, i2);
		m_matches.SetMatch(idx);

		fgets(buf, 256, in_file);
		sscanf(buf, "%d", &nMatches);

		vector<KeypointMatch> matches;
		for (int i = 0; i < nMatches; i++) 
		{
			int k1, k2;
			fgets(buf, 256, in_file);
			sscanf(buf, "%d %d", &k1, &k2);
			//in_file >> k1 >> k2;

			// 			if (k1 > KEY_LIMIT || k2 > KEY_LIMIT)
			// 				continue;

			KeypointMatch m;

			m.m_idx1 = k1;
			m.m_idx2 = k2;

			matches.push_back(m);
		}
		m_matches.GetMatchList(idx) = matches;
	}
	fclose(in_file);
	fflush(stdout);
}

inline void MakeMatchListSymmetric(unsigned int num_images, MatchTable &m_matches)
{
	for (unsigned int i = 0; i < num_images; i++) 
	{
		MatchAdjList::const_iterator iter;
		for (iter = m_matches.Begin(i); iter != m_matches.End(i); iter++)
		{
			unsigned int j = iter->m_index; 

			if (j <= i) continue;

			//assert(ImagesMatch(i, j));   

			MatchIndex idx = GetMatchIndex(i, j);
			MatchIndex idx_rev = GetMatchIndex(j, i);

			const std::vector<KeypointMatch> &list = iter->m_match_list;
			unsigned int num_matches = list.size();

			m_matches.SetMatch(idx_rev);
			m_matches.ClearMatch(idx_rev);

			for (unsigned int k = 0; k < num_matches; k++)
			{
				KeypointMatch m1, m2;

				m1 = list[k];

				m2.m_idx1 = m1.m_idx2;
				m2.m_idx2 = m1.m_idx1;
				m_matches.AddMatch(idx_rev, m2);
			}
		}
	}
}

inline void PruneDoubleMatches(char *filename, MatchTable &m_matches, unsigned int num_images)
{
	for (unsigned int i = 0; i < num_images; i++)
	{
		MatchAdjList::iterator iter;

		std::vector<unsigned int> remove;
		for (iter = m_matches.Begin(i); iter != m_matches.End(i); iter++)
		{
			HashSetInt seen;

			int num_pruned = 0;

			vector<KeypointMatch> &list = iter->m_match_list;

			/* Unmark keys */
			int num_matches = (int) list.size();

			for (int k = 0; k < num_matches; k++) 
			{
				int idx2 = list[k].m_idx2;

				if (seen.find(idx2) != seen.end())
				{
					/* This is a repeat */
					list.erase(list.begin() + k);
					num_matches--;
					k--;

					num_pruned++;
				} else
				{
					/* Mark this key as matched */
					seen.insert(idx2);
				}
			}

			unsigned int j = iter->m_index; // first;

			fprintf(EGsOutPutFile,"[PruneDoubleMatches] Pruned[%d,%d] = %d / %d\n",
				i, j, num_pruned, num_matches + num_pruned);

			if (num_matches < MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs) 
			{
				/* Get rid of... */
				remove.push_back(iter->m_index); // first);
			}
		}

		for (unsigned int j = 0; j < remove.size(); j++) 
		{
			int idx2 = remove[j];
			m_matches.RemoveMatch(GetMatchIndex(i, idx2));
			fprintf(EGsOutPutFile,"[PruneDoubleMatches] Removing[%d,%d]\n", i, idx2);
		}
	}

	//ofstream out_file;
	FILE *out_file = NULL;
	fopen_s(&out_file, filename, "w");

	for (unsigned int i = 0; i < num_images; i++)
	{
		for (MatchAdjList::iterator iter = m_matches.Begin(i); iter != m_matches.End(i); iter++)
		{
			if (i > iter->m_index) continue;

			//out_file << i << " " << iter->m_index;
			fprintf_s(out_file, "%d %d", int(i), int(iter->m_index));
			vector<KeypointMatch> &list = iter->m_match_list;

			int num_matches = (int) list.size();
			for (int k = 0; k < num_matches; k++) 
			{
				//out_file << list[k].m_idx1 << " " << list[k].m_idx2;
				fprintf_s(out_file, "%d %d", int(list[k].m_idx1), int(list[k].m_idx2));
			}
		}
	}
	//out_file.close();
	fclose(out_file);
	fflush(stdout);
}


inline void LoadAllMatchesfromVisualSFMResults(char *filename, MatchTable &m_matches, vector<map<int, Vector2r>> &SelectKeys)
{
	//printf("[LoadAllMatches] Loading matches from VisualSFM Results\n");
	fflush(stdout);
	m_matches.RemoveAll();

	//ifstream in_file; 
	FILE *in_file = NULL;
	//in_file.open(filename);
	fopen_s(&in_file, filename, "r");

	if (in_file == NULL)
	{
		printf("[LoadAllMatches] Error opening file %s for reading\n", filename);
		//in_file.close();
		fflush(stdout);
		exit(1);
	}

	char buf[256],file1[256],file2[256];
	while (fgets(buf, 256, in_file)!=NULL)
	{
		if (buf[0] == '#')
		{
			printf("%s\n",buf);
			continue;
		}

		int i1 = -1, i2 = -1;
		int nMatches;
		sscanf(buf, "%d %s", &i1, file1);
		fgets(buf, 256, in_file);
		sscanf(buf, "%d %s", &i2, file2);

		MatchIndex idx = GetMatchIndex(i1, i2);
		m_matches.SetMatch(idx);

		fgets(buf, 256, in_file);
		sscanf(buf, "%d", &nMatches);

		vector<KeypointMatch> matches;
		int k1, k2, x1,y1,x2,y2;
		for (int i = 0; i < nMatches; i++) 
		{
			fgets(buf, 256, in_file);
			sscanf(buf, "%d %f %f %d %f %f", &k1, &x1, &y1, &k2, &x2, &y2);
			//in_file >> k1 >> k2;

			// 			if (k1 > KEY_LIMIT || k2 > KEY_LIMIT)
			// 				continue;

			KeypointMatch m;

			m.m_idx1 = k1;
			m.m_idx2 = k2;

			matches.push_back(m);

			SelectKeys[i1].insert(make_pair(k1, Vector2r(x1,y1)));
			SelectKeys[i2].insert(make_pair(k2, Vector2r(x2,y2)));
		}
		m_matches.GetMatchList(idx) = matches;
	}
	fclose(in_file);
	fflush(stdout);
}