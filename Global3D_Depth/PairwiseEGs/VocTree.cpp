#include "VocTree.h"

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <algorithm>

#include "keys.h"

using namespace std;
VocTree::VocTree(void)
{
}

VocTree::~VocTree(void)
{
	db.clean();
	voctree.clean();
}

void VocTree::Clean()
{
	db.clean();
	voctree.clean();
}

int VocTree::LoadVocTree(char *filename)
{
	voctree.clean();
	if (voctree.init(filename) < 0)
		return - 1;

	printf("Voctree loaded\n");
	printf("Levels: %d\n", voctree.nrlevels());
	printf("Branch factor: %d\n", voctree.nrsplits());
	printf("Visual words: %d\n", voctree.nrvisualwords());
	return 1;
}

int VocTree::FindImageCandidates(char *keyfile_in, int**match_id_p, int &nm)
{
	FILE *f = fopen(keyfile_in, "r");
	if (f == NULL)
	{
		printf("Error opening file %s for reading\n", keyfile_in);
		fflush(stdout);
		return -1;
	}

	int num_imgs = 0;
	char buf[512];
	vector<string> key_files;
	while (fgets(buf, 512, f))
	{
		if (buf[strlen(buf) - 1] == '\n')
			buf[strlen(buf) - 1] = 0;
		key_files.push_back(string(buf));
		num_imgs++;
	}
	fclose(f);

	printf("Process %d images\n", num_imgs);
	fflush(stdout);

	nm = min(NMATCH, num_imgs);
	if (nm < NMATCH)
	{
		printf("Use all images as candidate matches\n");

		int *match_id = new int[num_imgs * num_imgs];
		memset(match_id, -1, sizeof(int) * nm * num_imgs);

		for (int i = 0; i < num_imgs - 1; i++)
		{
			///printf("Image %d is matched to: \n", i);
			for (int j = i + 1; j < num_imgs; j++)
			{
				//if (i == j) continue;
				match_id[i * num_imgs + j] = j;
				//printf("%d ", j);
			}
			//printf("\n");
		}


		*match_id_p = match_id; 
		match_id = NULL;
		fflush(stdout);

		return num_imgs;
	}

	printf("Find matching candidates using voctree.\n");
	fflush(stdout);

/*	Build database*/
	db.clean();
	db.init(MAXNIMG, voctree.nrvisualwords(), MAXNVW);
	printf("Database initialized.\n");
	fflush(stdout);

	unsigned int *vwread = new unsigned int[MAXNVW];
	for (int i = 0; i < num_imgs; i++)
	{
		printf("Insert features from image %d.\n", i);
		fflush(stdout);

		unsigned char *desc = NULL;
		int num_keys = ReadKeyDesc(key_files[i].c_str(), &desc);

		int nr = min(MAXNVW, num_keys);
		for (int j = 0; j < nr; j++) 
			voctree.quantize(&vwread[j], &desc[j * 128]);
		db.insertdoc(vwread, nr, i);

		delete []desc;
	}

	// compute idf score of the images in the database
	printf("Compute IDF score of the images in the database.\n");
	fflush(stdout);
	db.computeidf();

	// update normalization in database according to changed idf-weights
	db.normalize();

	int *match_id = new int[nm * num_imgs];
	memset(match_id, -1, sizeof(int) * nm * num_imgs);

/*	Query database*/
	int *qdocnames = new int [nm];
	float *qscores = new float[nm];
	for (int i = 0; i < num_imgs; i++)
	{
		unsigned char *desc = NULL;
		int num_keys = ReadKeyDesc(key_files[i].c_str(), &desc);

		int nr = min(MAXNVW, num_keys);
		for (int j = 0; j < nr; j++) 
			voctree.quantize(&vwread[j], &desc[j * 128]);
		delete []desc;

		db.querytopn(vwread, nr, nm, qdocnames, qscores);   

		// The database is containing 1 instances of each object. Ideally for
		// every query the top ranked image will be of the same object. 
		for (int j = 0; j < nm; j++) 
		{
			if (qdocnames[j] <= i) 
			{
				match_id[i * nm + j] = -1;
				continue;
			}
			match_id[i * nm + j] = qdocnames[j];
		}
	}
	delete []vwread;
	delete []qdocnames;
	delete []qscores;

#if 1
	for (int i = 0; i < num_imgs - 1; i++)
	{
		printf("Image %d is matched to: \n", i);
		for (int j = 0; j < nm; j++)
		{
			if (match_id[i * nm + j] >= 0 && match_id[i * nm + j] != i)
				printf("%d ", match_id[i * nm + j]);
		}
		printf("\n");
	}
	printf("Done\n");
#endif

	fflush(stdout);

	*match_id_p = match_id; 
	match_id = NULL;

	return num_imgs;
}
