#pragma once
#include <vector>
#include <string>
#include <map>
#include <assert.h>
#include "config.h"

#include "keys.h"

using namespace std;


//#define ADDMATCHES

class MatchPoint
{
public:
	MatchPoint(void);
	~MatchPoint(void);


	void BruteForceMatch(double **intrinsic);
	void MatchUsingVoctree(int num_images, double **intrinsic, vector<string> &key_files,
		map<pair<int, int>, int> &matchList, map<pair<int, int>, int> &old_matchList, const char* matches_out, 
		const char* voctree_in, const char* keylist_in, int split_id = 0, double ratio = MATCHING_SCORE_RATIO);
	void GeometricVerification(int num_images,
		                       double **intrinsic,
		                       std::vector<std::string> &key_files,
							   map<pair<int, int>, int> &old_matchList, //temp
		                       const char *EGs_out, const char *matches_out, const char *matches_out2,
							   int start_id = 0, int split_id = 0, int add_id = 0);
	void GeometricVerificationNew(int num_images,
		double **intrinsic,
		std::vector<std::string> &key_files,
		map<pair<int, int>, int> &old_matchList, //temp
		const char *EGs_out, const char *matches_out, const char *matches_out2,
		int start_id = 0, int split_id = 0, int add_id = 0);
	void RelativePoseFromSyntheticData(int num_images, const char *matches_in, const char *EGs_out, double **cam_intrinsic, 
		double noise);

	///cui 20130307
	std::vector<KeypointMatch> GuidedMatching(vector<Keypoint> &keys0, vector<Keypoint> &keys1, unsigned char *desc0,  unsigned char *desc1, vector<bool> &img0_status,vector<bool> &img1_status, double F[9],double thr_guided,double ratio);

	///cui 20130429
	void ComputeEformVisualSFMResults(int num_images,std::vector<std::string> & img_files,
		double **intrinsic,	const char *EGs_out, const char *VisualSFMResults,
		int start_id = 0, int split_id = 0, int add_id = 0);

	///cui20141102 -- Preprocessing the data release from 1DSfM, change it to our EGs while the R and t is wrong
	void ComputeEfromWrongEGs(int num_images,std::vector<std::string> & img_files,
		double **intrinsic,	const char *EGs_out, const char *WrongEGs,
		int start_id = 0, int split_id = 0, int add_id = 0);

	///cui20141216 -- Preprocessing the data release from 1DSfM, the R and t Encoded in E is from Bundler, which
	/// considered to be right. So just to remove bad matches
	void RemoveBadMatches(int num_images,std::vector<std::string> & img_files,
		double **intrinsic,	const char *EGs_out, const char *WrongEGs,
		int start_id = 0, int split_id = 0, int add_id = 0);

	///cui 20130528
	void SortKeys(int num_images, vector<string> &key_files);
	void MatchUsingVoctreeFast(int num_images, double **intrinsic, vector<string> &key_files,
		map<pair<int, int>, int> &matchList, map<pair<int, int>, int> &old_matchList, const char* matches_out, 
		const char* voctree_in, const char* keylist_in, int split_id = 0, double ratio = MATCHING_SCORE_RATIO, int PreemptiveMatchingThr=0);

	void MatchUsingVoctreeSort(int num_images, double **intrinsic, vector<string> &key_files,
		map<pair<int, int>, int> &matchList, map<pair<int, int>, int> &old_matchList, const char* matches_out, 
		const char* voctree_in, const char* keylist_in, int split_id = 0, double ratio = MATCHING_SCORE_RATIO, int PreemptiveMatchingThr=0);
	void SortKeys(int num_images, vector<string> &key_files, unsigned char ** SortDescAll, int TestNum);

	void MatchUsingVoctreeGPU(int num_images, double **intrinsic, vector<string> &key_files,
		map<pair<int, int>, int> &matchList, map<pair<int, int>, int> &old_matchList, const char* matches_out, 
		const char* voctree_in, const char* keylist_in, int split_id = 0, double ratio = MATCHING_SCORE_RATIO);

	//2014.3.4
	void MatchUsingVoctreeOpenMP(int num_images, double **intrinsic, vector<string> &key_files,
		map<pair<int, int>, int> &matchList, map<pair<int, int>, int> &old_matchList, const char* matches_out, 
		const char* voctree_in, const char* keylist_in, int split_id = 0, double ratio = MATCHING_SCORE_RATIO);

};

