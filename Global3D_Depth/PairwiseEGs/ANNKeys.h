#include "keys.h"
#include "ann/ANN.h"
#include <vector>


inline ANNkd_tree *CreateSearchTree(const std::vector<KeypointWithDesc> &k, 
							 bool spatial, double alpha) 
{
	/* Create a new array of points */
	int num_pts = (int) k.size();

	int dim = 128;
	if (spatial) dim = 130;

	ANNpointArray pts = annAllocPts(num_pts, dim);

	int offset = 0;
	if (spatial) offset = 2;

	for (int i = 0; i < num_pts; i++) {
		int j;

		assert(k[i].m_d != NULL);

		if (spatial) 
		{
			pts[i][0] = alpha * k[i].m_x;
			pts[i][1] = alpha * k[i].m_y;
		}

		double d_norm = 0;
		for (j = 0; j < 128; j++)
		{
			pts[i][j+offset] = k[i].m_d[j];
			d_norm += pts[i][j+offset] * pts[i][j+offset];
		}
		d_norm = sqrt(d_norm);
		for (j = 0; j < 128; j++)
			pts[i][j+offset] /= d_norm;
	}

	/* Create a search tree for k2 */
	ANNkd_tree *tree = new ANNkd_tree(pts, num_pts, dim, 4);
	return tree;
}

/* Create a search tree for the given set of keypoints */
inline ANNkd_tree *CreateSearchTree(int num_keys, unsigned char *keys, ANNpointArray pts)
{
	/* Create a new array of points */
	//ANNpointArray pts = annAllocPts(num_keys, 128);

	for (int i = 0; i < num_keys; i++) 
	{
		double d_norm = 0;
		for (int j = 0; j < 128; j++)
		{
			pts[i][j] = double(keys[128 * i + j]);
			d_norm += pts[i][j] * pts[i][j];
		}
		d_norm = sqrt(d_norm);

		for (int j = 0; j < 128; j++)
			pts[i][j] /= d_norm;
	}

	ANNkd_tree *tree = new ANNkd_tree(pts, num_keys, 128, 4);
	return tree;
}

inline ANNkd_tree *CreateSearchTree(vector<int> & candidate, unsigned char *keys, ANNpointArray pts)
{
	/* Create a new array of points */
	//ANNpointArray pts = annAllocPts(num_keys, 128);

	for (int i = 0; i < candidate.size(); i++) 
	{
		double d_norm = 0;
		for (int j = 0; j < 128; j++)
		{
			pts[i][j] = double(keys[128 * candidate[i] + j]);
			d_norm += pts[i][j] * pts[i][j];
		}
		d_norm = sqrt(d_norm);

		for (int j = 0; j < 128; j++)
			pts[i][j] /= d_norm;
	}

	ANNkd_tree *tree = new ANNkd_tree(pts, candidate.size(), 128, 4);
	return tree;
}


inline std::vector<KeypointMatch> MatchKeys(int num_keys2, unsigned char *k2, 
									 ANNkd_tree *tree1,
									 double ratio = 0.6, int max_pts_visit = 200)
{
	annMaxPtsVisit(max_pts_visit);
	std::vector<KeypointMatch> matches;

	ANNidx nn_idx[2];
	ANNdist dist[2];
	ANNpoint data_Pt = annAllocPt(128);
	for (int i = 0; i < num_keys2; i++) 
	{
		double sum = 0;
		for (int j = 0; j < 128; j++)
		{
			data_Pt[j] = double(k2[i * 128 + j]);
			sum += data_Pt[j] * data_Pt[j];
		}
		sum = sqrt(sum);

		for (int j = 0; j < 128; j++)
			data_Pt[j] /= sum;

		tree1->annkPriSearch(data_Pt, 2, nn_idx, dist, 0.0);

		if (double(dist[0]) < ratio * ratio * double(dist[1])) 
		{
			unsigned short m_idx1 = unsigned short(nn_idx[0]);
			unsigned short m_idx2 = unsigned short(i);
			matches.push_back(KeypointMatch(m_idx1, m_idx2));
		}
	}
	annDeallocPt(data_Pt);

	return matches;    
}

/* Compute likely matches between two sets of keypoints */
inline std::vector<KeypointMatch> MatchKeys(int num_keys1, unsigned char *k1, 
									 int num_keys2, unsigned char *k2, 
									 double ratio, int max_pts_visit) 
{
	annMaxPtsVisit(max_pts_visit);

	int num_pts = 0;
	std::vector<KeypointMatch> matches;

	num_pts = num_keys2;
	clock_t start = clock();

	/* Create a new array of points */
	ANNpointArray pts = annAllocPts(num_pts, 128);

	for (int i = 0; i < num_pts; i++)
	{
		for (int j = 0; j < 128; j++)
			pts[i][j] = k2[i * 128 + j];
	}

	/* Create a search tree for k2 */
	ANNkd_tree *tree = new ANNkd_tree(pts, num_pts, 128, 16);
	clock_t end = clock();

	start = clock();
	ANNidx nn_idx[2];
	ANNdist dist[2];
	ANNpoint data_Pt = annAllocPt(128);
	for (int i = 0; i < num_keys1; i++) 
	{
		for (int j = 0; j < 128; j++)
			data_Pt[j] = k1[i * 1128 + j];

		tree->annkPriSearch(data_Pt, 2, nn_idx, dist, 0.0);

		if (((double) dist[0]) < ratio * ratio * ((double) dist[1])) 
		{
			matches.push_back(KeypointMatch(i, nn_idx[0]));
		}
	}
	end = clock();

	/* Cleanup */
	annDeallocPts(pts);
	annDeallocPt(data_Pt);

	delete tree;

	return matches;
}

/* Compute likely matches between two sets of keypoints */
inline std::vector<KeypointMatch> MatchKeys(const std::vector<KeypointWithDesc> &k1, 
									 const std::vector<KeypointWithDesc> &k2, 
									 bool registered, double ratio) 
{
	annMaxPtsVisit(200);

	int num_pts = 0;
	std::vector<KeypointMatch> matches;

	int *registered_idxs = NULL;

	if (!registered) {
		num_pts = (int) k2.size();
	} else {
		registered_idxs = new int[(int) k2.size()];
		for (int i = 0; i < (int) k2.size(); i++) {
			if (k2[i].m_extra >= 0) {
				registered_idxs[num_pts] = i;
				num_pts++;
			}
		}
	}

	/* Create a new array of points */
	ANNpointArray pts = annAllocPts(num_pts, 128);

	if (!registered) {
		for (int i = 0; i < num_pts; i++) {
			int j;

			for (j = 0; j < 128; j++) {
				pts[i][j] = k2[i].m_d[j];
			}
		}
	} else {
		for (int i = 0; i < num_pts; i++) {
			int j;
			int idx = registered_idxs[i];

			for (j = 0; j < 128; j++) {
				pts[i][j] = k2[idx].m_d[j];
			}
		}	
	}

	clock_t start = clock();
	/* Create a search tree for k2 */
	ANNkd_tree *tree = new ANNkd_tree(pts, num_pts, 128, 4);
	clock_t end = clock();

	/* Now do the search */
	ANNpoint query = annAllocPt(128);
	start = clock();
	for (int i = 0; i < (int) k1.size(); i++) {
		int j;

		for (j = 0; j < 128; j++) {
			query[j] = k1[i].m_d[j];
		}

		ANNidx nn_idx[2];
		ANNdist dist[2];

		tree->annkPriSearch(query, 2, nn_idx, dist, 0.0);

		if (sqrt(((double) dist[0]) / ((double) dist[1])) <= ratio) {
			if (!registered) {
				matches.push_back(KeypointMatch(i, nn_idx[0]));
			} else {
				KeypointMatch match = 
					KeypointMatch(i, registered_idxs[nn_idx[0]]);
				matches.push_back(match);
			}
		}
	}
	end = clock();

	int num_matches = (int) matches.size();

	printf("[MatchKeys] Found %d matches\n", num_matches);

	/* Cleanup */
	annDeallocPts(pts);
	annDeallocPt(query);

	delete tree;

	return matches;
}

inline /* Compute likely matches between two sets of keypoints */
std::vector<KeypointMatchWithScore> 
MatchKeysWithScore(const std::vector<KeypointWithDesc> &k1, 
				   const std::vector<KeypointWithDesc> &k2,
				   bool registered, 
				   double ratio)
{
	annMaxPtsVisit(200);

	int num_pts = 0;
	std::vector<KeypointMatchWithScore> matches;

	int *registered_idxs = NULL;

	if (!registered) {
		num_pts = (int) k2.size();
	} else {
		registered_idxs = new int[(int) k2.size()];
		for (int i = 0; i < (int) k2.size(); i++) {
			if (k2[i].m_extra >= 0) {
				registered_idxs[num_pts] = i;
				num_pts++;
			}
		}
	}

	/* Create a new array of points */
	ANNpointArray pts = annAllocPts(num_pts, 128);

	if (!registered) {
		for (int i = 0; i < num_pts; i++) {
			int j;

			for (j = 0; j < 128; j++) {
				pts[i][j] = k2[i].m_d[j];
			}
		}
	} else {
		for (int i = 0; i < num_pts; i++) {
			int j;
			int idx = registered_idxs[i];

			for (j = 0; j < 128; j++) {
				pts[i][j] = k2[idx].m_d[j];
			}
		}	
	}

	clock_t start = clock();
	/* Create a search tree for k2 */
	ANNkd_tree *tree = new ANNkd_tree(pts, num_pts, 128, 4);
	clock_t end = clock();

	// printf("Building tree took %0.3fs\n", 
	//        (end - start) / ((double) CLOCKS_PER_SEC));

	/* Now do the search */
	ANNpoint query = annAllocPt(128);
	start = clock();
	for (int i = 0; i < (int) k1.size(); i++) {
		int j;

		for (j = 0; j < 128; j++) {
			query[j] = k1[i].m_d[j];
		}

		ANNidx nn_idx[2];
		ANNdist dist[2];

		tree->annkPriSearch(query, 2, nn_idx, dist, 0.0);

		if (sqrt(((double) dist[0]) / ((double) dist[1])) <= ratio) {
			if (!registered) {
				KeypointMatchWithScore match = 
					KeypointMatchWithScore(i, nn_idx[0], (float) dist[0]);
				matches.push_back(match);
			} else {
				KeypointMatchWithScore match = 
					KeypointMatchWithScore(i, registered_idxs[nn_idx[0]], 
					(float) dist[0]);
				matches.push_back(match);
			}
		}
	}
	end = clock();
	// printf("Searching tree took %0.3fs\n",
	//        (end - start) / ((double) CLOCKS_PER_SEC));

	int num_matches = (int) matches.size();

	printf("[MatchKeysWithScore] Found %d matches\n", num_matches);

	/* Cleanup */
	annDeallocPts(pts);
	annDeallocPt(query);

	delete tree;

	return matches;    
}

inline /* Compute likely matches between two sets of keypoints */
std::vector<KeypointMatch>
MatchKeysExhaustive(const std::vector<KeypointWithDesc> &k1, 
					const std::vector<KeypointWithDesc> &k2, 
					bool registered, double ratio) 
{
	int num_pts = 0;
	std::vector<KeypointMatch> matches;

	int *registered_idxs = NULL;

	if (!registered) {
		num_pts = (int) k2.size();
	} else {
		registered_idxs = new int[(int) k2.size()];
		for (int i = 0; i < (int) k2.size(); i++) {
			if (k2[i].m_extra >= 0) {
				registered_idxs[num_pts] = i;
				num_pts++;
			}
		}
	}

	/* Create a new array of points */
	ANNpointArray pts = annAllocPts(num_pts, 128);

	if (!registered) {
		for (int i = 0; i < num_pts; i++) {
			int j;

			for (j = 0; j < 128; j++) {
				pts[i][j] = k2[i].m_d[j];
			}
		}
	} else {
		for (int i = 0; i < num_pts; i++) {
			int j;
			int idx = registered_idxs[i];

			for (j = 0; j < 128; j++) {
				pts[i][j] = k2[idx].m_d[j];
			}
		}	
	}

	clock_t start = clock();
	/* Create a search tree for k2 */
	ANNkd_tree *tree = 
		new ANNkd_tree(pts, num_pts, 128, 4);
	clock_t end = clock();

	// printf("Building tree took %0.3fs\n", 
	//        (end - start) / ((double) CLOCKS_PER_SEC));

	/* Now do the search */
	ANNpoint query = annAllocPt(128);
	start = clock();
	for (int i = 0; i < (int) k1.size(); i++) {
		int j;

		for (j = 0; j < 128; j++) {
			query[j] = k1[i].m_d[j];
		}

		ANNidx nn_idx[2];
		ANNdist dist[2];

		tree->annkSearch(query, 2, nn_idx, dist, 0.0);

		if (sqrt(((double) dist[0]) / ((double) dist[1])) <= ratio) {
			if (!registered) {
				matches.push_back(KeypointMatch(i, nn_idx[0]));
			} else {
				KeypointMatch match = 
					KeypointMatch(i, registered_idxs[nn_idx[0]]);
				matches.push_back(match);
			}
		}
	}
	end = clock();
	// printf("Searching tree took %0.3fs\n",
	//        (end - start) / ((double) CLOCKS_PER_SEC));

	int num_matches = (int) matches.size();

	printf("[MatchKeys] Found %d matches\n", num_matches);

	/* Cleanup */
	annDeallocPts(pts);
	annDeallocPt(query);

	delete tree;
	return matches;
}