/* 
*  Copyright (c) 2011-2012  Nianjuan Jiang (g0700213@nus.edu.sg)
*    and the National University of Singapore
*/

// PairwiseEGs.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <time.h>
#include <string.h>
#include <stdio.h>

#include <map>
#include "Windows.h"
#include "atlimage.h"
#include "MatchPoint.h"
#include "config.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <functional>

#include "vector.h"
#include "5point.h"
#include "Epipolar.h"

using namespace std;
#define ADDIMAGE
//#define TRIANGULATE
#define RELEASE

FILE *EGsOutPutFile;

#define NO_OF_ITERATIONS 500

#define OUTLIERS_START 0.05
#define OUTLIERS_END 0.505
#define OUTLIERS_STEP 0.05

#define NUMBER_OF_POINTS_START 5000
#define NUMBER_OF_POINTS_END 5000
#define NUMBER_OF_POINTS_STEP 450


const int NUM_ALGORITHMS = 4;
const char arr_algorithms[NUM_ALGORITHMS][20] = {
	"preSolve",
	"stew",
	// "nister",	
	"sevenpt",
	"eightpt"
	// "matlab5pt"
};

void printRt(double R[], double t[])
{
	printf("R: \n");
	for (int i = 0; i<3; i++)
	{
		for (int j=0; j<3; j++)
		{
			printf("%f ", R[3*i+j]);
		}
		printf("\n");
	}

	printf("t: \n");
	for (int i = 0; i<3; i++)
	{
		printf("%f ", t[i]);
	}
	printf("\n");
}

std::pair<double, double> vectPairAverage(std::vector< std::pair<double, double> > &vect)
{
	std::pair<double, double> ret = std::accumulate(vect.begin(), vect.end(), std::make_pair((double)0.0, (double)0.0),
		[](std::pair<double, double> sum, std::pair<double, double> current)->std::pair<double, double> {
			sum.first += current.first;
			sum.second += current.second;
			
			return sum;
		}
	);
	
	ret.first /= vect.size();
	ret.second /= vect.size();

	return ret;
}

std::pair<double, double> vectPairStandardDeviation(std::vector< std::pair<double, double> > &vect)
{
	std::pair<double, double> avg = vectPairAverage(vect);

	std::pair<double, double> std_deviation = std::accumulate(vect.begin(), vect.end(), std::make_pair((double)0.0, (double)0.0),
		[avg](std::pair<double, double> sum, std::pair<double, double> current)->std::pair<double, double> {
			sum.first += std::pow(current.first - avg.first, 2);
			sum.second += std::pow(current.second - avg.second, 2);
			
			return sum;
		}
	);

	std_deviation.first = std::sqrt(std_deviation.first / vect.size());
	std_deviation.second = std::sqrt(std_deviation.second / vect.size());

	return std_deviation;
}

int main(int argc, char **argv)
{
	double E_THRESHOLD_NEW = 0.05;

	if (argc < 3) {
        std::cerr << "Usage: parser <num_points> <base_path>" << std::endl;
        return 1;
    }
    
    std::string base_path = argv[2];

    int num_iterations;
    int num_points_start, num_points_end, num_points_step;
    int noise_start, noise_end, noise_step;
	double outliers_start, outliers_end, outliers_step;
    // camera indices
    int v1 = 0, v2 = 1;
    char buf[256];

	/*std::stringstream meta_info_filename;
    meta_info_filename << base_path << "\\" <<  "meta.in";
    FILE * fptr_meta_info = fopen(meta_info_filename.str().c_str(), "r");
    if (!fptr_meta_info) {
        std::cerr << "ERROR: Unable to open meta info file " << meta_info_filename.str() << std::endl;
		return 1;
    }*/
	 // get meta info
    /*fgets(buf, 256, fptr_meta_info);
    sscanf(buf, "%d", &num_iterations);
    
    fgets(buf, 256, fptr_meta_info);
    sscanf(buf, "%d %d %d", &num_points_start, &num_points_end, &num_points_step);

    fgets(buf, 256, fptr_meta_info);
    sscanf(buf, "%d %d %d", &noise_start, &noise_end, &noise_step);

	fgets(buf, 256, fptr_meta_info);
    sscanf(buf, "%f %f %f", &outliers_start, &outliers_end, &outliers_step);*/

	num_iterations = NO_OF_ITERATIONS;
	
	num_points_start = NUMBER_OF_POINTS_START;
	num_points_end = NUMBER_OF_POINTS_END;
	num_points_step = NUMBER_OF_POINTS_STEP;

	/*noise_start = 1;
	noise_end = 0.505;
	noise_step = 0.05;*/

	outliers_start = OUTLIERS_START; 
	outliers_end = OUTLIERS_END;
	outliers_step = OUTLIERS_STEP;

	std::cout << "Outliers: " << outliers_start << ", " << outliers_end << ", " << outliers_step << std::endl;
	
	// Camera matrix (identity in our bearing vector case)
    double K1[9], K2[9];
    for (int i = 0; i < 9; i++) {
        K1[i] = K2[i] = 0;
    }
    K1[0] = K1[4] = K1[8] = 1;
    K2[0] = K2[4] = K2[8] = 1;
	
	for (int nMatches = num_points_start; nMatches <= num_points_end; nMatches += num_points_step) {
		for (int algorithm_idx = 0; algorithm_idx < NUM_ALGORITHMS; algorithm_idx++) {
			// Open the bearing vector points
			std::stringstream bearing_vector_filename;
			bearing_vector_filename << base_path << "\\" << nMatches << "_bearing.in";
			std::cout << bearing_vector_filename.str().c_str() << std::endl;
			FILE * fptr_bearing_vector = fopen(bearing_vector_filename.str().c_str(), "r");
			if (!fptr_bearing_vector) {
				std::cerr << "ERROR: Unable to open bearing vector file" << std::endl;
				return 1;
			}

			// Read the algorithm file (containing the output of relative pose estimation //
			std::stringstream algorithm_filename;
			algorithm_filename << base_path << "\\" << nMatches << "_" << arr_algorithms[algorithm_idx] << "Output.out";
		
			FILE * fptr_algorithm_file = fopen(algorithm_filename.str().c_str(), "r");
			if (!fptr_algorithm_file) {
				std::cerr << "ERROR: Unable to open " << algorithm_filename.str() << std::endl;
				return 1;
			}

			// Open inliers file
			algorithm_filename.str("");
			algorithm_filename << base_path << "\\" << nMatches << "_" << arr_algorithms[algorithm_idx] << "Inliers.out";
			std::cout << algorithm_filename.str() << std::endl;
			std::ifstream inliers_file(algorithm_filename.str());
			if (!inliers_file) {
				std::cerr << "ERROR: Unable to open " << algorithm_filename.str() << std::endl;
				return 1;
			}

			// Open output file
			algorithm_filename.str("");
			algorithm_filename << base_path << "\\" << nMatches << "_" << arr_algorithms[algorithm_idx] << "RefinedError.out";
			std::cout << algorithm_filename.str() << std::endl;
			std::fstream output_file(algorithm_filename.str(), std::fstream::out);

			if (!output_file) {
				std::cerr << "ERROR: Unable to open " << algorithm_filename.str() << std::endl;
				return 1;
			}

			// Ground truth file
			std::stringstream ground_truth_filename;
			ground_truth_filename << base_path << "\\" << nMatches << "_ground_truth1.in";
			FILE * fptr_ground_truth = fopen(ground_truth_filename.str().c_str(), "r");
			if (!fptr_ground_truth) {
				std::cerr << "ERROR: Unable to open ground_truth_filename file" << std::endl;
				return 1;
			}

			double lx, ly, rx, ry;
			for (double outliers = outliers_start; outliers <= outliers_end; outliers += outliers_step) {
				// store error of all iterations for aggregate calculations
				std::vector< std::pair<double, double> > vect_angle_error;
				vect_angle_error.reserve(num_iterations);

				std::vector< std::pair<double, double> > vect_translation_error;
				vect_translation_error.reserve(num_iterations);
			
				for (int iteration_idx = 0; iteration_idx < num_iterations; iteration_idx++) {
					// Read the ground truth measurements (from frame 2 to frame 1)
					double R_gt_t[9], t_gt_t[3];
					for (int i = 0; i < 3; i++) {
						fgets(buf, 256, fptr_ground_truth);
						sscanf(	buf, "%lf %lf %lf %lf", 
								&R_gt_t[3*i], &R_gt_t[3*i + 1], &R_gt_t[3*i + 2],
								&t_gt_t[i]
						);
					}
					VectorNormalize(t_gt_t, 3);
					// from frame 1 to frame 2
					double R_gt[9], t_gt[3];
					MatrixTranspose(R_gt_t, R_gt, 3);
					MatrixMatrixProduct(3, 3, 1, R_gt, t_gt_t, t_gt);
					t_gt[0] = - t_gt[0];
					t_gt[1] = - t_gt[1];
					t_gt[2] = - t_gt[2];


					// Read the feature positions (bearing vectors)
					v2_t *k1_pts = new v2_t[nMatches];
					v2_t *k2_pts = new v2_t[nMatches];
            
					for (int bearing_vector_idx = 0; bearing_vector_idx < nMatches; bearing_vector_idx++) {
						double bearing_vectors1[3]; 
						double bearing_vectors2[3];

						fgets(buf, 256, fptr_bearing_vector);
						sscanf( buf, "%lf %lf %lf %lf %lf %lf", 
								&bearing_vectors1[0], &bearing_vectors1[1], &bearing_vectors1[2],
								&bearing_vectors2[0], &bearing_vectors2[1], &bearing_vectors2[2]
						);

						// normalize the features
						lx = bearing_vectors1[0] / bearing_vectors1[2];
						ly = bearing_vectors1[1] / bearing_vectors1[2];

						rx = bearing_vectors2[0] / bearing_vectors2[2];
						ry = bearing_vectors2[1] / bearing_vectors2[2];

						// The coordinate used in bundler is different form normal ones
						k1_pts[bearing_vector_idx] = v2_new(lx-K1[2], -ly+K1[5]);
						k2_pts[bearing_vector_idx] = v2_new(rx-K2[2], -ry+K2[5]);
					}

					// Read the algorithm output (from RANSAC)
					double R_ransac_t[9], t_ransac_t[3];
					for (int i = 0; i < 3; i++) {
						fgets(buf, 256, fptr_algorithm_file);
						sscanf(	buf, "%lf %lf %lf %lf", 
								&R_ransac_t[3*i], &R_ransac_t[3*i + 1], &R_ransac_t[3*i + 2],
								&t_ransac_t[i]
						);
					}
					VectorNormalize(t_ransac_t, 3);

					// change transformation from frame 1 to 2 (instead of the original frame 2 to 1)
					// R = R', t = -R't
					double R_ransac[9], t_ransac[3];
					memcpy(R_ransac, R_ransac_t, 9 * sizeof(double));
					memcpy(t_ransac, t_ransac_t, 3 * sizeof(double));

					// TODO: remove the 'if' when preSolve convention is fixed
					// if ( std::string(arr_algorithms[algorithm_idx]).compare("preSolve") )
					{
						MatrixTranspose(R_ransac_t, R_ransac, 3);
						MatrixMatrixProduct(3, 3, 1, R_ransac, t_ransac_t, t_ransac);
						t_ransac[0] = - t_ransac[0];
						t_ransac[1] = - t_ransac[1];
						t_ransac[2] = - t_ransac[2];
					}

					// Nonlinear optimization results
					double R_r[9], t_r[3];

					// Perform nonlinear optimization
					// The coordinate used in bundler was different form normal ones, fix it now
					for (int i = 0; i < nMatches; i++) 
					{
						k1_pts[i].p[0] /= K1[0];
						k1_pts[i].p[1] = -k1_pts[i].p[1]/K1[0];

						k2_pts[i].p[0] /= K1[0];
						k2_pts[i].p[1] = -k2_pts[i].p[1]/K1[0];
					}

					// get the inliers
					std::string line;
					std::vector<int> inliers;
					if (!std::getline(inliers_file, line))
					{
						std::cerr << "No inliers at outlier: " << outliers << " iteration: " << iteration_idx << std::endl;
					}
					
					std::stringstream line_stream(line);
					std::string inlier;
					for ( ; std::getline(line_stream, inlier, ' '); )
					{
						inliers.push_back(atoi(inlier.c_str()));
					}

					// std::cout << "Inliers: " << inliers.size() << std::endl;
									
					v2_t *k1_pts_inliers = new v2_t[inliers.size()];
					v2_t *k2_pts_inliers = new v2_t[inliers.size()];

					for (int inlier_idx = 0; inlier_idx < inliers.size(); inlier_idx++)
					{
						k1_pts_inliers[inlier_idx] = k1_pts[inliers[inlier_idx]];
						k2_pts_inliers[inlier_idx] = k2_pts[inliers[inlier_idx]];
					}

					RefineRtNonlinear(inliers.size(), k1_pts_inliers, k2_pts_inliers, K1[0], K2[0], R_ransac, t_ransac, R_r, t_r);
					double R_r_t[9], t_r_t[3];
					{
						MatrixTranspose(R_r, R_r_t, 3);
						MatrixMatrixProduct(3, 3, 1, R_r_t, t_r, t_r_t);
						t_r_t[0] = - t_r_t[0];
						t_r_t[1] = - t_r_t[1];
						t_r_t[2] = - t_r_t[2];
					}

					double rotation_error_ransac = calculateRotationError(Eigen::Matrix3d(R_gt_t), Eigen::Matrix3d(R_ransac_t));
					double translation_error_ransac = calculateTranslationError(Eigen::Vector3d(t_gt_t), Eigen::Vector3d(t_ransac_t));

					double rotation_error_refined = calculateRotationError(Eigen::Matrix3d(R_gt_t), Eigen::Matrix3d(R_r_t));
					double translation_error_refined = calculateTranslationError(Eigen::Vector3d(t_gt_t), Eigen::Vector3d(t_r_t));

					vect_angle_error.push_back(
						std::make_pair(rotation_error_ransac, rotation_error_refined)
					);

					vect_translation_error.push_back(
						std::make_pair(translation_error_ransac, translation_error_refined)
					);

					/*printf("Ransac output\n");
					printRt(R_ransac, t_ransac);

					printf("\nRefined R and t.\n");
					printRt(R_r, t_r);

					printf("\nGroundtruth output\n");
					printRt(R_gt, t_gt);

					printf("Ransac Rotation error\n");
					printf( "%lf\n", calculateRotationError(Eigen::Matrix3d(R_gt), Eigen::Matrix3d(R_ransac)) );

					printf("Ransac Translation error\n");
					printf("%lf\n", calculateTranslationError(Eigen::Vector3d(t_gt), Eigen::Vector3d(t_ransac)));
			
					printf("Optimization Rotation error\n");
					printf("%lf\n", calculateRotationError(Eigen::Matrix3d(R_gt), Eigen::Matrix3d(R_r)) );

					printf("Optimization Translation error\n");
					printf("%lf\n", calculateTranslationError(Eigen::Vector3d(t_gt), Eigen::Vector3d(t_r)));

					printf("------------------------------------ Iteration %d ------------------------------------", iteration_idx);*/

				} // iteration_idx loop end

				// output median and max
				std::pair<double, double> average_angle_error = vectPairAverage(vect_angle_error);
				std::pair<double, double> average_translation_error = vectPairAverage(vect_translation_error);

				std::pair<double, double> std_angle_error = vectPairStandardDeviation(vect_angle_error);
				std::pair<double, double> std_translation_error = vectPairStandardDeviation(vect_translation_error);

				// we want to sort pairs based on one of the two elements
				std::function<bool(const std::pair<double, double> &firstElem, const std::pair<double, double> &secondElem)> pairCompareFirst = 
					[](const std::pair<double, double> &firstElem, const std::pair<double, double> &secondElem) -> bool {
						return firstElem.first < secondElem.first;
					};

				std::function<bool(const std::pair<double, double> &firstElem, const std::pair<double, double> &secondElem)> pairCompareSecond = 
					[](const std::pair<double, double> &firstElem, const std::pair<double, double> &secondElem) -> bool {
						return firstElem.second < secondElem.second;
					};

				std::sort(vect_angle_error.begin(), vect_angle_error.end(), pairCompareFirst);
				std::sort(vect_translation_error.begin(), vect_translation_error.end(), pairCompareFirst);
				// order: angle median, angle max, angle average, translation median, translation max, translation average
				// ransac output
				output_file << vect_angle_error[num_iterations / 2].first << " "
							<< vect_angle_error[num_iterations - 1].first << " "
							<< average_angle_error.first << " "
							<< std_angle_error.first << " "
							<< vect_angle_error[num_iterations * 3 / 4].first << " " 
							<< vect_angle_error[num_iterations / 4].first << " "
							// translation
							<< vect_translation_error[num_iterations / 2].first << " "
							<< vect_translation_error[num_iterations - 1].first << " "
							<< average_translation_error.first << " "
							<< std_translation_error.first << " "
							<< vect_translation_error[num_iterations * 3 / 4].first << " " 
							<< vect_translation_error[num_iterations / 4].first << " "
							<< std::endl;
				
				std::sort(vect_angle_error.begin(), vect_angle_error.end(), pairCompareSecond);
				std::sort(vect_translation_error.begin(), vect_translation_error.end(), pairCompareSecond);
				// refined output
				output_file << vect_angle_error[num_iterations / 2].second << " "
							<< vect_angle_error[num_iterations - 1].second << " "
							<< average_angle_error.second << " "
							<< std_angle_error.second << " "
							<< vect_angle_error[num_iterations * 3 / 4].second << " " 
							<< vect_angle_error[num_iterations / 4].second << " " 
							// translation
							<< vect_translation_error[num_iterations / 2].second << " "
							<< vect_translation_error[num_iterations - 1].second << " "
							<< average_translation_error.second << " "
							<< std_translation_error.second << " "
							<< vect_translation_error[num_iterations * 3 / 4].second << " "
							<< vect_translation_error[num_iterations / 4].second << " "
							<< std::endl;
			
			} // outliers loop end
			fclose(fptr_algorithm_file);
			fclose(fptr_ground_truth);
			fclose(fptr_bearing_vector);
			output_file.close();
		} // algorithm_idx loop end
		
		
	} // nMatches loop end

	//fclose(fptr_meta_info);
	
	
	return 0;
}

