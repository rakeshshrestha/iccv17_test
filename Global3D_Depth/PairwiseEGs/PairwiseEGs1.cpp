#include <stdio.h>
#include <string.h>
#include <iostream>
#include <vector>

//#include "keys.h"
#include "vector.h"
#include "5point.h"
#include "Epipolar.h"

using namespace std;


bool LoadCalibratioinFile(const char* filename, vector<v3_t> & cam_intrinsics)
{
	FILE* fptr = fopen(filename, "r");
	if (fptr == NULL) 
	{
		return false;
	}

	double f, cx, cy;
	char buf[256];

	while (fgets(buf, 256, fptr))
	{
		sscanf(buf, "%lf %lf %lf", &f, &cx, &cy);
		cam_intrinsics.push_back(v3_new(f, cx, cy));
	}

	fclose(fptr);
	return true;

}


int main(int argc, char** argv) {

	if (argc != 4) {
		cerr << "usage: FivePointAlgorithm <calibration.txt> <corrs.txt>\n";
		return 1;
	}

	vector<v3_t> cam_intrinsics;
	if (!LoadCalibratioinFile(argv[1], cam_intrinsics)) 
	{
		cerr << "ERROR: unable to open file " << argv[1] << "\n";
		return 1;
	}

	// Load correspondence and compute E
	FILE* fptr = fopen(argv[2], "r");
	if (fptr == NULL) 
	{
		cerr << "ERROR: unable to open file " << argv[2] << "\n";
		return false;
	}

	FILE* fptr_out = fopen("ComputedRT.txt", "w+");
	if (fptr_out == NULL)
	{
		cerr << "ERROR: unable to write output file\n";
		return false;
	}

	double E_THRESHOLD = 4.0;
	sscanf(argv[3], "%lf", &E_THRESHOLD);

	int nMatches, v1, v2;
	double lx, ly, rx, ry;
	char buf[256];
	while (fgets(buf, 256, fptr))
	{
		sscanf(buf, "%d %d %d", &nMatches, &v1, &v2);
		v2_t *k1_pts = new v2_t[nMatches];
		v2_t *k2_pts = new v2_t[nMatches];

		for (int i=0; i<nMatches; i++)
		{
			fgets(buf, 256, fptr);
			sscanf(buf, "%lf %lf %lf %lf ", &lx, &ly, &rx, &ry);

			// The coordinate used in bundler is different form normal ones
			k1_pts[i] = v2_new(lx-cam_intrinsics[v1].p[1], -ly+cam_intrinsics[v1].p[2]);
			k2_pts[i] = v2_new(rx-cam_intrinsics[v2].p[1], -ry+cam_intrinsics[v2].p[2]);
		}

		double K1[9], K2[9];
		K1[0] = K1[4] = cam_intrinsics[v1].p[0]; K1[8] = 1.0;
		K1[1] = K1[2] = K1[3] = K1[5] = K1[6] = K1[7] = 0;

		K2[0] = K2[4] = cam_intrinsics[v2].p[0]; K2[8] = 1.0;
		K2[1] = K2[2] = K2[3] = K2[5] = K2[6] = K2[7] = 0;

		double R[9], t[3];

		int num_inliers = compute_pose_ransac(nMatches, k1_pts, k2_pts, 
			K1, K2, E_THRESHOLD, 200, R, t); // 200 is the ransac round and you may change it.

		//change coordinate system
		R[1] = -R[1]; R[2] = -R[2];
		R[3] = -R[3]; R[6] = -R[6];
		t[1] = -t[1]; t[2] = -t[2];

		fprintf(fptr_out,"%d %d %d\n", num_inliers, v1,v2);
		printf("\n[%d %d] have %d inliers among total %d feature matches.\n", v1,v2, num_inliers,nMatches);

		printf("R: \n");
		for (int i = 0; i<3; i++)
		{
			for (int j=0; j<3; j++)
			{
				fprintf(fptr_out, "%f ", R[3*i+j]);
				printf("%f ", R[3*i+j]);
			}
			fprintf(fptr_out, "\n");
			printf("\n");
		}

		printf("t: \n");
		for (int i = 0; i<3; i++)
		{
			fprintf(fptr_out, "%f ", t[i]);
			printf("%f ", t[i]);
		}
		fprintf(fptr_out, "\n");
		printf("\n");

		
		// Do the nonlinear optimiziation 
		double R_r[9], t_r[3];

		for (int i = 0; i < nMatches; i++) 
		{
			k1_pts[i].p[0] /= cam_intrinsics[v1].p[0];
			k1_pts[i].p[1] = -k1_pts[i].p[1]/cam_intrinsics[v1].p[0];

			k2_pts[i].p[0] /= cam_intrinsics[v2].p[0];
			k2_pts[i].p[1] = -k2_pts[i].p[1]/cam_intrinsics[v2].p[0];
		}
		
		RefineRtNonlinear(nMatches, k1_pts, k2_pts, cam_intrinsics[v1].p[0], cam_intrinsics[v2].p[0], R, t, R_r, t_r);


		delete [] k1_pts;
		delete [] k2_pts;
	}

	fclose(fptr);
	fclose(fptr_out);
	return true;

}