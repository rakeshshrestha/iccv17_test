#include "keys.h"
#include <vector>

using namespace std;

#define  MAX_KEY_MATCH 1000

inline vector<KeypointMatch> MatchKeyRobust
(int num_keys2, unsigned char *k2, ANNkd_tree *tree1, double ratio)
{
	int max_pts_visit = int(floor(0.1 * tree1->nPoints()));

	annMaxPtsVisit(max_pts_visit);
	vector<KeypointMatch> matches;

	ANNidx nn_idx[2];
	ANNdist dist[2];
	ANNpoint data_Pt = annAllocPt(128);
	vector<double> score;
	for (int i = 0; i < num_keys2; i++) 
	{
		double sum = 0;
		for (int j = 0; j < 128; j++)
		{
			data_Pt[j] = double(k2[i * 128 + j]);
			sum += data_Pt[j] * data_Pt[j];
		}
		sum = sqrt(sum);

		for (int j = 0; j < 128; j++)
			data_Pt[j] /= sum;

		tree1->annkPriSearch(data_Pt, 2, nn_idx, dist, 0.0);

		if (double(dist[0]) < ratio * ratio * double(dist[1])) 
		{
			unsigned short m_idx1 = unsigned short(nn_idx[0]);
			unsigned short m_idx2 = unsigned short(i);
			matches.push_back(KeypointMatch(m_idx1, m_idx2));
			score.push_back(dist[0]/dist[1]);
		}
	}
	annDeallocPt(data_Pt);

	if (matches.size() > MAX_KEY_MATCH)
	{
		//sort
		for (int i = 0; i < int(score.size()); i++)
		{

		}
	}
	return matches;    
}