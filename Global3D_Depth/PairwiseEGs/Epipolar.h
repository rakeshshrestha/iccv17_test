#pragma once
#include "Utilities.h"
#include "MyMatrix.h"
#include <vector.h>
#include "keys.h"
#include "eigen/Eigen/Eigen"

using namespace std;

struct RT_global_data
{
	double ini_R[9];
	double ini_t[9];

	double fr;
	double fl;

	double *weights;

	v2_t *r_pts;
	v2_t *l_pts;
};

double calculateRotationError(  Eigen::Matrix3d trueRotation,
                                Eigen::Matrix3d measuredRotation    );

double calculateTranslationError( Eigen::Vector3d trueTranslation,
                                Eigen::Vector3d measuredTranslation);

void calculateError(Eigen::Matrix3d trueRotation,
                    Eigen::Vector3d trueTranslation,
                    Eigen::Matrix3d measuredRotation,
                    Eigen::Vector3d measuredTranslation,
                    double &errorAngle,
                    double &errorTranslation);

void computeRelativeMotion(v2_t *k1_pts, v2_t *k2_pts, int nMatches, double K1[9], double K2[9], double E_THRESHOLD_NEW, double R_ransac[], double t_ransac[], double R_optimized[], double t_optimized[], int &num_inliers, double E[]);

static RT_global_data m_global_data;

void fillWeights(int num_pts, v2_t *l_pts, v2_t *r_pts, 
				 double fl, double fr, double *R, double *t, double *w, double sampsonThreshold);
void UpdateRT(double *p, double *R, double *t);
void rotationMatrixfromRodrigues(double *omega, double *R);
void computeEssentialFromRT(double *E, double *R, double *t);
void evaluateRT(double *par, double *hx, int m, int n, void *adata);
void calJacobian(double *par, double *jacb, int m, int n, void *adata);

/* Estimate an E-matrix from a given set of point matches */
vector<int> EstimateEMatrix(const std::vector<Keypoint> &k1, 
								 const std::vector<Keypoint> &k2, 
								 std::vector<KeypointMatch> matches, 
								 int num_trials, double threshold, 
								 double f1, double f2, 
								 double *E, double *F);

/* Estimate relative pose from a given set of point matches */
int EstimatePose5Point(const std::vector<Keypoint> &k1, 
					   const std::vector<Keypoint> &k2, 
					   std::vector<KeypointMatch> matches, 
					   int num_trials, double threshold, 
					   double *K1, double *K2, 
					   double *R, double *t);

/* Refine an R, t estimate using LM */
void RefineRtNonlinear(int num_pts,  v2_t *l_pts, v2_t *r_pts, double fl, double fr, 
					   double *R0, double *t0, double *R_out, double *t_out);

/* Estimate an F-matrix from a given set of point matches */
vector<int> EstimateFMatrix(const std::vector<Keypoint> &k1, 
								 const std::vector<Keypoint> &k2, 
								 std::vector<KeypointMatch> matches, 
								 int num_trials, double threshold, 
								 double *F, bool essential = false);

vector<int> EstimateFMatrix(const std::vector<KeypointWithDesc> &k1, 
								 const std::vector<KeypointWithDesc> &k2, 
								 std::vector<KeypointMatch> matches, 
								 int num_trials, double threshold, 
								 double *F, bool essential = false);

bool LoadCalibratioinFile(const char* filename, vector<v3_t> & cam_intrinsics);
