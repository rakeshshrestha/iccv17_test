#include "MatchPoint.h"
#include "VocTree.h"
#include "..\Common\keys.h"
#include "MatchTable.h"
#include <time.h>
#include "Epipolar.h"
#include "TwoViewGeometry.h"
#include "sba-1.5/sba.h"
#include "Bundle.h"
#include "ANNKeys.h"
#include "5point.h"
#include "SiftGPU.h"
using namespace std;
#include <fstream>
#include <iomanip>

#include <ppl.h>
#include <concurrent_vector.h>
#include <omp.h>

using namespace Concurrency;

////////////////////////////////////////////////////////////////////////////
#if !defined(SIFTGPU_STATIC) && !defined(SIFTGPU_DLL_RUNTIME) 
// SIFTGPU_STATIC comes from compiler
#define SIFTGPU_DLL_RUNTIME
// Load at runtime if the above macro defined
// comment the macro above to use static linking
#endif

#ifdef _WIN32
#ifdef SIFTGPU_DLL_RUNTIME
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#define FREE_MYLIB FreeLibrary
#define GET_MYPROC GetProcAddress
#else
//define this to get dll import definition for win32
#define SIFTGPU_DLL
#ifdef _DEBUG 
#pragma comment(lib, "../../lib/siftgpu_d.lib")
#else
#pragma comment(lib, "../../lib/siftgpu.lib")
#endif
#endif
#else
#ifdef SIFTGPU_DLL_RUNTIME
#include <dlfcn.h>
#define FREE_MYLIB dlclose
#define GET_MYPROC dlsym
#endif
#endif
extern FILE *EGsOutPutFile;
template <typename T>
vector<size_t> sort_indexes(const vector<T> &v) {

	// initialize original index locations
	vector<size_t> idx(v.size());
	for (size_t i = 0; i != idx.size(); ++i) idx[i] = i;

	// sort indexes based on comparing values in v
	sort(idx.begin(), idx.end(),
		[&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

	return idx;
}


MatchPoint::MatchPoint(void)
{
}


MatchPoint::~MatchPoint(void)
{
}

void MatchPoint::BruteForceMatch(double **cam_intrinsic)
{
#if 0
	vector<KeypointWithDesc> **keys_d;
	vector<Keypoint> **keys;
	unsigned char **desc; 

	keys_d = new vector<KeypointWithDesc>* [num_images];
	keys = new vector<Keypoint> *[num_images];
	desc = new unsigned char *[num_images];

	for (int i = 0; i < num_images; i++)
	{
		keys_d[i] = new vector<KeypointWithDesc>;
		*(keys_d[i]) = ReadKeyFileWithDesc(key_files[i].c_str(), true);

		int num_keys = (int) keys_d[i]->size();
		keys[i] = new vector<Keypoint>;
		desc[i]= new unsigned char [128 * num_keys];

		GetKeyDesc(*(keys_d[i]), desc[i]);
		*(keys[i]) = GetKeys(*(keys_d[i]));
	}
	end = clock();    
	printf("Reading keys took %0.3fs\n",  (end - start) / ((double) CLOCKS_PER_SEC));


	CameraMatrix cameras[2];
	double R[9], t[3];
	Rotation cR1; cR1.SetIdentity();
	cameras[0].SetExtPara(cR1, Vector3f(0, 0, 0));

	float c[12];
	for (int i = 0; i < num_images - 1; i++)
	{
		int num_keys_i = (int) keys_d[i]->size();
		if (num_keys_i < MINIMUM_RAW_CORRESPONDENCES)	continue;

		start = clock();
		ANNkd_tree *tree = CreateSearchTree(num_keys_i, desc[i]);

		cameras[0].SetIntPara(float(cam_intrinsic[i][0]),  
			                  float(cam_intrinsic[i][3]),  
							  float(cam_intrinsic[i][4]));

		for (int j = i + 1; j < num_images; j++) 
		{
			int num_keys_j = (int) keys[j]->size();
			if (num_keys_j < MINIMUM_RAW_CORRESPONDENCES)	continue;

			std::vector<KeypointMatch> matches = MatchKeys(num_keys_j, desc[j], tree, ratio); //(i, j)

			int num_matches = (int) matches.size();
			if (num_matches >= MINIMUM_RAW_CORRESPONDENCES) 
			{
				matches_f << i << " " << j << endl;
				matches_f << matches.size() << endl;

				for (int k = 0; k < num_matches; k++) 
					matches_f << matches[k].m_idx1 << " " << matches[k].m_idx2 << endl;

				/////////////////////////////////
				printf("Match view %d and view %d\n", i, j);
				int num_inlier = EstimatePose5Point(*(keys[i]), *(keys[j]), matches, 
					              EG_RANSAC_ROUND, 
					              MAXREPROERROR/(cam_intrinsic[i][0] + cam_intrinsic[j][0]), 
					              cam_intrinsic[i],
					              cam_intrinsic[j], R, t);

				if (num_inlier > 0)
				{
					for (int u = 0; u < 3; u++)
					{
						c[0] = float(R[0]); c[1] = float(R[1]); c[2] = float(R[2]);
						c[4] = float(R[3]); c[5] = float(R[4]); c[6] = float(R[5]);
						c[8] = float(R[6]); c[9] = float(R[7]); c[10] = float(R[8]);
						c[3] = float(t[0]); c[7] = float(t[1]); c[11] = float(t[2]);
					}
					cameras[1].Set(c);
					cameras[1].SetIntPara(float(cam_intrinsic[j][0]), 
						float(cam_intrinsic[j][3]), 
						float(cam_intrinsic[j][4]));

					int pt_count = 0;
					vector<int>  Inlier;
					for (int k = 0; k < num_matches; k++) 
					{
						int key_idx1 = matches[k].m_idx1;
						int key_idx2 = matches[k].m_idx2;

						float error;
						Vector2f p = Vector2f((*(keys[i]))[key_idx1].m_x, (*(keys[i]))[key_idx1].m_y);
						Vector2f q = Vector2f((*(keys[j]))[key_idx2].m_x, (*(keys[j]))[key_idx2].m_y);

						bool in_front = true;
						float angle = 0.0;
						Vector3f _apoint = 
							Triangulate(p, q, cameras[0], cameras[1], error, in_front, angle);

						if (error > MAXREPROERROR 
							|| !in_front 
							|| angle < MIN_TRIANGULATE_ANGLE) 	
							continue;

						Inlier.push_back(k);
						pt_count++;
					}

					if (Inlier.size() > MININUM_INLIER_CORRESPONDENCES)
					{
						EGs_f << "p " << Inlier.size() << " " << i << " " << j << endl;

						EGs_f << R[0] << " " << R[1] << " " << R[2] << " " << endl;
						EGs_f <<  R[3] << " " << R[4] << " " << R[5] << " " << endl;
						EGs_f <<  R[6] << " " << R[7] << " " << R[8] << endl;

						double tn = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
						t[0] /= tn; t[1] /= tn; t[2] /= tn;

						EGs_f << t[0] << " " << t[1] << " " << t[2] << endl;

						for (size_t k = 0; k < Inlier.size(); ++k)
						{
							int key_idx1 = matches[Inlier[k]].m_idx1;
							int key_idx2 = matches[Inlier[k]].m_idx2;

							double x_proj1 = (*(keys[i]))[key_idx1].m_x/cam_intrinsic[i][0];
							double y_proj1 = (*(keys[i]))[key_idx1].m_y/cam_intrinsic[i][0];
							double x_proj2 = (*(keys[j]))[key_idx2].m_x/cam_intrinsic[j][0];
							double y_proj2 = (*(keys[j]))[key_idx2].m_y/cam_intrinsic[j][0];

							EGs_f << key_idx1 << " " << x_proj1 << " " << y_proj1 << " " ;
							EGs_f << key_idx2 << " " << x_proj2 << " " << y_proj2 << endl;
						}
					}
				}        
			}
		}

		end = clock();    
		printf("Matching took %0.3fs\n", (end - start) / ((double) CLOCKS_PER_SEC));
		fflush(stdout);

		delete tree;
	}
	matches_f.close();
	EGs_f.close();

	for (int i = 0; i < num_images; i++) {
		if (keys[i] != NULL) 
			delete keys[i];
		if (keys_d[i] != NULL)
			delete keys_d[i];
		if (desc[i] != NULL)
			delete []desc[i];
		if (cam_intrinsic[i] != NULL)
			delete []cam_intrinsic[i];
	}
	delete []cam_intrinsic;
	delete [] keys;
	delete [] keys_d;
	delete []desc;
#endif
}

void MatchPoint::MatchUsingVoctree
	(int num_images, double **cam_intrinsic, 
	vector<string> &key_files, map<pair<int, int>, int> &match_list, 
	map<pair<int, int>, int> &old_matchList,
	const char* matches_out, const char* voctree_in, const char* keylist_in, int split_id, double ratio)
{
	clock_t start, end;
	FILE *matches_f;
#ifndef ADDMATCHES
	fopen_s(&matches_f, matches_out, "w");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#else
	fopen_s(&matches_f, matches_out, "a");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#endif


	//load voctree
	VocTree m_voctree;
	int nm = 0;
	int *match_id_p = NULL;
	if (match_list.empty())
	{
		if (m_voctree.LoadVocTree((char*)(voctree_in)) < 0) return ;
		printf("Finding image candidates...\n");
		num_images = m_voctree.FindImageCandidates((char*)(keylist_in), &match_id_p, nm);
		printf("Match %d images.\n", num_images);
		fflush(stdout);
		m_voctree.Clean();
	}
	else
		nm = num_images;
	//initialize camera parameters
	CameraParam cameras[2];
	for (int i = 0; i < 9; i++)
		cameras[0].R[i] = REAL(0.0);
	cameras[0].R[0] = cameras[0].R[4] = cameras[0].R[8] = REAL(1.0);

	for (int i = 0; i < 3; i++) cameras[0].cen[i] = REAL(0.0);

	//#define FINDMATCHIMAGEONLY
#ifdef FINDMATCHIMAGEONLY
	FILE *match_list = NULL;
	fopen_s(&match_list, "matchList.txt", "w");
	for (int i = 0; i < num_images - 1; i++)
	{
		printf("Match view %d\n", i);
		fflush(stdout);
		int img_id1 = i;

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;
			img_id2  = match_id_p[i * nm + j];

			if (img_id2 < 0) continue; 

			map<pair<int, int>, int>::iterator miter = black_list.find(make_pair(img_id1, img_id2));
			if (miter != black_list.end()) continue;

			const char *imgName0 = key_files[img_id1].c_str();
			const char *imgName1 = key_files[img_id2].c_str();

			fprintf_s(match_list, "%s %s\n", imgName0+2, imgName1+2);
		}
		end = clock();    
		fflush(stdout);
	}
	fclose(match_list);
#else
	vector<KeypointWithDesc> keys_d0, keys_d1;
	unsigned char *desc0, *desc1; 
	for (int i = 0; i < num_images - 1; i++)
	{
		printf("Pairwise matching for view %d / %d\n", i,num_images);
		fflush(stdout);
		int img_id1 = i;

		if (cam_intrinsic[img_id1][0] == 0) continue;


		keys_d0 = ReadKeyFileWithDesc(key_files[img_id1].c_str(), true);

		int num_keys1 = (int) keys_d0.size();
		if (num_keys1 < MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
		{
			ReleaseKey(keys_d0);
			continue;
		}

		desc0= new unsigned char [128 * num_keys1];
		GetKeyDesc(keys_d0, desc0);

		ANNpointArray pts = annAllocPts(num_keys1, 128);
		ANNkd_tree *tree = CreateSearchTree(num_keys1, desc0, pts);

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;

			if (match_id_p != NULL)
				img_id2  = match_id_p[i * nm + j];
			else
				img_id2 = j;

			if (img_id2 < 0) continue; 

			if (cam_intrinsic[img_id2][0] == 0) continue;

			//#ifdef ADDIMAGE
			if (img_id2 < split_id) continue;
			//#endif
			if (img_id2 <= img_id1) continue;

			if (!match_list.empty())
			{
				map<pair<int, int>, int>::iterator miter = match_list.find(make_pair(img_id1, img_id2));
				if (miter == match_list.end()) 
				{
					// 					printf("Skip pair [%d %d]\n", img_id1, img_id2);
					// 					fflush(stdout);
					continue;
				}
				else
				{
#ifdef ADDMATCHES
					map<pair<int, int>, int>::iterator miter_old = old_matchList.find(make_pair(img_id1, img_id2));
					if (miter_old != old_matchList.end())
						continue;
#endif
				}
			}

			keys_d1 = ReadKeyFileWithDesc(key_files[img_id2].c_str(), true);

			int num_keys2 = (int) keys_d1.size();
			if (num_keys2< MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
			{
				ReleaseKey(keys_d1);
				continue;
			}

			desc1 = new unsigned char [128 * num_keys2];
			GetKeyDesc(keys_d1, desc1);

			std::vector<KeypointMatch> matches = MatchKeys(num_keys2, desc1, tree, ratio); //(i, j)

			int num_matches = (int) matches.size();
			if (num_matches >= MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs) 
			{
				fprintf_s(matches_f, "%d %d\n", img_id1, img_id2);
				fprintf_s(matches_f, "%d\n", int(matches.size()));

				for (int k = 0; k < num_matches; k++) 
					fprintf_s(matches_f, "%d %d\n", int(matches[k].m_idx1), int(matches[k].m_idx2));
			}

			delete []desc1;
			ReleaseKey(keys_d1);
		}
		end = clock();    
		fflush(stdout);

		delete []desc0;
		ReleaseKey(keys_d0);

		annDeallocPts(pts);  
		delete tree;  
		annClose();
	}
#endif

	fclose(matches_f);
	if (match_id_p != NULL)
		delete []match_id_p;
}


void MatchPoint::MatchUsingVoctreeGPU
	(int num_images, double **cam_intrinsic, 
	vector<string> &key_files, map<pair<int, int>, int> &match_list, 
	map<pair<int, int>, int> &old_matchList,
	const char* matches_out, const char* voctree_in, const char* keylist_in, int split_id, double ratio)
{
	printf("MatchUsingVoctree\n");
	clock_t start, end;
	FILE *matches_f;
#ifndef ADDMATCHES
	fopen_s(&matches_f, matches_out, "w");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#else
	fopen_s(&matches_f, matches_out, "a");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#endif

	//Copy from SimpleSIFT
#ifdef SIFTGPU_DLL_RUNTIME
#ifdef _WIN32
#ifdef _DEBUG
	HMODULE  hsiftgpu = LoadLibrary("siftgpu_d.dll");
#else
	HMODULE  hsiftgpu = LoadLibrary("siftgpu.dll");
#endif
#else
	void * hsiftgpu = dlopen("libsiftgpu.so", RTLD_LAZY);
#endif

	
	if(hsiftgpu == NULL) 
	{
		printf("Can not load library SiftGPU\n");
		return;
	}

#ifdef REMOTE_SIFTGPU
	ComboSiftGPU* (*pCreateRemoteSiftGPU) (int, char*) = NULL;
	pCreateRemoteSiftGPU = (ComboSiftGPU* (*) (int, char*)) GET_MYPROC(hsiftgpu, "CreateRemoteSiftGPU");
	ComboSiftGPU * combo = pCreateRemoteSiftGPU(REMOTE_SERVER_PORT, REMOTE_SERVER);
	SiftGPU* sift = combo;
	SiftMatchGPU* matcher = combo;
#else
	
	int MatchMaxNum = 8192;
	//SiftGPU* (*pCreateNewSiftGPU)(int) = NULL;
	SiftMatchGPU* (*pCreateNewSiftMatchGPU)(int) = NULL;
	//pCreateNewSiftGPU = (SiftGPU* (*) (int)) GET_MYPROC(hsiftgpu, "CreateNewSiftGPU");
	pCreateNewSiftMatchGPU = (SiftMatchGPU* (*)(int)) GET_MYPROC(hsiftgpu, "CreateNewSiftMatchGPU");
	//SiftGPU* sift = pCreateNewSiftGPU(1);
	SiftMatchGPU* matcher = pCreateNewSiftMatchGPU(MatchMaxNum);
#endif

#elif defined(REMOTE_SIFTGPU)
	ComboSiftGPU * combo = CreateRemoteSiftGPU(REMOTE_SERVER_PORT, REMOTE_SERVER);
	SiftGPU* sift = combo;
	SiftMatchGPU* matcher = combo;
#else
	//this will use overloaded new operators
	SiftGPU  *sift = new SiftGPU;
	SiftMatchGPU *matcher = new SiftMatchGPU(4096);
#endif

	matcher->CreateContextGL();

	//load voctree
	VocTree m_voctree;
	int nm = 0;
	int *match_id_p = NULL;
    if (match_list.empty())
	{
		if (m_voctree.LoadVocTree((char*)(voctree_in)) < 0) return ;
		printf("Finding image candidates...\n");
		num_images = m_voctree.FindImageCandidates((char*)(keylist_in), &match_id_p, nm);
		printf("Match %d images.\n", num_images);
		fflush(stdout);
		m_voctree.Clean();
	}
	else
		nm = num_images;
	//initialize camera parameters
	CameraParam cameras[2];
	for (int i = 0; i < 9; i++)
		cameras[0].R[i] = REAL(0.0);
	cameras[0].R[0] = cameras[0].R[4] = cameras[0].R[8] = REAL(1.0);

	for (int i = 0; i < 3; i++) cameras[0].cen[i] = REAL(0.0);

//#define FINDMATCHIMAGEONLY
#ifdef FINDMATCHIMAGEONLY
	FILE *match_list = NULL;
	fopen_s(&match_list, "matchList.txt", "w");
	for (int i = 0; i < num_images - 1; i++)
	{
		printf("Match view %d\n", i);
		fflush(stdout);
		int img_id1 = i;

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;
			img_id2  = match_id_p[i * nm + j];

			if (img_id2 < 0) continue; 

			map<pair<int, int>, int>::iterator miter = black_list.find(make_pair(img_id1, img_id2));
			if (miter != black_list.end()) continue;

			const char *imgName0 = key_files[img_id1].c_str();
			const char *imgName1 = key_files[img_id2].c_str();

			fprintf_s(match_list, "%s %s\n", imgName0+2, imgName1+2);
		}
		end = clock();    
		fflush(stdout);
	}
	fclose(match_list);
#else
	vector<KeypointWithDesc> keys_d0, keys_d1;
	unsigned char *desc0, *desc1; 
	for (int i = 0; i < num_images - 1; i++)
	{
		printf("Pairwise matching for view %d / %d\n", i,num_images);
		fflush(stdout);
		int img_id1 = i;

		if (cam_intrinsic[img_id1][0] == 0) continue;


		keys_d0 = ReadKeyFileWithDesc(key_files[img_id1].c_str(), true);

		int num_keys1 = (int) keys_d0.size();
		if (num_keys1 < MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
		{
			ReleaseKey(keys_d0);
			continue;
		}

		desc0= new unsigned char [128 * num_keys1];
		GetKeyDesc(keys_d0, desc0);

		ANNpointArray pts = annAllocPts(num_keys1, 128);
		ANNkd_tree *tree = CreateSearchTree(num_keys1, desc0, pts);

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;

			if (match_id_p != NULL)
				img_id2  = match_id_p[i * nm + j];
			else
				img_id2 = j;

			if (img_id2 < 0) continue; 

			if (cam_intrinsic[img_id2][0] == 0) continue;

//#ifdef ADDIMAGE
			if (img_id2 < split_id) continue;
//#endif
			if (img_id2 <= img_id1) continue;

			if (!match_list.empty())
			{
				map<pair<int, int>, int>::iterator miter = match_list.find(make_pair(img_id1, img_id2));
				if (miter == match_list.end()) 
				{
// 					printf("Skip pair [%d %d]\n", img_id1, img_id2);
// 					fflush(stdout);
					continue;
				}
				else
				{
#ifdef ADDMATCHES
					map<pair<int, int>, int>::iterator miter_old = old_matchList.find(make_pair(img_id1, img_id2));
					if (miter_old != old_matchList.end())
						continue;
#endif
				}
			}

			keys_d1 = ReadKeyFileWithDesc(key_files[img_id2].c_str(), true);

			int num_keys2 = (int) keys_d1.size();
			if (num_keys2< MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
			{
				ReleaseKey(keys_d1);
				continue;
			}

			desc1 = new unsigned char [128 * num_keys2];
			GetKeyDesc(keys_d1, desc1);

			
			//Set descriptors for SIFTMatch
			matcher->SetDescriptors(0, num_keys1, desc0);
			matcher->SetDescriptors(1, num_keys2, desc1);
			
			//match and get result.
			int num_max = max(num_keys1,num_keys2);
			int (*match_buf)[2] = new int[min(num_max,MatchMaxNum)][2];
			//use the default thresholds. Check the declaration in SiftGPU.h
			int num_match = matcher->GetSiftMatch(num_max, match_buf,0.7,ratio);
			
			fprintf(EGsOutPutFile,"Image %d and Image %d: %d\n",img_id1,img_id2,num_match);

			std::vector<KeypointMatch> matches;
			for (int match_i = 0;match_i<num_match;match_i++)
			{
				matches.push_back(KeypointMatch(match_buf[match_i][0], match_buf[match_i][1]));
			}
			
			delete[] match_buf;
			
			//std::vector<KeypointMatch> matches = MatchKeys(num_keys2, desc1, tree, ratio); //(i, j)

			int num_matches = (int) matches.size();
			if (num_matches >= MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs) 
			{
				fprintf_s(matches_f, "%d %d\n", img_id1, img_id2);
				fprintf_s(matches_f, "%d\n", int(matches.size()));

				for (int k = 0; k < num_matches; k++) 
					fprintf_s(matches_f, "%d %d\n", int(matches[k].m_idx1), int(matches[k].m_idx2));
			}

			delete []desc1;
			ReleaseKey(keys_d1);
		}
		end = clock();    
		fflush(stdout);

		delete []desc0;
		ReleaseKey(keys_d0);

		annDeallocPts(pts);  
		delete tree;  
		annClose();
	}
#endif

	fclose(matches_f);
	if (match_id_p != NULL)
		delete []match_id_p;

	#ifdef REMOTE_SIFTGPU
		delete combo;
	#else
//		delete sift;
		delete matcher;
	#endif

	#ifdef SIFTGPU_DLL_RUNTIME
		FREE_MYLIB(hsiftgpu);
	#endif
}


void MatchPoint::GeometricVerification
	(int num_images, double **cam_intrinsic, 
	std::vector<std::string> &key_files, 
	map<pair<int, int>, int> &old_matchList, //temp
	const char *EGs_out, const char *matches_out, const char *matches_out2, 
	int start_id /* = 0 */, int split_id /* = 0 */, int add_id /* = 0 */)
{
	printf("Compute relative poses using 5 point algorithm. \n");
	fflush(stdout);

	FILE *EGs_f,*fid_matches;
#ifndef ADDMATCHES
	fopen_s(&EGs_f, EGs_out, "w");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}
#else
	fopen_s(&EGs_f, EGs_out, "a");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}
#endif

	fopen_s(&fid_matches, "matches.txt", "w");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}

	MatchTable m_matches(num_images);
	LoadAllMatches((char*)(matches_out), m_matches);
	PruneDoubleMatches((char*)(matches_out2), m_matches, num_images);

	vector<Keypoint> keys0, keys1;

	///cui 20130307
	vector<bool>  img0_status,img1_status; 
	vector<KeypointWithDesc> keys_d0, keys_d1;
	unsigned char *desc0, *desc1; 

	for (int img_id1 = 0; img_id1 < num_images; img_id1++)
	{
		keys0.clear();
		keys0 = ReadKeyFile(key_files[img_id1].c_str());
		printf("Compute relative pose for view %d / %d\n", img_id1, num_images);
		///cui 210130308
		//keys_d0 = ReadKeyFileWithDesc(key_files[img_id1].c_str(), true);
		//desc0= new unsigned char [128 * keys_d0.size()];
		//GetKeyDesc(keys_d0, desc0);
		//keys0 = ReadKeyFile(keys_d0);

		for (MatchAdjList::iterator iter = m_matches.Begin(img_id1); iter != m_matches.End(img_id1); iter++)
		{
			int img_id2 = int(iter->m_index);

#ifdef ADDMATCHES
			map<pair<int, int>, int>::iterator miter_old = old_matchList.find(make_pair(img_id1, img_id2));
			if (miter_old != old_matchList.end())
				continue;
#endif
			vector<KeypointMatch> &matches = iter->m_match_list;
			int num_matches = int(matches.size());

			keys1.clear();
			keys1 = ReadKeyFile(key_files[img_id2].c_str());
			//keys_d1 = ReadKeyFileWithDesc(key_files[img_id2].c_str(), true);
			
			//desc1= new unsigned char [128 * keys_d1.size()];
			//GetKeyDesc(keys_d1, desc1);

			//keys1 = ReadKeyFile(keys_d1);

			/////////////////////////////////
			//5-point
			double R[9], t[3];
			//printf("Compute relative pose for view %d and view %d\n", img_id1, img_id2);
			fprintf(EGsOutPutFile,"Compute relative pose for view %d and view %d\n", img_id1, img_id2);
			int num_inlier = EstimatePose5Point(keys0, keys1, matches, 
			EG_RANSAC_ROUND, E_THRESHOLD, cam_intrinsic[img_id1], cam_intrinsic[img_id2], R, t);

			fprintf(EGsOutPutFile,"Inliers: %d / %d \n", num_inlier, num_matches);

			if (num_inlier > MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
			{
				vector<KeypointMatch> matches_guided;
				vector<KeypointMatch> Inlier_loop;

				double E[9], F[9],R0[9],t0[3];
				double K1_inv[9], K2_inv[9];

				/*for (int loop=0;loop<2;loop++)
				{
					computeEssentialFromRT(E, R, t);
					for (int k = 0; k < 9; k++) 
					{
						K1_inv[k] = 0;
						K2_inv[k] = 0;
					}
					K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[img_id1][0]; K1_inv[8] = 1;
					K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[img_id2][0]; K2_inv[8] = 1;
					MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
					MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

					double t_sqr = double(E_THRESHOLD * E_THRESHOLD);

					///cui 20130307
					img0_status.clear();
					img0_status.assign(keys0.size(), false);

					img1_status.clear();
					img1_status.assign(keys1.size(), false);


					for (int k = 0; k < num_matches; k++)
					{
						unsigned short key_idx1 = matches[k].m_idx1;
						unsigned short key_idx2 = matches[k].m_idx2;

						double error, p[3], q[3];
						p[0] = double(keys0[key_idx1].m_x - cam_intrinsic[img_id1][1]);
						p[1] = double(keys0[key_idx1].m_y - cam_intrinsic[img_id1][2]);
						p[2] = 1;

						q[0] = double(keys1[key_idx2].m_x - cam_intrinsic[img_id2][1]);
						q[1] = double(keys1[key_idx2].m_y - cam_intrinsic[img_id2][2]);
						q[2] = 1;

						error = sampsonError(p, q, F); //qTFp
						if (error < t_sqr)
						{
							Inlier_loop.push_back(KeypointMatch(matches[k].m_idx1,matches[k].m_idx2));

							///cui 20130307
							img0_status[matches[k].m_idx1] = true;
							img1_status[matches[k].m_idx2] = true;
						}
					}

					matches_guided = GuidedMatching(keys0, keys1,desc0,desc1,img0_status,img1_status, F,5.0,0);

					cout<<loop<<"th Loop: Inlier.size()"<<Inlier_loop.size()<<" matches_guided.size()"<<matches_guided.size()<<" Total:"<<Inlier_loop.size()+matches_guided.size()<<endl;

					matches.clear();
					for (size_t k = 0; k < Inlier_loop.size(); ++k)
					{
						matches.push_back(Inlier_loop[k]);
					}

					for (size_t k = 0; k < matches_guided.size(); ++k)
					{
						matches.push_back(matches_guided[k]);
					}

					for (int k=0;k<9;k++)
						R0[k] = R[k];

					for (int k=0;k<3;k++)
						t0[k] = t[k];

					printf("Compute relative pose for view %d and view %d\n", img_id1, img_id2);
					int num_inlier = EstimatePose5Point(keys0, keys1, matches, 
						EG_RANSAC_ROUND, E_THRESHOLD, cam_intrinsic[img_id1], cam_intrinsic[img_id2], R, t);

					num_matches = matches.size();
					printf("Inliers after %d th loop: %d / %d \n", loop, num_inlier, num_matches);

					if (num_inlier<Inlier_loop.size())
					{
						Inlier_loop.clear();
						matches_guided.clear();

						for (int k=0;k<9;k++)
							R[k] = R0[k];
						for (int k=0;k<3;k++)
							t[k] = t0[k];
						break;
					}
					Inlier_loop.clear();
					matches_guided.clear();
				} */
				
				//Get inlier index
				computeEssentialFromRT(E, R, t);
				for (int k = 0; k < 9; k++) 
				{
					K1_inv[k] = 0;
					K2_inv[k] = 0;
				}
				K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[img_id1][0]; K1_inv[8] = 1;
				K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[img_id2][0]; K2_inv[8] = 1;
				MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
				MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

				vector<int> Inlier;
				double t_sqr = double(E_THRESHOLD * E_THRESHOLD);

				for (int k = 0; k < num_matches; k++)
				{
					unsigned short key_idx1 = matches[k].m_idx1;
					unsigned short key_idx2 = matches[k].m_idx2;

					double error, p[3], q[3];
					p[0] = double(keys0[key_idx1].m_x - cam_intrinsic[img_id1][1]);
					p[1] = double(keys0[key_idx1].m_y - cam_intrinsic[img_id1][2]);
					p[2] = 1;

					q[0] = double(keys1[key_idx2].m_x - cam_intrinsic[img_id2][1]);
					q[1] = double(keys1[key_idx2].m_y - cam_intrinsic[img_id2][2]);
					q[2] = 1;

					error = sampsonError(p, q, F); //qTFp
					if (error < t_sqr)
					{
						Inlier.push_back(k);
					}
				}

				if (Inlier.size()> MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
				{
					bool bad_estimate = false;

#ifdef TRIANGULATE
					vector<int> ba_inlier;
					vector<sba_point_t> points;

					for (int u = 0; u < 3; u++)
					{
						cameras[1].R[0] = REAL(R[0]); cameras[1].R[1] = REAL(R[1]); cameras[1].R[2] = REAL(R[2]);
						cameras[1].R[3] = REAL(R[3]); cameras[1].R[4] = REAL(R[4]); cameras[1].R[5] = REAL(R[5]);
						cameras[1].R[6] = REAL(R[6]); cameras[1].R[7] = REAL(R[7]); cameras[1].R[8] = REAL(R[8]);
						cameras[1].cen[0] = REAL(t[0]); cameras[1].cen[1] = REAL(t[1]); cameras[1].cen[2] = REAL(t[2]);

						MatrixMatrixProduct(1, 3, 3, cameras[1].cen, cameras[1].R, cameras[1].cen);
						MatrixScalarProduct(3, 1, cameras[1].cen, REAL(-1.0), cameras[1].cen);
					}

					for (int k = int(Inlier.size()) - 1; k >= 0; k--) 
					{
						int key_idx1 = matches[Inlier[k]].m_idx1;
						int key_idx2 = matches[Inlier[k]].m_idx2;

						float error;
						Vector2r p, q;
						p.v[0] = REAL((keys0[key_idx1].m_x - cam_intrinsic[img_id1][1])/cam_intrinsic[img_id1][0]);
						p.v[1] = REAL((keys0[key_idx1].m_y - cam_intrinsic[img_id1][2])/cam_intrinsic[img_id1][0]);

						q.v[0] = REAL((keys1[key_idx2].m_x - cam_intrinsic[img_id2][1])/cam_intrinsic[img_id2][0]);
						q.v[1] = REAL((keys1[key_idx2].m_y - cam_intrinsic[img_id2][2])/cam_intrinsic[img_id2][0]);

						bool in_front = true;
						float angle = 0.0;
						Vector3r _apoint = 
							Triangulate(p, q, cameras[0], cameras[1], error, in_front, angle);

						if (!in_front)
							//||error * 0.5 * (cam_intrinsic[img_id1][0] + cam_intrinsic[img_id2][0]) > OUTLIER_THRESHOLD)
						{
							Inlier.erase(Inlier.begin() + k);
							continue;
						}

						points.push_back(sba_point_t(_apoint.v[0], _apoint.v[1], _apoint.v[2]));
					}  //end for k

					if (Inlier.size() < MININUM_INLIER_CORRESPONDENCES)
						bad_estimate = true;
					else if (enable_BA)
					{
						double focal[2];
						focal[0] = cam_intrinsic[img_id1][0]; focal[1] = cam_intrinsic[img_id2][0];
						double R_new[9], t_new[3];
						double *projections = new double[Inlier.size() * 4];

						for (int k = 0; k < int(Inlier.size()); k++)
						{
							int key_idx1 = matches[Inlier[k]].m_idx1;
							int key_idx2 = matches[Inlier[k]].m_idx2;

							projections[2 * (k * 2 + 0) + 0] = keys0[key_idx1].m_x - cam_intrinsic[img_id1][1];
							projections[2 * (k * 2 + 0) + 1] = keys0[key_idx1].m_y - cam_intrinsic[img_id1][2];
							projections[2 * (k * 2 + 1) + 0] = keys1[key_idx2].m_x - cam_intrinsic[img_id2][1];
							projections[2 * (k * 2 + 1) + 1] = keys1[key_idx2].m_y - cam_intrinsic[img_id2][2];
						} //end for

						TwoViewBundleAdjustment(R, t, focal, &points[0], projections, Inlier, ba_inlier);
						delete []projections;
						
						if (ba_inlier.size() > MININUM_INLIER_CORRESPONDENCES)
							Inlier.assign(ba_inlier.begin(), ba_inlier.end());
						else
							bad_estimate = true;
						///////////////////////////////////////////////////////////////
					}//end enable_BA
#endif
					if (!bad_estimate)
					{
#ifdef TRIANGULATE
						CImage img1, img2;
						img1.Load(img_files[img_id1].c_str());
						img2.Load(img_files[img_id2].c_str());
#endif
						fprintf_s(EGs_f, "p %d %d %d\n", int(Inlier.size()), 
							             img_id1 + start_id, img_id2 + add_id);

						fprintf_s(EGs_f, "%f %f %f\n", float(R[0]), float(R[1]), float(R[2]));
						fprintf_s(EGs_f, "%f %f %f\n", float(R[3]), float(R[4]), float(R[5]));
						fprintf_s(EGs_f, "%f %f %f\n", float(R[6]), float(R[7]), float(R[8]));

						///cui 20130308
						fprintf_s(fid_matches, "%d %d\n%d\n", img_id1 + start_id, img_id2 + add_id, int(Inlier.size()));

						double tn = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
						t[0] /= tn; t[1] /= tn; t[2] /= tn;

						fprintf_s(EGs_f, "%f %f %f\n", float(t[0]), float(t[1]), float(t[2]));

						for (size_t k = 0; k < Inlier.size(); ++k)
						{
							unsigned short key_idx1 = matches[Inlier[k]].m_idx1;
							unsigned short key_idx2 = matches[Inlier[k]].m_idx2;

							double x_proj1 = (keys0[key_idx1].m_x - cam_intrinsic[img_id1][1])
								             /cam_intrinsic[img_id1][0];
							double y_proj1 = (keys0[key_idx1].m_y - cam_intrinsic[img_id1][2])
								             /cam_intrinsic[img_id1][0];

							double x_proj2 = (keys1[key_idx2].m_x - cam_intrinsic[img_id2][1])
								             /cam_intrinsic[img_id2][0];
							double y_proj2 = (keys1[key_idx2].m_y - cam_intrinsic[img_id2][2])
								             /cam_intrinsic[img_id2][0];
#ifdef TRIANGULATE
							pixel = (BYTE *)(img1.GetPixelAddress(keys0[key_idx1].m_x, keys0[key_idx1].m_y));
							color1[0] = int(*(pixel));
							color1[1] = int(*(pixel + 1));
							color1[2] = int(*(pixel + 2));

							pixel = (BYTE *)(img2.GetPixelAddress(keys1[key_idx2].m_x, keys1[key_idx2].m_y));
							color2[0] = int(*(pixel));
							color2[1] = int(*(pixel + 1));
							color2[2] = int(*(pixel + 2));

							int g = (color1[0] + color2[0])/2;
							int b = (color1[1] + color2[1])/2;
							int r =  (color1[2] + color2[2])/2;

							EGs_f << points[k].p[0] << " " << points[k].p[1] << " " << points[k].p[2] << " ";
							EGs_f << r << " " << g << " " << b << endl;
#endif
							fprintf_s(EGs_f, "%d %f %f  %d %f %f\n", int(key_idx1), float(x_proj1), float(y_proj1), 
								                                                              int(key_idx2), float(x_proj2), float(y_proj2));

							fprintf_s(fid_matches, "%d %d\n", int(key_idx1), int(key_idx2));
						} //end for


						//for (size_t k = 0; k < matches_guided.size(); ++k)
						//{
						//	unsigned short key_idx1 = matches_guided[k].m_idx1;
						//	unsigned short key_idx2 = matches_guided[k].m_idx2;

						//	double x_proj1 = (keys0[key_idx1].m_x - cam_intrinsic[img_id1][1])
						//		/cam_intrinsic[img_id1][0];
						//	double y_proj1 = (keys0[key_idx1].m_y - cam_intrinsic[img_id1][2])
						//		/cam_intrinsic[img_id1][0];

						//	double x_proj2 = (keys1[key_idx2].m_x - cam_intrinsic[img_id2][1])
						//		/cam_intrinsic[img_id2][0];
						//	double y_proj2 = (keys1[key_idx2].m_y - cam_intrinsic[img_id2][2])
						//		/cam_intrinsic[img_id2][0];

						//	fprintf_s(EGs_f, "%d %f %f  %d %f %f\n", int(key_idx1), float(x_proj1), float(y_proj1), 
						//		int(key_idx2), float(x_proj2), float(y_proj2));

						//	fprintf_s(fid_matches, "%d %d\n", int(key_idx1), int(key_idx2));
						//} //end for

#ifdef TRIANGULATE
						img1.Destroy();
						img2.Destroy();
#endif
					} // end bad_estimate
               }//end inlier.size()
		   }      //end number of inlier  
		}
	}
	fclose(EGs_f);
	fclose(fid_matches);
	m_matches.RemoveAll();
}

void MatchPoint::GeometricVerificationNew
	(int num_images, double **cam_intrinsic, 
	std::vector<std::string> &key_files, 
	map<pair<int, int>, int> &old_matchList, //temp
	const char *EGs_out, const char *matches_out, const char *matches_out2, 
	int start_id /* = 0 */, int split_id /* = 0 */, int add_id /* = 0 */)
{
	printf("Compute relative poses using 5 point algorithm. \n");
	fflush(stdout);

	FILE *EGs_f,*fid_matches;
#ifndef ADDMATCHES
	fopen_s(&EGs_f, EGs_out, "w");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}
#else
	fopen_s(&EGs_f, EGs_out, "a");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}
#endif

	fopen_s(&fid_matches, "matches.txt", "w");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}

	MatchTable m_matches(num_images);
	LoadAllMatches((char*)(matches_out), m_matches);
	PruneDoubleMatches((char*)(matches_out2), m_matches, num_images);

	vector<Keypoint> keys0, keys1;

	///cui 20130307
	vector<bool>  img0_status,img1_status; 
	vector<KeypointWithDesc> keys_d0, keys_d1;
	unsigned char *desc0, *desc1; 

	for (int img_id1 = 0; img_id1 < num_images; img_id1++)
	{
		keys0.clear();
		keys0 = ReadKeyFile(key_files[img_id1].c_str());
		printf("Compute relative pose for view %d / %d\n", img_id1, num_images);
		///cui 210130308
		//keys_d0 = ReadKeyFileWithDesc(key_files[img_id1].c_str(), true);
		//desc0= new unsigned char [128 * keys_d0.size()];
		//GetKeyDesc(keys_d0, desc0);
		//keys0 = ReadKeyFile(keys_d0);

		for (MatchAdjList::iterator iter = m_matches.Begin(img_id1); iter != m_matches.End(img_id1); iter++)
		{
			int img_id2 = int(iter->m_index);

#ifdef ADDMATCHES
			map<pair<int, int>, int>::iterator miter_old = old_matchList.find(make_pair(img_id1, img_id2));
			if (miter_old != old_matchList.end())
				continue;
#endif
			vector<KeypointMatch> &matches = iter->m_match_list;
			int num_matches = int(matches.size());

			keys1.clear();
			keys1 = ReadKeyFile(key_files[img_id2].c_str());
			//keys_d1 = ReadKeyFileWithDesc(key_files[img_id2].c_str(), true);
			
			//desc1= new unsigned char [128 * keys_d1.size()];
			//GetKeyDesc(keys_d1, desc1);

			//keys1 = ReadKeyFile(keys_d1);

			/////////////////////////////////
			//5-point
			double R[9], t[3];
			//printf("Compute relative pose for view %d and view %d\n", img_id1, img_id2);
			fprintf(EGsOutPutFile,"Compute relative pose for view %d and view %d\n", img_id1, img_id2);
			int num_inlier = EstimatePose5Point(keys0, keys1, matches, 
			EG_RANSAC_ROUND, E_THRESHOLD, cam_intrinsic[img_id1], cam_intrinsic[img_id2], R, t);

			fprintf(EGsOutPutFile,"Inliers: %d / %d \n", num_inlier, num_matches);

			if (num_inlier > MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
			{
				vector<KeypointMatch> matches_guided;
				vector<KeypointMatch> Inlier_loop;

				double E[9], F[9],R0[9],t0[3];
				double K1_inv[9], K2_inv[9];

				/*for (int loop=0;loop<2;loop++)
				{
					computeEssentialFromRT(E, R, t);
					for (int k = 0; k < 9; k++) 
					{
						K1_inv[k] = 0;
						K2_inv[k] = 0;
					}
					K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[img_id1][0]; K1_inv[8] = 1;
					K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[img_id2][0]; K2_inv[8] = 1;
					MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
					MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

					double t_sqr = double(E_THRESHOLD * E_THRESHOLD);

					///cui 20130307
					img0_status.clear();
					img0_status.assign(keys0.size(), false);

					img1_status.clear();
					img1_status.assign(keys1.size(), false);


					for (int k = 0; k < num_matches; k++)
					{
						unsigned short key_idx1 = matches[k].m_idx1;
						unsigned short key_idx2 = matches[k].m_idx2;

						double error, p[3], q[3];
						p[0] = double(keys0[key_idx1].m_x - cam_intrinsic[img_id1][1]);
						p[1] = double(keys0[key_idx1].m_y - cam_intrinsic[img_id1][2]);
						p[2] = 1;

						q[0] = double(keys1[key_idx2].m_x - cam_intrinsic[img_id2][1]);
						q[1] = double(keys1[key_idx2].m_y - cam_intrinsic[img_id2][2]);
						q[2] = 1;

						error = sampsonError(p, q, F); //qTFp
						if (error < t_sqr)
						{
							Inlier_loop.push_back(KeypointMatch(matches[k].m_idx1,matches[k].m_idx2));

							///cui 20130307
							img0_status[matches[k].m_idx1] = true;
							img1_status[matches[k].m_idx2] = true;
						}
					}

					matches_guided = GuidedMatching(keys0, keys1,desc0,desc1,img0_status,img1_status, F,5.0,0);

					cout<<loop<<"th Loop: Inlier.size()"<<Inlier_loop.size()<<" matches_guided.size()"<<matches_guided.size()<<" Total:"<<Inlier_loop.size()+matches_guided.size()<<endl;

					matches.clear();
					for (size_t k = 0; k < Inlier_loop.size(); ++k)
					{
						matches.push_back(Inlier_loop[k]);
					}

					for (size_t k = 0; k < matches_guided.size(); ++k)
					{
						matches.push_back(matches_guided[k]);
					}

					for (int k=0;k<9;k++)
						R0[k] = R[k];

					for (int k=0;k<3;k++)
						t0[k] = t[k];

					printf("Compute relative pose for view %d and view %d\n", img_id1, img_id2);
					int num_inlier = EstimatePose5Point(keys0, keys1, matches, 
						EG_RANSAC_ROUND, E_THRESHOLD, cam_intrinsic[img_id1], cam_intrinsic[img_id2], R, t);

					num_matches = matches.size();
					printf("Inliers after %d th loop: %d / %d \n", loop, num_inlier, num_matches);

					if (num_inlier<Inlier_loop.size())
					{
						Inlier_loop.clear();
						matches_guided.clear();

						for (int k=0;k<9;k++)
							R[k] = R0[k];
						for (int k=0;k<3;k++)
							t[k] = t0[k];
						break;
					}
					Inlier_loop.clear();
					matches_guided.clear();
				} */
				
				//Get inlier index
				computeEssentialFromRT(E, R, t);
				for (int k = 0; k < 9; k++) 
				{
					K1_inv[k] = 0;
					K2_inv[k] = 0;
				}
				K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[img_id1][0]; K1_inv[8] = 1;
				K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[img_id2][0]; K2_inv[8] = 1;
				MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
				MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

				vector<int> Inlier;
				double t_sqr = double(E_THRESHOLD * E_THRESHOLD);

				for (int k = 0; k < num_matches; k++)
				{
					unsigned short key_idx1 = matches[k].m_idx1;
					unsigned short key_idx2 = matches[k].m_idx2;

					double error, p[3], q[3];
					p[0] = double(keys0[key_idx1].m_x - cam_intrinsic[img_id1][1]);
					p[1] = double(keys0[key_idx1].m_y - cam_intrinsic[img_id1][2]);
					p[2] = 1;

					q[0] = double(keys1[key_idx2].m_x - cam_intrinsic[img_id2][1]);
					q[1] = double(keys1[key_idx2].m_y - cam_intrinsic[img_id2][2]);
					q[2] = 1;

					error = sampsonError(p, q, F); //qTFp
					if (error < t_sqr)
					{
						Inlier.push_back(k);
					}
				}

				if (Inlier.size()> MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
				{
					bool bad_estimate = false;

#ifdef TRIANGULATE
					vector<int> ba_inlier;
					vector<sba_point_t> points;

					for (int u = 0; u < 3; u++)
					{
						cameras[1].R[0] = REAL(R[0]); cameras[1].R[1] = REAL(R[1]); cameras[1].R[2] = REAL(R[2]);
						cameras[1].R[3] = REAL(R[3]); cameras[1].R[4] = REAL(R[4]); cameras[1].R[5] = REAL(R[5]);
						cameras[1].R[6] = REAL(R[6]); cameras[1].R[7] = REAL(R[7]); cameras[1].R[8] = REAL(R[8]);
						cameras[1].cen[0] = REAL(t[0]); cameras[1].cen[1] = REAL(t[1]); cameras[1].cen[2] = REAL(t[2]);

						MatrixMatrixProduct(1, 3, 3, cameras[1].cen, cameras[1].R, cameras[1].cen);
						MatrixScalarProduct(3, 1, cameras[1].cen, REAL(-1.0), cameras[1].cen);
					}

					for (int k = int(Inlier.size()) - 1; k >= 0; k--) 
					{
						int key_idx1 = matches[Inlier[k]].m_idx1;
						int key_idx2 = matches[Inlier[k]].m_idx2;

						float error;
						Vector2r p, q;
						p.v[0] = REAL((keys0[key_idx1].m_x - cam_intrinsic[img_id1][1])/cam_intrinsic[img_id1][0]);
						p.v[1] = REAL((keys0[key_idx1].m_y - cam_intrinsic[img_id1][2])/cam_intrinsic[img_id1][0]);

						q.v[0] = REAL((keys1[key_idx2].m_x - cam_intrinsic[img_id2][1])/cam_intrinsic[img_id2][0]);
						q.v[1] = REAL((keys1[key_idx2].m_y - cam_intrinsic[img_id2][2])/cam_intrinsic[img_id2][0]);

						bool in_front = true;
						float angle = 0.0;
						Vector3r _apoint = 
							Triangulate(p, q, cameras[0], cameras[1], error, in_front, angle);

						if (!in_front)
							//||error * 0.5 * (cam_intrinsic[img_id1][0] + cam_intrinsic[img_id2][0]) > OUTLIER_THRESHOLD)
						{
							Inlier.erase(Inlier.begin() + k);
							continue;
						}

						points.push_back(sba_point_t(_apoint.v[0], _apoint.v[1], _apoint.v[2]));
					}  //end for k

					if (Inlier.size() < MININUM_INLIER_CORRESPONDENCES)
						bad_estimate = true;
					else if (enable_BA)
					{
						double focal[2];
						focal[0] = cam_intrinsic[img_id1][0]; focal[1] = cam_intrinsic[img_id2][0];
						double R_new[9], t_new[3];
						double *projections = new double[Inlier.size() * 4];

						for (int k = 0; k < int(Inlier.size()); k++)
						{
							int key_idx1 = matches[Inlier[k]].m_idx1;
							int key_idx2 = matches[Inlier[k]].m_idx2;

							projections[2 * (k * 2 + 0) + 0] = keys0[key_idx1].m_x - cam_intrinsic[img_id1][1];
							projections[2 * (k * 2 + 0) + 1] = keys0[key_idx1].m_y - cam_intrinsic[img_id1][2];
							projections[2 * (k * 2 + 1) + 0] = keys1[key_idx2].m_x - cam_intrinsic[img_id2][1];
							projections[2 * (k * 2 + 1) + 1] = keys1[key_idx2].m_y - cam_intrinsic[img_id2][2];
						} //end for

						TwoViewBundleAdjustment(R, t, focal, &points[0], projections, Inlier, ba_inlier);
						delete []projections;
						
						if (ba_inlier.size() > MININUM_INLIER_CORRESPONDENCES)
							Inlier.assign(ba_inlier.begin(), ba_inlier.end());
						else
							bad_estimate = true;
						///////////////////////////////////////////////////////////////
					}//end enable_BA
#endif
					if (!bad_estimate)
					{
#ifdef TRIANGULATE
						CImage img1, img2;
						img1.Load(img_files[img_id1].c_str());
						img2.Load(img_files[img_id2].c_str());
#endif
						fprintf_s(EGs_f, "p %d %d %d\n", int(Inlier.size()), 
							             img_id1 + start_id, img_id2 + add_id);

						fprintf_s(EGs_f, "%f %f %f\n", float(R[0]), float(R[1]), float(R[2]));
						fprintf_s(EGs_f, "%f %f %f\n", float(R[3]), float(R[4]), float(R[5]));
						fprintf_s(EGs_f, "%f %f %f\n", float(R[6]), float(R[7]), float(R[8]));

						///cui 20130308
						fprintf_s(fid_matches, "%d %d\n%d\n", img_id1 + start_id, img_id2 + add_id, int(Inlier.size()));

						double tn = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
						t[0] /= tn; t[1] /= tn; t[2] /= tn;

						fprintf_s(EGs_f, "%f %f %f\n", float(t[0]), float(t[1]), float(t[2]));

						for (size_t k = 0; k < Inlier.size(); ++k)
						{
							unsigned short key_idx1 = matches[Inlier[k]].m_idx1;
							unsigned short key_idx2 = matches[Inlier[k]].m_idx2;

							double x_proj1 = (keys0[key_idx1].m_x - cam_intrinsic[img_id1][1])
								             /cam_intrinsic[img_id1][0];
							double y_proj1 = (keys0[key_idx1].m_y - cam_intrinsic[img_id1][2])
								             /cam_intrinsic[img_id1][0];

							double x_proj2 = (keys1[key_idx2].m_x - cam_intrinsic[img_id2][1])
								             /cam_intrinsic[img_id2][0];
							double y_proj2 = (keys1[key_idx2].m_y - cam_intrinsic[img_id2][2])
								             /cam_intrinsic[img_id2][0];
#ifdef TRIANGULATE
							pixel = (BYTE *)(img1.GetPixelAddress(keys0[key_idx1].m_x, keys0[key_idx1].m_y));
							color1[0] = int(*(pixel));
							color1[1] = int(*(pixel + 1));
							color1[2] = int(*(pixel + 2));

							pixel = (BYTE *)(img2.GetPixelAddress(keys1[key_idx2].m_x, keys1[key_idx2].m_y));
							color2[0] = int(*(pixel));
							color2[1] = int(*(pixel + 1));
							color2[2] = int(*(pixel + 2));

							int g = (color1[0] + color2[0])/2;
							int b = (color1[1] + color2[1])/2;
							int r =  (color1[2] + color2[2])/2;

							EGs_f << points[k].p[0] << " " << points[k].p[1] << " " << points[k].p[2] << " ";
							EGs_f << r << " " << g << " " << b << endl;
#endif
							fprintf_s(EGs_f, "%d %f %f %f %f  %d %f %f %f %f\n", int(key_idx1), float(x_proj1), float(y_proj1), keys0[key_idx1].m_x, keys0[key_idx1].m_y,
								                                                              int(key_idx2), float(x_proj2), float(y_proj2),keys1[key_idx2].m_x,keys1[key_idx2].m_y);

							fprintf_s(fid_matches, "%d %d\n", int(key_idx1), int(key_idx2));
						} //end for


						//for (size_t k = 0; k < matches_guided.size(); ++k)
						//{
						//	unsigned short key_idx1 = matches_guided[k].m_idx1;
						//	unsigned short key_idx2 = matches_guided[k].m_idx2;

						//	double x_proj1 = (keys0[key_idx1].m_x - cam_intrinsic[img_id1][1])
						//		/cam_intrinsic[img_id1][0];
						//	double y_proj1 = (keys0[key_idx1].m_y - cam_intrinsic[img_id1][2])
						//		/cam_intrinsic[img_id1][0];

						//	double x_proj2 = (keys1[key_idx2].m_x - cam_intrinsic[img_id2][1])
						//		/cam_intrinsic[img_id2][0];
						//	double y_proj2 = (keys1[key_idx2].m_y - cam_intrinsic[img_id2][2])
						//		/cam_intrinsic[img_id2][0];

						//	fprintf_s(EGs_f, "%d %f %f  %d %f %f\n", int(key_idx1), float(x_proj1), float(y_proj1), 
						//		int(key_idx2), float(x_proj2), float(y_proj2));

						//	fprintf_s(fid_matches, "%d %d\n", int(key_idx1), int(key_idx2));
						//} //end for

#ifdef TRIANGULATE
						img1.Destroy();
						img2.Destroy();
#endif
					} // end bad_estimate
               }//end inlier.size()
		   }      //end number of inlier  
		}
	}
	fclose(EGs_f);
	fclose(fid_matches);
	m_matches.RemoveAll();
}

void MatchPoint::RelativePoseFromSyntheticData
	(int num_images, const char *matches_in, const char *EGs_out,  
	double **cam_intrinsic, double noise)
{
	printf("Compute relative poses using 5 point algorithm. \n");

	FILE *EGs_f;
	fopen_s(&EGs_f, EGs_out, "w");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}

	FILE *matches_f;
	fopen_s(&matches_f, matches_in, "r");
	if (matches_f == NULL)
	{
		printf("Error opening file for reading\n");
		fflush(stdout);
		exit(1);
	}
	
	//read matches xi_j: ith image, jth point
	//x0_0 y0_0 x1_0, y1_0...xn_0, yn_0
	//x0_1 y0_1 x1_1, y1_1...xn_1, yn_1
	char buf[1024];
	vector<vector<Keypoint>> match_keys; match_keys.reserve(num_images);
	while (fgets(buf, 1024, matches_f))
	{
		float x, y;
		Keypoint keypt;
		for (int i = 0; i < num_images; i++)
		{
			sscanf_s(buf, "%f", &x);
			sscanf_s(buf, "%f", &y);

			keypt.m_x = x;
			keypt.m_y = y;
			match_keys[i].push_back(keypt);
		}
	}

	for (int vid0 = 0; vid0 < num_images; vid0++)
	{
		printf("Compute relative pose for view %d / %d\n", vid0, num_images);
		vector<Keypoint> &keys0 = match_keys[vid0];
		for (int vid1 = vid0+1; vid1 < num_images; vid1++)
		{
			vector<Keypoint> &keys1 = match_keys[vid1];
			vector<KeypointMatch> matches;
			
			for (int i = 0; i < int(keys0.size()); i++)
			{
				if (keys0[i].m_x == -1 && keys0[i].m_y == -1)
					continue;
				KeypointMatch km;
				km.m_idx1 = i;
				km.m_idx2 = i;
				matches.push_back(km);
			}

			double R[9], t[3];
			//printf("Compute relative pose for view %d and view %d\n", vid0, vid1);
			fprintf(EGsOutPutFile,"Compute relative pose for view %d and view %d\n", vid0, vid1);
			int num_inlier = EstimatePose5Point(keys0, keys1, matches, 
				             EG_RANSAC_ROUND, E_THRESHOLD, cam_intrinsic[vid0], cam_intrinsic[vid1], R, t);

			fprintf(EGsOutPutFile,"Inliers: %d / %d \n", num_inlier, matches.size());

			if (num_inlier > MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
			{
				double E[9], F[9];
				double K1_inv[9], K2_inv[9];

				computeEssentialFromRT(E, R, t);
				for (int k = 0; k < 9; k++) 
				{
					K1_inv[k] = 0;
					K2_inv[k] = 0;
				}
				K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[vid0][0]; K1_inv[8] = 1;
				K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[vid1][0]; K2_inv[8] = 1;
				MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
				MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

				vector<int> Inlier;
				double t_sqr = double(E_THRESHOLD * E_THRESHOLD);
				for (int k = 0; k < int(matches.size()); k++)
				{
					unsigned short key_idx1 = matches[k].m_idx1;
					unsigned short key_idx2 = matches[k].m_idx2;

					double error, p[3], q[3];
					p[0] = double(keys0[key_idx1].m_x - cam_intrinsic[vid0][1]);
					p[1] = double(keys0[key_idx1].m_y - cam_intrinsic[vid0][2]);
					p[2] = 1;

					q[0] = double(keys1[key_idx2].m_x - cam_intrinsic[vid1][1]);
					q[1] = double(keys1[key_idx2].m_y - cam_intrinsic[vid1][2]);
					q[2] = 1;

					error = sampsonError(p, q, F); //qTFp
					if (error < t_sqr)
						Inlier.push_back(k);
				}

				if (Inlier.size() > MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
				{
					bool bad_estimate = false;

#ifdef TRIANGULATE
					vector<int> ba_inlier;
					vector<sba_point_t> points;

					for (int u = 0; u < 3; u++)
					{
						cameras[1].R[0] = REAL(R[0]); cameras[1].R[1] = REAL(R[1]); cameras[1].R[2] = REAL(R[2]);
						cameras[1].R[3] = REAL(R[3]); cameras[1].R[4] = REAL(R[4]); cameras[1].R[5] = REAL(R[5]);
						cameras[1].R[6] = REAL(R[6]); cameras[1].R[7] = REAL(R[7]); cameras[1].R[8] = REAL(R[8]);
						cameras[1].cen[0] = REAL(t[0]); cameras[1].cen[1] = REAL(t[1]); cameras[1].cen[2] = REAL(t[2]);

						MatrixMatrixProduct(1, 3, 3, cameras[1].cen, cameras[1].R, cameras[1].cen);
						MatrixScalarProduct(3, 1, cameras[1].cen, REAL(-1.0), cameras[1].cen);
					}

					for (int k = int(Inlier.size()) - 1; k >= 0; k--) 
					{
						int key_idx1 = matches[Inlier[k]].m_idx1;
						int key_idx2 = matches[Inlier[k]].m_idx2;

						float error;
						Vector2r p, q;
						p.v[0] = REAL((keys0[key_idx1].m_x - cam_intrinsic[vid0][1])/cam_intrinsic[vid0][0]);
						p.v[1] = REAL((keys0[key_idx1].m_y - cam_intrinsic[vid0][2])/cam_intrinsic[vid0][0]);

						q.v[0] = REAL((keys1[key_idx2].m_x - cam_intrinsic[vid1][1])/cam_intrinsic[vid1][0]);
						q.v[1] = REAL((keys1[key_idx2].m_y - cam_intrinsic[vid1][2])/cam_intrinsic[vid1][0]);

						bool in_front = true;
						float angle = 0.0;
						Vector3r _apoint = 
							Triangulate(p, q, cameras[0], cameras[1], error, in_front, angle);

						if (!in_front)
							//||error * 0.5 * (cam_intrinsic[img_id1][0] + cam_intrinsic[img_id2][0]) > OUTLIER_THRESHOLD)
						{
							Inlier.erase(Inlier.begin() + k);
							continue;
						}

						points.push_back(sba_point_t(_apoint.v[0], _apoint.v[1], _apoint.v[2]));
					}  //end for k

					if (Inlier.size() < MININUM_INLIER_CORRESPONDENCES)
						bad_estimate = true;
					else if (enable_BA)
					{
						double focal[2];
						focal[0] = cam_intrinsic[vid0][0]; focal[1] = cam_intrinsic[vid0][0];
						double R_new[9], t_new[3];
						double *projections = new double[Inlier.size() * 4];

						for (int k = 0; k < int(Inlier.size()); k++)
						{
							int key_idx1 = matches[Inlier[k]].m_idx1;
							int key_idx2 = matches[Inlier[k]].m_idx2;

							projections[2 * (k * 2 + 0) + 0] = keys0[key_idx1].m_x - cam_intrinsic[vid0][1];
							projections[2 * (k * 2 + 0) + 1] = keys0[key_idx1].m_y - cam_intrinsic[vid0][2];
							projections[2 * (k * 2 + 1) + 0] = keys1[key_idx2].m_x - cam_intrinsic[vid1][1];
							projections[2 * (k * 2 + 1) + 1] = keys1[key_idx2].m_y - cam_intrinsic[vid1][2];
						} //end for

						TwoViewBundleAdjustment(R, t, focal, &points[0], projections, Inlier, ba_inlier);
						delete []projections;

						if (ba_inlier.size() > MININUM_INLIER_CORRESPONDENCES)
							Inlier.assign(ba_inlier.begin(), ba_inlier.end());
						else
							bad_estimate = true;
						///////////////////////////////////////////////////////////////
					}//end enable_BA
#endif
					if (!bad_estimate)
					{
#ifdef TRIANGULATE
						CImage img1, img2;
						img1.Load(img_files[img_id1].c_str());
						img2.Load(img_files[img_id2].c_str());
#endif
						fprintf_s(EGs_f, "p %d %d %d\n", int(Inlier.size()), 
							vid0, vid1);

						fprintf_s(EGs_f, "%f %f %f\n", float(R[0]), float(R[1]), float(R[2]));
						fprintf_s(EGs_f, "%f %f %f\n", float(R[3]), float(R[4]), float(R[5]));
						fprintf_s(EGs_f, "%f %f %f\n", float(R[6]), float(R[7]), float(R[8]));

						double tn = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
						t[0] /= tn; t[1] /= tn; t[2] /= tn;

						fprintf_s(EGs_f, "%f %f %f\n", float(t[0]), float(t[1]), float(t[2]));

						for (size_t k = 0; k < Inlier.size(); ++k)
						{
							unsigned short key_idx1 = matches[Inlier[k]].m_idx1;
							unsigned short key_idx2 = matches[Inlier[k]].m_idx2;

							double x_proj1 = (keys0[key_idx1].m_x - cam_intrinsic[vid0][1])
								/cam_intrinsic[vid0][0];
							double y_proj1 = (keys0[key_idx1].m_y - cam_intrinsic[vid0][2])
								/cam_intrinsic[vid0][0];

							double x_proj2 = (keys1[key_idx2].m_x - cam_intrinsic[vid1][1])
								/cam_intrinsic[vid1][0];
							double y_proj2 = (keys1[key_idx2].m_y - cam_intrinsic[vid1][2])
								/cam_intrinsic[vid1][0];
#ifdef TRIANGULATE
							pixel = (BYTE *)(img1.GetPixelAddress(keys0[key_idx1].m_x, keys0[key_idx1].m_y));
							color1[0] = int(*(pixel));
							color1[1] = int(*(pixel + 1));
							color1[2] = int(*(pixel + 2));

							pixel = (BYTE *)(img2.GetPixelAddress(keys1[key_idx2].m_x, keys1[key_idx2].m_y));
							color2[0] = int(*(pixel));
							color2[1] = int(*(pixel + 1));
							color2[2] = int(*(pixel + 2));

							int g = (color1[0] + color2[0])/2;
							int b = (color1[1] + color2[1])/2;
							int r =  (color1[2] + color2[2])/2;

							EGs_f << points[k].p[0] << " " << points[k].p[1] << " " << points[k].p[2] << " ";
							EGs_f << r << " " << g << " " << b << endl;
#endif
							fprintf_s(EGs_f, "%d %f %f  %d %f %f\n", int(key_idx1), float(x_proj1), float(y_proj1), 
								int(key_idx2), float(x_proj2), float(y_proj2));
						} //end for

#ifdef TRIANGULATE
						img1.Destroy();
						img2.Destroy();
#endif
					} // end bad_estimate
				}//end inlier.size()
			}      //end number of inlier  
		}
	}
	fclose(EGs_f);
}

std::vector<KeypointMatch> MatchPoint::GuidedMatching(vector<Keypoint>  &keys0, vector<Keypoint> &keys1,unsigned char *desc0,  unsigned char *desc1,vector<bool> &img0_status,vector<bool> &img1_status, double F[9],double thr_guided,double ratio)
{
	int i,j;
	double X0[3] = {0.0, 0.0,1.0};
	double L1[3];
	vector <int> candidate;

	vector<KeypointMatch> matches_guided;

	//annMaxPtsVisit(max_pts_visit);

	ANNidx nn_idx[2];
	ANNdist dist[2];
	ANNpoint data_Pt = annAllocPt(128);
	ANNpoint data_best = annAllocPt(128);

	cout<<keys0.size()<<endl;

	for(i=0;i<keys0.size();i++)
	{
		if (!img0_status[i])
		{
			X0[0] = double(keys0[i].m_x);
			X0[1] = double(keys0[i].m_y);

			MatrixMatrixProduct(3,3,1,F,X0,L1);

			double temp = sqrt(L1[0]*L1[0]+L1[1]*L1[1]);

			candidate.clear();
			for (j=0;j<keys1.size();j++)
			{
				if (!img1_status[j])
				{
					double dist = abs(keys1[j].m_x*L1[0]+keys1[j].m_y*L1[1]+L1[2])/temp;
					if (dist<thr_guided)
					{
						candidate.push_back(j);

					}
				}
			}

			if (ratio)
			{
				if (candidate.size()>1)
				{
					//cout<<i<<" "<<candidate.size()<<endl;
					double sum = 0;
					for (int k = 0; k < 128; k++)
					{
						data_Pt[k] = double(desc0[i * 128 + k]);
						sum += data_Pt[k] * data_Pt[k];
					}
					sum = sqrt(sum);

					for (int k = 0; k < 128; k++)
						data_Pt[k] /= sum;

					ANNpointArray pts = annAllocPts(candidate.size(), 128);
					ANNkd_tree *tree = CreateSearchTree(candidate, desc1, pts);
					tree->annkPriSearch(data_Pt, 2, nn_idx, dist, 0.0);

					if (double(dist[0]) < ratio * ratio * double(dist[1])) 
					{
						unsigned short m_idx1 = unsigned short(candidate[nn_idx[0]]);
						unsigned short m_idx0 = unsigned short(i);
						matches_guided.push_back(KeypointMatch(m_idx0, m_idx1));
						//cout<<"Success: "<<m_idx0<<" "<<m_idx1<<" "<<keys0[m_idx0].m_x<<" "<<keys0[m_idx0].m_y<<" "<<keys1[m_idx1].m_x<<" "<<keys1[m_idx1].m_y<<endl;
						img1_status[candidate[nn_idx[0]]] = true;
					}

					/* Cleanup */
					annDeallocPts(pts);
					delete tree;
				}

			}
			else
			{
				if (candidate.size()>0)
				{
					//cout<<i<<" "<<candidate.size()<<endl;
					double sum = 0;
					for (int k = 0; k < 128; k++)
					{
						data_Pt[k] = double(desc0[i * 128 + k]);
						sum += data_Pt[k] * data_Pt[k];
					}
					sum = sqrt(sum);

					for (int k = 0; k < 128; k++)
						data_Pt[k] /= sum;

					ANNpointArray pts = annAllocPts(candidate.size(), 128);
					ANNkd_tree *tree = CreateSearchTree(candidate, desc1, pts);
					tree->annkPriSearch(data_Pt, 2, nn_idx, dist, 0.0);

					/// 20130311  Compute the angle differences between two sift vector
					//int temp_index = candidate[nn_idx[0]];
					//for (int k = 0; k < 128; k++)
					//{
					//	data_best[k] = double(desc1[temp_index * 128 + k]);
					//	sum += data_best[k] * data_best[k];
					//}
					//sum = sqrt(sum);
					//double diff_v = 0.0;
					//for(int k=0;k<128;k++)
					//	diff_v += data_Pt[k] * data_best[k];

					//diff_v = diff_v/sum;

					double diff_v = 0.0;
					for(int k=0;k<128;k++)
						diff_v += data_Pt[k] * pts[nn_idx[0]][k];
					
					//cout<<diff_v<<endl;

					if (diff_v> 0.866)  //cos(pi/6) =  0.866025403784439
					{
						unsigned short m_idx1 = unsigned short(candidate[nn_idx[0]]);
						unsigned short m_idx0 = unsigned short(i);
						matches_guided.push_back(KeypointMatch(m_idx0, m_idx1));
						//cout<<"Success: "<<m_idx0<<" "<<m_idx1<<" "<<keys0[m_idx0].m_x<<" "<<keys0[m_idx0].m_y<<" "<<keys1[m_idx1].m_x<<" "<<keys1[m_idx1].m_y<<endl;
						img1_status[candidate[nn_idx[0]]] = true;
					}
					

					/* Cleanup */
					annDeallocPts(pts);
					delete tree;
				}
			}		
		}
	}
	annDeallocPt(data_Pt);
	return matches_guided;
}


void MatchPoint::ComputeEformVisualSFMResults
	(int num_images, std::vector<std::string> & img_files,double **cam_intrinsic,  
	const char *EGs_out, const char *VisualSFMResults, 
	int start_id /* = 0 */, int split_id /* = 0 */, int add_id /* = 0 */)
{
	printf("Compute relative poses using 5 point algorithm based on the results of VisualSFM. \n");
	fflush(stdout);

	FILE *EGs_f;
	fopen_s(&EGs_f, EGs_out, "w");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}

	//ifstream in_file; 
	FILE *in_file = NULL;
	//in_file.open(filename);
	fopen_s(&in_file, VisualSFMResults, "r");

	char buf[256],file1[256],file2[256];

	for (int i=0;i<9;i++)
	{
		fgets(buf, 256, in_file);
	}

	while (fgets(buf, 256, in_file)!=NULL)
	{
		if (buf[0] == '#')
		{
			cout<<buf;
			continue;
		}

		int i1 = -1, i2 = -1;
		int nMatches;
		sscanf(buf, "%d %s", &i1, file1);
		fgets(buf, 256, in_file);
		sscanf(buf, "%d %s", &i2, file2);

		fgets(buf, 256, in_file);
		sscanf(buf, "%d", &nMatches);

		if (i1<0||i2<0)
		{
			printf("It's wrong when reading the machtes stored from VisualSFM");
			exit(0);
		}

		int special_flag=0;

		char * imgname1_0= strrchr(file1, 92); // '\'= 92
		char imgname1[256];
		for (int i=1;i<strlen(imgname1_0);i++)
		{
			imgname1[i-1] = imgname1_0[i];	
		}
		
		imgname1[strlen(imgname1_0)-1] = '\0';
		char * imgname2_0= strrchr(file2, 92); // '\'= 92
		char imgname2[256];
		for (int i=1;i<strlen(imgname2_0);i++)
		{
			imgname2[i-1] = imgname2_0[i];
		}
		imgname2[strlen(imgname2_0)-1] = '\0';

		//As the image index of VisualSFM may be different from ours, we do the check as follows.
		//What's more, the index of VisualSFM is from 0;
		if (strstr(img_files[i1].c_str(),imgname1) == NULL)
		{
			for(int i=0;i<num_images;i++)
				if (strstr(img_files[i].c_str(),imgname1)!=NULL)
				{
					i1 = i;
					break;
				}
		}

		if (strstr(img_files[i2].c_str(),imgname2) == NULL)
		{
			for(int i=0;i<num_images;i++)
				if (strstr(img_files[i].c_str(),imgname2) != NULL)
				{
					i2 = i;
					break;
				}
		}


		/*
		if (strstr(img_files[i2-1].c_str(),imgname2)!=NULL)
		{
			i2 = i2-1;
			if (i1)
				i1 = i1-1;
			else
				i1 = num_images-1;
		}

		//for (int i=0;i<num_images;i++)
		//{
		//	if (strstr(img_files[i].c_str(),imgname1)!=NULL)
		//	{
		//		i1 = i;
		//		break;
		//	}
		//}

		//for (int i=0;i<num_images;i++)
		//{
		//	if (strstr(img_files[i].c_str(),imgname2)!=NULL)
		//	{
		//		i2 = i;
		//		break;
		//	}
		//} */

		if (i2<i1)
		{
			special_flag = 1;
		}
		
		printf("[%d %d]\n",i1,i2);

		if (!special_flag)
		{
			vector<KeypointMatch> matches;
			matches.resize(nMatches);
			v2_t *k1_pts = new v2_t[nMatches];
			v2_t *k2_pts = new v2_t[nMatches];
			Vector2r *k1_pts0  = new Vector2r[nMatches];
			Vector2r *k2_pts0  = new Vector2r[nMatches];

			int k1, k2;
			float x1,y1,x2,y2;
			for (int i = 0; i < nMatches; i++) 
			{
				fgets(buf, 256, in_file);
				sscanf(buf, "%d %f %f %d %f %f", &k1, &x1, &y1, &k2, &x2, &y2);
				//in_file >> k1 >> k2;

				// 			if (k1 > KEY_LIMIT || k2 > KEY_LIMIT)
				// 				continue;

				matches[i].m_idx1 = k1;
				matches[i].m_idx2 = k2;
			//	printf("x1: %f  x2: %f ", x1,x2);
				k1_pts[i] = v2_new(x1 -  cam_intrinsic[i1][1], -y1 + cam_intrinsic[i1][2]);
				k2_pts[i] = v2_new(x2 -  cam_intrinsic[i2][1], -y2 + cam_intrinsic[i2][2]);
				k1_pts0[i].v[0] = x1; k1_pts0[i].v[1] = y1;
				k2_pts0[i].v[0] = x2; k2_pts0[i].v[1] = y2;
			//	printf("k1: %f %f K2: %f %f \n", k1_pts[i].p[0],k1_pts[i].p[1], k2_pts[i].p[0],k2_pts[i].p[1]);
			}
			
			double K1[9], K2[9];
			K1[0] = K1[4] = cam_intrinsic[i1][0]; K1[8] = 1.0;
			K1[1] = K1[2] = K1[3] = K1[5] = K1[6] = K1[7] = 0;

			K2[0] = K2[4] = cam_intrinsic[i2][0]; K2[8] = 1.0;
			K2[1] = K2[2] = K2[3] = K2[5] = K2[6] = K2[7] = 0;

			double R0[9], t0[3];
			double R[9], t[3];

			int num_inliers = compute_pose_ransac(nMatches, k1_pts, k2_pts, 
				K1, K2, E_THRESHOLD*10, 10, R0, t0);

			//change coordinate system
			R0[1] = -R0[1]; R0[2] = -R0[2];
			R0[3] = -R0[3]; R0[6] = -R0[6];
			t0[1] = -t0[1]; t0[2] = -t0[2];
			fprintf_s(EGsOutPutFile,"R0, t0: \n");
			fprintf_s(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
				R0[0], R0[1], R0[2],R0[3],R0[4],R0[5],R0[6],R0[7],R0[8], t0[0], t0[1], t0[2]);
			//exit(0);
			for (int i = 0; i < nMatches; i++) 
			{
				k1_pts[i].p[0] /= cam_intrinsic[i1][0];
				k1_pts[i].p[1] = -k1_pts[i].p[1]/cam_intrinsic[i1][0];

				k2_pts[i].p[0] /= cam_intrinsic[i2][0];
				k2_pts[i].p[1] = -k2_pts[i].p[1]/cam_intrinsic[i2][0];
			}

			RefineRtNonlinear(nMatches, k1_pts, k2_pts, cam_intrinsic[i1][0], cam_intrinsic[i2][0], R0, t0, R, t);

			fprintf(EGsOutPutFile,"R, t: \n");
			fprintf(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
				R[0], R[1], R[2],R[3],R[4],R[5],R[6],R[7],R[8], t[0], t[1], t[2]);
			fflush(stdout);

			//Compute the inlier
			fprintf(EGsOutPutFile,"Inliers: %d / %d \n", num_inliers, nMatches);
			if (num_inliers > MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
			{
				vector<KeypointMatch> Inlier_loop;

				double E[9], F[9];
				double K1_inv[9], K2_inv[9];

				//Get inlier index
				computeEssentialFromRT(E, R, t);
				for (int k = 0; k < 9; k++) 
				{
					K1_inv[k] = 0;
					K2_inv[k] = 0;
				}
				K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[i1][0]; K1_inv[8] = 1;
				K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[i2][0]; K2_inv[8] = 1;
				MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
				MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

				vector<int> Inlier;
				double t_sqr = double(E_THRESHOLD * E_THRESHOLD*100);

				for (int k = 0; k < nMatches; k++)
				{
					unsigned short key_idx1 = matches[k].m_idx1;
					unsigned short key_idx2 = matches[k].m_idx2;

					double error, p[3], q[3];
					p[0] = k1_pts[k].p[0]*cam_intrinsic[i1][0];
					p[1] = k1_pts[k].p[1]*cam_intrinsic[i1][0];
					p[2] = 1;

					q[0] = k2_pts[k].p[0]*cam_intrinsic[i2][0];
					q[1] = k2_pts[k].p[1]*cam_intrinsic[i2][0];
					q[2] = 1;

					error = sampsonError(p, q, F); //qTFp
					if (error < t_sqr)
					{
						Inlier.push_back(k);
					}
				}

				if (Inlier.size()> MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
				{
					fprintf_s(EGs_f, "p %d %d %d\n", int(Inlier.size()), 
						i1 + start_id, i2 + add_id);
					fprintf_s(EGs_f, "%f %f %f\n", float(R[0]), float(R[1]), float(R[2]));
					fprintf_s(EGs_f, "%f %f %f\n", float(R[3]), float(R[4]), float(R[5]));
					fprintf_s(EGs_f, "%f %f %f\n", float(R[6]), float(R[7]), float(R[8]));

					double tn = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
					t[0] /= tn; t[1] /= tn; t[2] /= tn;

					fprintf_s(EGs_f, "%f %f %f\n", float(t[0]), float(t[1]), float(t[2]));

					for (size_t k = 0; k < Inlier.size(); ++k)
					{
						int temp_index = Inlier[k];
						fprintf_s(EGs_f, "%d %f %f %f %f  %d %f %f %f %f\n", int(matches[temp_index].m_idx1), float(k1_pts[temp_index].p[0]), float(k1_pts[temp_index].p[1]), k1_pts0[temp_index].v[0],k1_pts0[temp_index].v[1],
							int(matches[temp_index].m_idx2), float(k2_pts[temp_index].p[0]), float(k2_pts[temp_index].p[1]), k2_pts0[temp_index].v[0], k2_pts0[temp_index].v[1]);
					} //end for
				}//end inlier.size()
			}      //end number of inlier  

			delete [] k1_pts;
			delete [] k2_pts;
			fgets(buf, 256, in_file);
		}
		else
		{
			int i_temp=i2;
			i2 = i1;
			i1 = i_temp;
		    
			vector<KeypointMatch> matches;
			matches.resize(nMatches);
			v2_t *k1_pts = new v2_t[nMatches];
			v2_t *k2_pts = new v2_t[nMatches];
			Vector2r *k1_pts0  = new Vector2r[nMatches];
			Vector2r *k2_pts0  = new Vector2r[nMatches];

			int k1, k2;
			float x1,y1,x2,y2;
			for (int i = 0; i < nMatches; i++) 
			{
				fgets(buf, 256, in_file);
				sscanf(buf, "%d %f %f %d %f %f", &k2, &x2, &y2, &k1, &x1, &y1);
				//in_file >> k1 >> k2;

				// 			if (k1 > KEY_LIMIT || k2 > KEY_LIMIT)
				// 				continue;

				matches[i].m_idx1 = k1;
				matches[i].m_idx2 = k2;
				k1_pts[i] = v2_new(x1 -  cam_intrinsic[i1][1], -y1 + cam_intrinsic[i1][2]);
				k2_pts[i] = v2_new(x2 -  cam_intrinsic[i2][1], -y2 + cam_intrinsic[i2][2]);
				k1_pts0[i].v[0] = x1; k1_pts0[i].v[1] = y1;
				k2_pts0[i].v[0] = x2; k2_pts0[i].v[1] = y2;
			}

			double K1[9], K2[9];
			K1[0] = K1[4] = cam_intrinsic[i1][0]; K1[8] = 1.0;
			K1[1] = K1[2] = K1[3] = K1[5] = K1[6] = K1[7] = 0;

			K2[0] = K2[4] = cam_intrinsic[i2][0]; K2[8] = 1.0;
			K2[1] = K2[2] = K2[3] = K2[5] = K2[6] = K2[7] = 0;

			double R0[9], t0[3];
			double R[9], t[3];

			int num_inliers = compute_pose_ransac(nMatches, k1_pts, k2_pts, 
				K1, K2, E_THRESHOLD*10, 10, R0, t0);

			//change coordinate system
			R0[1] = -R0[1]; R0[2] = -R0[2];
			R0[3] = -R0[3]; R0[6] = -R0[6];
			t0[1] = -t0[1]; t0[2] = -t0[2];
			fprintf_s(EGsOutPutFile,"R0, t0: \n");
			fprintf_s(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
				R0[0], R0[1], R0[2],R0[3],R0[4],R0[5],R0[6],R0[7],R0[8], t0[0], t0[1], t0[2]);

			for (int i = 0; i < nMatches; i++) 
			{
				k1_pts[i].p[0] /= cam_intrinsic[i1][0];
				k1_pts[i].p[1] = -k1_pts[i].p[1]/cam_intrinsic[i1][0];

				k2_pts[i].p[0] /= cam_intrinsic[i2][0];
				k2_pts[i].p[1] = -k2_pts[i].p[1]/cam_intrinsic[i2][0];
			}

			RefineRtNonlinear(nMatches, k1_pts, k2_pts, cam_intrinsic[i1][0], cam_intrinsic[i2][0], R0, t0, R, t);

			fprintf(EGsOutPutFile,"R, t: \n");
			fprintf(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
				R[0], R[1], R[2],R[3],R[4],R[5],R[6],R[7],R[8], t[0], t[1], t[2]);
			fflush(stdout);

			//Compute the inlier
			fprintf(EGsOutPutFile,"Inliers: %d / %d \n", num_inliers, nMatches);
			if (num_inliers > MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
			{
				vector<KeypointMatch> Inlier_loop;

				double  F[9], E[9];
				double K1_inv[9], K2_inv[9];

				//Get inlier index
				computeEssentialFromRT(E, R, t);
				for (int k = 0; k < 9; k++) 
				{
					K1_inv[k] = 0;
					K2_inv[k] = 0;
				}
				K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[i1][0]; K1_inv[8] = 1;
				K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[i2][0]; K2_inv[8] = 1;
				MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
				MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

				vector<int> Inlier;
				double t_sqr = double(E_THRESHOLD * E_THRESHOLD*100);

				for (int k = 0; k < nMatches; k++)
				{
					unsigned short key_idx1 = matches[k].m_idx1;
					unsigned short key_idx2 = matches[k].m_idx2;

					double error, p[3], q[3];
					p[0] = k1_pts[k].p[0]*cam_intrinsic[i1][0];
					p[1] = k1_pts[k].p[1]*cam_intrinsic[i1][0];
					p[2] = 1;

					q[0] = k2_pts[k].p[0]*cam_intrinsic[i2][0];
					q[1] = k2_pts[k].p[1]*cam_intrinsic[i2][0];
					q[2] = 1;

					error = sampsonError(p, q, F); //qTFp
					if (error < t_sqr)
					{
						Inlier.push_back(k);
					}
				}

				if (Inlier.size()> MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
				{
					fprintf_s(EGs_f, "p %d %d %d\n", int(Inlier.size()), 
						i1 + start_id, i2 + add_id);
					fprintf_s(EGs_f, "%f %f %f\n", float(R[0]), float(R[1]), float(R[2]));
					fprintf_s(EGs_f, "%f %f %f\n", float(R[3]), float(R[4]), float(R[5]));
					fprintf_s(EGs_f, "%f %f %f\n", float(R[6]), float(R[7]), float(R[8]));

					double tn = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
					t[0] /= tn; t[1] /= tn; t[2] /= tn;

					fprintf_s(EGs_f, "%f %f %f\n", float(t[0]), float(t[1]), float(t[2]));

					for (size_t k = 0; k < Inlier.size(); ++k)
					{
						int temp_index = Inlier[k];
						fprintf_s(EGs_f, "%d %f %f %f %f  %d %f %f %f %f\n", int(matches[temp_index].m_idx1), float(k1_pts[temp_index].p[0]), float(k1_pts[temp_index].p[1]), k1_pts0[temp_index].v[0],k1_pts0[temp_index].v[1],
							int(matches[temp_index].m_idx2), float(k2_pts[temp_index].p[0]), float(k2_pts[temp_index].p[1]), k2_pts0[temp_index].v[0], k2_pts0[temp_index].v[1]);
					} //end for
				}//end inlier.size()
			}      //end number of inlier  

			delete [] k1_pts;
			delete [] k2_pts;
			fgets(buf, 256, in_file);
		}
	}
	fclose(in_file);
	fflush(stdout);
	fclose(EGs_f);
}

void MatchPoint::ComputeEfromWrongEGs
	(int num_images, std::vector<std::string> & img_files,double **cam_intrinsic,  
	const char *EGs_out, const char *WrongEGs, 
	int start_id /* = 0 */, int split_id /* = 0 */, int add_id /* = 0 */)
{
	printf("Compute relative poses using 5 point algorithm based on the results of WrongEGs. \n");
	fflush(stdout);

	FILE *EGs_f;
	fopen_s(&EGs_f, EGs_out, "w");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}

	//ifstream in_file; 
	FILE *in_file = NULL;
	//in_file.open(filename);
	fopen_s(&in_file, WrongEGs, "r");

	char buf[256],file1[256];

	while (fgets(buf, 256, in_file)!=NULL)
	{

		int i1 = -1, i2 = -1;
		int nMatches;
		sscanf(buf, "%s %d %d %d", file1, &nMatches, &i1, &i2);
	

		if (i1<0||i2<0)
		{
			printf("It's wrong when reading the matches stored from VisualSFM");
			exit(0);
		}

		printf("[%d %d]\n",i1,i2);

		//Old R and t
		for(int i=0;i<4;i++)
		{
			fgets(buf, 256, in_file);
		}

		vector<KeypointMatch> matches;
		matches.resize(nMatches);
		v2_t *k1_pts = new v2_t[nMatches];
		v2_t *k2_pts = new v2_t[nMatches];
		Vector2r *k1_pts0  = new Vector2r[nMatches];
		Vector2r *k2_pts0  = new Vector2r[nMatches];

		int k1, k2;
		float x1_norm, y1_norm, x2_norm, y2_norm;
		float x1,y1,x2,y2;
		for (int i = 0; i < nMatches; i++) 
		{
			fgets(buf, 256, in_file);
			sscanf(buf, "%d %f %f %f %f %d %f %f %f %f", &k1, &x1_norm, &y1_norm, &x1, &y1, &k2, &x2_norm, &y2_norm, &x2, &y2);

			matches[i].m_idx1 = k1;
			matches[i].m_idx2 = k2;
		//	printf("x1: %f  x2: %f ", x1,x2);
			k1_pts[i] = v2_new(x1 -  cam_intrinsic[i1][1], -y1 + cam_intrinsic[i1][2]);
			k2_pts[i] = v2_new(x2 -  cam_intrinsic[i2][1], -y2 + cam_intrinsic[i2][2]);
			k1_pts0[i].v[0] = x1; k1_pts0[i].v[1] = y1;
			k2_pts0[i].v[0] = x2; k2_pts0[i].v[1] = y2;
		//	printf("k1: %f %f K2: %f %f \n", k1_pts[i].p[0],k1_pts[i].p[1], k2_pts[i].p[0],k2_pts[i].p[1]);
		}
			
		double K1[9], K2[9];
		K1[0] = K1[4] = cam_intrinsic[i1][0]; K1[8] = 1.0;
		K1[1] = K1[2] = K1[3] = K1[5] = K1[6] = K1[7] = 0;

		K2[0] = K2[4] = cam_intrinsic[i2][0]; K2[8] = 1.0;
		K2[1] = K2[2] = K2[3] = K2[5] = K2[6] = K2[7] = 0;

		double R0[9], t0[3];
		double R[9], t[3];

		int num_inliers = compute_pose_ransac(nMatches, k1_pts, k2_pts, 
			K1, K2, E_THRESHOLD*10, 10, R0, t0);

		//change coordinate system
		R0[1] = -R0[1]; R0[2] = -R0[2];
		R0[3] = -R0[3]; R0[6] = -R0[6];
		t0[1] = -t0[1]; t0[2] = -t0[2];
		fprintf_s(EGsOutPutFile,"R0, t0: \n");
		fprintf_s(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
			R0[0], R0[1], R0[2],R0[3],R0[4],R0[5],R0[6],R0[7],R0[8], t0[0], t0[1], t0[2]);
		//exit(0);
		for (int i = 0; i < nMatches; i++) 
		{
			k1_pts[i].p[0] /= cam_intrinsic[i1][0];
			k1_pts[i].p[1] = -k1_pts[i].p[1]/cam_intrinsic[i1][0];

			k2_pts[i].p[0] /= cam_intrinsic[i2][0];
			k2_pts[i].p[1] = -k2_pts[i].p[1]/cam_intrinsic[i2][0];
		}

		RefineRtNonlinear(nMatches, k1_pts, k2_pts, cam_intrinsic[i1][0], cam_intrinsic[i2][0], R0, t0, R, t);

		fprintf(EGsOutPutFile,"R, t: \n");
		fprintf(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
			R[0], R[1], R[2],R[3],R[4],R[5],R[6],R[7],R[8], t[0], t[1], t[2]);
		fflush(stdout);

		//Compute the inlier
		fprintf(EGsOutPutFile,"Inliers: %d / %d \n", num_inliers, nMatches);
		if (num_inliers > MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
		{
			vector<KeypointMatch> Inlier_loop;

			double E[9], F[9];
			double K1_inv[9], K2_inv[9];

			//Get inlier index
			computeEssentialFromRT(E, R, t);
			for (int k = 0; k < 9; k++) 
			{
				K1_inv[k] = 0;
				K2_inv[k] = 0;
			}
			K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[i1][0]; K1_inv[8] = 1;
			K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[i2][0]; K2_inv[8] = 1;
			MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
			MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

			vector<int> Inlier;
			double t_sqr = double(E_THRESHOLD * E_THRESHOLD*100);

			for (int k = 0; k < nMatches; k++)
			{
				unsigned short key_idx1 = matches[k].m_idx1;
				unsigned short key_idx2 = matches[k].m_idx2;

				double error, p[3], q[3];
				p[0] = k1_pts[k].p[0]*cam_intrinsic[i1][0];
				p[1] = k1_pts[k].p[1]*cam_intrinsic[i1][0];
				p[2] = 1;

				q[0] = k2_pts[k].p[0]*cam_intrinsic[i2][0];
				q[1] = k2_pts[k].p[1]*cam_intrinsic[i2][0];
				q[2] = 1;

				error = sampsonError(p, q, F); //qTFp
				if (error < t_sqr)
				{
					Inlier.push_back(k);
				}
			}

			if (Inlier.size()> MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
			{
				fprintf_s(EGs_f, "p %d %d %d\n", int(Inlier.size()), 
					i1 + start_id, i2 + add_id);
				fprintf_s(EGs_f, "%f %f %f\n", float(R[0]), float(R[1]), float(R[2]));
				fprintf_s(EGs_f, "%f %f %f\n", float(R[3]), float(R[4]), float(R[5]));
				fprintf_s(EGs_f, "%f %f %f\n", float(R[6]), float(R[7]), float(R[8]));

				double tn = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
				t[0] /= tn; t[1] /= tn; t[2] /= tn;

				fprintf_s(EGs_f, "%f %f %f\n", float(t[0]), float(t[1]), float(t[2]));

				for (size_t k = 0; k < Inlier.size(); ++k)
				{
					int temp_index = Inlier[k];
					fprintf_s(EGs_f, "%d %f %f %f %f  %d %f %f %f %f\n", int(matches[temp_index].m_idx1), float(k1_pts[temp_index].p[0]), float(k1_pts[temp_index].p[1]), k1_pts0[temp_index].v[0],k1_pts0[temp_index].v[1],
						int(matches[temp_index].m_idx2), float(k2_pts[temp_index].p[0]), float(k2_pts[temp_index].p[1]), k2_pts0[temp_index].v[0], k2_pts0[temp_index].v[1]);
				} //end for
			}//end inlier.size()
		}      //end number of inlier  

		delete [] k1_pts;
		delete [] k2_pts;

	}
	fclose(in_file);
	fflush(stdout);
	fclose(EGs_f);
}

void MatchPoint::RemoveBadMatches
	(int num_images, std::vector<std::string> & img_files,double **cam_intrinsic,  
	const char *EGs_out, const char *GoodEBadMatches, 
	int start_id /* = 0 */, int split_id /* = 0 */, int add_id /* = 0 */)
{
	printf("Remove bad matches according to the coded R and t. \n");
	fflush(stdout);

	FILE *EGs_f;
	fopen_s(&EGs_f, EGs_out, "w");
	if (EGs_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		exit(1);
	}

	//ifstream in_file; 
	FILE *in_file = NULL;
	//in_file.open(filename);
	fopen_s(&in_file, GoodEBadMatches, "r");

	char buf[256],file1[256];

	while (fgets(buf, 256, in_file)!=NULL)
	{

		int i1 = -1, i2 = -1;
		int nMatches;
		sscanf(buf, "%s %d %d %d", file1, &nMatches, &i1, &i2);


		if (i1<0||i2<0)
		{
			printf("It's wrong when reading the matches\n");
			exit(0);
		}

		printf("[%d %d]\n",i1,i2);

		//Old R and t
		double R[9], t[3];
		for(int i=0;i<3;i++)
		{
			fgets(buf, 256, in_file);
			sscanf(buf, "%lf %lf %lf", &R[i*3], &R[i*3+1],&R[i*3+2]);
		}
		fgets(buf, 256, in_file);
		sscanf(buf, "%lf %lf %lf", &t[0], &t[1],&t[2]);

		vector<KeypointMatch> matches;
		matches.resize(nMatches);
		v2_t *k1_pts = new v2_t[nMatches];
		v2_t *k2_pts = new v2_t[nMatches];
		Vector2r *k1_pts0  = new Vector2r[nMatches];
		Vector2r *k2_pts0  = new Vector2r[nMatches];

		int k1, k2;
		float x1_norm, y1_norm, x2_norm, y2_norm;
		float x1,y1,x2,y2;
		for (int i = 0; i < nMatches; i++) 
		{
			fgets(buf, 256, in_file);
			sscanf(buf, "%d %f %f %f %f %d %f %f %f %f", &k1, &x1_norm, &y1_norm, &x1, &y1, &k2, &x2_norm, &y2_norm, &x2, &y2);

			matches[i].m_idx1 = k1;
			matches[i].m_idx2 = k2;
			//	printf("x1: %f  x2: %f ", x1,x2);
			k1_pts[i] = v2_new(x1 -  cam_intrinsic[i1][1], -y1 + cam_intrinsic[i1][2]);
			k2_pts[i] = v2_new(x2 -  cam_intrinsic[i2][1], -y2 + cam_intrinsic[i2][2]);
			k1_pts0[i].v[0] = x1; k1_pts0[i].v[1] = y1;
			k2_pts0[i].v[0] = x2; k2_pts0[i].v[1] = y2;
			//	printf("k1: %f %f K2: %f %f \n", k1_pts[i].p[0],k1_pts[i].p[1], k2_pts[i].p[0],k2_pts[i].p[1]);
		}


		for (int i = 0; i < nMatches; i++) 
		{
			k1_pts[i].p[0] /= cam_intrinsic[i1][0];
			k1_pts[i].p[1] = -k1_pts[i].p[1]/cam_intrinsic[i1][0];

			k2_pts[i].p[0] /= cam_intrinsic[i2][0];
			k2_pts[i].p[1] = -k2_pts[i].p[1]/cam_intrinsic[i2][0];
		}

		double K1[9], K2[9];
		K1[0] = K1[4] = cam_intrinsic[i1][0]; K1[8] = 1.0;
		K1[1] = K1[2] = K1[3] = K1[5] = K1[6] = K1[7] = 0;

		K2[0] = K2[4] = cam_intrinsic[i2][0]; K2[8] = 1.0;
		K2[1] = K2[2] = K2[3] = K2[5] = K2[6] = K2[7] = 0;

		//double R0[9], t0[3];
		//

		//int num_inliers = compute_pose_ransac(nMatches, k1_pts, k2_pts, 
		//	K1, K2, E_THRESHOLD*10, 10, R0, t0);

		////change coordinate system
		//R0[1] = -R0[1]; R0[2] = -R0[2];
		//R0[3] = -R0[3]; R0[6] = -R0[6];
		//t0[1] = -t0[1]; t0[2] = -t0[2];
		//fprintf_s(EGsOutPutFile,"R0, t0: \n");
		//fprintf_s(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
		//	R0[0], R0[1], R0[2],R0[3],R0[4],R0[5],R0[6],R0[7],R0[8], t0[0], t0[1], t0[2]);
		////exit(0);
		//for (int i = 0; i < nMatches; i++) 
		//{
		//	k1_pts[i].p[0] /= cam_intrinsic[i1][0];
		//	k1_pts[i].p[1] = -k1_pts[i].p[1]/cam_intrinsic[i1][0];

		//	k2_pts[i].p[0] /= cam_intrinsic[i2][0];
		//	k2_pts[i].p[1] = -k2_pts[i].p[1]/cam_intrinsic[i2][0];
		//}

		//RefineRtNonlinear(nMatches, k1_pts, k2_pts, cam_intrinsic[i1][0], cam_intrinsic[i2][0], R0, t0, R, t);

		fprintf(EGsOutPutFile,"R, t: \n");
		fprintf(EGsOutPutFile,"%f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n", 
			R[0], R[1], R[2],R[3],R[4],R[5],R[6],R[7],R[8], t[0], t[1], t[2]);
		fflush(stdout);

		//Compute the inlier
		fprintf(EGsOutPutFile,"Input Paris:  %d\n", nMatches);
		if (nMatches > MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
		{
			vector<KeypointMatch> Inlier_loop;

			double E[9], F[9];
			double K1_inv[9], K2_inv[9];

			//Get inlier index
			computeEssentialFromRT(E, R, t);
			for (int k = 0; k < 9; k++) 
			{
				K1_inv[k] = 0;
				K2_inv[k] = 0;
			}
			K1_inv[0] = K1_inv[4] = 1/cam_intrinsic[i1][0]; K1_inv[8] = 1;
			K2_inv[0] = K2_inv[4] = 1/cam_intrinsic[i2][0]; K2_inv[8] = 1;
			MatrixMatrixProduct(3, 3, 3, E, K1_inv, F);
			MatrixMatrixProduct(3, 3, 3, K2_inv, F, F);

			vector<int> Inlier;
			double t_sqr = double(E_THRESHOLD * E_THRESHOLD*100);

			for (int k = 0; k < nMatches; k++)
			{
				unsigned short key_idx1 = matches[k].m_idx1;
				unsigned short key_idx2 = matches[k].m_idx2;

				double error, p[3], q[3];
				p[0] = k1_pts[k].p[0]*cam_intrinsic[i1][0];
				p[1] = k1_pts[k].p[1]*cam_intrinsic[i1][0];
				p[2] = 1;

				q[0] = k2_pts[k].p[0]*cam_intrinsic[i2][0];
				q[1] = k2_pts[k].p[1]*cam_intrinsic[i2][0];
				q[2] = 1;

				error = sampsonError(p, q, F); //qTFp
				if (error < t_sqr)
				{
					Inlier.push_back(k);
				}
			}

			fprintf_s(EGsOutPutFile, "The number of inliers is %d\n", Inlier.size());

			if (Inlier.size()> MININUM_INLIER_CORRESPONDENCES_PairwiseEGs)
			{
				fprintf_s(EGs_f, "p %d %d %d\n", int(Inlier.size()), 
					i1 + start_id, i2 + add_id);
				fprintf_s(EGs_f, "%f %f %f\n", float(R[0]), float(R[1]), float(R[2]));
				fprintf_s(EGs_f, "%f %f %f\n", float(R[3]), float(R[4]), float(R[5]));
				fprintf_s(EGs_f, "%f %f %f\n", float(R[6]), float(R[7]), float(R[8]));

				double tn = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
				t[0] /= tn; t[1] /= tn; t[2] /= tn;

				fprintf_s(EGs_f, "%f %f %f\n", float(t[0]), float(t[1]), float(t[2]));

				for (size_t k = 0; k < Inlier.size(); ++k)
				{
					int temp_index = Inlier[k];
					fprintf_s(EGs_f, "%d %f %f %f %f  %d %f %f %f %f\n", int(matches[temp_index].m_idx1), float(k1_pts[temp_index].p[0]), float(k1_pts[temp_index].p[1]), k1_pts0[temp_index].v[0],k1_pts0[temp_index].v[1],
						int(matches[temp_index].m_idx2), float(k2_pts[temp_index].p[0]), float(k2_pts[temp_index].p[1]), k2_pts0[temp_index].v[0], k2_pts0[temp_index].v[1]);
				} //end for
			}//end inlier.size()
		}      //end number of inlier  

		delete [] k1_pts;
		delete [] k2_pts;

	}
	fclose(in_file);
	fflush(stdout);
	fclose(EGs_f);
}


void MatchPoint::SortKeys
	(int num_images, vector<string> &key_files)
{
	printf("SortKeys: there are %d images\n",num_images);
	clock_t start, end;
	
	//#define FINDMATCHIMAGEONLY

	vector<KeypointWithScaleRot> keys_d0;

	vector<float> ScaleAll;
	for (int i = 0; i < num_images; i++)
	{
		printf("Sort view %d\n", i);
		fflush(stdout);
		int img_id1 = i;

		keys_d0 = ReadKeyFileWithScaleRot(key_files[img_id1].c_str(), true);

		int num_keys0 = (int) keys_d0.size();

		ScaleAll.resize(num_keys0);

		for(int j=0;j<num_keys0-1;j++)
			ScaleAll[j] = keys_d0[j].m_scale;

		vector<size_t> SortIndex;

		SortIndex = sort_indexes(ScaleAll);
		
		std::ofstream out(key_files[img_id1].c_str());

		if (!out)
		{
			printf("Failed when opened.\n");
			fflush(stdout);

		}

		out<<num_keys0<<" 128"<<endl;
		out.flags(ios::fixed);

		for (size_t j=0;j<num_keys0;j++)
		{
			int temp_index = SortIndex[j]; 
			//in y, x, scale, orientation order
			out<<setprecision(2) << keys_d0[temp_index].m_y<<" "<<setprecision(2) << keys_d0[temp_index].m_x<<" "
				<<setprecision(3) << keys_d0[temp_index].m_scale<<" " <<setprecision(3) <<  keys_d0[temp_index].m_orient<< endl; 
			
			for(int k = 0; k < 128; k ++) 
			{
				out<<int(keys_d0[temp_index].m_d[k])<<" ";

				if ( (k+1)%20 == 0 ) out<<endl; 

			}
			out<<endl;

		}

		out.close();

		//desc0= new unsigned char [128 * num_keys0];
		//GetKeyDesc(keys_d0, desc0);

		ScaleAll.clear();
		ReleaseKey(keys_d0);


	}

}

void MatchPoint::MatchUsingVoctreeFast
	(int num_images, double **cam_intrinsic, 
	vector<string> &key_files, map<pair<int, int>, int> &match_list, 
	map<pair<int, int>, int> &old_matchList,
	const char* matches_out, const char* voctree_in, const char* keylist_in, int split_id, double ratio, int PreemptiveMatchingThr)
{
	printf("MatchUsingVoctreeFast\n");
	clock_t start, end;
	FILE *matches_f;
#ifndef ADDMATCHES
	fopen_s(&matches_f, matches_out, "w");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#else
	fopen_s(&matches_f, matches_out, "a");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#endif

	//Copy from SimpleSIFT
#ifdef SIFTGPU_DLL_RUNTIME
#ifdef _WIN32
#ifdef _DEBUG
	HMODULE  hsiftgpu = LoadLibrary("siftgpu_d.dll");
#else
	HMODULE  hsiftgpu = LoadLibrary("siftgpu.dll");
#endif
#else
	void * hsiftgpu = dlopen("libsiftgpu.so", RTLD_LAZY);
#endif


	if(hsiftgpu == NULL) 
	{
		printf("Can not load library SiftGPU\n");
		return;
	}

#ifdef REMOTE_SIFTGPU
	ComboSiftGPU* (*pCreateRemoteSiftGPU) (int, char*) = NULL;
	pCreateRemoteSiftGPU = (ComboSiftGPU* (*) (int, char*)) GET_MYPROC(hsiftgpu, "CreateRemoteSiftGPU");
	ComboSiftGPU * combo = pCreateRemoteSiftGPU(REMOTE_SERVER_PORT, REMOTE_SERVER);
	SiftGPU* sift = combo;
	SiftMatchGPU* matcher = combo;
#else

	int TestNum = 100;
	int MatchMaxNum = 8192;
	//SiftGPU* (*pCreateNewSiftGPU)(int) = NULL;
	SiftMatchGPU* (*pCreateNewSiftMatchGPU)(int) = NULL;
	//pCreateNewSiftGPU = (SiftGPU* (*) (int)) GET_MYPROC(hsiftgpu, "CreateNewSiftGPU");
	pCreateNewSiftMatchGPU = (SiftMatchGPU* (*)(int)) GET_MYPROC(hsiftgpu, "CreateNewSiftMatchGPU");
	//SiftGPU* sift = pCreateNewSiftGPU(1);
	SiftMatchGPU* matcher = pCreateNewSiftMatchGPU(MatchMaxNum);
#endif

#elif defined(REMOTE_SIFTGPU)
	ComboSiftGPU * combo = CreateRemoteSiftGPU(REMOTE_SERVER_PORT, REMOTE_SERVER);
	SiftGPU* sift = combo;
	SiftMatchGPU* matcher = combo;
#else
	//this will use overloaded new operators
	SiftGPU  *sift = new SiftGPU;
	SiftMatchGPU *matcher = new SiftMatchGPU(4096);
#endif

	matcher->CreateContextGL();

	//load voctree
	VocTree m_voctree;
	int nm = 0;
	int *match_id_p = NULL;
	if (match_list.empty())
	{
		if (m_voctree.LoadVocTree((char*)(voctree_in)) < 0) return ;
		printf("Finding image candidates...\n");
		num_images = m_voctree.FindImageCandidates((char*)(keylist_in), &match_id_p, nm);
		printf("Match %d images.\n", num_images);
		fflush(stdout);
		m_voctree.Clean();
	}
	else
		nm = num_images;
	//initialize camera parameters
	CameraParam cameras[2];
	for (int i = 0; i < 9; i++)
		cameras[0].R[i] = REAL(0.0);
	cameras[0].R[0] = cameras[0].R[4] = cameras[0].R[8] = REAL(1.0);

	for (int i = 0; i < 3; i++) cameras[0].cen[i] = REAL(0.0);

	//#define FINDMATCHIMAGEONLY
#ifdef FINDMATCHIMAGEONLY
	FILE *match_list = NULL;
	fopen_s(&match_list, "matchList.txt", "w");
	for (int i = 0; i < num_images - 1; i++)
	{
		printf("Match view %d\n", i);
		fflush(stdout);
		int img_id1 = i;

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;
			img_id2  = match_id_p[i * nm + j];

			if (img_id2 < 0) continue; 

			map<pair<int, int>, int>::iterator miter = black_list.find(make_pair(img_id1, img_id2));
			if (miter != black_list.end()) continue;

			const char *imgName0 = key_files[img_id1].c_str();
			const char *imgName1 = key_files[img_id2].c_str();

			fprintf_s(match_list, "%s %s\n", imgName0+2, imgName1+2);
		}
		end = clock();    
		fflush(stdout);
	}
	fclose(match_list);
#else
	vector<KeypointWithDesc> keys_d0, keys_d1;
	unsigned char *desc0, *desc1; 
	for (int i = 0; i < num_images - 1; i++)
	{
		printf("Pairwise matching for view %d / %d\n", i,num_images);
		fflush(stdout);
		int img_id1 = i;

		if (cam_intrinsic[img_id1][0] == 0) continue;


		keys_d0 = ReadKeyFileWithDesc(key_files[img_id1].c_str(), true);

		int num_keys1 = (int) keys_d0.size();
		if (num_keys1 < MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
		{
			ReleaseKey(keys_d0);
			continue;
		}

		desc0= new unsigned char [128 * num_keys1];
		GetKeyDesc(keys_d0, desc0);

		ANNpointArray pts = annAllocPts(num_keys1, 128);
		ANNkd_tree *tree = CreateSearchTree(num_keys1, desc0, pts);

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;

			if (match_id_p != NULL)
				img_id2  = match_id_p[i * nm + j];
			else
				img_id2 = j;

			if (img_id2 < 0) continue; 

			if (cam_intrinsic[img_id2][0] == 0) continue;

			//#ifdef ADDIMAGE
			if (img_id2 < split_id) continue;
			//#endif
			if (img_id2 <= img_id1) continue;

			if (!match_list.empty())
			{
				map<pair<int, int>, int>::iterator miter = match_list.find(make_pair(img_id1, img_id2));
				if (miter == match_list.end()) 
				{
					// 					printf("Skip pair [%d %d]\n", img_id1, img_id2);
					// 					fflush(stdout);
					continue;
				}
				else
				{
#ifdef ADDMATCHES
					map<pair<int, int>, int>::iterator miter_old = old_matchList.find(make_pair(img_id1, img_id2));
					if (miter_old != old_matchList.end())
						continue;
#endif
				}
			}

			keys_d1 = ReadKeyFileWithDesc(key_files[img_id2].c_str(), true);

			int num_keys2 = (int) keys_d1.size();
			if (num_keys2< MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
			{
				ReleaseKey(keys_d1);
				continue;
			}

			desc1 = new unsigned char [128 * num_keys2];
			GetKeyDesc(keys_d1, desc1);

			if(PreemptiveMatchingThr>0)
			{
				//Set descriptors for SIFTMatch
				matcher->SetMaxSift(TestNum);  //Auctual larger than TestNum(125%);
				matcher->SetDescriptors(0, num_keys1, desc0);
				matcher->SetDescriptors(1, num_keys2, desc1);

				//Preemptive feature matching
				int (*match_buf_test)[2] = new int[TestNum][2];
				//use the default thresholds. Check the declaration in SiftGPU.h
				int num_match_test = matcher->GetSiftMatch(TestNum, match_buf_test,0.7,ratio);

				for (int ii=0;ii<num_match_test;ii++)
				{
					printf("%d-%d ",match_buf_test[ii][0],match_buf_test[ii][1]);
				}
				printf("\n");

				delete[] match_buf_test;


				printf("Preemptive feature matching result: %d inliers among top %d features.\n", num_match_test,TestNum);	
				//if (float(num_match_test)/TestNum<0.1)
				if (num_match_test<PreemptiveMatchingThr)
				{
					delete []desc1;
					ReleaseKey(keys_d1);
					continue;
				}
			}
			
			//Regular matching using up to 8192 features.
			int num_max = max(num_keys1,num_keys2);
			int (*match_buf)[2] = new int[min(num_max,MatchMaxNum)][2];
			matcher->SetMaxSift(min(num_max,MatchMaxNum));
			matcher->SetDescriptors(0, num_keys1, desc0);
			matcher->SetDescriptors(1, num_keys2, desc1);
			//use the default thresholds. Check the declaration in SiftGPU.h
			int num_match = matcher->GetSiftMatch(num_max, match_buf,0.7,ratio);

			fprintf(EGsOutPutFile,"Image %d and Image %d: %d\n",img_id1,img_id2,num_match);

			std::vector<KeypointMatch> matches;
			for (int match_i = 0;match_i<num_match;match_i++)
			{
				matches.push_back(KeypointMatch(match_buf[match_i][0], match_buf[match_i][1]));
			}

			delete[] match_buf;

			//std::vector<KeypointMatch> matches = MatchKeys(num_keys2, desc1, tree, ratio); //(i, j)

			int num_matches = (int) matches.size();
			if (num_matches >= MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs) 
			{
				fprintf_s(matches_f, "%d %d\n", img_id1, img_id2);
				fprintf_s(matches_f, "%d\n", int(matches.size()));

				for (int k = 0; k < num_matches; k++) 
					fprintf_s(matches_f, "%d %d\n", int(matches[k].m_idx1), int(matches[k].m_idx2));
			}

			delete []desc1;
			ReleaseKey(keys_d1);
		}
		end = clock();    
		fflush(stdout);

		delete []desc0;
		ReleaseKey(keys_d0);

		annDeallocPts(pts);  
		delete tree;  
		annClose();
	}
#endif

	fclose(matches_f);
	if (match_id_p != NULL)
		delete []match_id_p;

#ifdef REMOTE_SIFTGPU
	delete combo;
#else
	//		delete sift;
	delete matcher;
#endif

#ifdef SIFTGPU_DLL_RUNTIME
	FREE_MYLIB(hsiftgpu);
#endif
}


void MatchPoint::MatchUsingVoctreeSort
	(int num_images, double **cam_intrinsic, 
	vector<string> &key_files, map<pair<int, int>, int> &match_list, 
	map<pair<int, int>, int> &old_matchList,
	const char* matches_out, const char* voctree_in, const char* keylist_in, int split_id, double ratio,int PreemptiveMatchingThr)
{
	printf("MatchUsingVoctreeSort\n");
	clock_t start, end;
	FILE *matches_f;
#ifndef ADDMATCHES
	fopen_s(&matches_f, matches_out, "w");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#else
	fopen_s(&matches_f, matches_out, "a");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#endif

	//Copy from SimpleSIFT
#ifdef SIFTGPU_DLL_RUNTIME
#ifdef _WIN32
#ifdef _DEBUG
	HMODULE  hsiftgpu = LoadLibrary("siftgpu_d.dll");
#else
	HMODULE  hsiftgpu = LoadLibrary("siftgpu.dll");
#endif
#else
	void * hsiftgpu = dlopen("libsiftgpu.so", RTLD_LAZY);
#endif


	if(hsiftgpu == NULL) 
	{
		printf("Can not load library SiftGPU\n");
		return;
	}

#ifdef REMOTE_SIFTGPU
	ComboSiftGPU* (*pCreateRemoteSiftGPU) (int, char*) = NULL;
	pCreateRemoteSiftGPU = (ComboSiftGPU* (*) (int, char*)) GET_MYPROC(hsiftgpu, "CreateRemoteSiftGPU");
	ComboSiftGPU * combo = pCreateRemoteSiftGPU(REMOTE_SERVER_PORT, REMOTE_SERVER);
	SiftGPU* sift = combo;
	SiftMatchGPU* matcher = combo;
#else

	int TestNum = 100;
	int MatchMaxNum = 8192;
	//SiftGPU* (*pCreateNewSiftGPU)(int) = NULL;
	SiftMatchGPU* (*pCreateNewSiftMatchGPU)(int) = NULL;
	//pCreateNewSiftGPU = (SiftGPU* (*) (int)) GET_MYPROC(hsiftgpu, "CreateNewSiftGPU");
	pCreateNewSiftMatchGPU = (SiftMatchGPU* (*)(int)) GET_MYPROC(hsiftgpu, "CreateNewSiftMatchGPU");
	//SiftGPU* sift = pCreateNewSiftGPU(1);
	SiftMatchGPU* matcher = pCreateNewSiftMatchGPU(MatchMaxNum);
#endif

#elif defined(REMOTE_SIFTGPU)
	ComboSiftGPU * combo = CreateRemoteSiftGPU(REMOTE_SERVER_PORT, REMOTE_SERVER);
	SiftGPU* sift = combo;
	SiftMatchGPU* matcher = combo;
#else
	//this will use overloaded new operators
	SiftGPU  *sift = new SiftGPU;
	SiftMatchGPU *matcher = new SiftMatchGPU(4096);
#endif
	unsigned char **SortDescAll, *SortDescAll_p;

	if(PreemptiveMatchingThr>0)
	{
		SortDescAll = new unsigned char*[num_images];	
		SortDescAll_p  = new unsigned char[num_images*TestNum*128];	
		for (int i = 0; i < num_images; i++) {
			SortDescAll[i] = &(SortDescAll_p[i*TestNum*128]);
		}

		SortKeys(num_images,key_files, SortDescAll, TestNum);

	}
	matcher->CreateContextGL();

	//load voctree
	VocTree m_voctree;
	int nm = 0;
	int *match_id_p = NULL;
	if (match_list.empty())
	{
		if (m_voctree.LoadVocTree((char*)(voctree_in)) < 0) return ;
		printf("Finding image candidates...\n");
		num_images = m_voctree.FindImageCandidates((char*)(keylist_in), &match_id_p, nm);
		printf("Match %d images.\n", num_images);
		fflush(stdout);
		m_voctree.Clean();
	}
	else
		nm = num_images;
	//initialize camera parameters
	CameraParam cameras[2];
	for (int i = 0; i < 9; i++)
		cameras[0].R[i] = REAL(0.0);
	cameras[0].R[0] = cameras[0].R[4] = cameras[0].R[8] = REAL(1.0);

	for (int i = 0; i < 3; i++) cameras[0].cen[i] = REAL(0.0);

	//#define FINDMATCHIMAGEONLY
#ifdef FINDMATCHIMAGEONLY
	FILE *match_list = NULL;
	fopen_s(&match_list, "matchList.txt", "w");
	for (int i = 0; i < num_images - 1; i++)
	{
		printf("Match view %d\n", i);
		fflush(stdout);
		int img_id1 = i;

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;
			img_id2  = match_id_p[i * nm + j];

			if (img_id2 < 0) continue; 

			map<pair<int, int>, int>::iterator miter = black_list.find(make_pair(img_id1, img_id2));
			if (miter != black_list.end()) continue;

			const char *imgName0 = key_files[img_id1].c_str();
			const char *imgName1 = key_files[img_id2].c_str();

			fprintf_s(match_list, "%s %s\n", imgName0+2, imgName1+2);
		}
		end = clock();    
		fflush(stdout);
	}
	fclose(match_list);
#else
	vector<KeypointWithDesc> keys_d0, keys_d1;
	unsigned char *desc0, *desc1; 
	for (int i = 0; i < num_images - 1; i++)
	{
		printf("Pairwise matching for view %d / %d\n", i,num_images);
		fflush(stdout);
		int img_id1 = i;

		if (cam_intrinsic[img_id1][0] == 0) continue;


		keys_d0 = ReadKeyFileWithDesc(key_files[img_id1].c_str(), true);

		int num_keys1 = (int) keys_d0.size();
		if (num_keys1 < MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
		{
			ReleaseKey(keys_d0);
			continue;
		}

		desc0= new unsigned char [128 * num_keys1];
		GetKeyDesc(keys_d0, desc0);

		ANNpointArray pts = annAllocPts(num_keys1, 128);
		ANNkd_tree *tree = CreateSearchTree(num_keys1, desc0, pts);

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;

			if (match_id_p != NULL)
				img_id2  = match_id_p[i * nm + j];
			else
				img_id2 = j;

			if (img_id2 < 0) continue; 

			if (cam_intrinsic[img_id2][0] == 0) continue;

			//#ifdef ADDIMAGE
			if (img_id2 < split_id) continue;
			//#endif
			if (img_id2 <= img_id1) continue;

			if (!match_list.empty())
			{
				map<pair<int, int>, int>::iterator miter = match_list.find(make_pair(img_id1, img_id2));
				if (miter == match_list.end()) 
				{
					// 					printf("Skip pair [%d %d]\n", img_id1, img_id2);
					// 					fflush(stdout);
					continue;
				}
				else
				{
#ifdef ADDMATCHES
					map<pair<int, int>, int>::iterator miter_old = old_matchList.find(make_pair(img_id1, img_id2));
					if (miter_old != old_matchList.end())
						continue;
#endif
				}
			}

			keys_d1 = ReadKeyFileWithDesc(key_files[img_id2].c_str(), true);

			int num_keys2 = (int) keys_d1.size();
			if (num_keys2< MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
			{
				ReleaseKey(keys_d1);
				continue;
			}

			desc1 = new unsigned char [128 * num_keys2];
			GetKeyDesc(keys_d1, desc1);


			if(PreemptiveMatchingThr>0)
			{
				//Preemptive feature matching

				matcher->SetDescriptors(0, TestNum, SortDescAll[img_id1]);
				matcher->SetDescriptors(1, TestNum, SortDescAll[img_id2]);

				int (*match_buf_test)[2] = new int[TestNum][2];
				//use the default thresholds. Check the declaration in SiftGPU.h
				int num_match_test = matcher->GetSiftMatch(TestNum, match_buf_test,0.7,ratio);
				delete[] match_buf_test;

				printf("Preemptive feature matching result: %d inliers among top %d features.\n", num_match_test,TestNum);	
				//if (float(num_match_test)/TestNum<0.1)
				if (num_match_test<PreemptiveMatchingThr)
				{
					delete []desc1;
					ReleaseKey(keys_d1);
					continue;
				}

			}
			
			//Set descriptors for SIFTMatch
			matcher->SetDescriptors(0, num_keys1, desc0);
			matcher->SetDescriptors(1, num_keys2, desc1);

			//match and get result.
			int num_max = max(num_keys1,num_keys2);
			int (*match_buf)[2] = new int[min(num_max,MatchMaxNum)][2];
			//use the default thresholds. Check the declaration in SiftGPU.h
			int num_match = matcher->GetSiftMatch(num_max, match_buf,0.7,ratio);

			fprintf(EGsOutPutFile,"Image %d and Image %d: %d\n",img_id1,img_id2,num_match);

			std::vector<KeypointMatch> matches;
			for (int match_i = 0;match_i<num_match;match_i++)
			{
				matches.push_back(KeypointMatch(match_buf[match_i][0], match_buf[match_i][1]));
			}

			delete[] match_buf;

			//std::vector<KeypointMatch> matches = MatchKeys(num_keys2, desc1, tree, ratio); //(i, j)

			int num_matches = (int) matches.size();
			if (num_matches >= MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs) 
			{
				fprintf_s(matches_f, "%d %d\n", img_id1, img_id2);
				fprintf_s(matches_f, "%d\n", int(matches.size()));

				for (int k = 0; k < num_matches; k++) 
					fprintf_s(matches_f, "%d %d\n", int(matches[k].m_idx1), int(matches[k].m_idx2));
			}

			delete []desc1;
			ReleaseKey(keys_d1);
		}
		end = clock();    
		fflush(stdout);

		delete []desc0;
		ReleaseKey(keys_d0);

		annDeallocPts(pts);  
		delete tree;  
		annClose();
	}
#endif

	fclose(matches_f);
	if (match_id_p != NULL)
		delete []match_id_p;

#ifdef REMOTE_SIFTGPU
	delete combo;
#else
	//		delete sift;
	delete matcher;
#endif

#ifdef SIFTGPU_DLL_RUNTIME
	FREE_MYLIB(hsiftgpu);
#endif

	if(PreemptiveMatchingThr>0)
	{
		// Release SortDescAll
		delete [] SortDescAll[0];
		delete [] SortDescAll;
		SortDescAll = NULL;
	}
}


void MatchPoint::SortKeys(int num_images, vector<string> &key_files, unsigned char ** SortDescAll, int TestNum)
{
	printf("SortKeys: there are %d images\n",num_images);
	clock_t start, end;

	//#define FINDMATCHIMAGEONLY

	vector<KeypointWithScaleRot> keys_d0;

	vector<float> ScaleAll;
	for (int i = 0; i < num_images; i++)
	{
		printf("Sort view %d\n", i);
		fflush(stdout);
		int img_id1 = i;

		keys_d0 = ReadKeyFileWithScaleRot(key_files[img_id1].c_str(), true);

		int num_keys0 = (int) keys_d0.size();

		ScaleAll.resize(num_keys0);

		for(int j=0;j<num_keys0-1;j++)
			ScaleAll[j] = keys_d0[j].m_scale;

		vector<size_t> SortIndex;

		SortIndex = sort_indexes(ScaleAll);

		unsigned char * SortDesc = SortDescAll[i];
		
		for (size_t j=0;j<TestNum;j++)
		{
			int temp_index = SortIndex[j];
			KeypointWithDesc  &key = keys_d0[temp_index];
			unsigned char  *desc = key.GetDesc();
			//printf("key %d--%dth\n", j,temp_index);
			//fflush(stdout);
			memcpy(SortDesc + j * 128, desc, sizeof (unsigned char) * 128);
		}


		//desc0= new unsigned char [128 * num_keys0];
		//GetKeyDesc(keys_d0, desc0);

		ScaleAll.clear();
		ReleaseKey(keys_d0);
	}

}


void MatchPoint::MatchUsingVoctreeOpenMP
	(int num_images, double **cam_intrinsic, 
	vector<string> &key_files, map<pair<int, int>, int> &match_list, 
	map<pair<int, int>, int> &old_matchList,
	const char* matches_out, const char* voctree_in, const char* keylist_in, int split_id, double ratio)
{
	clock_t start, end;
	FILE *matches_f;
#ifndef ADDMATCHES
	fopen_s(&matches_f, matches_out, "w");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#else
	fopen_s(&matches_f, matches_out, "a");
	if (matches_f == NULL)
	{
		printf("Error opening file for writing\n");
		fflush(stdout);
		return;
	}
#endif


	//load voctree
	VocTree m_voctree;
	int nm = 0;
	int *match_id_p = NULL;
	if (match_list.empty())
	{
		if (m_voctree.LoadVocTree((char*)(voctree_in)) < 0) return ;
		printf("Finding image candidates...\n");
		num_images = m_voctree.FindImageCandidates((char*)(keylist_in), &match_id_p, nm);
		printf("Match %d images.\n", num_images);
		fflush(stdout);
		m_voctree.Clean();
	}
	else
		nm = num_images;
	//initialize camera parameters
	CameraParam cameras[2];
	for (int i = 0; i < 9; i++)
		cameras[0].R[i] = REAL(0.0);
	cameras[0].R[0] = cameras[0].R[4] = cameras[0].R[8] = REAL(1.0);

	for (int i = 0; i < 3; i++) cameras[0].cen[i] = REAL(0.0);


	vector<KeypointWithDesc> keys_d0, keys_d1;
	unsigned char *desc0, *desc1; 


	concurrent_vector<concurrent_vector<vector<KeypointMatch>>> AllMatches;
	concurrent_vector<concurrent_vector<int>> AllPairs;
	AllPairs.resize(num_images);
	AllMatches.resize(num_images);

	omp_set_dynamic(0);
	omp_set_num_threads(16); 

	#pragma omp parallel for 
	for (int i = 0; i < num_images - 1; i++)
	{

		printf("Pairwise matching for view %d / %d\n", i,num_images);
		fflush(stdout);
		int img_id1 = i;

		if (cam_intrinsic[img_id1][0] == 0) continue;


		keys_d0 = ReadKeyFileWithDesc(key_files[img_id1].c_str(), true);

		int num_keys1 = (int) keys_d0.size();
		if (num_keys1 < MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
		{
			ReleaseKey(keys_d0);
			continue;
		}

		desc0= new unsigned char [128 * num_keys1];
		GetKeyDesc(keys_d0, desc0);

		ANNpointArray pts = annAllocPts(num_keys1, 128);
		ANNkd_tree *tree = CreateSearchTree(num_keys1, desc0, pts);

		int j = 0;
		for (; j < nm; j++) 
		{
			int img_id2;

			if (match_id_p != NULL)
				img_id2  = match_id_p[i * nm + j];
			else
				img_id2 = j;

			if (img_id2 < 0) continue; 

			if (cam_intrinsic[img_id2][0] == 0) continue;

			//#ifdef ADDIMAGE
			if (img_id2 < split_id) continue;
			//#endif
			if (img_id2 <= img_id1) continue;

			if (!match_list.empty())
			{
				map<pair<int, int>, int>::iterator miter = match_list.find(make_pair(img_id1, img_id2));
				if (miter == match_list.end()) 
				{
					// 					printf("Skip pair [%d %d]\n", img_id1, img_id2);
					// 					fflush(stdout);
					continue;
				}
				else
				{
#ifdef ADDMATCHES
					map<pair<int, int>, int>::iterator miter_old = old_matchList.find(make_pair(img_id1, img_id2));
					if (miter_old != old_matchList.end())
						continue;
#endif
				}
			}

			keys_d1 = ReadKeyFileWithDesc(key_files[img_id2].c_str(), true);

			int num_keys2 = (int) keys_d1.size();
			if (num_keys2< MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs)	
			{
				ReleaseKey(keys_d1);
				continue;
			}

			desc1 = new unsigned char [128 * num_keys2];
			GetKeyDesc(keys_d1, desc1);

			std::vector<KeypointMatch> matches = MatchKeys(num_keys2, desc1, tree, ratio); //(i, j)

			int num_matches = (int) matches.size();
			if (num_matches >= MINIMUM_RAW_CORRESPONDENCES_PairwiseEGs) 
			{
				AllPairs[i].push_back(img_id2);
				AllMatches[i].push_back(matches);
/*				fprintf_s(matches_f, "%d %d\n", img_id1, img_id2);
				fprintf_s(matches_f, "%d\n", int(matches.size()));

				for (int k = 0; k < num_matches; k++) 
					fprintf_s(matches_f, "%d %d\n", int(matches[k].m_idx1), int(matches[k].m_idx2));
		*/	
			}

			delete []desc1;
			ReleaseKey(keys_d1);
		}
		end = clock();    
		fflush(stdout);

		delete []desc0;
		ReleaseKey(keys_d0);

		annDeallocPts(pts);  
		delete tree;  
		annClose();
	}


	for (int i = 0; i < num_images - 1; i++)
	{
		for (int k=0;k<AllMatches[i].size();k++)
		{
			std::vector<KeypointMatch> matches = AllMatches[i][k];
			fprintf_s(matches_f, "%d %d\n",i, AllPairs[i][k]);
			fprintf_s(matches_f, "%d\n", int(matches.size()));

			for (int k = 0; k < matches.size(); k++) 		
				fprintf_s(matches_f, "%d %d\n", int(matches[k].m_idx1), int(matches[k].m_idx2));

		}		
	}

	fclose(matches_f);
	if (match_id_p != NULL)
		delete []match_id_p;
}
