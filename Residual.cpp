#include <iostream>

#include "ceres/rotation.h"
#include "ceres/ceres.h"

double p1[] = {0.40160966,  0.56225353,  0.7228974};
double p2[] = {0.46156633,  0.59344243,  0.65938047};
double camera[] = {.002, .003, .004, .5, .6, .7};
double residuals[3];

template <typename T>
bool calculateResidual(const T* const camera,
                T* residuals) {
    
    T rotated1[3];
    // T p1[3]={(T)x1,(T)y1,(T)z1};
    // T p2[3]={(T)x2,(T)y2,(T)z2};
    ceres::AngleAxisRotatePoint(camera,p1,rotated1);

    T norm=camera[3]*camera[3]+camera[4]*camera[4]+camera[5]*camera[5];
    
    // if(sqrt(norm)>=(T)std::numeric_limits<double>::min()){//translation is not zero
        
        T axis[3];
        ceres::CrossProduct(rotated1,&camera[3],axis);
        T D=-(axis[0]*rotated1[0]+axis[1]*rotated1[1]+axis[2]*rotated1[2]);
        
        norm=axis[0]*axis[0]+axis[1]*axis[1]+axis[2]*axis[2];
        T t=(axis[0]*p2[0]+axis[1]*p2[1]+axis[2]*p2[2]+D)/norm;
        
        rotated1[0]=p2[0]+axis[0]*t;
        rotated1[1]=p2[1]+axis[1]*t;
        rotated1[2]=p2[2]+axis[2]*t;
    // }
    
    norm=sqrt(rotated1[0]*rotated1[0]+rotated1[1]*rotated1[1]+rotated1[2]*rotated1[2]);

    for (int i = 0; i < 3; i++) {
        std::cout << rotated1[i]/norm << ", ";
    }
    std::cout << std::endl << std::endl;

    std::cout << "norm: " << norm << std::endl;
    residuals[0]=(rotated1[0]/norm)-p2[0];
    residuals[1]=(rotated1[1]/norm)-p2[1];
    residuals[2]=(rotated1[2]/norm)-p2[2];

    for (int i = 0; i < 3; i++) {
        std::cout << p2[i] << ", ";
    }
    std::cout << std::endl;

    for (int i = 0; i < 3; i++) {
        std::cout << residuals[i] << ", ";
    }
    std::cout << std::endl;
    return true;
}

int main(int argc, char **argv) {
    calculateResidual<double>(camera, residuals);

}