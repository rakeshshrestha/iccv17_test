cmake_minimum_required(VERSION 2.8) # 3.2)

project(iccv17_test)

# c++ version
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

set(CMAKE_BUILD_TYPE Release)

# enable profiling
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg")
#SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pg")
#SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pg")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ggdb")
#SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -g")
#SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -g")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -gdwarf-3")

# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lprofiler")

# link_directories("/usr/local/lib")
# include_directories("/usr/local/include/opengv")

# Ceres
find_package(Ceres REQUIRED)
include_directories(${Ceres_INCLUDE_DIRS})

#opengv
find_package(opengv REQUIRED)
if (opengv_FOUND)
  message(STATUS "OPENGV found")
  message(STATUS "opengv include: " ${opengv_INCLUDE_DIRECTORIES})
endif (opengv_FOUND)

include_directories(${opengv_INCLUDE_DIRS})
message(STATUS "opengv" ${opengv_INCLUDE_DIRS})
message(STATUS "opengv" ${OPENGV_LIBRARIES})

#eigen
find_package(PkgConfig)
pkg_search_module(Eigen3 REQUIRED eigen3)
include_directories(${EIGEN_INCLUDE_DIRS})

#opencv
link_directories("/opt/ros/indigo/lib")
pkg_search_module(opencv REQUIRED opencv-3.1.0-dev)
# the ippicv library isn't there!
list(REMOVE_ITEM opencv_LIBRARIES "ippicv") 
#find_package(OpenCV 3.1.0 REQUIRED)
include_directories(${opencv_INCLUDE_DIRS})
# message("OPENCV: " ${opencv_LIBRARIES})
message("OPENCV: " ${opencv_INCLUDE_DIRS} ${opencv_LIBRARIES})


# in project stuffs
include_directories(
  ${PROJECT_SOURCE_DIR}/utils
  ${PROJECT_SOURCE_DIR}/include
)

add_library(${PROJECT_NAME} STATIC
  include/iccv17_test/utils/random_generators.hpp
  src/utils/random_generators.cpp

  include/iccv17_test/utils/time_measurement.hpp
  src/utils/time_measurement.cpp

  include/iccv17_test/utils/experiment_helpers.hpp
  src/utils/experiment_helpers.cpp

  include/iccv17_test/relative_motion/relative_pose_estimation.hpp
  src/relative_motion/relative_pose_estimation.cpp

  include/iccv17_test/helpers/opengv_helpers.hpp
  src/helpers/opengv_helpers.cpp

  include/iccv17_test/helpers/misc_helpers.hpp
  src/helpers/misc_helpers.cpp

  include/iccv17_test/optimization/RotationTranslation.hpp
  
  include/iccv17_test/optimization/Rotation.hpp

  include/iccv17_test/optimization/EpipolarOptimization.hpp
)


#executables
# add_executable(iccv_test ICCV17_Test.cpp)
# target_link_libraries(iccv_test ${PROJECT_NAME} ${CERES_LIBRARIES} ${OPENGV_LIBRARIES} ${EIGEN3_LIBRARIES})

# add_executable(with_time ICCV17_Test_With_Time.cpp)
# target_link_libraries(with_time ${PROJECT_NAME} ${opencv_LIBRARIES} ${CERES_LIBRARIES} ${OPENGV_LIBRARIES} ${EIGEN3_LIBRARIES})

# add_executable(with_outliers ICCV17_Test_With_Outliers.cpp)
# target_link_libraries(with_outliers ${PROJECT_NAME} ${opencv_LIBRARIES} ${CERES_LIBRARIES} ${OPENGV_LIBRARIES} ${EIGEN3_LIBRARIES})

add_executable(zhaopeng_synthetic zhaopeng_synthetic.cpp)
target_link_libraries(zhaopeng_synthetic ${PROJECT_NAME} ${opencv_LIBRARIES} ${CERES_LIBRARIES} ${OPENGV_LIBRARIES} ${EIGEN3_LIBRARIES})

add_executable(outliers_temp outliers_temp.cpp)
target_link_libraries(outliers_temp ${PROJECT_NAME} ${opencv_LIBRARIES} ${CERES_LIBRARIES} ${OPENGV_LIBRARIES} ${EIGEN3_LIBRARIES})

add_executable(solve_with_inliers solve_with_inliers.cpp)
target_link_libraries(solve_with_inliers ${PROJECT_NAME} ${opencv_LIBRARIES} ${CERES_LIBRARIES} ${OPENGV_LIBRARIES} ${EIGEN3_LIBRARIES})

add_executable(residual Residual.cpp)
target_link_libraries(residual ${CERES_LIBRARIES})
