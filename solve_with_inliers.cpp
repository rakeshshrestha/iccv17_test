#include "opencv2/core/core.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <limits.h>
#include <Eigen/Eigen>
#include <opengv/relative_pose/methods.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/relative_pose/CentralRelativePoseSacProblem.hpp>
#include <opengv/relative_pose/NoncentralRelativeMultiAdapter.hpp>
#include <opengv/sac/MultiRansac.hpp>
#include <opengv/sac_problems/relative_pose/MultiNoncentralRelativePoseSacProblem.hpp>
#include <opengv/sac_problems/relative_pose/EigensolverSacProblem.hpp>
#include <sstream>
#include <fstream>

#include <iccv17_test/utils/random_generators.hpp>
#include <iccv17_test/utils/experiment_helpers.hpp>
#include <iccv17_test/utils/time_measurement.hpp>

#include <iccv17_test/config/config.hpp>
#include <iccv17_test/helpers/opengv_helpers.hpp>
#include <iccv17_test/helpers/misc_helpers.hpp>

#include "ceres/rotation.h"
#include "ceres/ceres.h"


using namespace std;
using namespace Eigen;
using namespace opengv;

inline double computeError(const Eigen::Vector3d& p1,
                            const Eigen::Vector3d& p2,
                            const Eigen::Matrix3d& rotation,
                            const Eigen::Vector3d& translation){
    
    
    Eigen::Vector3d rotated=rotation*p1;
    Eigen::Vector3d axis;
    ceres::CrossProduct(rotated.data(),translation.data(),axis.data());
    
    double D=-(axis(0)*rotated(0)+axis(1)*rotated(1)+axis(2)*rotated(2));
    double norm=axis(0)*axis(0)+axis(1)*axis(1)+axis(2)*axis(2);
    double t=(axis(0)*p2(0)+axis(1)*p2(1)+axis(2)*p2(2)+D)/norm;
        
    rotated(0)=p2(0)+axis(0)*t;
    rotated(1)=p2(1)+axis(1)*t;
    rotated(2)=p2(2)+axis(2)*t;

    
    rotated.normalize();
    rotated=rotated-p2;
    
    //std::cout<<"evaluation error"<<rotated.norm()<<std::endl;
    return rotated.norm();
}

struct RotationTranslation{
    
    RotationTranslation(const double _x1,const double _y1,const double _z1,
                        const double _x2,const double _y2,const double _z2):
    x1(_x1),y1(_y1),z1(_z1),x2(_x2),y2(_y2),z2(_z2){
    }
    
    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const {
        
        T rotated1[3];
        T p1[3]={(T)x1,(T)y1,(T)z1};
        T p2[3]={(T)x2,(T)y2,(T)z2};
        ceres::AngleAxisRotatePoint(camera,p1,rotated1);
        T norm=camera[3]*camera[3]+camera[4]*camera[4]+camera[5]*camera[5];
        
        if(sqrt(norm)>=(T)std::numeric_limits<double>::min()){//translation is not zero
            
            T axis[3];
            ceres::CrossProduct(rotated1,&camera[3],axis);
            T D=-(axis[0]*rotated1[0]+axis[1]*rotated1[1]+axis[2]*rotated1[2]);
            
            norm=axis[0]*axis[0]+axis[1]*axis[1]+axis[2]*axis[2];
            T t=(axis[0]*p2[0]+axis[1]*p2[1]+axis[2]*p2[2]+D)/norm;
            
            rotated1[0]=p2[0]+axis[0]*t;
            rotated1[1]=p2[1]+axis[1]*t;
            rotated1[2]=p2[2]+axis[2]*t;
        }
        
        norm=sqrt(rotated1[0]*rotated1[0]+rotated1[1]*rotated1[1]+rotated1[2]*rotated1[2]);
        residuals[0]=rotated1[0]/norm-p2[0];
        residuals[1]=rotated1[1]/norm-p2[1];
        residuals[2]=rotated1[2]/norm-p2[2];
        return true;
    }
    
    static ceres::CostFunction* Create(const double x1,
                                       const double y1,
                                       const double z1,
                                       const double x2,
                                       const double y2,
                                       const double z2) {
        
        return (new ceres::AutoDiffCostFunction<RotationTranslation,3,6>(new RotationTranslation(x1,y1,z1,x2,y2,z2)));
        
    };
    
    double x1;
    double y1;
    double z1;
    double x2;
    double y2;
    double z2;
};



struct Rotation{
    
    Rotation(const double _x1,const double _y1,const double _z1,
             const double _x2,const double _y2,const double _z2):
    x1(_x1),y1(_y1),z1(_z1),x2(_x2),y2(_y2),z2(_z2){
    }
    
    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const {
        
        T rotated1[3];
        T p1[3]={(T)x1,(T)y1,(T)z1};
        T p2[3]={(T)x2,(T)y2,(T)z2};
        ceres::AngleAxisRotatePoint(camera,p1,rotated1);
        residuals[0]=rotated1[0]-p2[0];
        residuals[1]=rotated1[1]-p2[1];
        residuals[2]=rotated1[2]-p2[2];
        return true;
    }
    
    static ceres::CostFunction* Create(const double x1,
                                       const double y1,
                                       const double z1,
                                       const double x2,
                                       const double y2,
                                       const double z2) {
        
        return (new ceres::AutoDiffCostFunction<Rotation,3,3>(new Rotation(x1,y1,z1,x2,y2,z2)));
        
    };
    
    double x1;
    double y1;
    double z1;
    double x2;
    double y2;
    double z2;
};

Eigen::Vector3d estimateRelativeTranslation(const std::vector<Eigen::Vector3d> &pts1,
                                            const std::vector<Eigen::Vector3d> &pts2){
    
    int num_point=pts1.size();
    std::vector<Eigen::Vector3d> norms(num_point);
    Eigen::Matrix3Xd allNorms=Eigen::Matrix3Xd(3,num_point);
    Eigen::Vector3d preResult,curResult;
    for(int i=0;i<num_point;i++){
        norms[i]=pts1[i].cross(pts2[i]);
        norms[i].normalize();
        allNorms.col(i)=norms[i];
    }
    Eigen::Matrix3d NtN=allNorms*allNorms.transpose();
    Eigen::JacobiSVD<Eigen::Matrix3d> svd(NtN,Eigen::ComputeFullV);
    Eigen::Matrix3d V=svd.matrixV();
    curResult=V.col(2);
    return curResult;
}


void preSolve(const std::vector<Eigen::Vector3d> &pts1,
              const std::vector<Eigen::Vector3d> &pts2,
              Eigen::Matrix3d& rotation,
              Eigen::Vector3d& translation){
    
    
    double motion[3]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    ceres::Problem problem;
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=Rotation::Create(pts1[i](0),
                                                            pts1[i](1),
                                                            pts1[i](2),
                                                            pts2[i](0),
                                                            pts2[i](1),
                                                            pts2[i](2));
        problem.AddResidualBlock(cost_function,NULL,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;
    ceres::Solve(options,&problem, &summary);
    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    
    std::vector<Eigen::Vector3d> _pts1(pts1.size()),_pts2(pts2.size());
    
    for(int p=0;p<pts1.size();p++){
        _pts1[p]=rotation*pts1[p];
        _pts2[p]=pts2[p];
    }
    translation=estimateRelativeTranslation(_pts1,_pts2);
}


void estimateRotationTranslation(const std::vector<Eigen::Vector3d> &pts1,
                                 const std::vector<Eigen::Vector3d> &pts2,
                                 Eigen::Matrix3d& rotation,
                                 Eigen::Vector3d& translation,
                                 ceres::LossFunction* loss_function=NULL,
                                 bool initialized=false){
    
    
    assert(pts1.size()==pts2.size());
    if (!initialized) {
        preSolve(pts1,pts2,rotation,translation);
    }
    double motion[6]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    motion[3]=translation(0);
    motion[4]=translation(1);
    motion[5]=translation(2);
    
    
    ceres::Problem problem;
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=RotationTranslation::Create(pts1[i](0),
                                                                       pts1[i](1),
                                                                       pts1[i](2),
                                                                       pts2[i](0),
                                                                       pts2[i](1),
                                                                       pts2[i](2));
        problem.AddResidualBlock(cost_function,loss_function,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;
    ceres::Solve(options,&problem, &summary);
    
    
    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    translation(0)=motion[3];
    translation(1)=motion[4];
    translation(2)=motion[5];
    
}

static bool haveColPlannarPoints(const std::vector<Eigen::Vector3d>& pt3d,const int count){
    
    // check that the i-th selected point does not on
    // a plane contains some previously selected points
    
    for(int i = 0; i < count-1; i++ ){
        
        Eigen::Vector3d pt1=pt3d[i];
        Eigen::Vector3d pt2=pt3d[count-1];
        Eigen::Vector3d norm=pt1.cross(pt2);
        norm.normalize();
        
        for(int j = 0; j < i; j++ ){
            double angle=abs(norm.dot(pt3d[j]));
            if (angle<=DBL_EPSILON) {
                return true;
            }
        }
    }
    return false;
}

static bool checkSubset(const std::vector<Eigen::Vector3d>& spts1,
                        const std::vector<Eigen::Vector3d>& spts2,
                        const int count){
    return !haveColPlannarPoints(spts1,count)&&!haveColPlannarPoints(spts2,count);
}

static int modelPoints=5;
std::vector<int> testinliers;
bool getSubset(cv::RNG& rng,
               const std::vector<Eigen::Vector3d>& pts1,
               const std::vector<Eigen::Vector3d>& pts2,
               std::vector<Eigen::Vector3d>& spts1,
               std::vector<Eigen::Vector3d>& spts2){
    
    //modelPoints=pts1.size();
    //testinliers.clear();
    
    spts1.resize(modelPoints);
    spts2.resize(modelPoints);
    std::vector<int> _idx(modelPoints);
    int* idx = &_idx[0];
    int maxAttempts=1000;
    int count=pts1.size();
    int i = 0, j, k, iters = 0;
    
    
    for(; iters < maxAttempts; iters++){
        
        for(i = 0; i < modelPoints && iters < maxAttempts; ){
            
            int idx_i = 0;
            for(;;){
                idx_i = idx[i] = rng.uniform(0, count);
                // std::cout << "Random number1: " << idx_i << std::endl;
                for( j = 0; j < i; j++ )
                    if( idx_i == idx[j] )
                        break;
                if( j == i )
                    break;
            }
            
            spts1[i]=(pts1[idx_i]);
            spts2[i]=(pts2[idx_i]);
            
            if(!checkSubset(spts1,spts2,i+1)){
                i = rng.uniform(0, i+1);
                iters++;

                // std::cout << "Random number2: " << i << std::endl;
                continue;
            }
            i++;
        }
        
        if(i == modelPoints && !checkSubset(spts1,spts2,i))
            continue;
        break;
    }
    
    // for (int i=0;i<modelPoints;i++) {
    //     testinliers.push_back(idx[i]);
    //     std::cout<<"select"<<idx[i]<<std::endl;
    // }
    
    return i == modelPoints && iters < maxAttempts;
}

// int RANSACUpdateNumIters( double p, double ep, int modelPoints, int maxIters ){
    
//     if( modelPoints <= 0 )
//         std::cerr << "the number of model points should be positive" << std::endl;
//         // CV_Error(cv::Error::StsOutOfRange, "the number of model points should be positive" );
    
//     p  = MAX(p,0.);
//     p  = MIN(p,1.);
//     ep = MAX(ep,0.);
//     ep = MIN(ep,1.);
    
//     // avoid inf's & nan's
//     double num = MAX(1. - p, DBL_MIN);
//     double denom = 1. - std::pow(1. - ep, modelPoints);
//     if( denom < DBL_MIN )
//         return 0;
    
//     num = std::log(num);
//     denom = std::log(denom);
    
//     return denom >= 0 || -num >= maxIters*(-denom) ? maxIters : cvRound(num/denom);
// }

int RANSACUpdateNumIters( double confidence, double outlierFraction, int modelPoints, int numPoints, int maxIters ) {
    
    if( modelPoints <= 0 )
        std::cerr << "the number of model points should be positive" << std::endl;
        // CV_Error(cv::Error::StsOutOfRange, "the number of model points should be positive" );
    
    confidence  = std::max(confidence,0.);
    confidence  = std::min(confidence,1.);
    outlierFraction = std::max(outlierFraction,0.);
    outlierFraction = std::min(outlierFraction,1.);
    
    double numInliers = (double)numPoints * (1. - outlierFraction);

    // avoid inf's & nan's
    double num = std::max(1. - confidence, DBL_MIN); // the alarm rate
    // double denom = 1. - std::pow(1. - outlierFraction, modelPoints);

    // consider that the points chosen are not replaced for ransac (more accurate)
    double denom = 1.;
    // probability that all are inliers
    for (int i = 0; i < modelPoints; i++) {
        denom *= (numInliers - i) / ((double)numPoints - i);
    }
    // probability that all are outliers
    denom = 1 - denom;

    if( denom < DBL_MIN )
        return 0;
    
    num = std::log(num);
    denom = std::log(denom);
    
    return denom >= 0 || -num >= maxIters*(-denom) ? maxIters : round(num/denom);
}

void findInliers(const std::vector<Eigen::Vector3d>& pts1,
                const std::vector<Eigen::Vector3d>& pts2,
                const Eigen::Matrix3d& rotation,
                const Eigen::Vector3d& translation,
                std::vector<int>& inliers,
                double thresh){
    inliers.clear();
    for (int i=0;i<pts1.size();i++) {
        if (computeError(pts1[i],pts2[i],rotation,translation)<=thresh) {
            inliers.push_back(i);
        }
    }
}

bool estimateRotationTranslationRANSAC3(
                                       const std::vector<Eigen::Vector3d> &pts1,
                                       const std::vector<Eigen::Vector3d> &pts2,
                                       Eigen::Matrix3d& rotation,
                                       Eigen::Vector3d& translation,
                                       int   maxIterations=1000,
                                       const double lossThreshold=0.005,
                                       const double inlierThreshold=0.005){
    
    cv::RNG rng((uint64)-1);
    int maxGoodCount=0;
    int count=pts1.size();
    int niters = maxIterations;
    
    
    std::vector<int> bestInliers;
    Eigen::Matrix3d bestRotation;
    Eigen::Vector3d bestTranslation;
    Eigen::Matrix3d gtRotation=rotation;
    
    double bestError=DBL_MAX;
    for(int iter = 0; iter < niters; iter++){
        
        std::vector<Eigen::Vector3d> spts1,spts2;
        bool found = getSubset(rng,pts1,pts2,spts1,spts2);
        if(!found ){
            if( iter == 0 )
                return false;
            break;
        }
        
        Eigen::Matrix3d rotation;
        Eigen::Vector3d translation;
        estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        Eigen::Matrix3d diff=gtRotation.transpose()*rotation;
        Eigen::Vector3d angle;
        ceres::RotationMatrixToAngleAxis(diff.data(),angle.data());
        
        if (angle.norm()<bestError) {
            bestRotation=rotation;
        }
    }
    rotation=bestRotation;
    return true;
}

bool estimateRotationTranslationRANSAC(
                                 const std::vector<Eigen::Vector3d> &pts1,
                                 const std::vector<Eigen::Vector3d> &pts2,
                                 Eigen::Matrix3d& rotation,
                                 Eigen::Vector3d& translation,
                                 int   maxIterations=1000,
                                 const double lossThreshold=0.005,
                                 const double inlierThreshold=0.005){
    
    cv::RNG rng((uint64)-1);
    int maxGoodCount=0;
    int count=pts1.size();
    int niters = maxIterations;
    
    
    std::vector<int> bestInliers;
    Eigen::Matrix3d bestRotation;
    Eigen::Vector3d bestTranslation;
    
    /*for (int i=0;i<pts1.size();i++) {
        std::cout<<pts1[i].transpose()<<std::endl;
    }*/
    
    for(int iter = 0; iter < niters; iter++){
        
        std::vector<Eigen::Vector3d> spts1,spts2;
        bool found = getSubset(rng,pts1,pts2,spts1,spts2);
        if(!found ){
            if( iter == 0 )
                return false;
            break;
        }
        
        /*for (int i=0;i<spts1.size();i++) {
            std::cout<<"subset "<<spts1[i].transpose()<<std::endl;
        }
        getchar();*/
        
        
        Eigen::Matrix3d rotation;
        Eigen::Vector3d translation;
        estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        //std::cout<<"rotation 1"<<rotation.transpose()<<std::endl;
        //getchar();
        /*spts1.clear();
        spts2.clear();
        
        for (int i=0;i<testinliers.size();i++) {
            spts1.push_back(pts1[testinliers[i]]);
            spts2.push_back(pts2[testinliers[i]]);
        }
        estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        std::cout<<"rotation 2"<<rotation<<std::endl;

        
        std::cout<<rotation<<std::endl;*/
        
        std::vector<int> inliers;
        findInliers(pts1,pts2,rotation,translation,inliers,inlierThreshold);
        int goodCount=inliers.size();
        //std::cout<<"good count"<<goodCount<<std::endl;
        if( goodCount > MAX(maxGoodCount, modelPoints-1) ){
            bestInliers=inliers;
            bestRotation=rotation;
            bestTranslation=translation;
            maxGoodCount=goodCount;
            //niters = RANSACUpdateNumIters(0.98, (double)(count-goodCount)/count, modelPoints, niters );
        }
    }
    
    //std::cout<<"rotation 1"<<bestRotation.transpose()<<std::endl;
    //getchar();

    //getchar();
    //std::cout<<"final iter"<<niters<<std::endl;
    std::vector<Eigen::Vector3d> inliers1(bestInliers.size());
    std::vector<Eigen::Vector3d> inliers2(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    
    ceres::LossFunction* loss=new ceres::HuberLoss(lossThreshold);
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation);
    findInliers(pts1,pts2,bestRotation,bestTranslation,bestInliers,inlierThreshold);
    inliers1.resize(bestInliers.size());
    inliers2.resize(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation,NULL,true);
    
    rotation=bestRotation;
    translation=bestTranslation;
    return true;
}
static int run7Point(const cv::Mat& _m1,const cv::Mat& _m2,cv::Mat& _fmatrix )
{
    double a[7*9], w[7], u[9*9], v[9*9], c[4], r[3];
    double* f1, *f2;
    double t0, t1, t2;
    cv::Mat A( 7, 9, CV_64F, a );
    cv::Mat U( 7, 9, CV_64F, u );
    cv::Mat Vt( 9, 9, CV_64F, v );
    cv::Mat W( 7, 1, CV_64F, w );
    cv::Mat coeffs( 1, 4, CV_64F, c );
    cv::Mat roots( 1, 3, CV_64F, r );
    
    const cv::Point3d* m1 = _m1.ptr<cv::Point3d>();
    const cv::Point3d* m2 = _m2.ptr<cv::Point3d>();
    
    double* fmatrix = _fmatrix.ptr<double>();
    int i, k, n;
    
    // form a linear system: i-th row of A(=a) represents
    // the equation: (m2[i], 1)'*F*(m1[i], 1) = 0
    for( i = 0; i < 7; i++ ){
        
        double x0 = m1[i].x, y0 = m1[i].y,z0=m1[i].z;
        double x1 = m2[i].x, y1 = m2[i].y,z1=m2[i].z;
        
        a[i*9+0] = x1*x0;
        a[i*9+1] = x1*y0;
        a[i*9+2] = x1*z0;
        a[i*9+3] = y1*x0;
        a[i*9+4] = y1*y0;
        a[i*9+5] = y1*z0;
        a[i*9+6] = x0*z1;
        a[i*9+7] = y0*z1;
        a[i*9+8] = z0*z1;
    }
    
    // A*(f11 f12 ... f33)' = 0 is singular (7 equations for 9 variables), so
    // the solution is linear subspace of dimensionality 2.
    // => use the last two singular vectors as a basis of the space
    // (according to SVD properties)
    cv::SVDecomp( A, W, U, Vt,cv::SVD::MODIFY_A + cv::SVD::FULL_UV );
    f1 = v + 7*9;
    f2 = v + 8*9;
    
    // f1, f2 is a basis => lambda*f1 + mu*f2 is an arbitrary f. matrix.
    // as it is determined up to a scale, normalize lambda & mu (lambda + mu = 1),
    // so f ~ lambda*f1 + (1 - lambda)*f2.
    // use the additional constraint det(f) = det(lambda*f1 + (1-lambda)*f2) to find lambda.
    // it will be a cubic equation.
    // find c - polynomial coefficients.
    for( i = 0; i < 9; i++ )
        f1[i] -= f2[i];
    
    t0 = f2[4]*f2[8] - f2[5]*f2[7];
    t1 = f2[3]*f2[8] - f2[5]*f2[6];
    t2 = f2[3]*f2[7] - f2[4]*f2[6];
    
    c[3] = f2[0]*t0 - f2[1]*t1 + f2[2]*t2;
    
    c[2] = f1[0]*t0 - f1[1]*t1 + f1[2]*t2 -
    f1[3]*(f2[1]*f2[8] - f2[2]*f2[7]) +
    f1[4]*(f2[0]*f2[8] - f2[2]*f2[6]) -
    f1[5]*(f2[0]*f2[7] - f2[1]*f2[6]) +
    f1[6]*(f2[1]*f2[5] - f2[2]*f2[4]) -
    f1[7]*(f2[0]*f2[5] - f2[2]*f2[3]) +
    f1[8]*(f2[0]*f2[4] - f2[1]*f2[3]);
    
    t0 = f1[4]*f1[8] - f1[5]*f1[7];
    t1 = f1[3]*f1[8] - f1[5]*f1[6];
    t2 = f1[3]*f1[7] - f1[4]*f1[6];
    
    c[1] = f2[0]*t0 - f2[1]*t1 + f2[2]*t2 -
    f2[3]*(f1[1]*f1[8] - f1[2]*f1[7]) +
    f2[4]*(f1[0]*f1[8] - f1[2]*f1[6]) -
    f2[5]*(f1[0]*f1[7] - f1[1]*f1[6]) +
    f2[6]*(f1[1]*f1[5] - f1[2]*f1[4]) -
    f2[7]*(f1[0]*f1[5] - f1[2]*f1[3]) +
    f2[8]*(f1[0]*f1[4] - f1[1]*f1[3]);
    
    c[0] = f1[0]*t0 - f1[1]*t1 + f1[2]*t2;
    
    // solve the cubic equation; there can be 1 to 3 roots ...
    n = solveCubic( coeffs, roots );
    
    if( n < 1 || n > 3 )
        return n;
    
    for( k = 0; k < n; k++, fmatrix += 9 )
    {
        // for each root form the fundamental matrix
        double lambda = r[k], mu = 1.;
        double s = f1[8]*r[k] + f2[8];
        
        // normalize each matrix, so that F(3,3) (~fmatrix[8]) == 1
        if( fabs(s) > DBL_EPSILON )
        {
            mu = 1./s;
            lambda *= mu;
            fmatrix[8] = 1.;
        }
        else
            fmatrix[8] = 0.;
        
        for( i = 0; i < 8; i++ )
            fmatrix[i] = f1[i]*lambda + f2[i]*mu;
    }
    return n;
}
bool estimateRotationTranslationRANSAC2(const std::vector<Eigen::Vector3d> &pts1,
                                       const std::vector<Eigen::Vector3d> &pts2,
                                       std::vector<int>& bestInliers,
                                       Eigen::Matrix3d& rotation,
                                       Eigen::Vector3d& translation,
                                       const double lossThreshold=0.005,
                                       const double inlierThreshold=0.01) {

    Eigen::Matrix3d bestRotation = Eigen::Matrix3d::Identity();
    Eigen::Vector3d bestTranslation = Eigen::Vector3d::Zero();

    std::vector<Eigen::Vector3d> inliers1(bestInliers.size());
    std::vector<Eigen::Vector3d> inliers2(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }

    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation);
    // findInliers(pts1,pts2,bestRotation,bestTranslation,bestInliers,inlierThreshold);
    // inliers1.resize(bestInliers.size());
    // inliers2.resize(bestInliers.size());
    // for (int i=0;i<bestInliers.size();i++) {
    //     inliers1[i]=pts1[bestInliers[i]];
    //     inliers2[i]=pts2[bestInliers[i]];
    // }

    //std::cout<<"good count"<<bestInliers.size()<<std::endl;
    // ceres::LossFunction* loss=new ceres::HuberLoss(lossThreshold);
    // estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation,loss,true);
    
    rotation=bestRotation;
    translation=bestTranslation;

    std::cout << rotation << std::endl;
    std::cout << translation << std::endl;

    return true;
}

bool estimateRotationTranslationRANSAC5point(
                                        const std::vector<Eigen::Vector3d> &pts1,
                                       const std::vector<Eigen::Vector3d> &pts2,
                                       std::vector<int>& bestInliers,
                                       Eigen::Matrix3d& rotation,
                                       Eigen::Vector3d& translation,
                                       int   maxIterations=NO_OF_RANSAC_ITERATIONS,
                                       const double lossThreshold=0.005,
                                       const double inlierThreshold=0.01){
    modelPoints=7;
    cv::RNG rng((uint64)-1);
    int count=pts1.size();
    int niters = maxIterations;
    
    
    bestInliers.clear();
    Eigen::Matrix3d bestRotation;
    Eigen::Vector3d bestTranslation;
    
    /*for (int i=0;i<pts1.size();i++) {
     std::cout<<pts1[i].transpose()<<std::endl;
     }*/
    
    for(int iter = 0; iter < niters; iter++){
        
        std::vector<Eigen::Vector3d> spts1,spts2;
        bool found = getSubset(rng,pts1,pts2,spts1,spts2);
        if(!found ){
            if( iter == 0 )
                return false;
            break;
        }
        
        /*for (int i=0;i<spts1.size();i++) {
         std::cout<<"subset "<<spts1[i].transpose()<<std::endl;
         }
         getchar();*/
        
        
        //Eigen::Matrix3d rotation;
        //Eigen::Vector3d translation;
        //estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        //std::cout<<"rotation 1"<<rotation.transpose()<<std::endl;
        //getchar();
        /*spts1.clear();
         spts2.clear();
         
         for (int i=0;i<testinliers.size();i++) {
         spts1.push_back(pts1[testinliers[i]]);
         spts2.push_back(pts2[testinliers[i]]);
         }
         estimateRotationTranslation(spts1,spts2,rotation,translation);
         
         std::cout<<"rotation 2"<<rotation<<std::endl;
         
         
         std::cout<<rotation<<std::endl;*/
        cv::Mat _m1(modelPoints,3,CV_64FC1),_m2(modelPoints,3,CV_64FC1),_fmatrix(9,3,CV_64FC1);
        cv::Point3d* m1 = _m1.ptr<cv::Point3d>();
        cv::Point3d* m2 = _m2.ptr<cv::Point3d>();
        
        for(int s=0;s<modelPoints;s++){
            
            m1[s].x=spts1[s](0);
            m1[s].y=spts1[s](1);
            m1[s].z=spts1[s](2);
            
            m2[s].x=spts2[s](0);
            m2[s].y=spts2[s](1);
            m2[s].z=spts2[s](2);
        }

        int N=run7Point(_m1,_m2,_fmatrix);
        if(N < 1 || N > 3){
            continue;
        }
        
        
        std::vector<Eigen::Matrix3d> F(N);
        for(int n=0;n<N;n++){
            for(int j1=0;j1<3;j1++){
                for(int j2=0;j2<3;j2++){
                    F[n](j1,j2)=_fmatrix.at<double>(3*n+j1,j2);
                }
            }
        }
        
        for(int n=0;n<N;n++){
            
            std::vector<int> inliers(0);
            for (int j= 0; j< pts1.size(); j++){
                Eigen::Vector3d planeVector=F[n]*pts1[j];
                double distance=pts2[j].transpose()*planeVector;
                distance=std::abs(distance)/planeVector.norm();
                if (distance<inlierThreshold){
                    inliers.push_back(j);
                }
            }
            
            //int goodCount=inliers.size();
            //std::cout<<"good count"<<goodCount<<std::endl;
            if( inliers.size() > MAX(bestInliers.size(), modelPoints-1) ){
                bestInliers=inliers;
                bestRotation=rotation;
                bestTranslation=translation;
                // niters = RANSACUpdateNumIters(0.98, (double)(count-inliers.size())/count, modelPoints, niters );
            }
        }
        //std::vector<int> inliers;
        //findInliers(pts1,pts2,rotation,translation,inliers,inlierThreshold);
    }
    
    //std::cout<<"rotation 1"<<bestRotation.transpose()<<std::endl;
    //getchar();
    
    //getchar();
    //std::cout<<"final iter"<<niters<<std::endl;
    std::vector<Eigen::Vector3d> inliers1(bestInliers.size());
    std::vector<Eigen::Vector3d> inliers2(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    
    
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation);
    findInliers(pts1,pts2,bestRotation,bestTranslation,bestInliers,inlierThreshold);
    inliers1.resize(bestInliers.size());
    inliers2.resize(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    //std::cout<<"good count"<<bestInliers.size()<<std::endl;
    ceres::LossFunction* loss=new ceres::HuberLoss(lossThreshold);
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation,loss,true);
    
    rotation=bestRotation;
    translation=bestTranslation;
    return true;
}



void _detectOutliers(const std::vector<Eigen::Vector3d> &pts1,
                     const std::vector<Eigen::Vector3d> &pts2,
                     std::vector<int>                   &inliers,
                     const int maxIterations,
                     const double threshold){
    inliers.clear();
    struct timeval tic;
    gettimeofday(&tic,0);
    srand (tic.tv_usec );
    
    const int sampleSize=7;
    
    for (int i=0;i<maxIterations;i++) {
        
        cv::Mat _m1(7,3,CV_64FC1),_m2(7,3,CV_64FC1),_fmatrix(9,3,CV_64FC1);
        
        cv::Point3d* m1 = _m1.ptr<cv::Point3d>();
        cv::Point3d* m2 = _m2.ptr<cv::Point3d>();
        
        for(int s=0;s<sampleSize;s++){
            
            int index=rand()%pts1.size();
            
            m1[s].x=pts1[index](0);
            m1[s].y=pts1[index](1);
            m1[s].z=pts1[index](2);
            
            m2[s].x=pts2[index](0);
            m2[s].y=pts2[index](1);
            m2[s].z=pts2[index](2);
        }
        
        int N=run7Point(_m1,_m2,_fmatrix);
        
        if(N < 1 || N > 3){
            continue;
        }
        
        
        std::vector<Eigen::Matrix3d> F(N);
        for(int n=0;n<N;n++){
            for(int j1=0;j1<3;j1++){
                for(int j2=0;j2<3;j2++){
                    F[n](j1,j2)=_fmatrix.at<double>(3*n+j1,j2);
                }
            }
        }
        
        for(int n=0;n<N;n++){
            std::vector<int> _inliers;
            for (int j= 0; j< pts1.size(); j++){
                Eigen::Vector3d planeVector=F[n]*pts1[j];
                double distance=pts2[j].transpose()*planeVector;
                distance=std::abs(distance)/planeVector.norm();
                if (distance<threshold){
                    _inliers.push_back(j);
                }
            }
            
            if (_inliers.size()>inliers.size()){
                inliers=_inliers;
            }
        }
    }
}

int main( int argc, char** argv )
{
    if (argc < 3) {
        std::cerr << "Usage: parser <num_points> <base_path>" << std::endl;
        return 1;
    }
    
    std::string base_path = argv[2];

    int num_iterations;
    int num_points_start, num_points_end, num_points_step;
    int noise_start, noise_end, noise_step;
    double outliers_start, outliers_end, outliers_step;
    // camera indices
    int v1 = 0, v2 = 1;
    char buf[256];

    num_iterations = 1000;
    
    num_points_start = atoi(argv[1]);
    num_points_end = atoi(argv[1]);
    num_points_step = 1;

    outliers_start = 0.05; 
    outliers_end = 0.505;
    outliers_step = 0.05;

    for (int nMatches = num_points_start; nMatches <= num_points_end; nMatches += num_points_step) {
        // Open the bearing vector points
        std::stringstream bearing_vector_filename;
        bearing_vector_filename << base_path << "/" << nMatches << "_bearing.in";
        std::cout << bearing_vector_filename.str().c_str() << std::endl;
        FILE * fptr_bearing_vector = fopen(bearing_vector_filename.str().c_str(), "r");
        if (!fptr_bearing_vector) {
            std::cerr << "ERROR: Unable to open bearing vector file" << std::endl;
            return 1;
        }
        
        // Open inliers file
        std::stringstream algorithm_filename;
        algorithm_filename << base_path << "/" << nMatches << "_matlab5ptInliers.out";
        std::cout << algorithm_filename.str() << std::endl;
        std::ifstream inliers_file(algorithm_filename.str());
        if (!inliers_file) {
            std::cerr << "ERROR: Unable to open " << algorithm_filename.str() << std::endl;
            return 1;
        }

        // Open output file
        algorithm_filename.str("");
        algorithm_filename << base_path << "/" << nMatches << "_our5ptError.out";
        std::cout << algorithm_filename.str() << std::endl;
        std::fstream output_file(algorithm_filename.str(), std::fstream::out);

        if (!output_file) {
            std::cerr << "ERROR: Unable to open " << algorithm_filename.str() << std::endl;
            return 1;
        }

        // Ground truth file
        std::stringstream ground_truth_filename;
        ground_truth_filename << base_path << "/" << nMatches << "_ground_truth1.in";
        FILE * fptr_ground_truth = fopen(ground_truth_filename.str().c_str(), "r");
        if (!fptr_ground_truth) {
            std::cerr << "ERROR: Unable to open ground_truth_filename file" << std::endl;
            return 1;
        }

        double lx, ly, rx, ry;
        for (double outliers = outliers_start; outliers <= outliers_end; outliers += outliers_step) {
             // store errors for each iterations of aggregate calculations
            std::vector<double> arrVectAngleError;
            std::vector<double> arrVectTranslationError;
            
            for (int iteration_idx = 0; iteration_idx < num_iterations; iteration_idx++) {
                Eigen::Matrix3d rotation_algorithm = Eigen::Matrix3d::Identity();
                Eigen::Vector3d translation_algorithm = Eigen::Vector3d::Zero();

                // Read the ground truth measurements (from frame 2 to frame 1)
                Eigen::Matrix3d rotation_gt;
                Eigen::Vector3d translation_gt;
                
                for (int i = 0; i < 3; i++) {
                    fgets(buf, 256, fptr_ground_truth);
                    sscanf( buf, "%lf %lf %lf %lf", 
                            &rotation_gt(i, 0), &rotation_gt(i, 1), &rotation_gt(i, 2),
                            &translation_gt(i)
                    );
                }   

                // get the inliers
                std::string line;
                std::vector<int> inliers;
                if (!std::getline(inliers_file, line))
                {
                    std::cerr << "No inliers at outlier: " << outliers << " iteration: " << iteration_idx << std::endl;
                }
                
                std::stringstream line_stream(line);
                std::string inlier;
                for ( ; std::getline(line_stream, inlier, ' '); )
                {
                    inliers.push_back(atoi(inlier.c_str()));
                    std::cout << inlier << ", ";
                }
                std::cout << std::endl;
                
                bearingVectors_t bearingVectors1;
                bearingVectors_t bearingVectors2;

                for (int bearing_vector_idx = 0; bearing_vector_idx < nMatches; bearing_vector_idx++) {
                    

                    double b1_0, b1_1, b1_2, b2_0, b2_1, b2_2;

                    fgets(buf, 256, fptr_bearing_vector);
                    sscanf( buf, "%lf %lf %lf %lf %lf %lf", 
                            &b1_0, &b1_1, &b1_2,
                            &b2_0, &b2_1, &b2_2
                    );

                    bearingVectors1.push_back(bearingVector_t(b1_0, b1_1, b1_2));
                    bearingVectors2.push_back(bearingVector_t(b2_0, b2_1, b2_2));
                    
                }

                std::vector<Eigen::Vector3d> pts1,pts2;
                pts1.reserve(bearingVectors1.size());
                pts2.reserve(bearingVectors2.size());
                for (int i=0;i<bearingVectors1.size();i++) {
                    pts1.push_back(bearingVectors1[i]);
                    pts2.push_back(bearingVectors2[i]);
                }
                estimateRotationTranslationRANSAC2(
                    pts1,
                    pts2,
                    inliers,
                    rotation_algorithm,
                    translation_algorithm
                );

                // for custom algorithm, the transformation is from frame 1 to frame 2
                // convert to frame 2 to frame 1
                rotation_algorithm.transposeInPlace();
                translation_algorithm = -rotation_algorithm * translation_algorithm;
                
                double errorAngle, errorTranslation;
                
                iccv17::misc_helpers::calculateError( 
                    rotation_gt,
                    translation_gt,
                    rotation_algorithm,
                    translation_algorithm,
                    errorAngle,
                    errorTranslation 
                );

                std::cout << rotation_algorithm << std::endl;
                std::cout << translation_algorithm << std::endl;

                std::cout << errorAngle << ", " << errorTranslation << std::endl;

                std::cout << "----------------------" << std::endl;

                arrVectAngleError.push_back(errorAngle);
                arrVectTranslationError.push_back(errorTranslation);

            } // iteration_idx loop end

            std::sort(arrVectAngleError.begin(),arrVectAngleError.end());
            std::sort(arrVectTranslationError.begin(),arrVectTranslationError.end());  


            
            // -------------- File out ------------------ //
            output_file
                    // rotation errors
                    << arrVectAngleError[NO_OF_ITERATIONS/2] << " " 
                    << arrVectAngleError[NO_OF_ITERATIONS-1] << " " 
                    << iccv17::misc_helpers::vectAverage(arrVectAngleError) << " "
                    // translation errors
                    << arrVectTranslationError[NO_OF_ITERATIONS/2] << " " 
                    << arrVectTranslationError[NO_OF_ITERATIONS-1] << " "
                    << iccv17::misc_helpers::vectAverage(arrVectTranslationError) << " "
                    // time required
                    // << arrVectTimeRequired[NO_OF_ITERATIONS/2] << " "
                    // << arrVectTimeRequired[NO_OF_ITERATIONS-1] << " "
                    // << iccv17::misc_helpers::vectAverage(arrVectTimeRequired)
                    << std::endl; 
        
        } // outliers loop end
        
        fclose(fptr_ground_truth);
        fclose(fptr_bearing_vector);
        output_file.close();
        
    } // nMatches loop end
    
}