#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <limits.h>
#include <Eigen/Eigen>
#include <opengv/relative_pose/methods.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/relative_pose/CentralRelativePoseSacProblem.hpp>
#include <opengv/relative_pose/NoncentralRelativeMultiAdapter.hpp>
#include <opengv/sac/MultiRansac.hpp>
#include <opengv/sac_problems/relative_pose/MultiNoncentralRelativePoseSacProblem.hpp>
#include <opengv/sac_problems/relative_pose/EigensolverSacProblem.hpp>
#include <sstream>
#include <fstream>

#include "random_generators.hpp"
#include "experiment_helpers.hpp"
#include "time_measurement.hpp"

#include "ceres/rotation.h"
#include "ceres/ceres.h"



using namespace std;
using namespace Eigen;
using namespace opengv;

struct RotationTranslation{
    
    RotationTranslation(const double _x1,const double _y1,const double _z1,
                        const double _x2,const double _y2,const double _z2):
    x1(_x1),y1(_y1),z1(_z1),x2(_x2),y2(_y2),z2(_z2){
    }
    
    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const {
        
        T rotated1[3];
        T p1[3]={(T)x1,(T)y1,(T)z1};
        T p2[3]={(T)x2,(T)y2,(T)z2};
        ceres::AngleAxisRotatePoint(camera,p1,rotated1);
        T norm=camera[3]*camera[3]+camera[4]*camera[4]+camera[5]*camera[5];
        
        if(sqrt(norm)>=(T)std::numeric_limits<double>::min()){//translation is not zero
            
            T axis[3];
            ceres::CrossProduct(rotated1,&camera[3],axis);
            T D=-(axis[0]*rotated1[0]+axis[1]*rotated1[1]+axis[2]*rotated1[2]);
            
            norm=axis[0]*axis[0]+axis[1]*axis[1]+axis[2]*axis[2];
            T t=(axis[0]*p2[0]+axis[1]*p2[1]+axis[2]*p2[2]+D)/norm;
            
            rotated1[0]=p2[0]+axis[0]*t;
            rotated1[1]=p2[1]+axis[1]*t;
            rotated1[2]=p2[2]+axis[2]*t;
        }
        
        norm=sqrt(rotated1[0]*rotated1[0]+rotated1[1]*rotated1[1]+rotated1[2]*rotated1[2]);
        residuals[0]=rotated1[0]/norm-p2[0];
        residuals[1]=rotated1[1]/norm-p2[1];
        residuals[2]=rotated1[2]/norm-p2[2];
        return true;
    }
    
    static ceres::CostFunction* Create(const double x1,
                                       const double y1,
                                       const double z1,
                                       const double x2,
                                       const double y2,
                                       const double z2) {
        
        return (new ceres::AutoDiffCostFunction<RotationTranslation,3,6>(new RotationTranslation(x1,y1,z1,x2,y2,z2)));
        
    };
    
    double x1;
    double y1;
    double z1;
    double x2;
    double y2;
    double z2;
};



struct Rotation{
    
    Rotation(const double _x1,const double _y1,const double _z1,
             const double _x2,const double _y2,const double _z2):
    x1(_x1),y1(_y1),z1(_z1),x2(_x2),y2(_y2),z2(_z2){
    }
    
    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const {
        
        T rotated1[3];
        T p1[3]={(T)x1,(T)y1,(T)z1};
        T p2[3]={(T)x2,(T)y2,(T)z2};
        ceres::AngleAxisRotatePoint(camera,p1,rotated1);
        residuals[0]=rotated1[0]-p2[0];
        residuals[1]=rotated1[1]-p2[1];
        residuals[2]=rotated1[2]-p2[2];
        return true;
    }
    
    static ceres::CostFunction* Create(const double x1,
                                       const double y1,
                                       const double z1,
                                       const double x2,
                                       const double y2,
                                       const double z2) {
        
        return (new ceres::AutoDiffCostFunction<Rotation,3,3>(new Rotation(x1,y1,z1,x2,y2,z2)));
        
    };
    
    double x1;
    double y1;
    double z1;
    double x2;
    double y2;
    double z2;
};

Eigen::Vector3d estimateRelativeTranslation(const std::vector<Eigen::Vector3d> &pts1,
                                            const std::vector<Eigen::Vector3d> &pts2){
    
    int num_point=pts1.size();
    std::vector<Eigen::Vector3d> norms(num_point);
    Eigen::Matrix3Xd allNorms=Eigen::Matrix3Xd(3,num_point);
    Eigen::Vector3d preResult,curResult;
    for(int i=0;i<num_point;i++){
        norms[i]=pts1[i].cross(pts2[i]);
        norms[i].normalize();
        allNorms.col(i)=norms[i];
    }
    Eigen::Matrix3d NtN=allNorms*allNorms.transpose();
    Eigen::JacobiSVD<Eigen::Matrix3d> svd(NtN,Eigen::ComputeFullV);
    Eigen::Matrix3d V=svd.matrixV();
    curResult=V.col(2);
    return curResult;
}


void preSolve(const bearingVectors_t &pts1,
              const bearingVectors_t &pts2,
              Eigen::Matrix3d& rotation,
              Eigen::Vector3d& translation){

    
    double motion[3]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    ceres::Problem problem;
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=Rotation::Create(pts1[i](0),
                                                            pts1[i](1),
                                                            pts1[i](2),
                                                            pts2[i](0),
                                                            pts2[i](1),
                                                            pts2[i](2));
        problem.AddResidualBlock(cost_function,NULL,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;
    ceres::Solve(options,&problem, &summary);
    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    
    std::vector<Eigen::Vector3d> _pts1(pts1.size()),_pts2(pts2.size());
    
    for(int p=0;p<pts1.size();p++){
        _pts1[p]=rotation*pts1[p];
        _pts2[p]=pts2[p];
    }
    translation=estimateRelativeTranslation(_pts1,_pts2);
}


void estimateRotationTranslation(const double lossThreshold,
                                 const bearingVectors_t &pts1,
                                 const bearingVectors_t &pts2,
                                 Eigen::Matrix3d& rotation,
                                 Eigen::Vector3d& translation){
    
    
    assert(pts1.size()==pts2.size());
    preSolve(pts1,pts2,rotation,translation);
    
    double motion[6]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    motion[3]=translation(0);
    motion[4]=translation(1);
    motion[5]=translation(2);

    
    ceres::Problem problem;
    ceres::LossFunction* loss_function = new ceres::HuberLoss(lossThreshold);
    
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=RotationTranslation::Create(pts1[i](0),
                                                                       pts1[i](1),
                                                                       pts1[i](2),
                                                                       pts2[i](0),
                                                                       pts2[i](1),
                                                                       pts2[i](2));
        problem.AddResidualBlock(cost_function,NULL,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;
    ceres::Solve(options,&problem, &summary);
    

    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    translation(0)=motion[3];
    translation(1)=motion[4];
    translation(2)=motion[5];
}

    
void solveCentralSacProblem(bearingVectors_t &bearingVectors1, 
                            bearingVectors_t &bearingVectors2,
                            Eigen::Matrix3d &rotationOut,
                            Eigen::Vector3d &translationOut,
                            Eigen::Matrix3d &rotationOptimizedOut,
                            Eigen::Vector3d &translationOptimzedOut,
                            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::Algorithm algorithm) 
{
    //Run the experiment
    struct timeval tic;
    struct timeval toc;

    relative_pose::CentralRelativeAdapter adapter(  bearingVectors1,
                                                    bearingVectors2);
            
    sac::Ransac<sac_problems::relative_pose::CentralRelativePoseSacProblem> ransac;
    std::shared_ptr<sac_problems::relative_pose::CentralRelativePoseSacProblem>
    relposeproblem_ptr(new sac_problems::relative_pose::CentralRelativePoseSacProblem(adapter, algorithm));
    ransac.sac_model_ = relposeproblem_ptr;
    ransac.threshold_ = 10.0*(1.0 - cos(atan(sqrt(2.0)*0.5/800.0)));
    ransac.max_iterations_ = 200;
    ransac.computeModel();
    
    sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t optimizedModel;
    relposeproblem_ptr->optimizeModelCoefficients(ransac.inliers_,
                                                  ransac.model_coefficients_,
                                                  optimizedModel);

    rotationOut = ransac.model_coefficients_.block<3,3>(0,0);
    translationOut =ransac.model_coefficients_.col(3);

    rotationOptimizedOut = optimizedModel.block<3,3>(0,0);
    translationOptimzedOut = optimizedModel.col(3);

    return;
}

void solveEigenSacProblem(  bearingVectors_t &bearingVectors1, 
                            bearingVectors_t &bearingVectors2,
                            Eigen::Matrix3d &rotationOut,
                            Eigen::Vector3d &translationOut,
                            Eigen::Matrix3d &rotationOptimizedOut,
                            Eigen::Vector3d &translationOptimzedOut)
                            // double &ransacTimeOut,
                            // double &optimizationTimeOut ) 
{
    //Run the experiment
    struct timeval tic;
    struct timeval toc;

    relative_pose::CentralRelativeAdapter adapter(  bearingVectors1,
                                                    bearingVectors2);
            
    sac::Ransac<
    sac_problems::relative_pose::EigensolverSacProblem> ransac;
    std::shared_ptr<
    sac_problems::relative_pose::EigensolverSacProblem> eigenproblem_ptr(new sac_problems::relative_pose::EigensolverSacProblem(adapter,10));
    
    ransac.sac_model_ = eigenproblem_ptr;
    ransac.threshold_ = 2.0;
    ransac.max_iterations_ = 100;
    
   
    gettimeofday( &tic, 0 );
    ransac.computeModel();
    gettimeofday( &toc, 0 );
    double ransacTimeOut = TIMETODOUBLE(timeval_minus(toc,tic));
    
    //do final polishing of the model over all inliers
    sac_problems::relative_pose::EigensolverSacProblem::model_t optimizedModel;
    
    gettimeofday( &tic, 0 );
    eigenproblem_ptr->optimizeModelCoefficients(ransac.inliers_,
                                                ransac.model_coefficients_,
                                                optimizedModel);
    gettimeofday( &toc, 0 );
    double optimizationTimeOut = TIMETODOUBLE(timeval_minus(toc, tic));

    rotationOut = ransac.model_coefficients_.rotation;
    translationOut = ransac.model_coefficients_.translation;

    rotationOptimizedOut = optimizedModel.rotation;
    translationOptimzedOut = optimizedModel.translation;

    return;
}

void calculateError(Eigen::Matrix3d trueRotation,
                    Eigen::Vector3d trueTranslation,
                    Eigen::Matrix3d measuredRotation,
                    Eigen::Vector3d measuredTranslation,
                    double &errorAngle,
                    double &errorTranslation)
{
    // rotation error
    Eigen::Vector3d angleDiff;
    Eigen::Matrix3d rotationDiff=measuredRotation.transpose() * trueRotation;
    ceres::RotationMatrixToAngleAxis(rotationDiff.data(), angleDiff.data());
    errorAngle = angleDiff.norm();
    
    // translation error
    measuredTranslation.normalize();
    trueTranslation.normalize();
    errorTranslation = std::min(std::acos(measuredTranslation.dot(trueTranslation)),
                                std::acos(measuredTranslation.dot(-trueTranslation)));
}

#define NO_OF_ITERATIONS 1000

int main( int argc, char** argv )
{
    for (size_t numberPoints = 60; numberPoints <= 250; numberPoints+=5) {
        std::stringstream outputFileName;
        outputFileName << numberPoints << ".out";
        std::fstream outputFile(outputFileName.str(), std::fstream::out);
        
        std::cout << "numberPoints: " << numberPoints << std::endl;
        for (double noise=10.0;noise<=15.0;noise+=1.0) {
            
            std::cout << "Noise: " << noise << std::endl;

            std::vector<double> vectAngleErrorCustom;
            std::vector<double> vectTranslationErrorCustom;

            std::vector<double> vectAngleErrorStew, vectAngleOptimizedErrorStew;
            std::vector<double> vectTranslationErrorStew, vectTranslationOptimizedErrorStew;

            std::vector<double> vectAngleErrorNister, vectAngleOptimizedErrorNister;
            std::vector<double> vectTranslationErrorNister, vectTranslationOptimizedErrorNister;

            std::vector<double> vectAngleErrorSevenPt, vectAngleOptimizedErrorSevenPt;
            std::vector<double> vectTranslationErrorSevenPt, vectTranslationOptimizedErrorSevenPt;

            std::vector<double> vectAngleErrorEightPt, vectAngleOptimizedErrorEightPt;
            std::vector<double> vectTranslationErrorEightPt, vectTranslationOptimizedErrorEightPt;

            std::vector<double> vectAngleErrorEigen, vectAngleOptimizedErrorEigen;
            std::vector<double> vectTranslationErrorEigen, vectTranslationOptimizedErrorEigen;

            for (int iter=0;iter<NO_OF_ITERATIONS;iter++) {
                
                std::cout << "Iterations: " << iter << std::endl;
                bearingVectors_t bearingVectors1;
                bearingVectors_t bearingVectors2;
                
                initializeRandomSeed();
                double outlierFraction = 0.0;
                            
                //generate a random pose for viewpoint 1
                translation_t position1 = Eigen::Vector3d::Zero();
                rotation_t rotation1 = Eigen::Matrix3d::Identity();
                
                //generate a random pose for viewpoint 2
                translation_t position2 = generateRandomTranslation(2);
                rotation_t rotation2 = generateRandomRotation(0.5);
                
                //create a fake central camera
                translations_t camOffsets;
                rotations_t camRotations;
                generateCentralCameraSystem( camOffsets, camRotations );
                
                //derive correspondences based on random point-cloud
                
                std::vector<int> camCorrespondences1; //unused in the central case
                std::vector<int> camCorrespondences2; //unused in the central case
                Eigen::MatrixXd gt(3,numberPoints);
                generateRandom2D2DCorrespondences(position1, rotation1, position2, rotation2,
                                                  camOffsets, camRotations, numberPoints, noise, outlierFraction,
                                                  bearingVectors1, bearingVectors2,
                                                  camCorrespondences1, camCorrespondences2, gt );
                
                //Extract the relative pose
                translation_t position; rotation_t rotation;
                extractRelativePose(
                                    position1, position2, rotation1, rotation2, position, rotation );
                Eigen::Vector3d translation = position2-position1;
                
                
                //print experiment characteristics
                //printExperimentCharacteristics( position, rotation, noise, outlierFraction );
                
                //compute and print the essential-matrix
                //printEssentialMatrix( position, rotation );
                
                
                
                /*cv::RNG rng(-1);
                translationCustom(0)=rng.uniform(-0.01,0.01);
                translationCustom(1)=rng.uniform(-0.01,0.01);
                translationCustom(2)=rng.uniform(-0.01,0.01);*/
                
                // ------------------------------- calculate error from custom algorithm ------------------------------- //
                double errorAngleCustom, errorTranslationCustom;
                Eigen::Matrix3d rotationCustom=Eigen::Matrix3d::Identity();
                Eigen::Vector3d translationCustom=Eigen::Vector3d::Zero();

                estimateRotationTranslation(0.0,
                                             bearingVectors1,
                                             bearingVectors2,
                                             rotationCustom,
                                             translationCustom);

                calculateError( rotation,
                                translation,
                                rotationCustom.transpose(),
                                rotationCustom.transpose() * translationCustom,
                                errorAngleCustom,
                                errorTranslationCustom );

                vectAngleErrorCustom.push_back(errorAngleCustom);
                vectTranslationErrorCustom.push_back(errorTranslationCustom);

                // ------------------------------- calculate error from STEWENIUS ------------------------------- //
                double errorAngleStew, errorTranslationStew;
                double errorAngleOptimizedStew, errorTranslationOptimizedStew;
                Eigen::Matrix3d rotationStew, rotationOptimizedStew;
                Eigen::Vector3d translationStew, translationOptimizedStew;
                
                solveCentralSacProblem( bearingVectors1,
                                        bearingVectors2,
                                        rotationStew,
                                        translationStew,
                                        rotationOptimizedStew,
                                        translationOptimizedStew,
                                        sac_problems::relative_pose::CentralRelativePoseSacProblem::STEWENIUS );

                calculateError( rotation,
                                translation,
                                rotationStew,
                                translationStew,
                                errorAngleStew,
                                errorTranslationStew );

                calculateError( rotation,
                                translation,
                                rotationOptimizedStew,
                                translationOptimizedStew,
                                errorAngleOptimizedStew,
                                errorTranslationOptimizedStew );
                
                vectAngleErrorStew.push_back(errorAngleStew);
                vectTranslationErrorStew.push_back(errorTranslationStew);

                vectAngleOptimizedErrorStew.push_back(errorAngleOptimizedStew);
                vectTranslationOptimizedErrorStew.push_back(errorTranslationOptimizedStew);

                
                // ------------------------------- calculate error from NISTER ------------------------------- //
                double errorAngleNister, errorTranslationNister;
                double errorAngleOptimizedNister, errorTranslationOptimizedNister;
                Eigen::Matrix3d rotationNister, rotationOptimizedNister;
                Eigen::Vector3d translationNister, translationOptimizedNister;
                
                solveCentralSacProblem( bearingVectors1,
                                        bearingVectors2,
                                        rotationNister,
                                        translationNister,
                                        rotationOptimizedNister,
                                        translationOptimizedNister,
                                        sac_problems::relative_pose::CentralRelativePoseSacProblem::NISTER );

                calculateError( rotation,
                                translation,
                                rotationNister,
                                translationNister,
                                errorAngleNister,
                                errorTranslationNister );

                calculateError( rotation,
                                translation,
                                rotationOptimizedNister,
                                translationOptimizedNister,
                                errorAngleOptimizedNister,
                                errorTranslationOptimizedNister );
                
                vectAngleErrorNister.push_back(errorAngleNister);
                vectTranslationErrorNister.push_back(errorTranslationNister);

                vectAngleOptimizedErrorNister.push_back(errorAngleOptimizedNister);
                vectTranslationOptimizedErrorNister.push_back(errorTranslationOptimizedNister);

                // ------------------------------- calculate error from SEVENPT ------------------------------- //
                double errorAngleSevenPt, errorTranslationSevenPt;
                double errorAngleOptimizedSevenPt, errorTranslationOptimizedSevenPt;
                Eigen::Matrix3d rotationSevenPt, rotationOptimizedSevenPt;
                Eigen::Vector3d translationSevenPt, translationOptimizedSevenPt;
                
                solveCentralSacProblem( bearingVectors1,
                                        bearingVectors2,
                                        rotationSevenPt,
                                        translationSevenPt,
                                        rotationOptimizedSevenPt,
                                        translationOptimizedSevenPt,
                                        sac_problems::relative_pose::CentralRelativePoseSacProblem::SEVENPT );

                calculateError( rotation,
                                translation,
                                rotationSevenPt,
                                translationSevenPt,
                                errorAngleSevenPt,
                                errorTranslationSevenPt );

                calculateError( rotation,
                                translation,
                                rotationOptimizedSevenPt,
                                translationOptimizedSevenPt,
                                errorAngleOptimizedSevenPt,
                                errorTranslationOptimizedSevenPt );
                
                vectAngleErrorSevenPt.push_back(errorAngleSevenPt);
                vectTranslationErrorSevenPt.push_back(errorTranslationSevenPt);

                vectAngleOptimizedErrorSevenPt.push_back(errorAngleOptimizedSevenPt);
                vectTranslationOptimizedErrorSevenPt.push_back(errorTranslationOptimizedSevenPt);

                // ------------------------------- calculate error from EIGHTPT ------------------------------- //
                double errorAngleEightPt, errorTranslationEightPt;
                double errorAngleOptimizedEightPt, errorTranslationOptimizedEightPt;
                Eigen::Matrix3d rotationEightPt, rotationOptimizedEightPt;
                Eigen::Vector3d translationEightPt, translationOptimizedEightPt;
                
                solveCentralSacProblem( bearingVectors1,
                                        bearingVectors2,
                                        rotationEightPt,
                                        translationEightPt,
                                        rotationOptimizedEightPt,
                                        translationOptimizedEightPt,
                                        sac_problems::relative_pose::CentralRelativePoseSacProblem::EIGHTPT );

                calculateError( rotation,
                                translation,
                                rotationEightPt,
                                translationEightPt,
                                errorAngleEightPt,
                                errorTranslationEightPt );


                calculateError( rotation,
                                translation,
                                rotationOptimizedEightPt,
                                translationOptimizedEightPt,
                                errorAngleOptimizedEightPt,
                                errorTranslationOptimizedEightPt );

                vectAngleErrorEightPt.push_back(errorAngleEightPt);
                vectTranslationErrorEightPt.push_back(errorTranslationEightPt);

                vectAngleOptimizedErrorEightPt.push_back(errorAngleOptimizedEightPt);
                vectTranslationOptimizedErrorEightPt.push_back(errorTranslationOptimizedEightPt);

                // ------------------------------- calculate error from EIGEN ------------------------------- //
                double errorAngleEigen, errorTranslationEigen;
                double errorAngleOptimizedEigen, errorTranslationOptimizedEigen;
                Eigen::Matrix3d rotationEigen, rotationOptimizedEigen;
                Eigen::Vector3d translationEigen, translationOptimizedEigen;
                
                solveEigenSacProblem(   bearingVectors1,
                                        bearingVectors2,
                                        rotationEigen,
                                        translationEigen,
                                        rotationOptimizedEigen,
                                        translationOptimizedEigen   );

                calculateError( rotation,
                                translation,
                                rotationEigen,
                                translationEigen,
                                errorAngleEigen,
                                errorTranslationEigen );

                calculateError( rotation,
                                translation,
                                rotationOptimizedEigen,
                                translationOptimizedEigen,
                                errorAngleOptimizedEigen,
                                errorTranslationOptimizedEigen );
                
                vectAngleErrorEigen.push_back(errorAngleEigen);
                vectTranslationErrorEigen.push_back(errorTranslationEigen);

                vectAngleOptimizedErrorEigen.push_back(errorAngleOptimizedEigen);
                vectTranslationOptimizedErrorEigen.push_back(errorTranslationOptimizedEigen);

            }
            
            std::sort(vectAngleErrorCustom.begin(),vectAngleErrorCustom.end());
            std::sort(vectTranslationErrorCustom.begin(),vectTranslationErrorCustom.end());

            std::sort(vectAngleErrorStew.begin(),vectAngleErrorStew.end());
            std::sort(vectTranslationErrorStew.begin(),vectTranslationErrorStew.end());

            std::sort(vectAngleErrorNister.begin(),vectAngleErrorNister.end());
            std::sort(vectTranslationErrorNister.begin(),vectTranslationErrorNister.end());

            std::sort(vectAngleErrorSevenPt.begin(),vectAngleErrorSevenPt.end());
            std::sort(vectTranslationErrorSevenPt.begin(),vectTranslationErrorSevenPt.end());

            std::sort(vectAngleErrorEightPt.begin(),vectAngleErrorEightPt.end());
            std::sort(vectTranslationErrorEightPt.begin(),vectTranslationErrorEightPt.end());

            std::sort(vectAngleErrorEigen.begin(),vectAngleErrorEigen.end());
            std::sort(vectTranslationErrorEigen.begin(),vectTranslationErrorEigen.end());

            // ---- Optimized results ---- //
            std::sort(vectAngleOptimizedErrorStew.begin(),vectAngleOptimizedErrorStew.end());
            std::sort(vectTranslationOptimizedErrorStew.begin(),vectTranslationOptimizedErrorStew.end());

            std::sort(vectAngleOptimizedErrorNister.begin(),vectAngleOptimizedErrorNister.end());
            std::sort(vectTranslationOptimizedErrorNister.begin(),vectTranslationOptimizedErrorNister.end());

            std::sort(vectAngleOptimizedErrorSevenPt.begin(),vectAngleOptimizedErrorSevenPt.end());
            std::sort(vectTranslationOptimizedErrorSevenPt.begin(),vectTranslationOptimizedErrorSevenPt.end());

            std::sort(vectAngleOptimizedErrorEightPt.begin(),vectAngleOptimizedErrorEightPt.end());
            std::sort(vectTranslationOptimizedErrorEightPt.begin(),vectTranslationOptimizedErrorEightPt.end());

            std::sort(vectAngleOptimizedErrorEigen.begin(),vectAngleOptimizedErrorEigen.end());
            std::sort(vectTranslationOptimizedErrorEigen.begin(),vectTranslationOptimizedErrorEigen.end());
            
            // -------------- File out ------------------ //
            outputFile  << vectAngleErrorCustom[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleErrorCustom[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationErrorCustom[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationErrorCustom[NO_OF_ITERATIONS-1]
                        << std::endl;

            outputFile  << vectAngleErrorStew[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleErrorStew[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationErrorStew[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationErrorStew[NO_OF_ITERATIONS-1]
                        << std::endl;

            outputFile  << vectAngleErrorNister[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleErrorNister[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationErrorNister[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationErrorNister[NO_OF_ITERATIONS-1]
                        << std::endl;

            outputFile  << vectAngleErrorSevenPt[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleErrorSevenPt[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationErrorSevenPt[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationErrorSevenPt[NO_OF_ITERATIONS-1]
                        << std::endl;

            outputFile  << vectAngleErrorEightPt[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleErrorEightPt[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationErrorEightPt[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationErrorEightPt[NO_OF_ITERATIONS-1]
                        << std::endl;  

            outputFile  << vectAngleErrorEigen[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleErrorEigen[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationErrorEigen[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationErrorEigen[NO_OF_ITERATIONS-1]
                        << std::endl;                  

            // ---- Optimized results ---- //
            outputFile  << vectAngleOptimizedErrorStew[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleOptimizedErrorStew[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationOptimizedErrorStew[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationOptimizedErrorStew[NO_OF_ITERATIONS-1]
                        << std::endl;

            outputFile  << vectAngleOptimizedErrorNister[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleOptimizedErrorNister[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationOptimizedErrorNister[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationOptimizedErrorNister[NO_OF_ITERATIONS-1]
                        << std::endl;

            outputFile  << vectAngleOptimizedErrorSevenPt[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleOptimizedErrorSevenPt[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationOptimizedErrorSevenPt[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationOptimizedErrorSevenPt[NO_OF_ITERATIONS-1]
                        << std::endl;

            outputFile  << vectAngleOptimizedErrorEightPt[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleOptimizedErrorEightPt[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationOptimizedErrorEightPt[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationOptimizedErrorEightPt[NO_OF_ITERATIONS-1]
                        << std::endl;  

            outputFile  << vectAngleOptimizedErrorEigen[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectAngleOptimizedErrorEigen[NO_OF_ITERATIONS-1] << " " 
                        << vectTranslationOptimizedErrorEigen[NO_OF_ITERATIONS/2 - 1] << " " 
                        << vectTranslationOptimizedErrorEigen[NO_OF_ITERATIONS-1]
                        << std::endl;

            /*for(int i=0;i<vectAngleErrorCustom.size();i++){
                std::cout<<vectAngleErrorCustom[i]<<std::endl;
            }
            getchar();*/
            
            //getchar();
        }
        outputFile.close();
    }
    /*for (int i=0;i<errorsx.size();i++) {
        std::cout<<errorsx[i]<<' '<<errorsy[i]<<std::endl;
    }*/
    
    return 0;
}