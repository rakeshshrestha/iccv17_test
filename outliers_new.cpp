
#include "opencv2/core/core.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <limits.h>
#include <Eigen/Eigen>
#include <opengv/relative_pose/methods.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/relative_pose/CentralRelativePoseSacProblem.hpp>
#include <opengv/relative_pose/NoncentralRelativeMultiAdapter.hpp>
#include <opengv/sac/MultiRansac.hpp>
#include <opengv/sac_problems/relative_pose/MultiNoncentralRelativePoseSacProblem.hpp>
#include <opengv/sac_problems/relative_pose/EigensolverSacProblem.hpp>
#include <sstream>
#include <fstream>

#include <iccv17_test/utils/random_generators.hpp>
#include <iccv17_test/utils/experiment_helpers.hpp>
#include <iccv17_test/utils/time_measurement.hpp>

#include "ceres/rotation.h"
#include "ceres/ceres.h"



using namespace std;
using namespace Eigen;
using namespace opengv;

inline double computeError(const Eigen::Vector3d& p1,
                            const Eigen::Vector3d& p2,
                            const Eigen::Matrix3d& rotation,
                            const Eigen::Vector3d& translation){
    
    
    Eigen::Vector3d rotated=rotation*p1;
    Eigen::Vector3d axis;
    ceres::CrossProduct(rotated.data(),translation.data(),axis.data());
    
    double D=-(axis(0)*rotated(0)+axis(1)*rotated(1)+axis(2)*rotated(2));
    double norm=axis(0)*axis(0)+axis(1)*axis(1)+axis(2)*axis(2);
    double t=(axis(0)*p2(0)+axis(1)*p2(1)+axis(2)*p2(2)+D)/norm;
        
    rotated(0)=p2(0)+axis(0)*t;
    rotated(1)=p2(1)+axis(1)*t;
    rotated(2)=p2(2)+axis(2)*t;

    
    rotated.normalize();
    rotated=rotated-p2;
    
    //std::cout<<"evaluation error"<<rotated.norm()<<std::endl;
    return rotated.norm();
}

struct RotationTranslation{
    
    RotationTranslation(const double _x1,const double _y1,const double _z1,
                        const double _x2,const double _y2,const double _z2):
    x1(_x1),y1(_y1),z1(_z1),x2(_x2),y2(_y2),z2(_z2){
    }
    
    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const {
        
        T rotated1[3];
        T p1[3]={(T)x1,(T)y1,(T)z1};
        T p2[3]={(T)x2,(T)y2,(T)z2};
        ceres::AngleAxisRotatePoint(camera,p1,rotated1);
        T norm=camera[3]*camera[3]+camera[4]*camera[4]+camera[5]*camera[5];
        
        if(sqrt(norm)>=(T)std::numeric_limits<double>::min()){//translation is not zero
            
            T axis[3];
            ceres::CrossProduct(rotated1,&camera[3],axis);
            T D=-(axis[0]*rotated1[0]+axis[1]*rotated1[1]+axis[2]*rotated1[2]);
            
            norm=axis[0]*axis[0]+axis[1]*axis[1]+axis[2]*axis[2];
            T t=(axis[0]*p2[0]+axis[1]*p2[1]+axis[2]*p2[2]+D)/norm;
            
            rotated1[0]=p2[0]+axis[0]*t;
            rotated1[1]=p2[1]+axis[1]*t;
            rotated1[2]=p2[2]+axis[2]*t;
        }
        
        norm=sqrt(rotated1[0]*rotated1[0]+rotated1[1]*rotated1[1]+rotated1[2]*rotated1[2]);
        residuals[0]=rotated1[0]/norm-p2[0];
        residuals[1]=rotated1[1]/norm-p2[1];
        residuals[2]=rotated1[2]/norm-p2[2];
        return true;
    }
    
    static ceres::CostFunction* Create(const double x1,
                                       const double y1,
                                       const double z1,
                                       const double x2,
                                       const double y2,
                                       const double z2) {
        
        return (new ceres::AutoDiffCostFunction<RotationTranslation,3,6>(new RotationTranslation(x1,y1,z1,x2,y2,z2)));
        
    };
    
    double x1;
    double y1;
    double z1;
    double x2;
    double y2;
    double z2;
};



struct Rotation{
    
    Rotation(const double _x1,const double _y1,const double _z1,
             const double _x2,const double _y2,const double _z2):
    x1(_x1),y1(_y1),z1(_z1),x2(_x2),y2(_y2),z2(_z2){
    }
    
    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const {
        
        T rotated1[3];
        T p1[3]={(T)x1,(T)y1,(T)z1};
        T p2[3]={(T)x2,(T)y2,(T)z2};
        ceres::AngleAxisRotatePoint(camera,p1,rotated1);
        residuals[0]=rotated1[0]-p2[0];
        residuals[1]=rotated1[1]-p2[1];
        residuals[2]=rotated1[2]-p2[2];
        return true;
    }
    
    static ceres::CostFunction* Create(const double x1,
                                       const double y1,
                                       const double z1,
                                       const double x2,
                                       const double y2,
                                       const double z2) {
        
        return (new ceres::AutoDiffCostFunction<Rotation,3,3>(new Rotation(x1,y1,z1,x2,y2,z2)));
        
    };
    
    double x1;
    double y1;
    double z1;
    double x2;
    double y2;
    double z2;
};

Eigen::Vector3d estimateRelativeTranslation(const std::vector<Eigen::Vector3d> &pts1,
                                            const std::vector<Eigen::Vector3d> &pts2){
    
    int num_point=pts1.size();
    std::vector<Eigen::Vector3d> norms(num_point);
    Eigen::Matrix3Xd allNorms=Eigen::Matrix3Xd(3,num_point);
    Eigen::Vector3d preResult,curResult;
    for(int i=0;i<num_point;i++){
        norms[i]=pts1[i].cross(pts2[i]);
        norms[i].normalize();
        allNorms.col(i)=norms[i];
    }
    Eigen::Matrix3d NtN=allNorms*allNorms.transpose();
    Eigen::JacobiSVD<Eigen::Matrix3d> svd(NtN,Eigen::ComputeFullV);
    Eigen::Matrix3d V=svd.matrixV();
    curResult=V.col(2);
    return curResult;
}


void preSolve(const std::vector<Eigen::Vector3d> &pts1,
              const std::vector<Eigen::Vector3d> &pts2,
              Eigen::Matrix3d& rotation,
              Eigen::Vector3d& translation){
    
    
    double motion[3]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    ceres::Problem problem;
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=Rotation::Create(pts1[i](0),
                                                            pts1[i](1),
                                                            pts1[i](2),
                                                            pts2[i](0),
                                                            pts2[i](1),
                                                            pts2[i](2));
        problem.AddResidualBlock(cost_function,NULL,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;
    ceres::Solve(options,&problem, &summary);
    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    
    std::vector<Eigen::Vector3d> _pts1(pts1.size()),_pts2(pts2.size());
    
    for(int p=0;p<pts1.size();p++){
        _pts1[p]=rotation*pts1[p];
        _pts2[p]=pts2[p];
    }
    translation=estimateRelativeTranslation(_pts1,_pts2);
}


void estimateRotationTranslation(const std::vector<Eigen::Vector3d> &pts1,
                                 const std::vector<Eigen::Vector3d> &pts2,
                                 Eigen::Matrix3d& rotation,
                                 Eigen::Vector3d& translation,
                                 ceres::LossFunction* loss_function=NULL,
                                 bool initialized=false){
    
    
    assert(pts1.size()==pts2.size());
    if (!initialized) {
        preSolve(pts1,pts2,rotation,translation);
    }
    double motion[6]={0};
    ceres::RotationMatrixToAngleAxis(rotation.data(),&motion[0]);
    motion[3]=translation(0);
    motion[4]=translation(1);
    motion[5]=translation(2);
    
    
    ceres::Problem problem;
    for(int i=0;i<pts1.size();i++){
        ceres::CostFunction* cost_function=RotationTranslation::Create(pts1[i](0),
                                                                       pts1[i](1),
                                                                       pts1[i](2),
                                                                       pts2[i](0),
                                                                       pts2[i](1),
                                                                       pts2[i](2));
        problem.AddResidualBlock(cost_function,loss_function,motion);
    }
    
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout=false;
    ceres::Solver::Summary summary;
    ceres::Solve(options,&problem, &summary);
    
    
    ceres::AngleAxisToRotationMatrix(motion,rotation.data());
    translation(0)=motion[3];
    translation(1)=motion[4];
    translation(2)=motion[5];
    
}

static bool haveColPlannarPoints(const std::vector<Eigen::Vector3d>& pt3d,const int count){
    
    // check that the i-th selected point does not on
    // a plane contains some previously selected points
    
    for(int i = 0; i < count-1; i++ ){
        
        Eigen::Vector3d pt1=pt3d[i];
        Eigen::Vector3d pt2=pt3d[count-1];
        Eigen::Vector3d norm=pt1.cross(pt2);
        norm.normalize();
        
        for(int j = 0; j < i; j++ ){
            double angle=abs(norm.dot(pt3d[j]));
            if (angle<=DBL_EPSILON) {
                return true;
            }
        }
    }
    return false;
}

static bool checkSubset(const std::vector<Eigen::Vector3d>& spts1,
                        const std::vector<Eigen::Vector3d>& spts2,
                        const int count){
    return !haveColPlannarPoints(spts1,count)&&!haveColPlannarPoints(spts2,count);
}

static int modelPoints=5;
std::vector<int> testinliers;
bool getSubset(cv::RNG& rng,
               const std::vector<Eigen::Vector3d>& pts1,
               const std::vector<Eigen::Vector3d>& pts2,
               std::vector<Eigen::Vector3d>& spts1,
               std::vector<Eigen::Vector3d>& spts2){
    
    //modelPoints=pts1.size();
    //testinliers.clear();
    spts1.resize(modelPoints);
    spts2.resize(modelPoints);
    std::vector<int> _idx(modelPoints);
    int* idx = &_idx[0];
    int maxAttempts=1000;
    int count=pts1.size();
    int i = 0, j, k, iters = 0;
    
    
    for(; iters < maxAttempts; iters++){
        
        for(i = 0; i < modelPoints && iters < maxAttempts; ){
            
            int idx_i = 0;
            for(;;){
                idx_i = idx[i] = rng.uniform(0, count);
                for( j = 0; j < i; j++ )
                    if( idx_i == idx[j] )
                        break;
                if( j == i )
                    break;
            }
            
            spts1[i]=(pts1[idx_i]);
            spts2[i]=(pts2[idx_i]);
            
            if(!checkSubset(spts1,spts2,i+1)){
                i = rng.uniform(0, i+1);
                iters++;
                continue;
            }
            i++;
        }
        
        if(i == modelPoints && !checkSubset(spts1,spts2,i))
            continue;
        break;
    }
    
    /*for (int i=0;i<modelPoints;i++) {
        testinliers.push_back(idx[i]);
        std::cout<<"select"<<idx[i]<<std::endl;
    }*/
    
    return i == modelPoints && iters < maxAttempts;
}

int RANSACUpdateNumIters( double p, double ep, int modelPoints, int maxIters ){
    
    if( modelPoints <= 0 )
        // CV_Error(cv::Error::StsOutOfRange, "the number of model points should be positive" );
        std::cerr << "the number of model points should be positive" << std::endl;
    
    p  = MAX(p,0.);
    p  = MIN(p,1.);
    ep = MAX(ep,0.);
    ep = MIN(ep,1.);
    
    // avoid inf's & nan's
    double num = MAX(1. - p, DBL_MIN);
    double denom = 1. - std::pow(1. - ep, modelPoints);
    if( denom < DBL_MIN )
        return 0;
    
    num = std::log(num);
    denom = std::log(denom);
    
    return denom >= 0 || -num >= maxIters*(-denom) ? maxIters : cvRound(num/denom);
}

void findInliers(const std::vector<Eigen::Vector3d>& pts1,
                const std::vector<Eigen::Vector3d>& pts2,
                const Eigen::Matrix3d& rotation,
                const Eigen::Vector3d& translation,
                std::vector<int>& inliers,
                double thresh){
    inliers.clear();
    for (int i=0;i<pts1.size();i++) {
        if (computeError(pts1[i],pts2[i],rotation,translation)<=thresh) {
            inliers.push_back(i);
        }
    }
}

bool estimateRotationTranslationRANSAC3(
                                       const std::vector<Eigen::Vector3d> &pts1,
                                       const std::vector<Eigen::Vector3d> &pts2,
                                       Eigen::Matrix3d& rotation,
                                       Eigen::Vector3d& translation,
                                       int   maxIterations=1000,
                                       const double lossThreshold=0.005,
                                       const double inlierThreshold=0.005){
    
    cv::RNG rng((uint64)-1);
    int maxGoodCount=0;
    int count=pts1.size();
    int niters = maxIterations;
    
    
    std::vector<int> bestInliers;
    Eigen::Matrix3d bestRotation;
    Eigen::Vector3d bestTranslation;
    Eigen::Matrix3d gtRotation=rotation;
    
    double bestError=DBL_MAX;
    for(int iter = 0; iter < niters; iter++){
        
        std::vector<Eigen::Vector3d> spts1,spts2;
        bool found = getSubset(rng,pts1,pts2,spts1,spts2);
        if(!found ){
            if( iter == 0 )
                return false;
            break;
        }
        
        Eigen::Matrix3d rotation;
        Eigen::Vector3d translation;
        estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        Eigen::Matrix3d diff=gtRotation.transpose()*rotation;
        Eigen::Vector3d angle;
        ceres::RotationMatrixToAngleAxis(diff.data(),angle.data());
        
        if (angle.norm()<bestError) {
            bestRotation=rotation;
        }
    }
    rotation=bestRotation;
    return true;
}

bool estimateRotationTranslationRANSAC(
                                 const std::vector<Eigen::Vector3d> &pts1,
                                 const std::vector<Eigen::Vector3d> &pts2,
                                 Eigen::Matrix3d& rotation,
                                 Eigen::Vector3d& translation,
                                 int   maxIterations=1000,
                                 const double lossThreshold=0.005,
                                 const double inlierThreshold=0.005){
    
    cv::RNG rng((uint64)-1);
    int maxGoodCount=0;
    int count=pts1.size();
    int niters = maxIterations;
    
    
    std::vector<int> bestInliers;
    Eigen::Matrix3d bestRotation;
    Eigen::Vector3d bestTranslation;
    
    /*for (int i=0;i<pts1.size();i++) {
        std::cout<<pts1[i].transpose()<<std::endl;
    }*/
    
    for(int iter = 0; iter < niters; iter++){
        
        std::vector<Eigen::Vector3d> spts1,spts2;
        bool found = getSubset(rng,pts1,pts2,spts1,spts2);
        if(!found ){
            if( iter == 0 )
                return false;
            break;
        }
        
        /*for (int i=0;i<spts1.size();i++) {
            std::cout<<"subset "<<spts1[i].transpose()<<std::endl;
        }
        getchar();*/
        
        
        Eigen::Matrix3d rotation;
        Eigen::Vector3d translation;
        estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        //std::cout<<"rotation 1"<<rotation.transpose()<<std::endl;
        //getchar();
        /*spts1.clear();
        spts2.clear();
        
        for (int i=0;i<testinliers.size();i++) {
            spts1.push_back(pts1[testinliers[i]]);
            spts2.push_back(pts2[testinliers[i]]);
        }
        estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        std::cout<<"rotation 2"<<rotation<<std::endl;

        
        std::cout<<rotation<<std::endl;*/
        
        std::vector<int> inliers;
        findInliers(pts1,pts2,rotation,translation,inliers,inlierThreshold);
        int goodCount=inliers.size();
        //std::cout<<"good count"<<goodCount<<std::endl;
        if( goodCount > MAX(maxGoodCount, modelPoints-1) ){
            bestInliers=inliers;
            bestRotation=rotation;
            bestTranslation=translation;
            maxGoodCount=goodCount;
            //niters = RANSACUpdateNumIters(0.98, (double)(count-goodCount)/count, modelPoints, niters );
        }
    }
    
    //std::cout<<"rotation 1"<<bestRotation.transpose()<<std::endl;
    //getchar();

    //getchar();
    //std::cout<<"final iter"<<niters<<std::endl;
    std::vector<Eigen::Vector3d> inliers1(bestInliers.size());
    std::vector<Eigen::Vector3d> inliers2(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    
    ceres::LossFunction* loss=new ceres::HuberLoss(lossThreshold);
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation);
    findInliers(pts1,pts2,bestRotation,bestTranslation,bestInliers,inlierThreshold);
    inliers1.resize(bestInliers.size());
    inliers2.resize(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation,NULL,true);
    
    rotation=bestRotation;
    translation=bestTranslation;
    return true;
}
static int run7Point(const cv::Mat& _m1,const cv::Mat& _m2,cv::Mat& _fmatrix )
{
    double a[7*9], w[7], u[9*9], v[9*9], c[4], r[3];
    double* f1, *f2;
    double t0, t1, t2;
    cv::Mat A( 7, 9, CV_64F, a );
    cv::Mat U( 7, 9, CV_64F, u );
    cv::Mat Vt( 9, 9, CV_64F, v );
    cv::Mat W( 7, 1, CV_64F, w );
    cv::Mat coeffs( 1, 4, CV_64F, c );
    cv::Mat roots( 1, 3, CV_64F, r );
    
    const cv::Point3d* m1 = _m1.ptr<cv::Point3d>();
    const cv::Point3d* m2 = _m2.ptr<cv::Point3d>();
    
    double* fmatrix = _fmatrix.ptr<double>();
    int i, k, n;
    
    // form a linear system: i-th row of A(=a) represents
    // the equation: (m2[i], 1)'*F*(m1[i], 1) = 0
    for( i = 0; i < 7; i++ ){
        
        double x0 = m1[i].x, y0 = m1[i].y,z0=m1[i].z;
        double x1 = m2[i].x, y1 = m2[i].y,z1=m2[i].z;
        
        a[i*9+0] = x1*x0;
        a[i*9+1] = x1*y0;
        a[i*9+2] = x1*z0;
        a[i*9+3] = y1*x0;
        a[i*9+4] = y1*y0;
        a[i*9+5] = y1*z0;
        a[i*9+6] = x0*z1;
        a[i*9+7] = y0*z1;
        a[i*9+8] = z0*z1;
    }
    
    // A*(f11 f12 ... f33)' = 0 is singular (7 equations for 9 variables), so
    // the solution is linear subspace of dimensionality 2.
    // => use the last two singular vectors as a basis of the space
    // (according to SVD properties)
    cv::SVDecomp( A, W, U, Vt,cv::SVD::MODIFY_A + cv::SVD::FULL_UV );
    f1 = v + 7*9;
    f2 = v + 8*9;
    
    // f1, f2 is a basis => lambda*f1 + mu*f2 is an arbitrary f. matrix.
    // as it is determined up to a scale, normalize lambda & mu (lambda + mu = 1),
    // so f ~ lambda*f1 + (1 - lambda)*f2.
    // use the additional constraint det(f) = det(lambda*f1 + (1-lambda)*f2) to find lambda.
    // it will be a cubic equation.
    // find c - polynomial coefficients.
    for( i = 0; i < 9; i++ )
        f1[i] -= f2[i];
    
    t0 = f2[4]*f2[8] - f2[5]*f2[7];
    t1 = f2[3]*f2[8] - f2[5]*f2[6];
    t2 = f2[3]*f2[7] - f2[4]*f2[6];
    
    c[3] = f2[0]*t0 - f2[1]*t1 + f2[2]*t2;
    
    c[2] = f1[0]*t0 - f1[1]*t1 + f1[2]*t2 -
    f1[3]*(f2[1]*f2[8] - f2[2]*f2[7]) +
    f1[4]*(f2[0]*f2[8] - f2[2]*f2[6]) -
    f1[5]*(f2[0]*f2[7] - f2[1]*f2[6]) +
    f1[6]*(f2[1]*f2[5] - f2[2]*f2[4]) -
    f1[7]*(f2[0]*f2[5] - f2[2]*f2[3]) +
    f1[8]*(f2[0]*f2[4] - f2[1]*f2[3]);
    
    t0 = f1[4]*f1[8] - f1[5]*f1[7];
    t1 = f1[3]*f1[8] - f1[5]*f1[6];
    t2 = f1[3]*f1[7] - f1[4]*f1[6];
    
    c[1] = f2[0]*t0 - f2[1]*t1 + f2[2]*t2 -
    f2[3]*(f1[1]*f1[8] - f1[2]*f1[7]) +
    f2[4]*(f1[0]*f1[8] - f1[2]*f1[6]) -
    f2[5]*(f1[0]*f1[7] - f1[1]*f1[6]) +
    f2[6]*(f1[1]*f1[5] - f1[2]*f1[4]) -
    f2[7]*(f1[0]*f1[5] - f1[2]*f1[3]) +
    f2[8]*(f1[0]*f1[4] - f1[1]*f1[3]);
    
    c[0] = f1[0]*t0 - f1[1]*t1 + f1[2]*t2;
    
    // solve the cubic equation; there can be 1 to 3 roots ...
    n = solveCubic( coeffs, roots );
    
    if( n < 1 || n > 3 )
        return n;
    
    for( k = 0; k < n; k++, fmatrix += 9 )
    {
        // for each root form the fundamental matrix
        double lambda = r[k], mu = 1.;
        double s = f1[8]*r[k] + f2[8];
        
        // normalize each matrix, so that F(3,3) (~fmatrix[8]) == 1
        if( fabs(s) > DBL_EPSILON )
        {
            mu = 1./s;
            lambda *= mu;
            fmatrix[8] = 1.;
        }
        else
            fmatrix[8] = 0.;
        
        for( i = 0; i < 8; i++ )
            fmatrix[i] = f1[i]*lambda + f2[i]*mu;
    }
    return n;
}
bool estimateRotationTranslationRANSAC2(
                                        const std::vector<Eigen::Vector3d> &pts1,
                                       const std::vector<Eigen::Vector3d> &pts2,
                                       std::vector<int>& bestInliers,
                                       Eigen::Matrix3d& rotation,
                                       Eigen::Vector3d& translation,
                                       int   maxIterations=200,
                                       const double lossThreshold=0.005,
                                       const double inlierThreshold=0.01){
    modelPoints=7;
    cv::RNG rng((uint64)-1);
    int count=pts1.size();
    int niters = maxIterations;
    
    
    bestInliers.clear();
    Eigen::Matrix3d bestRotation;
    Eigen::Vector3d bestTranslation;
    
    /*for (int i=0;i<pts1.size();i++) {
     std::cout<<pts1[i].transpose()<<std::endl;
     }*/
    
    for(int iter = 0; iter < niters; iter++){
        
        std::vector<Eigen::Vector3d> spts1,spts2;
        bool found = getSubset(rng,pts1,pts2,spts1,spts2);
        if(!found ){
            if( iter == 0 )
                return false;
            break;
        }
        
        /*for (int i=0;i<spts1.size();i++) {
         std::cout<<"subset "<<spts1[i].transpose()<<std::endl;
         }
         getchar();*/
        
        
        //Eigen::Matrix3d rotation;
        //Eigen::Vector3d translation;
        //estimateRotationTranslation(spts1,spts2,rotation,translation);
        
        //std::cout<<"rotation 1"<<rotation.transpose()<<std::endl;
        //getchar();
        /*spts1.clear();
         spts2.clear();
         
         for (int i=0;i<testinliers.size();i++) {
         spts1.push_back(pts1[testinliers[i]]);
         spts2.push_back(pts2[testinliers[i]]);
         }
         estimateRotationTranslation(spts1,spts2,rotation,translation);
         
         std::cout<<"rotation 2"<<rotation<<std::endl;
         
         
         std::cout<<rotation<<std::endl;*/
        cv::Mat _m1(modelPoints,3,CV_64FC1),_m2(modelPoints,3,CV_64FC1),_fmatrix(9,3,CV_64FC1);
        cv::Point3d* m1 = _m1.ptr<cv::Point3d>();
        cv::Point3d* m2 = _m2.ptr<cv::Point3d>();
        
        for(int s=0;s<modelPoints;s++){
            
            m1[s].x=spts1[s](0);
            m1[s].y=spts1[s](1);
            m1[s].z=spts1[s](2);
            
            m2[s].x=spts2[s](0);
            m2[s].y=spts2[s](1);
            m2[s].z=spts2[s](2);
        }

        int N=run7Point(_m1,_m2,_fmatrix);
        if(N < 1 || N > 3){
            continue;
        }
        
        
        std::vector<Eigen::Matrix3d> F(N);
        for(int n=0;n<N;n++){
            for(int j1=0;j1<3;j1++){
                for(int j2=0;j2<3;j2++){
                    F[n](j1,j2)=_fmatrix.at<double>(3*n+j1,j2);
                }
            }
        }
        
        for(int n=0;n<N;n++){
            
            std::vector<int> inliers(0);
            for (int j= 0; j< pts1.size(); j++){
                Eigen::Vector3d planeVector=F[n]*pts1[j];
                double distance=pts2[j].transpose()*planeVector;
                distance=std::abs(distance)/planeVector.norm();
                if (distance<inlierThreshold){
                    inliers.push_back(j);
                }
            }
            
            //int goodCount=inliers.size();
            //std::cout<<"good count"<<goodCount<<std::endl;
            if( inliers.size() > MAX(bestInliers.size(), modelPoints-1) ){
                bestInliers=inliers;
                bestRotation=rotation;
                bestTranslation=translation;
                niters = RANSACUpdateNumIters(0.98, (double)(count-inliers.size())/count, modelPoints, niters );
            }
        }
        //std::vector<int> inliers;
        //findInliers(pts1,pts2,rotation,translation,inliers,inlierThreshold);
    }
    
    //std::cout<<"rotation 1"<<bestRotation.transpose()<<std::endl;
    //getchar();
    
    //getchar();
    //std::cout<<"final iter"<<niters<<std::endl;
    std::vector<Eigen::Vector3d> inliers1(bestInliers.size());
    std::vector<Eigen::Vector3d> inliers2(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    
    
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation);
    findInliers(pts1,pts2,bestRotation,bestTranslation,bestInliers,inlierThreshold);
    inliers1.resize(bestInliers.size());
    inliers2.resize(bestInliers.size());
    for (int i=0;i<bestInliers.size();i++) {
        inliers1[i]=pts1[bestInliers[i]];
        inliers2[i]=pts2[bestInliers[i]];
    }
    //std::cout<<"good count"<<bestInliers.size()<<std::endl;
    ceres::LossFunction* loss=new ceres::HuberLoss(lossThreshold);
    estimateRotationTranslation(inliers1,inliers2,bestRotation,bestTranslation,loss,true);
    
    rotation=bestRotation;
    translation=bestTranslation;
    return true;
}



void _detectOutliers(const std::vector<Eigen::Vector3d> &pts1,
                     const std::vector<Eigen::Vector3d> &pts2,
                     std::vector<int>                   &inliers,
                     const int maxIterations,
                     const double threshold){
    inliers.clear();
    struct timeval tic;
    gettimeofday(&tic,0);
    srand (tic.tv_usec );
    
    const int sampleSize=7;
    
    for (int i=0;i<maxIterations;i++) {
        
        cv::Mat _m1(7,3,CV_64FC1),_m2(7,3,CV_64FC1),_fmatrix(9,3,CV_64FC1);
        
        cv::Point3d* m1 = _m1.ptr<cv::Point3d>();
        cv::Point3d* m2 = _m2.ptr<cv::Point3d>();
        
        for(int s=0;s<sampleSize;s++){
            
            int index=rand()%pts1.size();
            
            m1[s].x=pts1[index](0);
            m1[s].y=pts1[index](1);
            m1[s].z=pts1[index](2);
            
            m2[s].x=pts2[index](0);
            m2[s].y=pts2[index](1);
            m2[s].z=pts2[index](2);
        }
        
        int N=run7Point(_m1,_m2,_fmatrix);
        
        if(N < 1 || N > 3){
            continue;
        }
        
        
        std::vector<Eigen::Matrix3d> F(N);
        for(int n=0;n<N;n++){
            for(int j1=0;j1<3;j1++){
                for(int j2=0;j2<3;j2++){
                    F[n](j1,j2)=_fmatrix.at<double>(3*n+j1,j2);
                }
            }
        }
        
        for(int n=0;n<N;n++){
            std::vector<int> _inliers;
            for (int j= 0; j< pts1.size(); j++){
                Eigen::Vector3d planeVector=F[n]*pts1[j];
                double distance=pts2[j].transpose()*planeVector;
                distance=std::abs(distance)/planeVector.norm();
                if (distance<threshold){
                    _inliers.push_back(j);
                }
            }
            
            if (_inliers.size()>inliers.size()){
                inliers=_inliers;
            }
        }
    }
}

int main( int argc, char** argv )
{
    // initialize random seed
    
    
    //set experiment parameters
    //std::vector<double> errorsx;
    //std::vector<double> errorsy;
    cv::RNG rng(-1);
    for (double noise=2.0;noise<=2.0;noise+=1.0) {
    //for(double outlierFraction=0.1;outlierFraction<0.2;outlierFraction+=0.01){
        //double noise=1.0;
        std::vector<double> suberrorsx(0);
        std::vector<double> suberrorsy(0);
        std::vector<double> suberrorsz(0);
        
        
        for (int iter=0;iter<=1000;iter++) {
            
            bearingVectors_t bearingVectors1;
            bearingVectors_t bearingVectors2;
            
            
            initializeRandomSeed();
            double outlierFraction = 0.5;
            size_t numberPoints = 100;
            
            //generate a random pose for viewpoint 1
            translation_t position1 = Eigen::Vector3d::Zero();
            rotation_t rotation1 = Eigen::Matrix3d::Identity();
            
            //generate a random pose for viewpoint 2
            double length=rng.uniform((double)1,(double)2);
            translation_t position2 = generateRandomDirectionTranslation(length);
            
            /*do {
                position2 = generateRandomDirectionTranslation(length);
                
            } while (
                     ( position2.dot(translation_t(0, 0, 1)) > .9 ) ||
                     ( abs(position2(2)) > 0.7 ) ||
                     ( abs(position2(2)) > abs(position2(0)) ) ||
                     ( abs(position2(2)) > abs(position2(1)) )
            );*/
            
            //std::cout<<length<<' '<<position2.transpose()<<std::endl;
            rotation_t rotation2 = generateRandomRotation(0.5);
            
            //create a fake central camera
            translations_t camOffsets;
            rotations_t camRotations;
            generateCentralCameraSystem( camOffsets, camRotations );
            
            //derive correspondences based on random point-cloud
            
            std::vector<int> camCorrespondences1; //unused in the central case
            std::vector<int> camCorrespondences2; //unused in the central case
            Eigen::MatrixXd gt(3,numberPoints);
            generateRandom2D2DCorrespondences(position1, rotation1, position2, rotation2,
                                              camOffsets, camRotations, numberPoints, noise, outlierFraction,
                                              bearingVectors1, bearingVectors2,
                                              camCorrespondences1, camCorrespondences2, gt );
            
            //Extract the relative pose
            translation_t position; rotation_t rotation;
            extractRelativePose(position1, position2, rotation1, rotation2, position, rotation );
            
            //std::cout<<"gt r"<<rotation<<std::endl;
            
            //print experiment characteristics
            //printExperimentCharacteristics( position, rotation, noise, outlierFraction );
            
            //compute and print the essential-matrix
            //printEssentialMatrix( position, rotation );
            
            Eigen::Matrix3d rotationx=Eigen::Matrix3d::Identity();
            Eigen::Vector3d translationx=Eigen::Vector3d::Zero();
            
            /*cv::RNG rng(-1);
             translationx(0)=rng.uniform(-0.01,0.01);
             translationx(1)=rng.uniform(-0.01,0.01);
             translationx(2)=rng.uniform(-0.01,0.01);*/
            
            
           
            std::vector<Eigen::Vector3d> inliers1,inliers2;
            std::vector<Eigen::Vector3d> pts1,pts2;
            for (int i=0;i<bearingVectors1.size();i++) {
                pts1.push_back(bearingVectors1[i]);
                pts2.push_back(bearingVectors2[i]);
            }
            std::vector<int> inliers;
            
            
            /*_detectOutliers(pts1,
                            pts2,
                            inliers,200,0.005);
            
            for (int i=0;i<inliers.size();i++) {
                inliers1.push_back(pts1[inliers[i]]);
                inliers2.push_back(pts2[inliers[i]]);
            }*/
            
            
            //inliers1=pts1;
            //inliers2=pts2;
            
            //estimateRotationTranslation(inliers1,inliers2,rotationx,translationx);
            //std::cout<<"detected"<<inliers.size()<<std::endl;
            //std::vector<int> inliers;
            estimateRotationTranslationRANSAC2(pts1,pts2,inliers,rotationx,translationx);
            //rotationx.transposeInPlace();
            //estimateRotationTranslationRANSAC3(pts1,pts2,rotationx,translationx);
            
            
            relative_pose::CentralRelativeAdapter adapter(bearingVectors1,
                                                          bearingVectors2);
            
            sac::Ransac<sac_problems::relative_pose::CentralRelativePoseSacProblem> ransac;
            std::shared_ptr<sac_problems::relative_pose::CentralRelativePoseSacProblem>
            relposeproblem_ptr(new sac_problems::relative_pose::CentralRelativePoseSacProblem(adapter,sac_problems::relative_pose::CentralRelativePoseSacProblem::NISTER));
            
            ransac.sac_model_ = relposeproblem_ptr;
            ransac.threshold_ = 10.0*(1.0 - cos(atan(sqrt(2.0)*0.5/800.0)));
            ransac.max_iterations_ = 200;
            ransac.computeModel();
            
            Eigen::Vector3d translationz=ransac.model_coefficients_.col(3);
            translationz.normalize();
            
            Eigen::Matrix3d rotationz=ransac.model_coefficients_.block<3,3>(0,0);
            
            sac_problems::relative_pose::CentralRelativePoseSacProblem::model_t optimizedModel;
            relposeproblem_ptr->optimizeModelCoefficients(ransac.inliers_,
                                                          ransac.model_coefficients_,
                                                          optimizedModel);
            
            /*sac::Ransac<
             sac_problems::relative_pose::EigensolverSacProblem> ransac;
             std::shared_ptr<
             sac_problems::relative_pose::EigensolverSacProblem> eigenproblem_ptr(new sac_problems::relative_pose::EigensolverSacProblem(adapter,10));
             
             ransac.sac_model_ = eigenproblem_ptr;
             ransac.threshold_ = 1.0;
             ransac.max_iterations_ = 100;
             
             //Run the experiment
             struct timeval tic;
             struct timeval toc;
             gettimeofday( &tic, 0 );
             ransac.computeModel();
             gettimeofday( &toc, 0 );
             double ransac_time = TIMETODOUBLE(timeval_minus(toc,tic));
             
             //do final polishing of the model over all inliers
             sac_problems::relative_pose::EigensolverSacProblem::model_t optimizedModel;
             eigenproblem_ptr->optimizeModelCoefficients(ransac.inliers_,
             ransac.model_coefficients_,
             optimizedModel);*/
            Eigen::Matrix3d rotationy=ransac.model_coefficients_.block<3,3>(0,0);
            
            
            Eigen::Matrix3d diffx=rotationx*rotation;
            Eigen::Matrix3d diffy=rotationy.transpose()*rotation;
            Eigen::Matrix3d diffz=rotationz.transpose()*rotation;
            
            
            Eigen::Vector3d anglex,angley,anglez;
            ceres::RotationMatrixToAngleAxis(diffx.data(),anglex.data());
            ceres::RotationMatrixToAngleAxis(diffy.data(),angley.data());
            ceres::RotationMatrixToAngleAxis(diffz.data(),anglez.data());
            
            
            suberrorsx.push_back(anglex.norm());
            suberrorsy.push_back(angley.norm());
            suberrorsz.push_back(anglez.norm());
            
            //std::cout<<rotationy<<std::endl;
            //std::cout<<rotationz<<std::endl;
            //getchar();
            
            /*
            //std::cout<<translationx<<std::endl<<position2;
            
            Eigen::Vector3d translationy=ransac.model_coefficients_.col(3);
            translationy.normalize();
            
            Eigen::Vector3d translation=position2-position1;
            translation.normalize();
            
            translationx.normalize();
            translationx=rotationx.transpose()*translationx;
             
             //std::cout<<translation<<std::endl;
             //std::cout<<translationy<<std::endl;
             //std::cout<<translationx<<std::endl;
             //getchar();
             
            suberrorsx.push_back(MIN(std::acos(translationx.dot(translation)),
                                      std::acos(translationx.dot(-translation))));
             
            suberrorsy.push_back(MIN(std::acos(translationy.dot(translation)),
                                     std::acos(translationy.dot(-translation))));
            
            suberrorsz.push_back(MIN(std::acos(translationz.dot(translation)),
                                     std::acos(translationz.dot(-translation))));
            
            /*std::cout<<"*********"<<std::endl;
             std::cout<<rotation<<std::endl;
             std::cout<<std::endl;
             std::cout<<rotationx.transpose()<<std::endl;
             std::cout<<std::endl;
             std::cout<<rotationy<<std::endl;
             std::cout<<"*********"<<std::endl;
             
             getchar();*/
            
        }
        
        std::sort(suberrorsx.begin(),suberrorsx.end());
        std::sort(suberrorsy.begin(),suberrorsy.end());
        std::sort(suberrorsz.begin(),suberrorsz.end());
        
        
        std::cout<<"median:"<<suberrorsx[suberrorsx.size()/2]<<' '
                            <<suberrorsy[suberrorsx.size()/2]<<' '
                            <<suberrorsz[suberrorsx.size()/2]<<std::endl;
        
        
        
        std::cout<<"max:"<<suberrorsx.back()<<' '
                         <<suberrorsy.back()<<' '
                         <<suberrorsz.back()<<std::endl;

        double avgx=0.0,avgy=0.0;
        for (int i=0;i<1000;i++) {
            avgx+=suberrorsx[i];
            avgy+=suberrorsy[i];
        }
        std::cout<<"mean:"<<avgx/1000<<' '
                          <<avgy/1000<<std::endl;
        
        /*for(int i=0;i<suberrorsx.size();i++){
         std::cout<<suberrorsx[i]<<std::endl;
         }
         getchar();*/
        
        //getchar();
    }
    
    /*for (int i=0;i<errorsx.size();i++) {
     std::cout<<errorsx[i]<<' '<<errorsy[i]<<std::endl;
     }*/
    
    return 0;
}
