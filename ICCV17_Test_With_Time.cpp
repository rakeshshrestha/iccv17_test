#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <limits.h>
#include <Eigen/Eigen>
#include <opengv/relative_pose/methods.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/relative_pose/CentralRelativePoseSacProblem.hpp>
#include <opengv/relative_pose/NoncentralRelativeMultiAdapter.hpp>
#include <opengv/sac/MultiRansac.hpp>
#include <opengv/sac_problems/relative_pose/MultiNoncentralRelativePoseSacProblem.hpp>
#include <opengv/sac_problems/relative_pose/EigensolverSacProblem.hpp>
#include <sstream>
#include <fstream>

#include <iccv17_test/config/config.hpp>

#include <iccv17_test/relative_motion/relative_pose_estimation.hpp>
#include <iccv17_test/utils/random_generators.hpp>
#include <iccv17_test/utils/experiment_helpers.hpp>
#include <iccv17_test/utils/time_measurement.hpp>

#include <iccv17_test/helpers/opengv_helpers.hpp>
#include <iccv17_test/helpers/misc_helpers.hpp>

#include "ceres/rotation.h"
#include "ceres/ceres.h"

using namespace std;
using namespace Eigen;
using namespace opengv;

#define NO_OF_ALGORITHMS  6
const char arrAlgorithms[NO_OF_ALGORITHMS][20] = {
    "preSolve",
    "our",
    "stew",
    "nister",
    "sevenpt",
    "eightpt"
};

/*
 * Wrapper for each algorithms
 */
void solveAlgorithm(    std::string algorithmName,
                        bearingVectors_t bearingVectors1,
                        bearingVectors_t bearingVectors2,
                        rotation_t &rotationOut,
                        translation_t &translationOut,
                        double &timeRequiredOut
                    );
int main( int argc, char** argv )
{
    std::fstream metaInfo("meta.in", std::fstream::out);
    metaInfo    << NO_OF_ITERATIONS << std::endl
                << NUMBER_OF_POINTS_START << " "
                << NUMBER_OF_POINTS_END << " "
                << NUMBER_OF_POINTS_STEP << std::endl
                << NOISE_START << " "
                << NOISE_END << " "
                << NOISE_STEP;
    metaInfo.close();

    for (size_t numberPoints = NUMBER_OF_POINTS_START; numberPoints <= NUMBER_OF_POINTS_END; numberPoints+=NUMBER_OF_POINTS_STEP) {
        // debug
        std::cout << std::endl << "Num Points: " << numberPoints;

        // --------------------- Output Files --------------------- //
        std::stringstream errorOutputFileName;
        errorOutputFileName << numberPoints << "_time.out";
        std::fstream errorOutputFile(errorOutputFileName.str(), std::fstream::out);

        std::stringstream bearingVectorsFileName;
        bearingVectorsFileName << numberPoints << "_bearing.in";
        std::fstream bearingVectorsFile(bearingVectorsFileName.str(), std::fstream::out);
        
        std::stringstream groundTruthFileName;
        groundTruthFileName << numberPoints << "_ground_truth1.in";
        std::fstream groundTruthFile(groundTruthFileName.str(), std::fstream::out);

        std::fstream arrAlgorithmOutputFile[NO_OF_ALGORITHMS];
        for (int algorithm_idx = 0; algorithm_idx < NO_OF_ALGORITHMS; algorithm_idx++) {
            std::stringstream algorithmOutputFileName;
            algorithmOutputFileName << numberPoints << "_" << arrAlgorithms[algorithm_idx] << "Output.out";
            arrAlgorithmOutputFile[algorithm_idx].open(algorithmOutputFileName.str(), std::fstream::out);
        }

        for (double noise=NOISE_START; noise<=NOISE_END;noise+=NOISE_STEP) {
            // debug
            std::cout << std::endl << "Noise: " << noise << std::endl;
            
            // store errors for each iterations of aggregate calculations
            std::vector<double> arrVectAngleError[NO_OF_ALGORITHMS];
            std::vector<double> arrVectTranslationError[NO_OF_ALGORITHMS];
            std::vector<double> arrVectTimeRequired[NO_OF_ALGORITHMS];

            for (int algorithm_idx = 0; algorithm_idx < NO_OF_ALGORITHMS; algorithm_idx++) {
                // reserve space for faster processing
                arrVectAngleError[algorithm_idx].reserve(NO_OF_ITERATIONS);
                arrVectTranslationError[algorithm_idx].reserve(NO_OF_ITERATIONS);
                arrVectTranslationError[algorithm_idx].reserve(NO_OF_ITERATIONS);
            }

            for (int iter=0;iter<NO_OF_ITERATIONS;iter++) {
                
                bearingVectors_t bearingVectors1;
                bearingVectors_t bearingVectors2;
                
                
                initializeRandomSeed();
                double outlierFraction = 0.0;
                            
                //generate a random pose for viewpoint 1
                translation_t position1 = Eigen::Vector3d::Zero();
                rotation_t rotation1 = Eigen::Matrix3d::Identity();
                
                //generate a random pose for viewpoint 2
                translation_t position2 = generateRandomDirectionTranslation(
                    iccv17::misc_helpers::getRandomBetween(MIN_TRANSLATION, MAX_TRANSLATION)
                );

                rotation_t rotation2 = generateRandomRotation(MAX_ROTATION);
                
                //create a fake central camera
                translations_t camOffsets;
                rotations_t camRotations;
                generateCentralCameraSystem( camOffsets, camRotations );
                
                //derive correspondences based on random point-cloud
                
                std::vector<int> camCorrespondences1; //unused in the central case
                std::vector<int> camCorrespondences2; //unused in the central case
                Eigen::MatrixXd gt(3,numberPoints);
                generateRandom2D2DCorrespondences(position1, rotation1, position2, rotation2,
                                                  camOffsets, camRotations, numberPoints, noise, outlierFraction,
                                                  bearingVectors1, bearingVectors2,
                                                  camCorrespondences1, camCorrespondences2, gt );
                //Extract the relative pose
                translation_t position_gt; rotation_t rotation_gt;
                extractRelativePose(
                                    position1, position2, rotation1, rotation2, position_gt, rotation_gt );
                Eigen::Vector3d translation_gt = position2-position1;

                // -------------------------- Save the bearing vectors and ground truth -------------------------- //
                for (   int bearing_idx = 0, bearing_len = bearingVectors1.size(); 
                        bearing_idx < bearing_len;
                        bearing_idx++   ) {
                    
                    bearingVectorsFile  << bearingVectors1[bearing_idx](0) << " "
                                        << bearingVectors1[bearing_idx](1) << " "
                                        << bearingVectors1[bearing_idx](2) << " ";

                    bearingVectorsFile  << bearingVectors2[bearing_idx](0) << " "
                                        << bearingVectors2[bearing_idx](1) << " "
                                        << bearingVectors2[bearing_idx](2) << std::endl;                                        
                }

                iccv17::misc_helpers::outputRotationTranslation(
                    groundTruthFile, 
                    rotation_gt, translation_gt
                );

                // ------------------------------- calculate error from different algorithm ------------------------------- //
                for (unsigned int algorithm_idx = 0; algorithm_idx < NO_OF_ALGORITHMS; algorithm_idx++) {
                    // debug
                    std::cout << "\r" << iter << ": Using " << arrAlgorithms[algorithm_idx] << " algorithm";    

                    double errorAngle, errorTranslation;
                    double timeRequired = 0;

                    Eigen::Matrix3d rotation_algorithm = Eigen::Matrix3d::Identity();
                    Eigen::Vector3d translation_algorithm = Eigen::Vector3d::Zero();

                    solveAlgorithm(
                        std::string(arrAlgorithms[algorithm_idx]),
                        bearingVectors1,
                        bearingVectors2,
                        rotation_algorithm,
                        translation_algorithm,
                        timeRequired
                    );

                    iccv17::misc_helpers::calculateError( 
                        rotation_gt,
                        translation_gt,
                        rotation_algorithm,
                        translation_algorithm,
                        errorAngle,
                        errorTranslation 
                    );

                    arrVectAngleError[algorithm_idx].push_back(errorAngle);
                    arrVectTranslationError[algorithm_idx].push_back(errorTranslation);
                    arrVectTimeRequired[algorithm_idx].push_back(timeRequired);

                    // ------------------------ Output the calculated pose for each algorithm ------------------------ //
                    iccv17::misc_helpers::outputRotationTranslation(
                          arrAlgorithmOutputFile[algorithm_idx],
                          rotation_algorithm, translation_algorithm                                                                  
                    );
                }
                
            } // iter loop ends

            for (int algorithm_idx = 0; algorithm_idx < NO_OF_ALGORITHMS; algorithm_idx++) {
                // sort the vectors
                std::sort(arrVectAngleError[algorithm_idx].begin(),arrVectAngleError[algorithm_idx].end());
                std::sort(arrVectTranslationError[algorithm_idx].begin(),arrVectTranslationError[algorithm_idx].end());    
                std::sort(arrVectTimeRequired[algorithm_idx].begin(),arrVectTimeRequired[algorithm_idx].end());    

                // -------------- File out ------------------ //
                errorOutputFile
                        // rotation errors
                        << arrVectAngleError[algorithm_idx][NO_OF_ITERATIONS/2] << " " 
                        << arrVectAngleError[algorithm_idx][NO_OF_ITERATIONS-1] << " " 
                        << iccv17::misc_helpers::vectAverage(arrVectAngleError[algorithm_idx]) << " "
                        // translation errors
                        << arrVectTranslationError[algorithm_idx][NO_OF_ITERATIONS/2] << " " 
                        << arrVectTranslationError[algorithm_idx][NO_OF_ITERATIONS-1] << " "
                        << iccv17::misc_helpers::vectAverage(arrVectTranslationError[algorithm_idx]) << " "
                        // time required
                        << arrVectTimeRequired[algorithm_idx][NO_OF_ITERATIONS/2] << " "
                        << arrVectTimeRequired[algorithm_idx][NO_OF_ITERATIONS-1] << " "
                        << iccv17::misc_helpers::vectAverage(arrVectTimeRequired[algorithm_idx])
                        << std::endl; 
            }
        } // noise loop ends
        
        // close the output files
        for (int algorithm_idx = 0; algorithm_idx < NO_OF_ALGORITHMS; algorithm_idx++) {
            arrAlgorithmOutputFile[algorithm_idx].close();
        }
        bearingVectorsFile.close();
        groundTruthFile.close();

    } // numberPoints loop ends
    // debug
    std::cout << std::endl;
    
    return 0;
}

/*
 * Wrapper for each algorithms
 */
void solveAlgorithm(    std::string algorithmName,
                        bearingVectors_t bearingVectors1,
                        bearingVectors_t bearingVectors2,
                        rotation_t &rotationOut,
                        translation_t &translationOut,
                        double &timeRequiredOut
                    )
{

    if ( algorithmName.compare(std::string("preSolve")) == 0 ) {
        iccv17::relative_pose_estimation::preSolve(
                bearingVectors1,
                bearingVectors2,
                rotationOut,
                translationOut,
                timeRequiredOut 
        );

        // for custom algorithm, the transformation is from frame 1 to frame 2
        // convert to frame 2 to frame 1
        rotationOut.transposeInPlace();
        translationOut = -rotationOut * translationOut;
        // to make scale consistent with other methods
        translationOut.normalize();
        translationOut /= sqrt(2);
        
    } else if ( algorithmName.compare(std::string("our")) == 0 ) {
        iccv17::relative_pose_estimation::estimateRotationTranslation(
            0.0,
            bearingVectors1,
            bearingVectors2,
            rotationOut,
            translationOut,
            timeRequiredOut
        );

        // for custom algorithm, the transformation is from frame 1 to frame 2
        // convert to frame 2 to frame 1
        rotationOut.transposeInPlace();
        translationOut = -rotationOut * translationOut;
        // to make scale consistent with other methods
        translationOut.normalize();
        translationOut /= sqrt(2);
        
    } else if ( algorithmName.compare(std::string("stew")) == 0 ) {
        iccv17::opengv_helpers::solveCentralSacProblemWithoutOptimization(
            bearingVectors1, 
            bearingVectors2,
            rotationOut,
            translationOut,
            timeRequiredOut,
            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::STEWENIUS
        );
    } else if ( algorithmName.compare(std::string("nister")) == 0 ) {
        iccv17::opengv_helpers::solveCentralSacProblemWithoutOptimization(
            bearingVectors1, 
            bearingVectors2,
            rotationOut,
            translationOut,
            timeRequiredOut,
            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::NISTER
        );
    } else if ( algorithmName.compare(std::string("sevenpt")) == 0 ) {
        iccv17::opengv_helpers::solveCentralSacProblemWithoutOptimization(
            bearingVectors1, 
            bearingVectors2,
            rotationOut,
            translationOut,
            timeRequiredOut,
            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::SEVENPT
        );
    } else if ( algorithmName.compare(std::string("eightpt")) == 0 ) {
        iccv17::opengv_helpers::solveCentralSacProblemWithoutOptimization(
            bearingVectors1, 
            bearingVectors2,
            rotationOut,
            translationOut,
            timeRequiredOut,
            opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem::EIGHTPT
        );
    } else {
        std::cerr << "Unrecognized algorithm: " << algorithmName << std::endl;
        exit(1);
    }
}