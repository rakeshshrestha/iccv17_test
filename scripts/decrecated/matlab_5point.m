function [] = matlab_5point(range_of_points, path_prefix, noise_levels)
    % addpath('/localhome/rakeshs/workspace/rakesh/iccv17_test/Matlab');
    addpath('/home/rakesh/workspace/rakesh/5point/ICCV17_Test/Matlab')

    % NUMBER OF ITERATION FOR EACH NOISE LEVEL
    NUM_ITERATIONS = 1000;
    
    for number_of_points=range_of_points
        disp(['no. of point' num2str(number_of_points)]);
        [~, ~, ~, ~, estimates, inliers] = solveFromBundler(NUM_ITERATIONS, number_of_points, noise_levels, path_prefix);
        dlmwrite( [num2str(number_of_points) '_matlab5ptOutput.out'], estimates,  ' ')

        % output the inliers
        inliers_file_id = fopen([num2str(number_of_points) '_matlab5ptInliers.out'], 'w');
        for row_idx = 1:length(inliers)
        	for col_idx = 1:length(inliers{row_idx}) 
        		fprintf(inliers_file_id, '%d ', inliers{row_idx}(col_idx));
        	end
        	fprintf(inliers_file_id, '\n');
        end
        fclose(inliers_file_id);
    end
end