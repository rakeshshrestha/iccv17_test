function [] = plot_cpp_vs_matlab(number_of_points, path_prefix, save_prefix, noise_levels)
    addpath('/localhome/rakeshs/workspace/rakesh/iccv17_test/Matlab');

    % NUMBER OF ITERATION FOR EACH NOISE LEVEL
    NUM_ITERATIONS = 200;
    NUM_ALGORITHMS = 14;

    algorithms = cell(NUM_ALGORITHMS, 2);
    algorithms{1, 1} = 'our';                   algorithms{1, 2} = 'r';
    algorithms{2, 1} = 'random optimized';      algorithms{2, 2} = 'm';
    algorithms{3, 1} = 'presolve';              algorithms{3, 2} = 'k';
    
    algorithms{4, 1} = 'stew';                  algorithms{4, 2} = 'g';
    algorithms{5, 1} = 'nister';                algorithms{5, 2} = 'b';
    algorithms{6, 1} = '7pt';                   algorithms{6, 2} = 'm';
    algorithms{7, 1} = '8pt';                   algorithms{7, 2} = 'c';
    algorithms{8, 1} = 'eigen';                 algorithms{8, 2} = 'g';
    
    algorithms{9, 1} = 'presolve optimized';    algorithms{9, 2} = 'k--';
    algorithms{10, 1} = 'stew optimized';       algorithms{10, 2} = 'g';
    algorithms{11, 1} = 'nister optimized';     algorithms{11, 2} = 'b';
    algorithms{12, 1} = '7pt optimized';        algorithms{12, 2} = 'm';
    algorithms{13, 1} = '8pt optimized';        algorithms{13, 2} = 'c';
    algorithms{14, 1} = 'eigen optimized';      algorithms{14, 2} = 'g--';

    bundler_color = 'k';

    high_error_algorithms = [1, 2, 3, 9];%, 8, 14];
    ransac_algorithms = [1, 4, 5, 6, 7];
    optimized_algorithms = [1, 10, 11, 12, 13];
    
    M = dlmread( [path_prefix num2str(number_of_points) '_time.out'] );
    M = M * 180 / pi;

    num_noise = size(M);
    num_noise = num_noise / NUM_ALGORITHMS;
    % assert(length(noise_levels) == num_noise)

    [R_errors_median, R_errors_max, t_errors_median, t_errors_max] = solveFromBundler(NUM_ITERATIONS, number_of_points, noise_levels, path_prefix);


    % --------------------------- Max Rotation Error ----------------------------- %
    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Max rotation error group 1 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = high_error_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 2), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(high_error_algorithms, 1), 'un', 0)]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'max_rotation_group1'));

    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Max rotation error group 2 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = ransac_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 2), algorithms{i, 2});
    end
    plot(noise_levels, R_errors_max, bundler_color);
    legend([...
        cellfun(@(x) strcat(x, ' max'), algorithms(ransac_algorithms, 1), 'un', 0); ...
        '5 point Matlab max'
    ]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'max_rotation_group2'));

    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Max rotation error group 3 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = optimized_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 2), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(optimized_algorithms, 1), 'un', 0)]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'max_rotation_group3'));
    
    % --------------------------- Median Rotation Error ----------------------------- %
    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Median rotation error group 1 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = high_error_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 1), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(high_error_algorithms, 1), 'un', 0)]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'median_rotation_group1'));

    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Median rotation error group 2 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = ransac_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 1), algorithms{i, 2});
    end
    plot(noise_levels, R_errors_median, bundler_color);
    legend([...
        cellfun(@(x) strcat(x, ' median'), algorithms(ransac_algorithms, 1), 'un', 0); ...
        '5 point Matlab median'
    ]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'median_rotation_group2'));

    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Median rotation error group 3 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = optimized_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 1), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(optimized_algorithms, 1), 'un', 0)]);
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'median_rotation_group3'));
    
    % --------------------------- Max Translation Error ----------------------------- %
    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Max translation error group 1 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = high_error_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 4), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(high_error_algorithms, 1), 'un', 0)]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'max_translation_group1'));

    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Max translation error group 2 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = ransac_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 4), algorithms{i, 2});
    end
    plot(noise_levels, t_errors_max, bundler_color);
    legend([...
        cellfun(@(x) strcat(x, ' max'), algorithms(ransac_algorithms, 1), 'un', 0); ...
        '5 point Matlab max'
    ]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'max_translation_group2'));

    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Max translation error group 3 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = optimized_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 4), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(optimized_algorithms, 1), 'un', 0)]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'max_translation_group3'));

    % --------------------------- Median Translation Error ----------------------------- %
    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Median translation error group 1 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = high_error_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 3), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(high_error_algorithms, 1), 'un', 0)]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'median_translation_group1'));

    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Median translation error group 2 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = ransac_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 3), algorithms{i, 2});
    end
    plot(noise_levels, t_errors_median, bundler_color);
    legend([...
        cellfun(@(x) strcat(x, ' median'), algorithms(ransac_algorithms, 1), 'un', 0); ...
        '5 point Matlab median'
    ]); 
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'median_translation_group2'));

    fig_handle = figure('Position',get(0,'ScreenSize'));
    title(['Median translation error group 3 (' num2str(number_of_points) ' points)']);
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [degree]')
    hold on
    grid on
    
    for i = optimized_algorithms
        plot(noise_levels, M(i:NUM_ALGORITHMS:end, 3), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(optimized_algorithms, 1), 'un', 0)]);
    pause(0.01) %in seconds
    saveFigureCustom(fig_handle, strcat(num2str(save_prefix), 'median_translation_group3'));


    % saveFigureCustom(max_error_plot_handle, strcat(num2str(save_prefix), 'max'));
    %saveFigureCustom(median_error_plot_handle, strcat(num2str(save_prefix), 'median'));

    return
    %---------------------------- Time required -------------------------------%
    % % set(gcf,'Visible','off');
    % fig_handle = figure('Position',get(0,'ScreenSize'))
    % xlabel('noise [pix]')
    % ylabel('time required (s)')
    % hold on
    % grid on

    % % ransac version
    % for i = 1:NUM_ALGORITHMS
    %   plot(noise_levels, M(i:row_skip:end, 7), algorithms{i, 2});
    % end

    % % optimized version
    % for i = (NUM_ALGORITHMS+1):row_skip
    %   plot(noise_levels, M(i:row_skip:end, 7), strcat(algorithms{i-NUM_ALGORITHMS+NON_OPTIMIZED_ALGORITHMS, 2}, '-.'));
    % end
    
    % % median translation
    
    % % ransac version
    % for i = 1:NUM_ALGORITHMS
    %   plot(noise_levels, M(i:row_skip:end, 6), strcat(algorithms{i, 2}, '--'));
    % end

    % % optimized version
    % for i = (NUM_ALGORITHMS+1):row_skip
    %   plot(noise_levels, M(i:row_skip:end, 6), strcat(algorithms{i-NUM_ALGORITHMS+NON_OPTIMIZED_ALGORITHMS, 2}, ':'));
    % end
    
    % hold off
    % legend([cellfun(@(x) strcat(x, ' max'), algorithms(1:NUM_ALGORITHMS, 1), 'un', 0); 
    %       cellfun(@(x) strcat(x, ' optimized max'), algorithms((1+NON_OPTIMIZED_ALGORITHMS):end, 1), 'un', 0); 
    %       cellfun(@(x) strcat(x, ' median'), algorithms(1:NUM_ALGORITHMS, 1), 'un', 0)
    %       cellfun(@(x) strcat(x, ' optimized median'), algorithms((1+NON_OPTIMIZED_ALGORITHMS):end, 1), 'un', 0)]);
end

function [] = saveFigureCustom(fig_handle, filename)
    f=getframe(fig_handle);                                                         
    [X, map] = frame2im(f);                                                  
    imwrite(X,[filename '.jpg']);    
end