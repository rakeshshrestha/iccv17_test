function [] = plot_multiple_cpp_vs_matlab()
    % set(gcf,'Visible','off');
    noise_levels = [0:10];
    for number_of_points = [10:120]
        plot_cpp_vs_matlab(number_of_points, '../build/', number_of_points, noise_levels)
        %set(gcf,'units','normalized','outerposition',[0 0 1 1])
        disp(number_of_points)
        % waitforbuttonpress;
        close all
    end
end