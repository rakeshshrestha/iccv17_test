function [] = plot_multiple()
	% set(gcf,'Visible','off');
	for i = [150, 300]
		file = strcat( strcat('../results/pure_rotation/expt5/', num2str(i)), '_time.out' );
		plot_error_without_eigen(file, i);
		set(gcf,'units','normalized','outerposition',[0 0 1 1])
		disp(i)
		waitforbuttonpress;
		close all
	end
end