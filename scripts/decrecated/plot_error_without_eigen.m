function [] = plot_error_without_eigen(filename, save_prefix)
    NUM_ALGORITHMS = 14;

    algorithms = cell(NUM_ALGORITHMS, 2);
    algorithms{1, 1} = 'our';                   algorithms{1, 2} = 'r';
    algorithms{2, 1} = 'random optimized';      algorithms{2, 2} = 'm';
    algorithms{3, 1} = 'presolve';              algorithms{3, 2} = 'k';
    
    algorithms{4, 1} = 'stew';                  algorithms{4, 2} = 'g';
    algorithms{5, 1} = 'nister';                algorithms{5, 2} = 'b';
    algorithms{6, 1} = '7pt';                   algorithms{6, 2} = 'm';
    algorithms{7, 1} = '8pt';                   algorithms{7, 2} = 'c';
    algorithms{8, 1} = 'eigen';                 algorithms{8, 2} = 'g';
    
    algorithms{9, 1} = 'presolve optimized';    algorithms{9, 2} = 'k--';
    algorithms{10, 1} = 'stew optimized';       algorithms{10, 2} = 'g';
    algorithms{11, 1} = 'nister optimized';     algorithms{11, 2} = 'b';
    algorithms{12, 1} = '7pt optimized';        algorithms{12, 2} = 'm';
    algorithms{13, 1} = '8pt optimized';        algorithms{13, 2} = 'c';
    algorithms{14, 1} = 'eigen optimized';      algorithms{14, 2} = 'g--';

    high_error_algorithms = [1, 2, 3, 9];%, 8, 14];
    ransac_algorithms = [1, 4, 5, 6, 7];
    optimized_algorithms = [1, 10, 11, 12, 13];
    
    M = dlmread(filename);

    num_noise = size(M);
    num_noise = num_noise / NUM_ALGORITHMS;

    % Plot windows
    set(gcf,'Visible','off');
    max_rotation_error_plot_handle = figure('Position',get(0,'ScreenSize'));
    suptitle('Max rotation error');

    max_translation_error_plot_handle = figure('Position',get(0,'ScreenSize'));
    suptitle('Max translation error');

    median_rotation_error_plot_handle = figure('Position',get(0,'ScreenSize'));
    suptitle('Median rotation error');

    median_translation_error_plot_handle = figure('Position',get(0,'ScreenSize'));
    suptitle('Median translation error');

    % --------------------------- Max Rotation Error ----------------------------- %
    figure(max_rotation_error_plot_handle)
    
    subplot(1, 3, 1)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = high_error_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 2), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(high_error_algorithms, 1), 'un', 0)]); 

    subplot(1, 3, 2)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = ransac_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 2), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(ransac_algorithms, 1), 'un', 0)]); 

    subplot(1, 3, 3)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = optimized_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 2), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(optimized_algorithms, 1), 'un', 0)]); 
    
    % --------------------------- Median Rotation Error ----------------------------- %
    figure(median_rotation_error_plot_handle)
    
    subplot(1, 3, 1)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = high_error_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 1), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(high_error_algorithms, 1), 'un', 0)]); 

    subplot(1, 3, 2)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = ransac_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 1), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(ransac_algorithms, 1), 'un', 0)]); 

    subplot(1, 3, 3)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = optimized_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 1), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(optimized_algorithms, 1), 'un', 0)]);
    
    % --------------------------- Max Translation Error ----------------------------- %
    figure(max_translation_error_plot_handle)
    
    subplot(1, 3, 1)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = high_error_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 4), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(high_error_algorithms, 1), 'un', 0)]); 

    subplot(1, 3, 2)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = ransac_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 4), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(ransac_algorithms, 1), 'un', 0)]); 

    subplot(1, 3, 3)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = optimized_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 4), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' max'), algorithms(optimized_algorithms, 1), 'un', 0)]); 
    
    % --------------------------- Median Translation Error ----------------------------- %
    figure(median_translation_error_plot_handle)
    
    subplot(1, 3, 1)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = high_error_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 3), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(high_error_algorithms, 1), 'un', 0)]); 

    subplot(1, 3, 2)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = ransac_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 3), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(ransac_algorithms, 1), 'un', 0)]); 

    subplot(1, 3, 3)
    xlabel('noise [pix]')
    ylabel('rot. diff. norm [rad]')
    hold on
    grid on
    
    for i = optimized_algorithms
        plot(1:num_noise, M(i:NUM_ALGORITHMS:end, 3), algorithms{i, 2});
    end
    legend([cellfun(@(x) strcat(x, ' median'), algorithms(optimized_algorithms, 1), 'un', 0)]);

    % saveFigure(max_error_plot_handle, strcat(num2str(save_prefix), 'max'));
    % saveFigure(median_error_plot_handle, strcat(num2str(save_prefix), 'median'));

    return
    %---------------------------- Time required -------------------------------%
    % % set(gcf,'Visible','off');
    % figure('Position',get(0,'ScreenSize'))
    % xlabel('noise [pix]')
    % ylabel('time required (s)')
    % hold on
    % grid on

    % % ransac version
    % for i = 1:NUM_ALGORITHMS
    %   plot(1:num_noise, M(i:row_skip:end, 7), algorithms{i, 2});
    % end

    % % optimized version
    % for i = (NUM_ALGORITHMS+1):row_skip
    %   plot(1:num_noise, M(i:row_skip:end, 7), strcat(algorithms{i-NUM_ALGORITHMS+NON_OPTIMIZED_ALGORITHMS, 2}, '-.'));
    % end
    
    % % median translation
    
    % % ransac version
    % for i = 1:NUM_ALGORITHMS
    %   plot(1:num_noise, M(i:row_skip:end, 6), strcat(algorithms{i, 2}, '--'));
    % end

    % % optimized version
    % for i = (NUM_ALGORITHMS+1):row_skip
    %   plot(1:num_noise, M(i:row_skip:end, 6), strcat(algorithms{i-NUM_ALGORITHMS+NON_OPTIMIZED_ALGORITHMS, 2}, ':'));
    % end
    
    % hold off
    % legend([cellfun(@(x) strcat(x, ' max'), algorithms(1:NUM_ALGORITHMS, 1), 'un', 0); 
    %       cellfun(@(x) strcat(x, ' optimized max'), algorithms((1+NON_OPTIMIZED_ALGORITHMS):end, 1), 'un', 0); 
    %       cellfun(@(x) strcat(x, ' median'), algorithms(1:NUM_ALGORITHMS, 1), 'un', 0)
    %       cellfun(@(x) strcat(x, ' optimized median'), algorithms((1+NON_OPTIMIZED_ALGORITHMS):end, 1), 'un', 0)]);
