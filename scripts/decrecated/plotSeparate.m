function [errors] = plotSeparate(path_prefix, noise_levels, numbers_of_points_list)
    % NUMBER OF ITERATION FOR EACH NOISE LEVEL
    NUM_ALGORITHMS = 6;
    % the three columns in the cell are file prefix of algorithm, color and name in the legends
    algorithms = cell(NUM_ALGORITHMS, 3);

    algorithms{1, 1} = 'stewOutputRefinedError';                   
    algorithms{1, 2} = 'g';
    algorithms{1, 3} = 'Stew';
    
    algorithms{2, 1} = 'nisterOutputRefinedError';                   
    algorithms{2, 2} = 'b';
    algorithms{2, 3} = 'Nister';

    algorithms{3, 1} = 'sevenPtRefinedError';                   
    algorithms{3, 2} = 'm';
    algorithms{3, 3} = 'Seven Pt';

    algorithms{4, 1} = 'eightPtRefinedError';                   
    algorithms{4, 2} = 'c';
    algorithms{4, 3} = 'Eight Pt';

    algorithms{5, 1} = 'matlab5ptOutputRefinedError';                   
    algorithms{5, 2} = 'b--';
    algorithms{5, 3} = 'Matlab 5 Pt';

    algorithms{6, 1} = 'preSolveOutputRefinedError';                   
    algorithms{6, 2} = 'k';
    algorithms{6, 3} = 'Presolve';
    

    for number_of_points = numbers_of_points_list

        M = dlmread( [path_prefix '/' num2str(number_of_points) '_time.out'] );
        M = M * 180 / pi;

        errors = cell(NUM_ALGORITHMS);
        for i=1:NUM_ALGORITHMS
            errors{i} = dlmread( [path_prefix '/' num2str(number_of_points) '_' algorithms{i, 1} '.out']);
            errors{i} = errors{i} * 180 / pi;
        end
        
        % error_names and error_axis corresponding to each columns of error matrix
        error_names = { ...
            'median rotation error' 'max rotation error' 'average rotation error', ...
            'median translation error' 'max translation error' 'average translation error'
        };

        error_axis = { ...
            'rot. diff. norm [degree]' 'rot. diff. norm [degree]' 'rot. diff. norm [degree]', ...
            'transl. diff. norm [degree]' 'transl. diff. norm [degree]' 'transl. diff. norm [degree]'
        };

        % map indices of _time.out (average is not present, so plot max for it too)
        map_indices = [1, 2, 2, 3, 4, 4];
        
        % error types corresponding to the alternating rows of error matrix
        error_types = { ...
            'ransac', 'optimized'
        };

        for error_idx = 1:length(error_names)
            for error_type_idx = 1:length(error_types)
                fig_handle = figure('Position',get(0,'ScreenSize'));
                title([error_names{error_idx} ' (' num2str(number_of_points) ' points)']);
                xlabel('noise [pix]')
                ylabel(error_axis{error_idx})
                hold on
                grid on

                plot(noise_levels, M(1:14:end, map_indices(error_idx)), 'r');

                for i=1:NUM_ALGORITHMS
                    % for ransac, the preSolve gives high error. So don't plot here
                    if ( strcmp(error_types{error_type_idx}, 'ransac') && strcmp(algorithms{i, 1}, 'preSolveOutputRefinedError') )
                        continue;
                    end
                    plot(noise_levels, errors{i}(error_type_idx:length(error_types):end, error_idx), algorithms{i, 2})
                end
                legend([ ...
                    'Our algorithm'; ...
                    cellfun(@(x) strcat(x, ' '), algorithms(:, 3), 'un', 0)
                ]); 
                pause(0.01) %in seconds
                saveFigureCustom( ...
                    fig_handle, ...
                    [num2str(number_of_points), error_types{error_type_idx}, '_', error_names{error_idx}] ...
                );

                close all
            end % end error_type_idx loop
        end %end error_idx loop
    end % end number of points loop
    return
    
end

function [] = saveFigureCustom(fig_handle, filename)
    f=getframe(fig_handle);                                                         
    [X, map] = frame2im(f);                                                  
    imwrite(X,[filename '.jpg']);    
end