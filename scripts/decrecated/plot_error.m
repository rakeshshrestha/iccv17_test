function [] = plot_error(filename)
	NUM_ALGORITHMS = 6;

	% each algorithm except custom one has ransac and optimized version
	row_skip = 2 * NUM_ALGORITHMS - 1;

	algorithms = cell(NUM_ALGORITHMS, 2);
	algorithms{1, 1} = 'our';		algorithms{1, 2} = 'r';
	algorithms{2, 1} = 'stew';		algorithms{2, 2} = 'g';
	algorithms{3, 1} = 'nister';	algorithms{3, 2} = 'b';
	algorithms{4, 1} = '7pt'; 		algorithms{4, 2} = 'm';
	algorithms{5, 1} = '8pt'; 		algorithms{5, 2} = 'c';
	algorithms{6, 1} = 'eigen'; 	algorithms{6, 2} = 'k';
	
	M = dlmread(filename);

	num_noise = size(M);
	num_noise = num_noise / row_skip;

	% --------------------------- Rotation Error ----------------------------- %
	max_error_plot_handle = figure('Position',get(0,'ScreenSize'));
	subplot(1, 2, 1)
	title('Max Rotation Error')
	xlabel('noise [pix]')
	ylabel('rot. diff. norm [rad]')
	hold on
	grid on
	% max angle errors

	% ransac version
	for i = 1:NUM_ALGORITHMS
		plot(1:num_noise, M(i:row_skip:end, 2), algorithms{i, 2});
	end

	% optimized version
	for i = (NUM_ALGORITHMS+1):row_skip
		plot(1:num_noise, M(i:row_skip:end, 2), strcat(algorithms{i-NUM_ALGORITHMS+1, 2}, '-.'));
	end

	legend([cellfun(@(x) strcat(x, ' max'), algorithms(:, 1), 'un', 0); 
			cellfun(@(x) strcat(x, ' optimized max'), algorithms(2:end, 1), 'un', 0)]);
	
	% median angle errors
	median_error_plot_handle = figure('Position',get(0,'ScreenSize'));
	subplot(1, 2, 1)
	title('Median Rotation Error')
	xlabel('noise [pix]')
	ylabel('rot. diff. norm [rad]')
	hold on
	grid on
	
	for i = 1:NUM_ALGORITHMS
		plot(1:num_noise, M(i:row_skip:end, 1), strcat(algorithms{i, 2}, '--'));
	end
	
	for i = (NUM_ALGORITHMS+1):row_skip
		plot(1:num_noise, M(i:row_skip:end, 1), strcat(algorithms{i-NUM_ALGORITHMS+1, 2}, ':'));
	end

	legend([cellfun(@(x) strcat(x, ' median'), algorithms(:, 1), 'un', 0)
			cellfun(@(x) strcat(x, ' optimized median'), algorithms(2:end, 1), 'un', 0)]);
			
	% --------------------------- Traslation Error ----------------------------- %
	figure(max_error_plot_handle)
	subplot(1, 2, 2)
	title('Max Translation Error')
	xlabel('noise [pix]')
	ylabel('trans. dir. error [rad]')
	hold on
	grid on
	% max translation
	
	% ransac version
	for i = 1:NUM_ALGORITHMS
		plot(1:num_noise, M(i:row_skip:end, 4), algorithms{i, 2});
	end

	% optimized version
	for i = (NUM_ALGORITHMS+1):row_skip
		plot(1:num_noise, M(i:row_skip:end, 4), strcat(algorithms{i-NUM_ALGORITHMS+1, 2}, '-.'));
	end
	legend([cellfun(@(x) strcat(x, ' max'), algorithms(:, 1), 'un', 0); 
			cellfun(@(x) strcat(x, ' optimized max'), algorithms(2:end, 1), 'un', 0)]);
	% median translation
	figure(median_error_plot_handle)
	subplot(1, 2, 2)
	title('Median Translation Error')
	xlabel('noise [pix]')
	ylabel('trans. dir. error [rad]')
	hold on
	grid on

	% ransac version
	for i = 1:NUM_ALGORITHMS
		plot(1:num_noise, M(i:row_skip:end, 3), strcat(algorithms{i, 2}, '--'));
	end

	% optimized version
	for i = (NUM_ALGORITHMS+1):row_skip
		plot(1:num_noise, M(i:row_skip:end, 3), strcat(algorithms{i-NUM_ALGORITHMS+1, 2}, ':'));
	end
	
	hold off
	legend([cellfun(@(x) strcat(x, ' median'), algorithms(:, 1), 'un', 0)
			cellfun(@(x) strcat(x, ' optimized median'), algorithms(2:end, 1), 'un', 0)]);
