#!/usr/bin/env python

import subprocess

for numFeatures in range(15, 205, 5):
	with open(str(numFeatures)+".out", mode="w") as outputFile:
		print 'running numFeatures: ', numFeatures
		process = subprocess.Popen(
			[
				'/localhome/rakeshs/workspace/rakesh/iccv17_test/build/iccv_test',
				str(numFeatures)
			], 
			stdout=subprocess.PIPE
		)

		process.wait()

		for line in process.stdout:
			outputFile.write(line)
