function [residual] = jacobian_calculation_rotation_translation()
    % our variables for optimization
    angle_axis = sym('R', [3, 1]);
    translation = sym('t', [3, 1]);
    % our invariants (two points)
    pt1 = sym('p1', [3, 1]);
    pt2 = sym('p2', [3, 1]);

    % tell that our stuffs are real
    for i=1:3
        assume(angle_axis(i), 'real');
        assume(translation(i), 'real');
        assume(pt1(i), 'real');
        assume(pt2(i), 'real');
    end

    % input_data = [0.40160966,  0.56225353,  0.7228974, 0.46156633,  0.59344243,  0.65938047, .002, .003, .004, .5, .6, .7];
    % input_symbols = {pt1(1), pt1(2), pt1(3), pt2(1), pt2(2), pt2(3), angle_axis(1), angle_axis(2), angle_axis(3), translation(1), translation(2), translation(3)};

    rotated1 = AngleAxisRotatePoint(angle_axis, pt1);
    axis_ = cross(rotated1, translation);
    D = -dot(axis_, rotated1);
    t = (dot(axis_, pt2) + D)/norm(axis_);
    rotated1 = pt2 + axis_ * t;
    
    norm_rotated = sqrt(rotated1(1)*rotated1(1) + rotated1(2)*rotated1(2) + rotated1(3)*rotated1(3));

    
    % eval(subs( ...
    %     rotated1/norm_rotated, input_symbols, {input_data} ...
    % )) 
    
    input_data(4:6)'

    residual = (rotated1/norm_rotated)- pt2;

    % eval(subs(residual, ...
    %           input_symbols, ...
    %           input_data ...
    % ))
    
    % diary('residual.txt');
    disp(residual);
    % diary off;

    % diary('jacobian.txt');
    disp(jacobian(residual, [angle_axis; translation]));
    % diary off
    
    

end

function [rotatated_pt] = AngleAxisRotatePoint(angle_axis, pt)
    % our rotations are always small
    w_cross_pt = cross(angle_axis, pt);
    rotatated_pt = pt + w_cross_pt;
end

