function [errors] = plotCombinedFile(path_prefix, noise_levels, numbers_of_points_list)
    % NUMBER OF ITERATION FOR EACH NOISE LEVEL
    NUM_ALGORITHMS = 5;
    % the three columns in the cell are file prefix of algorithm, color and name in the legends
    algorithms = cell(NUM_ALGORITHMS, 3);

    algorithms{1, 1} = 'stewRefinedError';                   
    algorithms{1, 2} = 'g';
    algorithms{1, 3} = 'Stew';
    
    % algorithms{2, 1} = 'nisterRefinedError';                   
    % algorithms{2, 2} = 'b';
    % algorithms{2, 3} = 'Nister';

    algorithms{2, 1} = 'sevenptRefinedError';                   
    algorithms{2, 2} = 'm';
    algorithms{2, 3} = 'Seven Pt';

    algorithms{3, 1} = 'eightptRefinedError';                   
    algorithms{3, 2} = 'c';
    algorithms{3, 3} = 'Eight Pt';

    algorithms{5, 1} = 'preSolveRefinedError';                   
    algorithms{5, 2} = 'k';
    algorithms{5, 3} = 'Presolve';
    
    algorithm_names = {'presolve', 'our', 'stew', 'sevenpt', 'eightpt'};

    for number_of_points = numbers_of_points_list

        M = dlmread( [path_prefix '/' num2str(number_of_points) '_time.out'] );
        M = M * 180 / pi;

        % errors = cell(NUM_ALGORITHMS);
        % for i=1:NUM_ALGORITHMS
        %     errors{i} = dlmread( [path_prefix '/' num2str(number_of_points) '_' algorithms{i, 1} '.out']);
        %     errors{i} = errors{i} * 180 / pi;
        % end

        % error_names and error_axis corresponding to each columns of error matrix
        error_names = { ...
            'median rotation error' 'average rotation error', ...
            'median translation error' 'average translation error'
        };

        error_axis = { ...
            'rot. diff. norm [degree]' 'rot. diff. norm [degree]', ...
            'transl. diff. norm [degree]' 'transl. diff. norm [degree]'
        };

        % map indices of _time.out (median rotation, average rotation, median transtation, average translation)
        map_indices = [1, 3, 7, 9];

        % variances
        std_indices = [ ...
            4, 4, ... % for rotation
            10, 10 ... % for translation
        ];
        Q3_indices = [ ...
            5, 5, ...
            11, 11 ...
        ];
        Q1_indices = [ ...
            6, 6, ...
            12, 12 ...
        ];
        
        % error types corresponding to the alternating rows of error matrix
        error_types = { ...
            'ransac' %, 'optimized'
        };

        for error_idx = 1:length(error_names)
            for error_type_idx = 1:length(error_types)
                fig_handle = figure('Position',get(0,'ScreenSize'));
                title([error_names{error_idx} ' (' num2str(number_of_points) ' points)']);
                xlabel('sigma (pixel)')
                ylabel(error_axis{error_idx})
                hold on
                grid on

                % ----------- Our algrithm ------------ %
                % length(noise_levels)
                % length(M(2:NUM_ALGORITHMS:end, map_indices(error_idx)))
                
                % plot(noise_levels*100, M(2:NUM_ALGORITHMS:end, map_indices(error_idx)), 'r');

                for i = 2:NUM_ALGORITHMS
                    if (strfind(error_names{error_idx}, 'median') ~= 0)
                        error1 = M(i:NUM_ALGORITHMS:end, map_indices(error_idx)) - M(i:NUM_ALGORITHMS:end, Q1_indices(error_idx));
                        error2 = M(i:NUM_ALGORITHMS:end, Q3_indices(error_idx)) - M(i:NUM_ALGORITHMS:end, map_indices(error_idx));
                    else
                        error1 = M(i:NUM_ALGORITHMS:end, std_indices(error_idx)) / 2.0;
                        % symmetric error bar in case of average
                        error2 = error1;
                    end
                    size(M(i:NUM_ALGORITHMS:end, map_indices(error_idx)))
                    plot(noise_levels, M(i:NUM_ALGORITHMS:end, map_indices(error_idx)));
                    % errorbar( ...
                    %     noise_levels*100, ...
                    %     M(2:NUM_ALGORITHMS:end, map_indices(error_idx)), ... 
                    %     error1, error2
                    % );
                end

                

                % --------------- Other's algorithm --------------- %
                % for i=1:NUM_ALGORITHMS
                %     % for ransac, the preSolve gives high error. So don't plot here
                %     if ( strcmp(error_types{error_type_idx}, 'ransac') && strcmp(algorithms{i, 1}, 'preSolveRefinedError') )
                %         continue;
                %     end

                %     if (strfind(error_names{error_idx}, 'median') ~= 0)
                %         error1 = errors{i}(error_type_idx:length(error_types):end, map_indices(error_idx)) - errors{i}(error_type_idx:length(error_types):end, Q1_indices(error_idx));
                %         error2 = errors{i}(error_type_idx:length(error_types):end, Q3_indices(error_idx)) - errors{i}(error_type_idx:length(error_types):end, map_indices(error_idx));
                %     else
                %         error1 = errors{i}(error_type_idx:length(error_types):end, std_indices(error_idx)) / 2.0;
                %         % symmetric error bar in case of average
                %         error2 = error1;
                %     end

                %     % length(errors{i}(error_type_idx:length(error_types):end, error_idx))
                %     % plot(noise_levels*100, errors{i}(error_type_idx:length(error_types):end, map_indices(error_idx)), algorithms{i, 2})
                %     errorbar( ...
                %         noise_levels*100, ...
                %         errors{i}(error_type_idx:length(error_types):end, map_indices(error_idx)), ...
                %         error1, error2, ...
                %         algorithms{i, 2} ...
                %     );
                % end

                legend(algorithm_names(2:end)');

                % legend([ ...
                %     'Our algorithm'; ...
                %     cellfun(@(x) strcat(x, ' '), algorithms(:, 3), 'un', 0)
                % ]); 
                pause(0.01) %in seconds
                saveFigureCustom( ...
                    fig_handle, ...
                    [num2str(number_of_points), error_types{error_type_idx}, '_', error_names{error_idx}] ...
                );

                close all
            end % end error_type_idx loop
        end %end error_idx loop
    end % end number of points loop
    return
    
end

function [] = saveFigureCustom(fig_handle, filename)
    f=getframe(fig_handle);                                                         
    [X, map] = frame2im(f);                                                  
    imwrite(X,[filename '.jpg']);    
end