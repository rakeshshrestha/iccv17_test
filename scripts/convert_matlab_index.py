import csv
import sys

if len(sys.argv) != 2:
    print 'Usage: python script.py <filename>'
    exit(0)

output_list = []

with open(sys.argv[1], 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ')
    for row in reader:
    	row = filter(lambda x: len(x), row)
        num_row = map(lambda x: int(x) - 1, row)
        output_list.append(num_row)

with open(sys.argv[1], 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=' ')
    writer.writerows(output_list)
