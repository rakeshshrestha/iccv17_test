function [] = visualize_points(path_prefix, numPoints, iterationIdx)
	NUM_CAMERAS = 3;
	MAX_IMAGE_COORD = 800;

	pointsFile = [path_prefix, '/', num2str(numPoints), '/Points3D.txt'];
	groundtruthFile = [path_prefix, '/', num2str(numPoints), '_GroundTruthMotion.txt'];
	featureFile = [path_prefix, '/', num2str(numPoints), '_FeatureTrack.txt'];

	points = dlmread(pointsFile);
	groundTruth = dlmread(groundtruthFile);
	features = dlmread(featureFile);

	% there are "numPoints" rows for each expt
	startFrom = numPoints * (iterationIdx - 1) + 1;
	points = points(startFrom:(startFrom+numPoints-1), :);
	features = features(startFrom:(startFrom+numPoints-1), :);

	% get only the points that are projected in all cameras
	valid_rows = ones(numPoints, 1);
	valid_mask = abs(features) <= MAX_IMAGE_COORD;
	for i=1:size(valid_mask, 2)
		valid_rows = valid_rows & valid_mask(:, i);
	end
	points = points(valid_rows, :);
	
	% there are 9 rows (for three cameras) for each expt
	startFrom = (NUM_CAMERAS * 3) * (iterationIdx - 1) + 1;
	groundTruth = groundTruth(startFrom:(startFrom+8), :);

	cam1 = groundTruth(1:3, :);
	cam2 = groundTruth(7:9, :);

	% Visualize the camera locations and orientations
	cameraSize = 0.05;
	figure
	plotCamera('Size', cameraSize, 'Color', 'r', 'Label', '1', 'Opacity', 0);
	hold on
	grid on
	plotCamera('Location', cam2(:, 4), 'Orientation', cam2(1:3, 1:3), 'Size', cameraSize, ...
	    'Color', 'b', 'Label', '2', 'Opacity', 0);

	% Visualize the point cloud
	pcshow(points, 'VerticalAxis', 'y', 'VerticalAxisDir', 'down', ...
	    'MarkerSize', 45);

	% Rotate and zoom the plot
	camorbit(0, -30);
	camzoom(1.5);

	% Label the axes
	xlabel('x-axis');
	ylabel('y-axis');
	zlabel('z-axis')

	title('Up to Scale Reconstruction of the Scene');
end