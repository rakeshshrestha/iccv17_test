function [] = jacobian_calculation()
    angle_axis = sym('angle_axis', [3, 1]);
    pt = sym('pt', [3, 1]);

    theta2 = dot(angle_axis, angle_axis);
     
    % for theta greater than 0
    theta = sqrt(theta2);
    w = angle_axis / theta;
    costheta = cos(theta);
    sintheta = sin(theta);
    w_cross_pt = cross(w, pt);
    w_dot_pt = dot(w, pt);
    rotatated_pt = pt * costheta + w_cross_pt * sintheta + w * (1 - costheta) * w_dot_pt;
    J = jacobian(rotatated_pt, angle_axis);

    disp(J);

    % for theta close to 0
    w_cross_pt = cross(angle_axis, pt);
    rotatated_pt = pt + w_cross_pt;
    J = jacobian(rotatated_pt, angle_axis);

    disp(J);


